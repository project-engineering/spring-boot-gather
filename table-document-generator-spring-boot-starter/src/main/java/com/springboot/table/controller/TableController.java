package com.springboot.table.controller;

import com.springboot.table.service.TableService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/table")
public class TableController {

    @Resource
    private TableService tableService;

    /**
     * 导出数据库所有表结构信息
     * @return 导出成功提示信息
     */
    @GetMapping("/export")
    public String exportTableStructure() {
        tableService.exportTableStructureToExcel(tableService.getCatalogName());
        return "Table structure exported to Excel successfully!";
    }
}