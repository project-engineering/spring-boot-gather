package com.springboot.table.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SpecialCharInfo {
    private final char character;
    private final int position;
}
