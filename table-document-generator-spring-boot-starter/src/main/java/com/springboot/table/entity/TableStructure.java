package com.springboot.table.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.alibaba.excel.enums.poi.BorderStyleEnum;
import com.alibaba.excel.enums.poi.FillPatternTypeEnum;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ColumnWidth(25)
@ContentRowHeight(30)
@HeadRowHeight(30)
@Builder
@HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND)
@HeadFontStyle(fontHeightInPoints = 12,color = 9)
@ContentFontStyle(fontHeightInPoints = 11)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER
        ,verticalAlignment = VerticalAlignmentEnum.CENTER
        ,borderLeft = BorderStyleEnum.MEDIUM
        ,borderRight = BorderStyleEnum.MEDIUM
        ,borderTop = BorderStyleEnum.MEDIUM
        ,borderBottom = BorderStyleEnum.MEDIUM)
public class TableStructure {

    @ExcelProperty(value={"返回目录","返回目录","序号"},index = 0)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER,
            verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String ordinalPosition;

    @ExcelProperty(value={"表名","描述","字段"},index = 1)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String columnName;

    @ExcelProperty(value={"${tableName}","${tableComment}","名称"},index = 2)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String columnComment;

    @ExcelProperty(value={"${tableName}","${tableComment}","数据类型"},index = 3)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String columnType;

    @ExcelProperty(value={"${tableName}","${tableComment}","主键"},index = 4)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER,
            verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String columnKey;

    @ExcelProperty(value={"${tableName}","${tableComment}","是否为空"},index = 5)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER,
            verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String isNullable;

    @ExcelProperty(value={"${tableName}","${tableComment}","默认值"},index = 6)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String columnDefault;

    @ExcelProperty(value={"${tableName}","${tableComment}","备注"},index = 7)
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT
            ,verticalAlignment = VerticalAlignmentEnum.CENTER
            ,borderLeft = BorderStyleEnum.MEDIUM
            ,borderRight = BorderStyleEnum.MEDIUM
            ,borderTop = BorderStyleEnum.MEDIUM
            ,borderBottom = BorderStyleEnum.MEDIUM)
    private String remark;

}
