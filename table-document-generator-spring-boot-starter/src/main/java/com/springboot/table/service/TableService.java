package com.springboot.table.service;

public interface TableService {
    /**
     * 导出数据库所有表结构信息
     * @param databaseName 数据库名称
     */
    void exportTableStructureToExcel(String databaseName);

    String getSchemaName();

    String getDatabaseName();

    String getDatabaseVersion();

    String getCatalogName();
}