package com.springboot.table.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.fastjson2.JSONObject;
import com.springboot.table.entity.SpecialCharInfo;
import com.springboot.table.entity.TableEntity;
import com.springboot.table.entity.TableStructure;
import com.springboot.table.handler.CustomWidthStyleStrategy;
import com.springboot.table.handler.EasyExcelCellWriteHandler;
import com.springboot.table.handler.ExcelHyperlinkHandler;
import com.springboot.table.mapper.TableMapper;
import com.springboot.table.service.TableService;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
@Service
public class TableServiceImpl implements TableService {
    @Resource
    private DataSource dataSource;
    @Resource
    private TableMapper tableMapper;

    /**
     * 导出数据库所有表结构信息
     * @param databaseName 数据库名称
     */
    @Override
    public void exportTableStructureToExcel(String databaseName) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("开始导出数据库 {} 的所有表结构信息到 Excel 文件", databaseName);
        log.info("数据库版本：{}", getDatabaseVersion());
        log.info("数据库目录：{}", getCatalogName());
        log.info("数据库Schema：{}", getSchemaName());
        log.info("数据库名称：{}", getDatabaseName());
        Map<String, List<TableStructure>> map = getAllTableStructure(databaseName);
        String filePath = databaseName + "数据库设计文档.xlsx";
        try (ExcelWriter excelWriter = EasyExcel.write(filePath).build()) {
            // 设置第一个sheet页
            setFirstSheet(excelWriter, map);
            // 设置其他sheet页
            setOthersSheets(excelWriter, map);
        } catch (Exception e) {
            log.error("Failed to export table structure to Excel", e);
            throw new RuntimeException("Failed to export table structure to Excel", e);
        }
        stopWatch.stop();
        log.info("一共导出 {} 张表结构信息", map.size());
        log.info("导出数据库 {} 的所有表结构信息到 Excel 文件完成，耗时：{} ms", databaseName, stopWatch.getTotalTimeMillis());
    }

    /**
     * 获取数据库Schema名称
     * @return Schema名称
     */
    @Override
    public String getSchemaName() {
        try (Connection connection = dataSource.getConnection()) {
            return connection.getSchema();
        } catch (SQLException e) {
            log.error("获取数据库Schema失败", e);
            return null;
        }
    }

    /**
     * 获取数据库名称
     * @return 数据库名称
     */
    @Override
    public String getDatabaseName() {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            return metaData.getDatabaseProductName();
        } catch (SQLException e) {
            log.error("获取数据库名称失败", e);
            return null;
        }
    }

    /**
     * 获取数据库版本
     * @return 数据库版本
     */
    @Override
    public String getDatabaseVersion() {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            return metaData.getDatabaseProductVersion();
        } catch (SQLException e) {
            log.error("获取数据库版本失败", e);
            return null;
        }
    }

    /**
     * 获取数据库目录
     * @return 数据库目录
     */
    @Override
    public String getCatalogName() {
        try (Connection connection = dataSource.getConnection()) {
            return connection.getCatalog();
        } catch (SQLException e) {
            log.error("获取数据库目录失败", e);
            return null;
        }
    }

    /**
     * 获取所有表结构信息
     * @param databaseName 数据库名称
     * @return 表结构信息
     */
    private Map<String, List<TableStructure>> getAllTableStructure(String databaseName) {
        List<TableEntity> tableList = tableMapper.getAllTableNames(databaseName);
        return tableList.stream().collect(Collectors.toMap(
                table -> table.getTableName() + "-" + table.getTableComment(),
                table -> {
                    List<Map<String, Object>> tableStructureList = tableMapper.getTableStructure(databaseName, table.getTableName());
                    tableMapper.getTableComment(databaseName, table.getTableName());
                    return tableStructureList.stream()
                            .map(this::mapToTableStructure)
                            .distinct()
                            .collect(Collectors.toList());
                }
        ));
    }

    /**
     * 将 Map 转换为 TableStructure 对象
     * @param column 列信息
     * @return TableStructure 对象
     */
    private TableStructure mapToTableStructure(Map<String, Object> column) {
        TableStructure tableStructure = TableStructure.builder().build();
        tableStructure.setOrdinalPosition(getString(column, "ORDINAL_POSITION"));
        tableStructure.setColumnName(getString(column, "COLUMN_NAME"));
        tableStructure.setColumnType(getString(column, "COLUMN_TYPE"));
        tableStructure.setIsNullable(getBoolean(column, "IS_NULLABLE") ? "√" : "");
        tableStructure.setColumnKey(getBoolean(column, "COLUMN_KEY", "PRI") ? "√" : "");
        tableStructure.setColumnDefault(getString(column, "COLUMN_DEFAULT"));

        String columnComment = getString(column, "COLUMN_COMMENT");
        if (StringUtils.isNotBlank(columnComment)) {
            SpecialCharInfo specialCharInfo = findFirstSpecialChar(columnComment);
            if (specialCharInfo != null) {
                int position = specialCharInfo.getPosition();
                tableStructure.setColumnComment(columnComment.substring(0, position));
                String remark = columnComment.substring(position);
                tableStructure.setRemark(replaceSpecialCharacters(remark));
            } else {
                tableStructure.setColumnComment(columnComment);
                tableStructure.setRemark("");
            }
        } else {
            tableStructure.setColumnComment("");
            tableStructure.setRemark("");
        }
        return tableStructure;
    }

    /**
     * 获取 Map 中指定 key 的值，并转换为字符串
     * @param map Map 对象
     * @param key 键
     * @return 值
     */
    private String getString(Map<String, Object> map, String key) {
        return Optional.ofNullable(map.get(key)).map(Object::toString).orElse("");
    }

    /**
     * 获取 Map 中指定 key 的值，并转换为布尔值
     * @param map Map 对象
     * @param key 键
     * @return 值
     */
    private boolean getBoolean(Map<String, Object> map, String key) {
        return "NO".equals(map.get(key));
    }

    /**
     * 获取 Map 中指定 key 的值，并转换为布尔值
     * @param map Map 对象
     * @param key 键
     * @param expectedValue 期望值
     * @return 值
     */
    private boolean getBoolean(Map<String, Object> map, String key, String expectedValue) {
        return expectedValue.equals(map.get(key));
    }

    /**
     * 设置第一个sheet页
     *
     * @param excelWriter ExcelWriter
     * @param map 表结构信息
     */
    private static void setFirstSheet(ExcelWriter excelWriter, Map<String, List<TableStructure>> map){
        // 第一次写入第一个sheet
        WriteSheet firstSheet = EasyExcel.writerSheet()
                .sheetNo(0)
                .sheetName("目录")
                .head(TableEntity.class)
                .registerWriteHandler(new ExcelHyperlinkHandler(1, new int[]{3}))//从第一行开始，对第3列的单元格添加超链接
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .build();
        // 使用 Lambda 表达式和 Stream API 获取 A 和 B 并生成新的 List<Map>
        // 使用 IntStream 生成序列，并映射到 TableEntity 对象
        List<TableEntity> tableEntityList = IntStream.range(0, map.size())
                .mapToObj(index -> {
                    String key = map.keySet().toArray(new String[0])[index];
                    String[] parts = key.split("-", 2);
                    TableEntity table = new TableEntity();
                    table.setSeq(index + 1);
                    table.setTableName(parts[0]);
                    table.setTableComment(parts.length > 1 ? parts[1] : "");
                    table.setLink("Link");
                    return table;
                })
                .collect(Collectors.toList());

        excelWriter.write(tableEntityList, firstSheet);
    }

    /**
     * 设置其他sheet页
     *
     * @param excelWriter ExcelWriter
     * @param map 表结构信息
     */
    private static void setOthersSheets(ExcelWriter excelWriter, Map<String, List<TableStructure>> map) {
        // 确保 existingSheetNames 在每次方法调用时都是空的
        Set<String> existingSheetNames = new HashSet<>();
        int sheetNo = 1;
        for (Map.Entry<String, List<TableStructure>> entry : map.entrySet()) {
            String[] tableNames = entry.getKey().split("-");
            String sheetName = tableNames.length > 1 ? tableNames[1] : tableNames[0];

            String uniqueSheetName = generateUniqueSheetName(existingSheetNames, sheetName);
            existingSheetNames.add(uniqueSheetName);

            log.info("Generated unique sheet name: {}", uniqueSheetName);

            JSONObject tableInfo = new JSONObject();
            tableInfo.put("tableName", sheetName);
            tableInfo.put("tableComment", sheetName);

            WriteSheet writeSheet = createWriteSheet(sheetNo++, uniqueSheetName, tableInfo, entry.getValue());

            if (!entry.getValue().isEmpty()) {
                excelWriter.write(entry.getValue(), writeSheet);
            }
        }
    }

    /**
     * 创建一个 WriteSheet 对象，用于写入数据到 Excel 文件中。
     *
     * @param sheetNo 工作表索引
     * @param uniqueSheetName 工作表名称
     * @param tableInfo 表信息
     * @param data 表结构信息
     * @return WriteSheet 对象
     */
    private static WriteSheet createWriteSheet(int sheetNo, String uniqueSheetName, JSONObject tableInfo, List<TableStructure> data) {
        EasyExcelCellWriteHandler easyExcelTitleHandler = new EasyExcelCellWriteHandler(tableInfo);
        return EasyExcel.writerSheet()
                .sheetNo(sheetNo)
                .sheetName(uniqueSheetName)
                .head(TableStructure.class)
                .registerWriteHandler(easyExcelTitleHandler)
                .registerWriteHandler(new ExcelHyperlinkHandler(0, new int[]{2}))
                .registerWriteHandler(new CustomWidthStyleStrategy())
                .build();
    }

    /**
     * 生成唯一的工作表名称
     *
     * @param usedSheetNames 已存在的工作表名称集合
     * @param baseName 基础工作表名称
     * @return 生成的唯一的工作表名称
     */
    private static String generateUniqueSheetName(Set<String> usedSheetNames, String baseName) {
        /*
         * 限制工作表名称的最大长度为 31 字符，超过该长度的名称将被截断
         */
        final int maxSheetNameLength = 31;
        String uniqueSheetName = baseName.length() > maxSheetNameLength ? baseName.substring(0, maxSheetNameLength) : baseName;
        int suffix = 1;

        /*
         * 确保生成的工作表名称不与已存在的工作表名称重复
         */
        while (usedSheetNames.contains(uniqueSheetName)) {
            /*
             * 工作表名称长度 + 数字的长度不能超过 31 字符
             */
            String newBaseName = baseName.length() + Integer.toString(suffix).length() > maxSheetNameLength
                    ? baseName.substring(0, maxSheetNameLength - Integer.toString(suffix).length())
                    : baseName;
            uniqueSheetName = newBaseName + suffix++;
        }

        usedSheetNames.add(uniqueSheetName);
        return uniqueSheetName;
    }

    /**
     * 查找字符串中第一个特殊字符
     *
     * @param str 字符串
     * @return 特殊字符信息
     */
    public static SpecialCharInfo findFirstSpecialChar(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isLetterOrDigit(c)) {
                return new SpecialCharInfo(c, i);
            }
        }
        // 表示未找到特殊字符
        return null;
    }

    /**
     * 替换字符串中的特殊字符
     *
     * @param str 字符串
     * @return 处理后的字符串
     */
    private static String replaceSpecialCharacters(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }

        int start = 0;
        int end = str.length() - 1;

        // 从字符串的两端同时向中间遍历，寻找首个和最后一个字母数字字符
        while (start <= end && !Character.isLetterOrDigit(str.charAt(start))) {
            start++;
        }
        while (start <= end && !Character.isLetterOrDigit(str.charAt(end))) {
            end--;
        }

        // 如果字符串全是特殊字符，则返回空字符串
        if (start > end) {
            return "";
        }

        // 使用subString方法直接返回需要的子字符串
        return str.substring(start, end + 1);
    }
}
