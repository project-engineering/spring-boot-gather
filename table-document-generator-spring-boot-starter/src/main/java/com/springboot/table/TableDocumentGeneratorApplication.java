package com.springboot.table;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableDocumentGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(TableDocumentGeneratorApplication.class, args);
    }

}
