package com.springboot.table.handler;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * 动态添加超链接
 */
public class ExcelHyperlinkHandler implements CellWriteHandler {
    /**
     * 添加超链接的字段的下标
     */
    private int[] mergeColumnIndex;
    /**
     * 从第几行开始添加，0代表标题行
     */
    private int mergeRowIndex;

    /**
     * 默认构造函数
     */
    public ExcelHyperlinkHandler() {
    }

    /**
     * 构造函数
     *
     * @param mergeRowIndex 从第几行开始添加，0代表标题行
     * @param mergeColumnIndex 合并字段的下标
     */
    public ExcelHyperlinkHandler(int mergeRowIndex, int[] mergeColumnIndex) {
        this.mergeRowIndex = mergeRowIndex;
        this.mergeColumnIndex = mergeColumnIndex;
    }

    /**
     * 单元格创建前处理器
     *
     * @param writeSheetHolder 工作表持有者
     * @param writeTableHolder 表持有者
     * @param row 行
     * @param head 表头
     * @param integer 行索引
     * @param integer1 列索引
     * @param isHeader 是否是标题
     */
    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                 Head head, Integer integer, Integer integer1, Boolean isHeader) {
        // No implementation needed
    }

    /**
     * 单元格创建后处理器
     *
     * @param writeSheetHolder 工作表持有者
     * @param writeTableHolder 表持有者
     * @param cell 单元格
     * @param head 表头
     * @param isHeader 是否是标题
     */
    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer integer, Boolean isHeader) {
        // No implementation needed
    }

    /**
     * 单元格数据转换器
     *
     * @param writeSheetHolder 工作表持有者
     * @param writeTableHolder 表持有者
     * @param cellData 单元格数据
     * @param cell 单元格
     * @param head 表头
     * @param relativeRowIndex 相对行索引
     * @param isHeader 是否是标题
     */
    @Override
    public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, WriteCellData<?> cellData, Cell cell, Head head, Integer relativeRowIndex, Boolean isHeader) {
        // No implementation needed
    }

    /**
     * 单元格处理器
     *
     * @param writeSheetHolder 工作表持有者
     * @param writeTableHolder 表持有者
     * @param cellDataList 单元格数据列表
     * @param cell 单元格
     * @param head 表头
     * @param relativeRowIndex 相对行索引
     * @param isHeader 是否是标题
     */
    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHeader) {
        // 当前行
        int curRowIndex = cell.getRowIndex();
        // 当前列
        int curColIndex = cell.getColumnIndex();

        if (curRowIndex >= mergeRowIndex) {
            for (int columnIndex : mergeColumnIndex) {
                if (curColIndex == columnIndex) {
                    mergeWithPrevRow(writeSheetHolder, curRowIndex);
                    break;
                }
            }
        }
    }

    /**
     * 合并单元格
     *
     * @param writeSheetHolder 工作表持有者
     * @param curRowIndex 当前行索引
     */
    private void mergeWithPrevRow(WriteSheetHolder writeSheetHolder, int curRowIndex) {
        Workbook workbook = writeSheetHolder.getSheet().getWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        int sheetNo = writeSheetHolder.getSheetNo();

        Row row = writeSheetHolder.getSheet().getRow(curRowIndex);
        if (row == null) {
            return;
        }

        Cell prevCell = getPreviousCell(row, sheetNo);

        if (prevCell == null) {
            return;
        }
        if (!"超链接".equals(prevCell.getStringCellValue())) {
            String prevCellValue = "";
            if ("Link".equals(prevCell.getStringCellValue())){
                prevCellValue = prevCell.getRow().getCell(2).getStringCellValue();
                if (!ObjectUtils.isEmpty(prevCellValue)){
                    // 如果长度超过31，则截取前31个字符
                    if (prevCellValue.length() > 31) {
                        prevCellValue = prevCellValue.substring(0, 31);
                    }
                } else {
                    prevCellValue = prevCell.getRow().getCell(1).getStringCellValue();
                    // 如果长度超过31，则截取前31个字符
                    if (prevCellValue.length() > 31) {
                        prevCellValue = prevCellValue.substring(0, 31);
                    }
                }
            } else {
                prevCellValue = getCellValue(prevCell, sheetNo);
            }
            if (isFirstSheet(sheetNo) || "返回目录".equals(prevCellValue)) {
                addHyperlink(createHelper, prevCell, prevCellValue, sheetNo);
            }
        }
    }

    /**
     * 获取当前列的单元格
     *
     * @param row 行
     * @param sheetNo 工作表索引
     * @return 上一行的单元格
     */
    private Cell getPreviousCell(Row row, int sheetNo) {
        return row.getCell(isFirstSheet(sheetNo) ? 3 : 0);
    }

    /**
     * 获取单元格的值
     *
     * @param cell 单元格
     * @param sheetNo 工作表索引
     * @return 单元格的值
     */
    private String getCellValue(Cell cell, int sheetNo) {
        if (isFirstSheet(sheetNo)) {
            Cell tempCell = cell.getRow().getCell(2);
            return tempCell.getStringCellValue();
        } else {
            return cell.getStringCellValue();
        }
    }

    /**
     * 是否是第一个工作表
     *
     * @param sheetNo 工作表索引
     * @return 是否是第一个工作表
     */
    private boolean isFirstSheet(int sheetNo) {
        return sheetNo == 0;
    }

    /**
     * 添加超链接
     *
     * @param createHelper 创建助手
     * @param cell 单元格
     * @param prevCellValue 当前的值
     * @param sheetNo 工作表索引
     */
    private void addHyperlink(CreationHelper createHelper, Cell cell, String prevCellValue, int sheetNo) {
        Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
        String address = isFirstSheet(sheetNo) ? "'" + prevCellValue + "'!A1" : "'目录'!A1";
        hyperlink.setAddress(address);
        cell.setHyperlink(hyperlink);

        // 获取单元格样式
        Workbook workbook = cell.getSheet().getWorkbook();
        CellStyle hlinkStyle = workbook.createCellStyle();
        Font hlinkFont = workbook.createFont();

        // 设置字体为宋体、加粗、大小为12
        hlinkFont.setFontName("宋体"); // 注意：字体名称可能因操作系统而异
        hlinkFont.setFontHeightInPoints((short) 12); // 设置字体大小
        hlinkFont.setBold(true); // 设置字体加粗

        // 设置字体为白色并加下划线
        hlinkFont.setColor(IndexedColors.WHITE.getIndex());
        hlinkFont.setUnderline(Font.U_SINGLE);
        hlinkStyle.setFont(hlinkFont);

        // 设置上下左右居中
        hlinkStyle.setAlignment(HorizontalAlignment.CENTER);
        hlinkStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        // 设置背景为灰色
        hlinkStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        hlinkStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // 设置样式
        cell.setCellStyle(hlinkStyle);
    }
}
