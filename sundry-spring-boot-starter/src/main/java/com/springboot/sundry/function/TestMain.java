package com.springboot.sundry.function;

/**
 * 用Java 8中的Function接口消灭代码中 if...else
 *
 * Supplier 供给型函数
 * Consumer 消费型函数
 * Runnable 无参无返回型函数
 * Function 函数的表现形式为接收一个参数，并返回一个值。
 * Supplier、Consumer、Runnable可以看作Function的一种特殊表现形式
 *
 * 处理抛出异常的if
 * 处理if分支操作
 * 如果存在值执行消费操作，否则执行基于空的操作
 */
public class TestMain {
    public static void main(String[] args) {
//        if (...) {
//            throw new RuntimeException("出现异常了");
//        }
//
//        if (...) {
//            doSomething();
//        } else {
//            doOther();
//        }
        VUtils.isBlankOrNoBlank("hello").presentOrElseHandle(System.out::println,
                ()->{
                    System.out.println("空字符串");
                });
        VUtils.isTure(true).throwMessage("hahaha");

    }
}
