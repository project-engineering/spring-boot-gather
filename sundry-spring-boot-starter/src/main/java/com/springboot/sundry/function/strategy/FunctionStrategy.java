package com.springboot.sundry.function.strategy;

/**
 * 用java 8的函数式接口和泛型 优化策略模式（简化写法）
 * 进而优化if else逻辑
 * 疫情期间 某人回村为例
 **/
public interface FunctionStrategy {

    void huicun();
}
