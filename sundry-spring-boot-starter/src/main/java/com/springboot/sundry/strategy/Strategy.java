package com.springboot.sundry.strategy;

public interface Strategy {
    String query(String resourceId);
}
