package com.springboot.sundry.decorator;

public class MainTest {
    public static void main(String[] args) {
//        IwatchFile iwatchFile = new HaohuaFile();
//        iwatchFile.watch();

//        IwatchFile iwatchFile = new HaohuaFile();
//        IwatchFile foodDecorator = new FoodDecorator(iwatchFile);
//        foodDecorator.watch();

        IwatchFile iwatchFile = new HaohuaFile();
        IwatchFile foodDecorator = new FoodDecorator(iwatchFile);
        IwatchFile girlFriendDecorator = new GirlFriendDecorator(foodDecorator);
        girlFriendDecorator.watch();
    }
}
