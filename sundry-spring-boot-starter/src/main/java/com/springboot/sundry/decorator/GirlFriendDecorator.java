package com.springboot.sundry.decorator;

/**
 * 女朋友装饰类
 */
public class GirlFriendDecorator extends FoodDecorator{

    public GirlFriendDecorator(IwatchFile iwatchFile) {
        super(iwatchFile);
    }

    @Override
    public void watch() {
        System.out.println("带着女朋友看电影");
        super.watch();
    }
}
