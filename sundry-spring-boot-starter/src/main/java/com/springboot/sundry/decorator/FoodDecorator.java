package com.springboot.sundry.decorator;

/**
 * 食物装饰类
 */
public class FoodDecorator implements IwatchFile{
    private IwatchFile iwatchFile;

    public FoodDecorator (IwatchFile iwatchFile){
        this.iwatchFile = iwatchFile;
    }

    @Override
    public void watch() {
        System.out.println("我买了好多零食!");
        iwatchFile.watch();
    }
}
