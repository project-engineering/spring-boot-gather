package com.springboot.sundry.rule;

import lombok.Data;

@Data
public class NationalityRuleDto {
    private String nationality;
}
