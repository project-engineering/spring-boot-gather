package com.springboot.sundry.rule;

// 规则抽象
public interface BaseRule {
    boolean execute(RuleDto dto);
}
