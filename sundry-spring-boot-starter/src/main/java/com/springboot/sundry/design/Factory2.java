package com.springboot.sundry.design;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 工厂设计模式
 */
public class Factory2 {
    private static Map<String,AbstractHandler> strategyMap = new HashMap<>();

    public static AbstractHandler getInvoleStrategy(String str){
        return strategyMap.get(str);
    }

    public static void register(String str,AbstractHandler handler){
        if (StringUtils.isBlank(str) || null == handler) {
            return;
        }
        strategyMap.put(str,handler);
    }
}
