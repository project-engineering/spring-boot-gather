package com.springboot.sundry.design;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 工厂设计模式
 */
public class Factory {
    private static Map<String,Handler> strategyMap = new HashMap<>();

    public static Handler getInvoleStrategy(String str){
        return strategyMap.get(str);
    }

    public static void register(String str,Handler handler){
        if (StringUtils.isBlank(str) || null == handler) {
            return;
        }
        strategyMap.put(str,handler);
    }
}
