package com.springboot.sundry.design;

import org.springframework.beans.factory.InitializingBean;

/**
 * 策略模式
 */
public interface Handler extends InitializingBean {
    void AA(String nikeName);
}
