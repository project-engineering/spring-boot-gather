SpringBoot整合flyway自动初始化数据库

https://blog.51cto.com/u_15896157/5895637


Flyway 8.2.1及以后版本不再支持MySQL？！

# 一、问题
使用最新的Springboot 3.1.1和Flyway 9.21.1开发Demo，连接本地的MySQL 8.0进行测试，发现抛出了异常
```
…nested exception is org.flywaydb.core.api.FlywayException: Unsupported Database: MySQL 8.0
```
# 二、排查过程
从错误信息上看，发现竟然是不支持MySQL 8.0，调整为连接MySQL 5.7还是不行，难道不支持MySQL？不会吧！？

对于这个问题，网上搜索了一番，找到的文档解决方案都是降低Flyway的版本。但是这样就不能使用Flyway新版本了，MySQL这么流行，Flyway怎么会不支持呢？！是不是哪里搞错了。

带着诸多的疑问，在gitee上找到镜像源码项目（https://gitee.com/mirrors_flyway/flyway） ，下载了Flyway的开源代码，找到了抛出异常的位置，如下图所示：
```
org.flywaydb.core.internal.database.DatabaseTypeRegister.getDatabaseTypeForConnection(DatabaseTypeRegister.java:105)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/1dcc27194eab48a09c57278241473940.png#pic_center)

接着找到数据库类型的包位置：org.flywaydb.core.internal.database，发现确实没有看到mysql
![在这里插入图片描述](https://img-blog.csdnimg.cn/ed43292f13bc47b4b75ec34fd42e6cad.png#pic_center)

接着逐个版本往前追溯，在8.2.0版本找到了移除的日志信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/86f5fa1454194d2aba61e82869c7152e.png#pic_center)

在8.2.1版本已经找不到mysql目录了
![在这里插入图片描述](https://img-blog.csdnimg.cn/2a954cc544064481b4bf7306c9d78b92.png#pic_center)

Github上有相关的issue，地址 https://github.com/flyway/flyway/issues/3340

至此，已经可以确定 flyway-core 8.2.1及以后的版本确实是不再支持MySQL。

# 三、解决方案
## 3.1、方案一（增加依赖库）
官方Flyway 8.2.1版本发布说明（https://flywaydb.org/documentation/learnmore/releaseNotes#8.2.1），如下图所示
![在这里插入图片描述](https://img-blog.csdnimg.cn/2d928198c5994698b909e65bbcf1b9ab.png)

从说明上可知，MySQL代码被提取出来作为插件，需要另外增加依赖，根据官方文档（https://flywaydb.org/documentation/database/mysql）的说明，解决方案如下：
```
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-mysql</artifactId>
</dependency>
```

## 3.2、方案二（指定版本）
pom.xml文件中，properties信息增加指定Flyway版本为最后支持MySQL的 8.2.0 版本，如下图所示

```
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
    <version>8.2.0</version>
</dependency>
```

flyway拓展知识
```
flyway.baseline-description对执行迁移时基准版本的描述.
flyway.baseline-on-migrate当迁移时发现目标schema非空，而且带有没有元数据的表时，是否自动执行基准迁移，默认false.
flyway.baseline-version开始执行基准迁移时对现有的schema的版本打标签，默认值为1.
flyway.check-location检查迁移脚本的位置是否存在，默认false.
flyway.clean-on-validation-error当发现校验错误时是否自动调用clean，默认false.
flyway.enabled是否开启flywary，默认true.
flyway.encoding设置迁移时的编码，默认UTF-8.
flyway.ignore-failed-future-migration当读取元数据表时是否忽略错误的迁移，默认false.
flyway.init-sqls当初始化好连接时要执行的SQL.
flyway.locations迁移脚本的位置，默认db/migration.
flyway.out-of-order是否允许无序的迁移，默认false.
flyway.password目标数据库的密码.
flyway.placeholder-prefix设置每个placeholder的前缀，默认${.
flyway.placeholder-replacementplaceholders是否要被替换，默认true.
flyway.placeholder-suffix设置每个placeholder的后缀，默认}.
flyway.placeholders.[placeholder name]设置placeholder的value
flyway.schemas设定需要flywary迁移的schema，大小写敏感，默认为连接默认的schema.
flyway.sql-migration-prefix迁移文件的前缀，默认为V.
flyway.sql-migration-separator迁移脚本的文件名分隔符，默认__
flyway.sql-migration-suffix迁移脚本的后缀，默认为.sql
flyway.tableflyway使用的元数据表名，默认为schema_version
flyway.target迁移时使用的目标版本，默认为latest version
flyway.url迁移时使用的JDBC URL，如果没有指定的话，将使用配置的主数据源
flyway.user迁移数据库的用户名
flyway.validate-on-migrate迁移时是否校验，默认为true.
```