package com.springboot.flyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;

/**
 * Spring Boot使用flayway自动执行数据库升级脚本
 */
@SpringBootApplication(exclude = {FlywayAutoConfiguration.class})
public class FlywayApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlywayApplication.class, args);
    }
}
