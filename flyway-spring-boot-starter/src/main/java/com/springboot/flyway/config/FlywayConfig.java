package com.springboot.flyway.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import java.util.Map;

@Log4j2
@Configuration
public class FlywayConfig implements InitializingBean, DisposableBean {

    @Resource
    private FlywayProperties flywayProperties;

    @Resource
    private DruidDataSource druidDataSource;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (flywayProperties.getFlywayAutoMigration()) {
            migrateAutomatically(druidDataSource.getName());
            log.info("########### [ flyway 数据库初始化成功! ]  ###########");
        }
    }

    @Override
    public void destroy() throws Exception {
        if (flywayProperties.getAutoCleanTable() && flywayProperties.getFlywayAutoMigration()) {
            cleanAutomatically(druidDataSource.getName());
            log.info("###########  [ flyway 数据库表清空成功! ]  ###########");
        }
    }

    private Map<String, Flyway> flyways() {
        Map<String, Flyway> flywayMap = Maps.newHashMap();
        Flyway flyway = Flyway.configure()
                .dataSource(druidDataSource)
//                .locations("classpath:db/migration", "filesystem:db/migration")
                .locations("classpath:db/migration")
                .baselineOnMigrate(true)
                .cleanOnValidationError(true)
                .validateOnMigrate(true)
                .load();
        flywayMap.put(druidDataSource.getName(), flyway);
        return flywayMap;
    }


    private void migrateAutomatically(String dbName) {
        Map<String, Flyway> flywayMap = flyways();
        flywayMap.get(dbName).migrate();
    }

    private void cleanAutomatically(String dbName) {
        Map<String, Flyway> flywayMap = flyways();
        flywayMap.get(dbName).clean();
    }

    private void checkState(String dbName) {
        Map<String, Flyway> flywayMap = flyways();

        MigrationInfo[] pendingMigrations = flywayMap.get(dbName).info().pending();

        if (pendingMigrations != null) {
            throw new RuntimeException(dbName + "-" + StringUtils.join(pendingMigrations, ","));
        }
    }
}