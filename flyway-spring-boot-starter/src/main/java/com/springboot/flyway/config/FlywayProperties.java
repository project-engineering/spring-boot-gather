package com.springboot.flyway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = FlywayProperties.PREFIX)
@Configuration
public class FlywayProperties {

    public static final String PREFIX = "flyway";
    /**
     * sql脚本文件路径
     */
    private String flywayScriptLocation = "classpath:db/migration/";
    /**
     * 自动迁移
     */
    private Boolean flywayAutoMigration = true;
    /**
     * 发生错误的时候清理表
     */
    private Boolean cleanOnValidationError = true;
    /**
     * 结束时间自动清理表
     */
    private Boolean autoCleanTable = true;

    public Boolean getCleanOnValidationError() {
        return cleanOnValidationError;
    }

    public void setCleanOnValidationError(Boolean cleanOnValidationError) {
        this.cleanOnValidationError = cleanOnValidationError;
    }

    public Boolean getAutoCleanTable() {
        return autoCleanTable;
    }

    public void setAutoCleanTable(Boolean autoCleanTable) {
        this.autoCleanTable = autoCleanTable;
    }

    public String getFlywayScriptLocation() {
        return flywayScriptLocation;
    }

    public void setFlywayScriptLocation(String flywayScriptLocation) {
        this.flywayScriptLocation = flywayScriptLocation;
    }

    public Boolean getFlywayAutoMigration() {
        return flywayAutoMigration;
    }

    public void setFlywayAutoMigration(Boolean flywayAutoMigration) {
        this.flywayAutoMigration = flywayAutoMigration;
    }
}
