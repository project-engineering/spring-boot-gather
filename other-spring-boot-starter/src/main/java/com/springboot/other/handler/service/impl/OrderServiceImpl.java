package com.springboot.other.handler.service.impl;

import com.springboot.other.handler.HandlerContext;
import com.springboot.other.handler.OrderBO;
import com.springboot.other.handler.service.OrderService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @author liuc
 * @date 2024-09-10 22:12
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private HandlerContext handlerContext;

    @Override
    public String handle(OrderBO bo) {
        return handlerContext.getInstance(bo.getType()).handle(bo);
    }
}
