package com.springboot.other.handler;

import lombok.Data;

/**
 * @author liuc
 * @date 2024-09-10 22:07
 */
@Data
public class OrderBO {
    private String orderNo;
    private String price;
    private String productName;
    private String type;
}
