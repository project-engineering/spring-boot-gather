package com.springboot.other.handler;

import java.util.Map;

/**
 * 订单策略模式环境
 * 这个类的注入由HandlerProccessor实现，这里只提供一个获取处理器实例的接口
 *
 * @author liuc
 * @date 2024-09-10 22:02
 */
public class HandlerContext {

    /**
     * 处理器映射表
     */
    private final Map<String, AbstractHandler> handlerMap;

    /**
     * 构造函数
     * @param handlerMap 处理器映射表
     */
    public HandlerContext(Map<String, AbstractHandler> handlerMap) {
        this.handlerMap = handlerMap;
    }

    /**
     * 根据类型获取处理器实例
     * @param type 类型
     * @return 处理器实例
     */
    public AbstractHandler getInstance(String type) {
        if (type == null) {
            throw new IllegalArgumentException("type参数不能为空");
        }
        AbstractHandler clazz = handlerMap.get(type);
        if (clazz == null) {
            throw new IllegalArgumentException("该类型没有在枚举OrderTypeHandlerAnno中定义，请定义：" + type);
        }
        return clazz;
    }

}