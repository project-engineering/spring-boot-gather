package com.springboot.other.handler.enums;

import lombok.Getter;

/**
 * 订单类型枚举
 * @author liuc
 * @date 2024-09-19 11:14
 */
@Getter
public enum OrderTypeEnum {
    NORMAL("1", "普通"),
    GROUP("2", "团队"),
    PROMOTION("3", "促销"),
    INTEGRAL("4", "积分"),
    OTHER("5", "其他");
    /**
     * 代码
     */
    private final String code;
    /**
     * 名称，描述
     */
    private final String name;

    OrderTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code属性获取name属性
     *
     * @param code 代码
     * @return 名称
     */
    public static String getNameByCode(String code) {
        for (OrderTypeEnum temp : OrderTypeEnum.values()) {
            if (temp.getCode().equals(code)) {
                return temp.getName();
            }
        }
        return null;
    }
}
