package com.springboot.other.handler;

/**
 * @author liuc
 * @date 2024-09-10 22:06
 */
public abstract class AbstractHandler {

    abstract public String handle(OrderBO bo);

}