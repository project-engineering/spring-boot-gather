package com.springboot.other.handler;

import com.springboot.other.handler.annotation.OrderTypeHandlerAnno;
import com.springboot.other.handler.enums.OrderTypeEnum;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @author liuc
 * @date 2024-09-19 11:31
 */
@Log4j2
@Component
@OrderTypeHandlerAnno(OrderTypeEnum.INTEGRAL)
public class IntegralHandler extends AbstractHandler {

    @Override
    public String handle(OrderBO bo) {
        log.info("处理积分订单");
        return "处理积分订单";
    }
}
