package com.springboot.other.handler;

import com.springboot.other.handler.annotation.OrderTypeHandlerAnno;
import com.springboot.other.handler.enums.OrderTypeEnum;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 促销订单处理器
 *
 * @author liuc
 * @date 2024-09-10 22:09
 */
@Log4j2
@Component
@OrderTypeHandlerAnno(OrderTypeEnum.PROMOTION)
public class PromotionHandler extends AbstractHandler {

    @Override
    public String handle(OrderBO bo) {
        log.info("处理促销订单");
        return "处理促销订单";
    }

}