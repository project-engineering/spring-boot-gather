package com.springboot.other.handler.service;

import com.springboot.other.handler.OrderBO;

public interface OrderService {
    String handle(OrderBO bo);
}
