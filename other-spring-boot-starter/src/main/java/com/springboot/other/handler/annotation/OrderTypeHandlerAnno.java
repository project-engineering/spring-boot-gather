package com.springboot.other.handler.annotation;

import com.springboot.other.handler.enums.OrderTypeEnum;
import java.lang.annotation.*;

/**
 * 订单类型注解
 * 使用方式：
 * 1：普通订单 @OrderTypeHandlerAnno("1")
 * 2：团购订单 @OrderTypeHandlerAnno("2")
 * 3：促销订单 @OrderTypeHandlerAnno("3")
 * 4：积分订单 @OrderTypeHandlerAnno("4")
 * 5：其他订单 @OrderTypeHandlerAnno("5")
 * @author liuc
 * @date 2024-09-19 11:17
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public@interface OrderTypeHandlerAnno {
    OrderTypeEnum value();
}