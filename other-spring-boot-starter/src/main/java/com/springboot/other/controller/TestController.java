package com.springboot.other.controller;

import com.springboot.other.handler.OrderBO;
import com.springboot.other.handler.enums.OrderTypeEnum;
import com.springboot.other.handler.service.OrderService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuc
 * @date 2024-09-11 10:49
 */
@RestController()
@RequestMapping("/test")
public class TestController {
    @Resource
    private OrderService orderService;
    @PostMapping("/pay")
    public String pay(){
        OrderBO bo = new OrderBO();
        bo.setOrderNo("123456");
        bo.setType(OrderTypeEnum.INTEGRAL.getCode());
        bo.setPrice("100");
        bo.setProductName("测试商品");
        return orderService.handle(bo);
    }
}
