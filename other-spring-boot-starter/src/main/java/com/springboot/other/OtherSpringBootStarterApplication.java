package com.springboot.other;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtherSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtherSpringBootStarterApplication.class, args);
    }

}
