package com.springboot.futool;

import java.io.*;
import java.util.Map;
import java.util.Properties;
import lombok.extern.log4j.Log4j2;

/**
 * properties 文件读取工具类
 */
@Log4j2
public class PropertyUtil {

    /**
     * 根据Properties文件名读取项目中的Properties文件
     * @param fileName 文件名
     * @return Properties 对象
     */
    public static Properties getProperties(String fileName){
        log.info("开始加载properties文件内容.......");
        Properties props = new Properties();
        InputStream in = null;
        String path ="";
        log.info("os.name:"+System.getProperty("user.dir"));
        if (System.getProperty("os.name").contains("Windows")) {
            log.info("user.dir:"+System.getProperty("user.dir"));
            String pathTmp = System.getProperty("user.dir"+"\\target\\classes\\config\\");
            if (null == pathTmp) {
                log.error(fileName+"文件加载失败");
            } else {
                if (new File(pathTmp).exists()) {
                    path = pathTmp + fileName.split("/config/")[1];
                } else {
                    path = System.getProperty("user.dir") + File.separator + fileName;
                }
                log.info("========path======="+path);
            }
        }else {
            path = System.getProperty("user.dir")+fileName;
            log.info("========path======="+path);
        }
        try {
            //加载Properties文件
            in = PropertyUtil.class.getClassLoader().getResourceAsStream(path);
            props.load(in);
        } catch (IOException e) {
            log.error(fileName + "文件加载失败,失败原因：" + e.getMessage());
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error(fileName + "文件加载失败,关闭InputStream失败：" + e.getMessage());
                }
            }
        }
        return props;
    }

    /**
     * 获取指定目录下的Properties文件
     * @param path 路径
     * @param fileName 文件名
     * @return Properties 对象
     */
    public static Properties getProperties(String path ,String fileName){
        log.info("开始加载properties文件内容.......");
        Properties props = new Properties();
        InputStream in = null;
        log.info("========path======="+path);
        if (null != path) {
            try {
                //加载Properties文件
                in = new BufferedInputStream(new FileInputStream(path+fileName));
                props.load(in);
            } catch (IOException e) {
                log.error(fileName+"文件加载失败,失败原因："+e.getMessage());
            } finally {
                if (null != in) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        log.error(fileName+"文件加载失败,关闭InputStream失败："+e.getMessage());
                    }
                }
            }
        }
        return props;
    }

    /**
     * 根据Properties文件名读取项目中的Properties文件中的属性信息
     * @param fileName 文件名
     * @param key 属性名
     * @return 属性值
     */
    public static String getValue(String fileName,String key) {
        Properties props = getProperties(fileName);
        try {
            return props.getProperty(key);
        } catch (Exception e) {
            log.error("获取属性值失败：{}", e.getMessage());
            return null;
        }
    }

    /**
     * 根据Properties文件名读取项目中的Properties文件中的属性信息
     * @param fileName 文件名
     * @param map 属性名和属性值的map
     * @return 属性值
     */
    public static Map<String, String> getValue(String fileName, Map<String, String> map) {
        Properties props = getProperties(fileName);
        try {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key=entry.getKey();
                String value = props.getProperty(key);
                map.put(key, value);
            }
            return map;
        } catch (Exception e) {
            log.error("获取属性值失败：{}", e.getMessage());
            return null;
        }
    }

    /**
     * 获取指定目录下的Properties文件中的属性信息
     * @param path 路径
     * @param fileName 文件名
     * @param key 属性名
     * @return 属性值
     */
    public static String getValue(String path,String fileName,String key) {
        Properties props = getProperties(path,fileName);
        try {
            return props.getProperty(key);
        } catch (Exception e) {
            log.error("获取属性值失败：{}", e.getMessage());;
            return null;
        }
    }

    /**
     * 获取指定目录下的Properties文件中的属性信息
     * @param path 路径
     * @param fileName 文件名
     * @param map 属性名和属性值的map
     * @return 属性值
     */
    public static Map<String, String> getValue(String path,String fileName, Map<String, String> map) {
        Properties props = getProperties(path,fileName);
        try {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key=entry.getKey();
                String value = props.getProperty(key);
                map.put(key, value);
            }
            return map;
        } catch (Exception e) {
            log.error("获取属性值失败：{}", e.getMessage());
            return null;
        }
    }

}
