package com.springboot.futool;

import lombok.extern.log4j.Log4j2;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author liuc
 * @date 2024-06-14 15:50
 */
@Log4j2
public class ObjectUtil {
    /**
     * 将对象中值为null的字段转换为空字符串
     * String类型字段会被转换为空字符串、List转换成空列表、Map转换成空Map
     * Character、Integer、Long、Double、Float、Short、Byte、Boolean、Date类型字段不会被转换为空字符串
     * @param object 类名
     */
    public static void convertNullToEmptyString(Object object) {
        Class<?> clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(object);
                if (value == null) {
                    Class<?> type = field.getType();
                    if (type == String.class) {
                        field.set(object, "");
                    } else if (Map.class.isAssignableFrom(type)) {
                        field.set(object, createEmptyCollection(type));
                    } else if (Queue.class.isAssignableFrom(type)) {
                        field.set(object, new LinkedList<>());
                    } else if (Deque.class.isAssignableFrom(type)) {
                        field.set(object, new LinkedList<>());
                    } else if (Stack.class.isAssignableFrom(type)) {
                        field.set(object, new Stack<>());
                    } else if (List.class.isAssignableFrom(type)) {
                        field.set(object, createEmptyCollection(type));
                    }
                }
            } catch (IllegalAccessException e) {
                log.error("convertNullToEmptyString error", e);
                throw new RuntimeException("将对象中值为null的字段转换为空字符串异常，原因：" + e.getMessage());
            }
        }
    }

    /**
     * 创建一个空的集合对象
     */
    private static Object createEmptyCollection(Class<?> type) {
        if (List.class == type) {
            return Collections.emptyList();
        } else if (Map.class == type) {
            return Collections.emptyMap();
        } else if (Set.class == type) {
            return Collections.emptySet();
        } else if (Hashtable.class == type)  {
            return new Hashtable<>();
        } else if (Vector.class == type) {
            return new Vector<>();
        }
        throw new IllegalArgumentException("Unsupported collection type: " + type.getName());
    }

    /**
     * 将源对象中的值复制到目标对象中
     *
     * @param source 源对象
     * @param destination 目标对象
     */
    public static void convert(Object source, Object destination){
        log.info("源对象：{}", JsonUtil.toJson(source));
        try {
            // 获取源对象的所有字段
            Field[] sourceFields = source.getClass().getDeclaredFields();
            // 获取目标对象的所有字段
            Field[] destinationFields = destination.getClass().getDeclaredFields();

            // 遍历源对象的字段
            for (Field sourceField : sourceFields) {
                // 设置字段可访问
                sourceField.setAccessible(true);

                // 遍历目标对象的字段，找到对应的字段名
                for (Field destinationField : destinationFields) {
                    // 设置字段可访问
                    destinationField.setAccessible(true);
                    // 如果字段名相同，则复制字段的值
                    if (sourceField.getName().equals(destinationField.getName())) {
                        destinationField.set(destination, sourceField.get(source));
                        // 找到对应字段后跳出内层循环
                        break;
                    }
                }
            }
        } catch (IllegalAccessException e) {
            log.error("convert error", e);
            throw new RuntimeException("将源对象中的值复制到目标对象中异常，原因："+e.getMessage());
        }
        log.info("目标对象：{}", JsonUtil.toJson(destination));
    }

    /**
     * 去除对象中所有字符串字段的首尾空白字符
     * @param object 对象
     */
    public static void trimWhitespace(Object object) {
        Class<?> clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.getType() == String.class) {
                try {
                    field.setAccessible(true);
                    String value = (String) field.get(object);
                    if (value != null) {
                        field.set(object, value.trim());
                    }
                } catch (IllegalAccessException e) {
                    log.error("trimWhitespace error", e);
                    throw new RuntimeException("去除对象中所有字符串字段的首尾空白字符异常，原因："+e.getMessage());
                }
            }
        }
    }
}
