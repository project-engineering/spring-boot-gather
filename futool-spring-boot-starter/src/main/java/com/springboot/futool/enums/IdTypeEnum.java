package com.springboot.futool.enums;

/**
 * 证件类型枚举类
 *
 */
public enum IdTypeEnum {
    ID_CARD_NO("0","身份证"),
    RESIDENCE_BOOKLET("1","户口簿"),
    PASSPORT("2", "护照"),
    OFFICERS_CERTIFICATE("3", "军官证"),
    SOLDIER_CERTIFICATE("4","士兵证"),
    HOME_RETURN_PERMIT("5","港澳居民来往内地通行证"),
    TAIWAN_COMPATRIOT_TRAVEL_PERMIT("6", "台湾同胞来往内地通行证"),
    LARGE_ENTERPRISE("7", "临时身份证"),
    TEMPORARY_ID_CARD("8", "外国人居留证"),
    POLICE_OFFICER_CERTIFICATE("9","警官证"),
    ORGANIZATION_CODE("a","组织机构代码"),
    ZHONGZHENG_CODE("c","贷款卡/中征码"),
    INSTITUTIONAL_CREDIT_CODE("d", "机构信用代码"),
    UNIFIED_SOCIAL_CREDIT_CODE("u", "统一社会信用代码"),
    OTHER_CERTIFICATE("x","其他证件"),
    FINANCIAL_INSTITUTION_CODE("z", "其他证件");

    private String type;
    private String desc;

    IdTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static String getDesc(String type){
        for(IdTypeEnum idTypeEnum : IdTypeEnum.values()) {
            if(idTypeEnum.type.equalsIgnoreCase(type)) {
                return idTypeEnum.desc;
            }
        }
        return null;
    }
}
