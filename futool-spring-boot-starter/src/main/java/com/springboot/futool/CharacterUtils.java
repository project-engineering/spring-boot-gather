package com.springboot.futool;

import org.apache.commons.lang3.StringUtils;
import java.util.*;

/**
 * 字符处理工具类
 */
public class CharacterUtils {

    /**
     * 需要替换的公共符号（特殊字符，全角字符等下游无法识别等字符）
     */
    private static final Map<Character, Character> SPECIAL_CHAR_REPLACE_COMMON_MAP = new HashMap<>();

    /**
     * 部分中文标点替换英文标点map
     */
    private static final Map<Character, Character> SPECIAL_CHAR_REPLACE_TITLE_MAP = new HashMap<>();

    /**
     * 全角空格正则匹配
     */
    private static final String REG_EX_BLANK_SBC = "[\\p{Zs}]";

    /**
     * 全角字符转半角字符偏移量
     */
    private static final int SBC_TO_DBC_OFFSET = 65248;

    /**
     * 空格字符
     */
    private static final char EMPTY_CHAR = ' ';

    /**
     * 需要去除的指定标点符号
     */
    private static final List<Character> TITLE_TRIM = Arrays.asList('：', ':', '，', ',', '、', '；', ';', '。', '.', '|', '/', '\\', ' ');

    /**
     * 需要保留的特殊标点符号
     */
    private static final String[] TITLE_RETAIN = {"···", "······", ".......", "..."};

    static {
        // 公共替换
        // 全角空格 -> 半角空格
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('\u3000', EMPTY_CHAR);
        // 特殊空格 -> 半角空格
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('\u00A0', EMPTY_CHAR);
        // 特殊符号 -> 半角空格
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('\u200B', EMPTY_CHAR);
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('丨', '|');
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('｜', '|');
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('︱', '|');
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('•', '·');
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('\uFEFF', EMPTY_CHAR);
        // 日文句号
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('\uFF61', '。');
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('™', EMPTY_CHAR);
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('℠', EMPTY_CHAR);
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('℗', EMPTY_CHAR);
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('©', EMPTY_CHAR);
        SPECIAL_CHAR_REPLACE_COMMON_MAP.put('®', EMPTY_CHAR);

        // 标题替换
        SPECIAL_CHAR_REPLACE_TITLE_MAP.putAll(SPECIAL_CHAR_REPLACE_COMMON_MAP);
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('【', '[');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('】', ']');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('「', '{');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('」', '}');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('《', '<');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('》', '>');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('“', '"');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('”', '"');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('‘', '\'');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('’', '\'');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('。', '.');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('，', ',');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('？', '?');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('；', ';');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('：', ':');
        SPECIAL_CHAR_REPLACE_TITLE_MAP.put('—', '-');
    }

    /**
     * 替换全角空格
     *
     * @param str 待替换的内容
     * @return 替换后的内容
     */
    public static String removeSpecial(String str) {
        return str.replaceAll(REG_EX_BLANK_SBC, " ");
    }

    /**
     * 发给下游的资讯正文txt格式统一格式化。
     * <li>8个空格替换为4个空格</li>
     *
     * @param str 待格式化正文txt内容
     * @return 格式化后的txt内容
     */
    public static String formatContentText(String str) {
        if (StringUtils.isBlank(str)) {
            return StringUtils.EMPTY;
        }
        return str.replaceAll(" {8}", "    ");
    }

    /**
     * 需要替换的公共特殊符号
     *
     * @param str 待替换内容
     * @return 替换后的内容
     */
    public static String replaceCommon(String str) {
        if (StringUtils.isBlank(str)) {
            return StringUtils.EMPTY;
        }
        char[] chars = str.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (char aChar : chars) {
            if (SPECIAL_CHAR_REPLACE_COMMON_MAP.containsKey(aChar)) {
                char replaceChar = SPECIAL_CHAR_REPLACE_COMMON_MAP.get(aChar);
                if (!Objects.equals(replaceChar, EMPTY_CHAR)) {
                    builder.append(replaceChar);
                }
            } else {
                builder.append(aChar);
            }
        }
        return builder.toString();
    }

    /**
     * 将中文标点符号替换为相应的英文标点符号，并去除多余空格
     *
     * @param str 待替换内容
     * @return 替换后的内容
     */
    public static String replaceSbc(String str) {
        if (StringUtils.isBlank(str)) {
            return StringUtils.EMPTY;
        }
        char[] chars = str.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (char aChar : chars) {
            if (SPECIAL_CHAR_REPLACE_TITLE_MAP.containsKey(aChar)) {
                char replaceChar = SPECIAL_CHAR_REPLACE_TITLE_MAP.get(aChar);
                if (!Objects.equals(replaceChar, EMPTY_CHAR)) {
                    builder.append(replaceChar);
                }
                continue;
            }
            if (aChar < 0x20 || aChar == 0x7F) {
                continue;
            }
            if (aChar > '\uFF00' && aChar < '\uFF5F') {
                // 全角替换为半角
                builder.append((char) (aChar - SBC_TO_DBC_OFFSET));
            } else {
                builder.append(aChar);
            }
        }
        return trimTitle(builder.toString().replaceAll(" {2,8}", " ").trim());
    }

    /**
     * 字符串去掉所有标点符号
     *
     * @param str 待去掉标点的字符串
     * @return 去掉标点后的字符串
     */
    public static String removePunctuation(String str) {
        if (StringUtils.isBlank(str)) {
            return StringUtils.EMPTY;
        }
        return str.replaceAll("[\\pP+~$`^=|<>～｀＄＾＋＝｜＜＞￥×]", "");
    }

    /**
     * 打印字符串Unicode码
     *
     * @param str 字符串
     */
    public static void printUnicode(String str) {
        StringBuilder unicode = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            unicode.append("\\u").append(Integer.toHexString(str.charAt(i))).append(" ");
        }
        System.out.println(unicode);
    }

    /**
     * 去除标题末尾指定标点符号（保留部分特殊标点符号）
     *
     * @param titleStr 标题字符串
     * @return 格式化后的标题字符串
     */
    public static String trimTitle(String titleStr) {
        if (StringUtils.isBlank(titleStr)) {
            return titleStr;
        }
        for (String retain : TITLE_RETAIN) {
            if (titleStr.endsWith(retain)) {
                return titleStr;
            }
        }
        char[] chars = titleStr.toCharArray();
        int len = chars.length;
        while (len > 0 && TITLE_TRIM.contains(chars[len - 1])) {
            len--;
        }
        return titleStr.substring(0, len);
    }
}