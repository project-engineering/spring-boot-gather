package com.springboot.futool.encrypt.rsa;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Base64;

/**
 * RSA加密解密工具类
 */
public class RsaUtil {
    //非对称密钥算法
    public static final String KEY_ALGORITHM="RSA";
    /**
     * 密钥长度，DH算法的默认密钥长度是1024
     * 密钥长度必须是64的倍数，在512到65536位之间
     * */
    private static final int KEY_SIZE=2048;

    /**
     * 生成密钥对
     * @return KeyPair 密钥对
     * @throws Exception 异常
     */
    public static KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        // 密钥长度为2048位
        keyPairGenerator.initialize(KEY_SIZE);
        return keyPairGenerator.generateKeyPair();
    }

    /**
     * 使用公钥加密
     * @param plainText 明文
     * @param publicKey 公钥
     * @return 加密后的字节数组
     * @throws Exception 异常
     */
    public static byte[] encrypt(String plainText, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(plainText.getBytes());
    }

    /**
     * 使用私钥解密
     * @param encryptedBytes 加密后的字节数组
     * @param privateKey 私钥
     * @return 解密后的字节数组
     * @throws Exception 异常
     */
    public static byte[] decrypt(byte[] encryptedBytes, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(encryptedBytes);
    }

    /**
     * 使用私钥签名
     * @param plainText 明文
     * @param privateKey 私钥
     * @return 签名后的字节数组
     * @throws Exception 异常
     */
    public static byte[] sign(String plainText, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(plainText.getBytes());
        return signature.sign();
    }

    /**
     * 使用公钥验证签名
     * @param plainText 明文
     * @param signatureBytes 签名后的字节数组
     * @param publicKey 公钥
     * @return 验证结果
     * @throws Exception 异常
     */
    public static boolean verify(String plainText, byte[] signatureBytes, PublicKey publicKey) throws Exception {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(plainText.getBytes());
        return signature.verify(signatureBytes);
    }

    public static void main(String[] args) throws Exception {
        String plainText = "Hello, World!";

        // 生成密钥对
        KeyPair keyPair = generateKeyPair();

        // 获取公钥和私钥
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        // 使用公钥加密
        byte[] encryptedBytes = encrypt(plainText, publicKey);
        String encryptedText = Base64.getEncoder().encodeToString(encryptedBytes);
        System.out.println("Encrypted Text: " + encryptedText);

        // 使用私钥解密
        byte[] decryptedBytes = decrypt(encryptedBytes, privateKey);
        String decryptedText = new String(decryptedBytes);
        System.out.println("Decrypted Text: " + decryptedText);

        // 使用私钥签名
        byte[] signatureBytes = sign(plainText, privateKey);
        String signatureText = Base64.getEncoder().encodeToString(signatureBytes);
        System.out.println("Signature: " + signatureText);

        // 使用公钥验证签名
        boolean isVerified = verify(plainText, signatureBytes, publicKey);
        System.out.println("Signature Verified: " + isVerified);
    }
}
