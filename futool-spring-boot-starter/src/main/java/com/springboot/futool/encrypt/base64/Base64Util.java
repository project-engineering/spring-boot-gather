package com.springboot.futool.encrypt.base64;

import com.springboot.futool.StringUtil;
import com.springboot.futool.UtilValidate;
import com.springboot.futool.constant.Constant;
import lombok.extern.log4j.Log4j2;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * Base64加解密工具
 */
@Log4j2
public class Base64Util {

    /**
     * 加密
     * @param src 需要加密的数据
     * @return 加密后的字符串
     */
    public static String encode(byte[] src) {
        if (UtilValidate.isEmpty(src)) {
            return null;
        } else {
            byte[] encodeBytes = Base64.getEncoder().encode(src);
            return new String(encodeBytes);
        }
    }

    /**
     * 加密
     * @param src 需要加密的数据
     * @return 加密后的字符串
     */
    public static String encode(String src) {
        if (UtilValidate.isEmpty(src)) {
            return null;
        } else {
            byte[] encodeBytes = Base64.getEncoder().encode(src.getBytes());
            return new String(encodeBytes);
        }
    }

    /**
     * 指定字符集加密
     * @param src 需要加密的数据
     * @param charset 字符集
     * @return 加密后的字符串
     */
    public static String encode(String src, String charset){
        if (UtilValidate.isEmpty(src)) {
            return null;
        }
        if (UtilValidate.isEmpty(charset)) {
            charset = Constant.CHARSET_UTF8;
        }
        byte[] encodeBytes;
        try {
            encodeBytes = Base64.getEncoder().encode(src.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            throw new RuntimeException("加密失败，失败原因："+e.getMessage());
        }
        return new String(encodeBytes);
    }

    /**
     * 指定字符集加密
     * @param obj 需要加密的数据
     * @param charset 字符集
     * @return 加密后的字符串
     */
    public static String encodeObj(Object obj, String charset){
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        if (UtilValidate.isEmpty(charset)) {
            charset = Constant.CHARSET_UTF8;
        }
        byte[] encodeBytes = new byte[0];
        try {
            String src = StringUtil.objToStr(obj);
            encodeBytes = Base64.getEncoder().encode(src.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            throw new RuntimeException("加密失败，失败原因："+e.getMessage());
        }
        return new String(encodeBytes);
    }

    /**
     * 解密
     * @param src 需要解密的数据
     * @return 解密后的字符串
     */
    public static String decode(String src) {
        if (UtilValidate.isEmpty(src)) {
            return null;
        } else {
            byte[] decodeBytes = Base64.getDecoder().decode(src.getBytes());
            return new String(decodeBytes);
        }
    }

    /**
     * 指定字符集解密
     * @param src 需要解密的数据
     * @param charset 字符集
     * @return 解密后的字符串
     */
    public static String decode(String src, String charset){
        if (UtilValidate.isEmpty(src)) {
            return null;
        }
        if (UtilValidate.isEmpty(charset)) {
            charset = Constant.CHARSET_UTF8;
        }
        byte[] decodeBytes;
        try {
            decodeBytes = Base64.getDecoder().decode(src.getBytes(charset));
            return new String(decodeBytes,charset);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            throw new RuntimeException("解密失败，失败原因："+e.getMessage());
        }
    }

    /**
     * 指定字符集解密
     * @param obj 需要解密的数据
     * @param charset 字符集
     * @return 解密后的字符串
     */
    public static String decodeObj(Object obj, String charset){
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        if (UtilValidate.isEmpty(charset)) {
            charset = Constant.CHARSET_UTF8;
        }
        byte[] decodeBytes = new byte[0];
        try {
            String src = StringUtil.objToStr(obj);
            decodeBytes = Base64.getDecoder().decode(src.getBytes(charset));
            return new String(decodeBytes,charset);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            throw new RuntimeException("解密失败，失败原因："+e.getMessage());
        }
    }
}
