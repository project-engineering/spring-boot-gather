package com.springboot.futool.encrypt.rsa;

import org.apache.commons.codec.binary.Base64;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 签名工具类
 */
public class DigitalSignatureUtil {
    /**
     * 参数分别代表
     */
    private static final String ALGORITHMS = "RSA";

    private static final String ALGORITHM = "SHA256withRSA";
    /**
     * 签名私钥 17759051997
     */
    private static final String SIGNATURE_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC9Gv5KNtk4chHBx332tLxgOnoJ9zpDiMQ6YcZuLO3GC/INLWM71I/LvyPpU0pwC1NwCG0GMgNPax7O6kiNkp/0f+3f7jPFQf0qP7JsGsk1bdmFIy6cOnkrvH1FyYJGdTxBwJxiwTBr3QUN3nh/djO5QPeRffn+Yh9ZkIsPeZjp3u3HMYeVaVXzhTJAMrVyoBiVS8zIeDsV/SrU2tXBdTR9ULw8G3GerO6SfLjagMQ3SvuMi+srSwqZlJUd+gdWNY+FUv1Fa438Y/ZFXSTSdvznoD1zF52VXbfqVIGk81iBaKCU8ZbFOavA4jroYTU9n8MwkGn5Fo+96HnY0DSYDgmhAgMBAAECggEAFaEoxMRRRCuH7Gos9jLl+Pu3SbyFZYQXLbZRQ/jPmX90SB2Q5B8D84IBUYa9VON2v7G3BqZhyouuEmypr8e9k/Gt+5b7ROyvUE3I0qSdrwbJgnjrs+LcSSxeB8VsqTJvmfW17XW7XBsDoPp5Pdr/P4k2x+Vo2rfObigiP7rgyp8KimHaq1u5RLYLUbPGBdRLRt0NUohO1V9Y0KyFTtxdfvJ2BdDANvhOYyCmp9dI5MXoNDh1w0z03Eq4PM30SVEv37mwSVEav4eKlN7/cuclcLF2pddNX7xScdH/kohDaR2xWh1HqkVKIMP+nBXMPWsSpzw+ktwQBROtMbtNsuIZIQKBgQDtDM1KJqoleKImYDc+S3+tU+z6PiYEQQRAzhCscvCvWNOzI0OB0CSEp3hfiUXL6jyOFOU/qI36EqXuqSAuNM+ZtfIzQhPkDNHjO9RqHZhan8AsP06Bmo2P+u83BGF2VVMfacM98aRpL6CwpBsBpnX0ASlC25WAESviXRRH01Za5wKBgQDMOQMTue9j1EA19VLnHXwuOJ3KoaAIdwyluQPgyjnIyTypMKelOrUJoUhRYJm/4XOekq0oyWhc++GnkMfHZYOzHXalTMoAP9EdRMRqUTHztBrUkqkInVEJrAif/OvXMlSIR15fIC+/fewLxEKjf2rU2AM7XRKVyHplkvCsoB0uNwKBgGpUhL1PLKEURH+8RuttiD7iV3lEaV8dHuBGzpncEPRGfudq2PwgtlC+ojMQazt1vWXqH473d4AF32J3gJTZYYnMYHD3od54lak9DCHxVobIA7aVSwy9m+RKpgTitSkUSu3bThW6D4qTL5wscGTEG0KxRqXTw3KnwSyPneo99Q1fAoGAOpyONoYhn3wWJaZP8cazkixrlPFIFcXdGl78LvK7HNYsk75EDxbHSIlCUSCxX7Gb1kHwcolDa5Ra0hWqUJ7g6nIlUBG209V89bJ70KuW84OYQ7QH0VIdJPJ70zbqlOt7+VTKT/DT41iHe2ULXxM9nPKWEt6Ga/iKsEY4zsJxPYcCgYEAoGW8YlcLiMpaTOyOnS5uH6at71sUc2wLR+ovLqqpQxLtOn597fB96X41fg07x0yh9RJ4MbSjamVOpcWsXJaTL5hf98ppw/ffeoWtRc2IiVVGZFNDYm5xXj+mMke8mFNCeKEo2Igf8jGXUTz6473cdljd4GB0T/JWGFX2HNzvEcs=";
    /**
     * 签名公钥
     */
    private static final String SIGNATURE_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvRr+SjbZOHIRwcd99rS8YDp6Cfc6Q4jEOmHGbiztxgvyDS1jO9SPy78j6VNKcAtTcAhtBjIDT2sezupIjZKf9H/t3+4zxUH9Kj+ybBrJNW3ZhSMunDp5K7x9RcmCRnU8QcCcYsEwa90FDd54f3YzuUD3kX35/mIfWZCLD3mY6d7txzGHlWlV84UyQDK1cqAYlUvMyHg7Ff0q1NrVwXU0fVC8PBtxnqzukny42oDEN0r7jIvrK0sKmZSVHfoHVjWPhVL9RWuN/GP2RV0k0nb856A9cxedlV236lSBpPNYgWiglPGWxTmrwOI66GE1PZ/DMJBp+RaPveh52NA0mA4JoQIDAQAB";

    /**
     *
     * @param signatureContent 签名内容
     * @param privateKeyEncoded 私钥
     * @return 签名
     * @throws Exception 异常
     */
    public static String generationSignature(String signatureContent, byte[] privateKeyEncoded) throws Exception {
        // 创建key的工厂
        KeyFactory keyFactory=KeyFactory.getInstance(ALGORITHMS);
        // 创建 已编码的私钥规格
        PKCS8EncodedKeySpec encPriKeySpec = new PKCS8EncodedKeySpec(privateKeyEncoded);
        // 获取指定算法的密钥工厂, 根据 已编码的私钥规格, 生成私钥对象
        PrivateKey privateKey = keyFactory.generatePrivate(encPriKeySpec);
        Signature signature = Signature.getInstance(ALGORITHM);
        signature.initSign(privateKey);
        signature.update(signatureContent.getBytes());
        byte[] sign = signature.sign();
        // 采用base64算法进行转码,避免出现中文乱码
        return  Base64.encodeBase64String(sign);
    }

    /**
     *  校验签名
     * @param signatureContent 签名内容
     * @param signature 签名
     * @param publicKeyEncoded 公钥
     * @return true 正确 false 错误
     * @throws Exception 异常
     */
    public static boolean verifySignature(String signatureContent,String signature, byte[] publicKeyEncoded) throws Exception {
        // 创建key的工厂
        KeyFactory keyFactory=KeyFactory.getInstance(ALGORITHMS);
        // 创建 已编码的公钥规格
        X509EncodedKeySpec encPubKeySpec = new X509EncodedKeySpec(publicKeyEncoded);
        // 获取指定算法的密钥工厂, 根据 已编码的公钥规格, 生成公钥对象
        PublicKey publicKey = keyFactory.generatePublic(encPubKeySpec);
        Signature verifySignature = Signature.getInstance(ALGORITHM);
        verifySignature.initVerify(publicKey);
        verifySignature.update(signatureContent.getBytes());
        return verifySignature.verify(Base64.decodeBase64(signature));
    }

    public static void main(String[] args) throws Exception {
        //---------------------------------生成签名
        //要签名的内容
        String conent="Hello,World!";
        String signature = DigitalSignatureUtil.generationSignature(conent, Base64.decodeBase64(SIGNATURE_PRIVATE_KEY));

        //---------------------------------效验签名
        boolean verifySignature = DigitalSignatureUtil.verifySignature(conent, signature, Base64.decodeBase64(SIGNATURE_PUBLIC_KEY));

        System.out.println("--------------------------生成的签名-----------------");
        System.out.println(signature);
        System.out.println("--------------------------校验签名、正确true、错误false-----------------");
        System.out.println(verifySignature);
    }
}
