package com.springboot.futool.encrypt.rsa;

import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;

/**
 * RSA加密算法
 */
public class RsaEncryptUtil {
    /**
     * 参数分别代表
     */
    private static final String ALGORITHMS = "RSA";
    /**
     * RSA算法私钥
     */
    private static final String RSA_PRIVATE_KEY ="MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCPEG5zZAEqSasGgEr1iHdunBLHip08GGct9EAIzw8zBq5zUwpX0KXLNJJ1d+AgcQWAYk0BrsX9lpR2ffodJci2qgo13kXc97xMRbFBSsZ4wnoK0L/cSy9P6NxdpJ+FuhKY6MnwOFApif7mJY8I4FumThOjC+7s3kW1hMctbFc0YyEc3sND9u001oiX9lDEiTYd4Ouq2IKDFbe2l38KCPgTpkpD4OXUFm3utgLlsrGostU9DQXalg9c8LLLRjBQoHyeCyOXzwWBGsZ2hoAvJNKIlOx66GKu6ebF/EV3Y2BsdjDQF0zCQ/c3xCz7bsv2BGcuA3VuYAtUmBmoSD6AL6aTAgMBAAECggEAKXMtTSlwXPwzHRMWpMUBNX7qwf2bSMoZOutFkkfLs5EAAlHQ8Vh2cMWumXI98ahNW8EfZploq+xw31Pon4FPAf2KL8lSnI76c27NyIkNr/dIvNF2hTETaCejkU4dGfWrZbosp/jo4OAmhi/jnULDyw9cCSVv+Bj+QXVS8AovyfFvv4Yj9j6G0vKvs7npJQEJ6eQjeKayWOMArGnxQeRwQBmdtwL46X9ARBt+vSCrWLcOi83orrgiR3R4eySNJ3nTPtyX6G5Co/GdiRm6h+aJQ3zXeE0DVUDsgOYj6rtwb9KJwLG3AM7Bx7evIyPcOY0iDowGnWdRXQo0TTjpIi3LoQKBgQDDRjxTxDYQX6NIaWtjs45tMtdGzavF4C5+7MSsWkf7SZ5JWu3b8XyE8WO4BGMr3q1meeA/PCQjA94LLKHYGj0UTXUDJzd8gw0wbFr9X+ta4FDuUfsW13rSmE6O4oz9oN7ciHDC4kJQjt3LPexuAKPHIqhIRUu3g1boF6pp9ubONQKBgQC7jcBRq9VlJRoxKDYVlNwq+Z/s2Segi4ip4oqky5y36jObg+50qGjrQ2DQnXnnfU1UaBVBnz4xKqgzPx7CSj90Rxjd4LwOtMKot2YwwwWuGRw1GRC7f11d0hJjJEv2Px7Uv6Y4Fb/uTDsC8wUnJddT5rXedYY93RImTTBuTK7apwKBgBCWY8N91FXZueYJxmyGsR+HhQ/noqLBY1G2zRIS7TBkVYTHq5LWVYx5cM4N2Vq9pJ8i1TCp7CvpRTBPz0OrHusdlUX6S9VQ0Ir/eU8ymaxzh4xm/Fw5W56N/4ZVqkJVKvkywdlcHOP47tA352CEv2TIBeF1uXPSPUyjzf8IURzNAoGAQyKLe/CZUzpDtOLi8Ti20r9GCr70m1g3FDCeVjz4HTE9Reak3adG3yfgKOylrUJCAvu62CGLXogZazal6QAw1It6kDmYt17m6wOzFbNJfjdIzIzrW2JM5n+Cy3A7rKqNe7QaaKsIZ+FrojOhXZEDEepcoYPKCKzwiTtKIcFbObMCgYBEzPX1r/JxDS9Qc+AxfE2v91WvzRQD9tWY0Zd1AYgKZiIXvc63vL9Cnsq4K7YM/YiXQv8lCq7R1CzlwWRC3yLnO3SWTMTORX8C0SX8Sy4BJ1e3veDFYlhfxcR4JmWEGSbnZcZBxAnUln/zUFmOthv02cDTSfOVRCFblRtnAVbhwA==";
    /**
     * RSA算法公钥
     */
    private static final String RSA_PUBLIC_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjxBuc2QBKkmrBoBK9Yh3bpwSx4qdPBhnLfRACM8PMwauc1MKV9ClyzSSdXfgIHEFgGJNAa7F/ZaUdn36HSXItqoKNd5F3Pe8TEWxQUrGeMJ6CtC/3EsvT+jcXaSfhboSmOjJ8DhQKYn+5iWPCOBbpk4Towvu7N5FtYTHLWxXNGMhHN7DQ/btNNaIl/ZQxIk2HeDrqtiCgxW3tpd/Cgj4E6ZKQ+Dl1BZt7rYC5bKxqLLVPQ0F2pYPXPCyy0YwUKB8ngsjl88FgRrGdoaALyTSiJTseuhirunmxfxFd2NgbHYw0BdMwkP3N8Qs+27L9gRnLgN1bmALVJgZqEg+gC+mkwIDAQAB";


    /**
     * 密钥长度
     */
    private static final int KEY_SIZE = 2048;

    /**
     * RSA最大加密明文大小 KEY_SIZE/8-11
     */
    private static final int MAX_ENCRYPT_BLOCK = 245;

    /**
     * RSA最大解密密文大小 KEY_SIZE/8
     */
    private static final int MAX_DECRYPT_BLOCK = 256;

    /**
     * 生成公钥，私钥
     * @return 返回值
     * @throws NoSuchAlgorithmException 抛出异常
     */
    public static HashMap<String,String> generatePublicPrivateKeys() throws NoSuchAlgorithmException {
        HashMap<String,String> keys=new HashMap<>();
        // KeyPairGenerator:秘钥对生成器对象
        KeyPairGenerator keyPairGenerator=KeyPairGenerator.getInstance(ALGORITHMS);
        // 生成密钥对
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        // 生成私钥
        PrivateKey privateKey = keyPair.getPrivate();
        // 生成公钥
        PublicKey publicKey = keyPair.getPublic();
        // 获取私销的字节数组
        byte[] privateKeyEncoded=privateKey.getEncoded();
        // 获取公销字节数组
        byte[] publicKeyEncoded = publicKey.getEncoded();
        // 使用base64进行编码
        String privateEncodeString = Base64.encodeBase64String(privateKeyEncoded);
        String publicEncodeString = Base64.encodeBase64String(publicKeyEncoded);
        // 打印公钥和私钥
        System.out.println("privateEncodeString="+privateEncodeString);
        System.out.println("publicEncodeString="+publicEncodeString);
        keys.put("privateKey",privateEncodeString);
        keys.put("publicKey",publicEncodeString);
        return keys;
    }

    /**
     * 分段加密
     * @param content 未加密的字符串
     * @param publicKeyEncoded 公钥
     * @return 加密后的字符串
     * @throws Exception 异常
     */
    public static String encrypt(String content, byte[] publicKeyEncoded) throws Exception {
        // 创建key的工厂
        KeyFactory keyFactory=KeyFactory.getInstance(ALGORITHMS);
        // 创建 已编码的公钥规格
        X509EncodedKeySpec encPubKeySpec = new X509EncodedKeySpec(publicKeyEncoded);
        // 获取指定算法的密钥工厂, 根据 已编码的公钥规格, 生成公钥对象
        PublicKey publicKey = keyFactory.generatePublic(encPubKeySpec);
        // 获取指定算法的密码器
        Cipher cipher = Cipher.getInstance(ALGORITHMS);
        // 初始化密码器（公钥加密模型）
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] dataBytes = content.getBytes(StandardCharsets.UTF_8);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        int dataLength = dataBytes.length;
        int offSet = 0;
        byte[] cache;

        while (dataLength - offSet > 0) {
            if (dataLength - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(dataBytes, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(dataBytes, offSet, dataLength - offSet);
            }
            output.write(cache, 0, cache.length);

            offSet += MAX_ENCRYPT_BLOCK;
        }
        output.close();
        // 采用base64算法进行转码,避免出现中文乱码
        return Base64.encodeBase64String(output.toByteArray());
    }

    /**
     * 分段解密
     * @param encodeEncryptString 未解密的字符串
     * @param privateKeyEncoded 私钥
     * @return 解密后的字符串
     * @throws Exception 异常
     */
    public static String decrypt(String encodeEncryptString, byte[] privateKeyEncoded) throws Exception {
        // 创建key的工厂
        KeyFactory keyFactory=KeyFactory.getInstance(ALGORITHMS);
        // 创建 已编码的私钥规格
        PKCS8EncodedKeySpec encPriKeySpec = new PKCS8EncodedKeySpec(privateKeyEncoded);
        // 获取指定算法的密钥工厂, 根据 已编码的私钥规格, 生成私钥对象
        PrivateKey privateKey = keyFactory.generatePrivate(encPriKeySpec);
        // 3.解码Base64字符串，转换为字节数组
        byte[] dataBytes = Base64.decodeBase64(encodeEncryptString);
        // 获取指定算法的密码器
        Cipher cipher = Cipher.getInstance(ALGORITHMS);
        // 初始化密码器（私钥解密模型）
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        int dataLength = dataBytes.length;
        int offSet = 0;
        byte[] cache;

        while (dataLength - offSet > 0) {
            if (dataLength - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(dataBytes, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(dataBytes, offSet, dataLength - offSet);
            }
            output.write(cache, 0, cache.length);

            offSet += MAX_DECRYPT_BLOCK;
        }
        output.close();
        // 返回解密好的原始数据
        return output.toString();
    }

    public static void main(String[] args) throws Exception {
        //------------------------RSA算法加密
        //要加密的内容
        String conent="减小数据长度。对加密后的数据不要直接转String，而是转16进制字符串。对于数据小，并且超长不多，且所有数据长度固定，可以通过该方式处理。如果数据长度不可控该方法也是不可行的。";
        String encrypt = RsaEncryptUtil.encrypt(conent, Base64.decodeBase64(RSA_PUBLIC_KEY));

        //------------------------RSA算法解密
        String decrypt = RsaEncryptUtil.decrypt(encrypt, Base64.decodeBase64(RSA_PRIVATE_KEY));

        System.out.println("-----------------加密------------------");
        System.out.println(encrypt);
        System.out.println("-----------------解密------------------");
        System.out.println(decrypt);
    }

}
