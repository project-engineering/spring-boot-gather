package com.springboot.futool.image;

import lombok.extern.log4j.Log4j2;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * image工具类
 */
@Log4j2
public class ImageUtil {

    /**
     * 获取图片
     * @param imagePath 图片路径
     * @return 图片字节数组
     */
    public static byte[] getImage(String imagePath){
        File file = new File(imagePath);
        try (FileInputStream fis = new FileInputStream(file);
             ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            byte[] buf = new byte[1024];
            int readNum;
            while ((readNum = fis.read(buf)) != -1) {
                // 将缓冲区的数据写入到输出流中
                bos.write(buf, 0, readNum);
            }
            return bos.toByteArray();
        } catch (IOException e) {
            log.error("获取图片失败,原因: {}" , e.getMessage());
            throw new RuntimeException("获取图片失败,原因："+e.getMessage());
        }
    }

    public static void main(String[] args) {
        String imagePath = "path/to/your/image.jpg";
        byte[] imageBytes = getImage(imagePath);
        System.out.println("Image converted to byte array successfully. Length: " + imageBytes.length);
    }
}