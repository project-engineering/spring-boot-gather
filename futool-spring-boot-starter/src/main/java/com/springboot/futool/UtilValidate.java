package com.springboot.futool;

import com.springboot.futool.lang.IsEmpty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 工具类-验证工具类
 * @author liuc
 * @date 2021/8/24 11:05
 * @since JDK1.8
 * @version V1.0
 */
@Slf4j
public class UtilValidate {
    /***
     * 判断对象是否为空
     * @param o 对象
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08
     */
    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof String) {
            return ((String) o).isEmpty();
        }
        if (o instanceof Collection) {
            return ((Collection<?>) o).isEmpty();
        }
        if (o instanceof Map) {
            return ((Map<?, ?>) o).isEmpty();
        }
        if (o instanceof Object[]) {
            if (((Object[]) o).length == 0) {
                return true;
            }
        }
        if (o instanceof int[]) {
            if (((int[]) o).length == 0) {
                return true;
            }
        }
        if (o instanceof long[]) {
            if (((long[]) o).length == 0) {
                return true;
            }
        }
        if (o instanceof CharSequence) {
            return ((CharSequence) o).isEmpty();
        }
        if (o instanceof IsEmpty) {
            return ((IsEmpty) o).isEmpty();
        }
        if (o instanceof Boolean) {
            return false;
        }
        if (o instanceof Number) {
            return false;
        }
        if (o instanceof Character) {
            return false;
        }
        if (o instanceof Date) {
            return false;
        }
        return false;
    }

    /***
     * 判断对象是否不为空
     * @param o 对象
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08
     */
    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }

    /***
     * 比较两个对象是否相等
     * @param obj 对象
     * @param obj2 对象
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08
     */
    public static boolean areEqual(Object obj, Object obj2){
        if (obj == null){
            return obj2 == null;
        }else {
            return obj.equals(obj2);
        }
    }

    public static void notNull(@Nullable Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }
}
