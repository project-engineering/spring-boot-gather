package com.springboot.futool.transfer;

import cn.hutool.core.date.TemporalAccessorUtil;
import cn.hutool.core.util.ObjectUtil;
import com.jcraft.jsch.*;
import lombok.extern.log4j.Log4j2;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * sftp工具类
 */
@Log4j2
public class SftpUtil {
    private ChannelSftp channelSftp;

    private Session session;
    /**
     * sftp用户名
     */
    private final String username;
    /**
     * sftp密码
     */
    private final String password;
    /**
     * sftp主机ip
     */
    private final String host;
    /**
     * sftp主机端口
     */
    private final int port;
    /**
     * 超时时间 毫秒
     */
    private final int timeout;
    /** 私钥 */
    private final String privateKey;
    /**
     * 密钥的密码
     */
    private final String passphrase;


    /**
     * 构造器
     * @param builder Builder对象
     */
    private SftpUtil (Builder builder){
        this.username = builder.username;
        this.password = builder.password;
        this.host = builder.host;
        this.port = builder.port;
        this.timeout = builder.timeout;
        this.privateKey = builder.privateKey;
        this.passphrase = builder.passphrase;
        this.connect();
    }

    public static class Builder {
        private String host;
        private int port;
        private String username;
        private String password;
        private int timeout;
        private String privateKey;
        private String passphrase;

        public Builder host(String host) {
            this.host = host;
            return this;
        }

        public Builder port(int port) {
            this.port = port;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder timeout(int timeout) {
            this.timeout = timeout;
            return this;
        }

        public Builder privateKey(String privateKey) {
            this.privateKey = privateKey;
            return this;
        }

        public Builder passphrase(String passphrase) {
            this.passphrase = passphrase;
            return this;
        }

        public SftpUtil build() {
            return new SftpUtil(this);
        }
    }

    /**
     * 连接sftp服务器
     */
    public synchronized void connect() {
        log.info("开始连接sftp服务器");
        log.info("--------------------------------------");
        log.info("|         host: "+host+"        |");
        log.info("|         port: "+port+"                   |");
        log.info("|     username: "+username+"               |");
        log.info("|     password: "+password+"               |");
        log.info("--------------------------------------");
        // 检查配置信息
        checkConfig();
        try {
            JSch jsch = new JSch();
            if (ObjectUtil.isNotEmpty(privateKey)) {
                log.info("开始设置私钥{}",privateKey);
                // 设置私钥
                if (ObjectUtil.isNotEmpty(passphrase)) {
                    jsch.addIdentity(privateKey, passphrase);
                } else {
                    jsch.addIdentity(privateKey);
                }
            }
            session = jsch.getSession(username, host, port);
            if (password != null) {
                session.setPassword(password);
            }
            Properties config = new Properties();
            //  设置为后台运行
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            // 设置timeout时间
            session.setTimeout(timeout);
            // 建立连接
            try {
                session.connect();
            } catch (Exception e) {
                if (session.isConnected())
                    session.disconnect();
                throw new Exception("连接服务器失败,请检查主机[" + host + "],端口[" + port + "],用户名[" + username + "],密码[" + password + "]是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
            }
            // 打开sftp通道
            Channel channel = session.openChannel("sftp");
            this.channelSftp = (ChannelSftp) channel;
            // 建立sftp连接
            try {
                this.channelSftp.connect();
            } catch (Exception e) {
                if (this.channelSftp.isConnected())
                    this.channelSftp.disconnect();
                throw new Exception("连接服务器失败,请检查主机[" + host + "],端口[" + port + "],用户名[" + username + "],密码[" + password + "]是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
            }
            //设置文件名编码
            this.channelSftp.setFilenameEncoding("UTF-8");
            log.info("sftp服务器连接成功");
        } catch (Exception e) {
            log.error("sftp服务器连接失败,原因：{}",e.getMessage());
            throw new RuntimeException("sftp服务器连接失败,原因：{}",e);
        }
    }

    public void checkConfig() {
        if (ObjectUtil.isEmpty(host)) {
            throw new IllegalArgumentException("host不能为空");
        }
        if (ObjectUtil.isEmpty(port)) {
            throw new IllegalArgumentException("port不能为空");
        }
        if (ObjectUtil.isEmpty(username)) {
            throw new IllegalArgumentException("username不能为空");
        }
        if (ObjectUtil.isEmpty(password)) {
            throw new IllegalArgumentException("password不能为空");
        }
    }

    /**
     * 上传单个文件
     * <br/>
     * <br/>
     * 将本地文件上传到sftp指定路径下
     *
     * @param localPath 本地路径下的文件 例如：C:\\local\\file.txt
     * @param localFileName 本地路径下的文件 例如：file.txt
     * @param remotePath 上传到该目录 上传的文件存放到sftp服务器的路径 例如：/home/remote/
     * @param remoteFileName 保存在sftp服务器上的文件名 例如：file1.txt
     * @return 上传成功返回true，否则返回false
     */
    public boolean uploadFile(String localPath,String localFileName,String remotePath, String remoteFileName) {
        return uploadFile(localPath,localFileName,remotePath,remoteFileName,true);
    }

    /**
     * 上传单个文件
     * <br/>
     * <br/>
     * 将本地文件上传到sftp指定路径下
     *
     * @param localPath 本地路径下的文件 例如：C:\\local\\file.txt
     * @param localFileName 本地路径下的文件 例如：file.txt
     * @param remotePath 上传到该目录 上传的文件存放到sftp服务器的路径 例如：/home/remote/
     * @param remoteFileName 保存在sftp服务器上的文件名 例如：file1.txt
     * @param closeFlag 是否关闭连接，默认关闭连接
     * @return 上传成功返回true，否则返回false
     */
    public boolean uploadFile(String localPath,String localFileName,String remotePath, String remoteFileName,boolean closeFlag) {
        try {
            try {
                this.isOpenConn();
            } catch (Exception e) {
                log.error("sftp连接异常,{}",e.getMessage());
                return false;
            }
            if (ObjectUtil.isEmpty(localPath)) {
                throw new IllegalArgumentException("localFile不能为空");
            }
            if (ObjectUtil.isEmpty(remotePath)) {
                throw new IllegalArgumentException("remotePath不能为空");
            }
            if (ObjectUtil.isEmpty(remoteFileName)) {
                throw new IllegalArgumentException("fileName不能为空");
            }
            if (ObjectUtil.isEmpty(closeFlag)) {
                closeFlag = true;
            }
            localPath = addTrailingSlash(localPath);
            File file = new File(localPath+localFileName);
            InputStream input ;
            try {
                input = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            String[] folds = remotePath.split("/");
            for(String fold : folds){
                if(!fold.isEmpty()){
                    try {
                        this.channelSftp.cd(fold);
                    }catch (Exception e){
                        log.info("目录"+fold+",不存在开始创建目录");
                        this.channelSftp.mkdir(fold);
                        this.channelSftp.cd(fold);
                    }
                }
            }
            // 切换到目标路径
            String path = this.channelSftp.pwd();
            // 获取目标路径中的文件列表
            Vector<ChannelSftp.LsEntry> fileList = channelSftp.ls(path);

            // 判断目标文件是否存在
            boolean fileExists = false;
            for (ChannelSftp.LsEntry entry : fileList) {
                if (entry.getFilename().equals(remoteFileName)) {
                    fileExists = true;
                    break;
                }
            }
            // 如果文件已存在，则将文件名重命名
            if (fileExists) {
                String[] fileNames = remoteFileName.split("\\.");
                // 获取当前时间
                String currentTime = TemporalAccessorUtil.format(LocalDateTime.now(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                remoteFileName = fileNames[0] + "_" + currentTime+"."+fileNames[1];
            }
            //上传
            channelSftp.put(input,remoteFileName);
            log.info("文件上传成功,{}",this.channelSftp.pwd()+"/"+remoteFileName);
            try {
                if (closeFlag) {
                    disconnect();
                }
            } catch (Exception e) {
                log.error("关闭连接失败,{}",e.getMessage());
                return false;
            }
        }catch (SftpException e) {
            log.error("上传文件失败,{}",e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 批量上传文件
     * @param localPath  本地文件路径
     * @param remotePath  远程文件路径
     * @return 上传成功返回true，否则返回false
     */
    public boolean batchUploadFile(String localPath, String remotePath) {
        log.info("批量上传文件,本地文件路径：{},远程文件路径：{}", localPath, remotePath);
        try {
            this.isOpenConn();
        } catch (Exception e) {
            log.error("sftp连接异常,{}", e.getMessage());
            return false;
        }
        boolean uploadFlag = recursiveUploadFile(localPath,remotePath);
        if (uploadFlag) {
           log.info("批量上传文件成功");
           disconnect();
        } else {
            log.info("批量上传文件失败");
        }
        return uploadFlag;
    }

    /**
     * 递归上传文件
     * @param localPath  本地文件路径
     * @param remotePath  远程文件路径
     * @return 上传成功返回true，否则返回false
     */
    private boolean recursiveUploadFile(String localPath, String remotePath) {
        try {
            File localDir = new File(localPath);
            if (localDir.isDirectory()) {
                try {
                    channelSftp.stat(remotePath);
                } catch (SftpException e) {
                    // 目录不存在，进行创建
                    channelSftp.mkdir(remotePath);
                }

                // 进入远程目录
                channelSftp.cd(remotePath);

                // 遍历本地目录并递归上传文件
                File[] files = localDir.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isFile()) {
                            // 上传文件
                            channelSftp.put(file.getAbsolutePath(), file.getName());
                        } else if (file.isDirectory()) {
                            // 递归上传子目录，传递远程子目录路径
                            recursiveUploadFile(file.getAbsolutePath(), remotePath + "/" + file.getName());
                        }
                    }
                }
                // 返回上级目录
                channelSftp.cd("..");
            }
        } catch (SftpException e) {
            log.error("上传文件失败", e);
            return false;
        }
        return true;
    }

    /**
     * 关闭连接
     */
    public synchronized void disconnect() {
        if (this.channelSftp != null) {
            if (this.channelSftp.isConnected()) {
                this.channelSftp.disconnect();
            }
        }
        if (this.session != null) {
            if (this.session.isConnected()) {
                this.session.disconnect();
            }
        }
        log.info("关闭连接成功");
    }

    /**
     * 检查是否打开连接
     */
    private void isOpenConn() {
        if (!this.session.isConnected()) {
            throw new IllegalArgumentException("Sftp Session未连接，请检查!");
        }
        if (!this.channelSftp.isConnected()) {
            throw new IllegalArgumentException("Sftp ChannelSftp未连接，请检查!");
        }
    }

    /**
     * 列出目录下的文件
     *
     * @param directory 要列出的目录
     * @return 目录下的文件列表
     * @throws Exception 异常
     */
    public Vector<?> listFiles(String directory) throws Exception {
        try {
            this.isOpenConn();
        } catch (Exception e) {
            throw new Exception("sftp连接异常,{}",e);
        }
        return this.channelSftp.ls(directory);
    }

    /**
     * 批量下载文件
     *
     * @param remotePath    远程文件存放路径
     * @param localPath     下载到本地的路径
     * @return 下载后的文件名集合
     */
    public List<String> batchDownLoadFile(String remotePath, String localPath){
        if (ObjectUtil.isEmpty(remotePath) || ObjectUtil.isEmpty(localPath)){
            log.error("远程服务器文件路径、下载到本地的路径不能为空");
            throw new IllegalArgumentException("远程服务器文件路径、下载到本地的路径不能为空");
        }
        try {
            this.isOpenConn();
        } catch (Exception e) {
            log.error("sftp连接异常,{}",e.getMessage());
        }
        //递归下载
        List<String> list =  recursiveDownload(remotePath, localPath);
        //关闭连接
        this.disconnect();
        return list;
    }

    /**
     * 递归下载
     * @param remotePath  远程文件路径
     * @param localPath   本地文件路径
     * @return 下载后的文件名集合
     */
    private List<String> recursiveDownload(String remotePath, String localPath) {
        List<String> downloadFiles = new ArrayList<>();

        try {
            remotePath = addTrailingSlash(remotePath);
            log.info("切换到指定目录：" + remotePath);
            boolean flag = openDir(remotePath, this.channelSftp);
            if (flag) {
                Vector<ChannelSftp.LsEntry> vv = (Vector<ChannelSftp.LsEntry>) listFiles("*");
                if (vv == null || vv.isEmpty()) {
                    log.error("remotePath：" + remotePath + "路径下，未匹配到文件！");
                    return downloadFiles;
                }

                // 检查本地文件夹是否存在，不存在则创建
                localPath = addTrailingSlash(localPath);
                Path localDirPath = Paths.get(localPath);
                if (!Files.exists(localDirPath)) {
                    log.info("本地文件夹不存在，正在创建：" + localPath);
                    Files.createDirectories(localDirPath);
                }

                for (ChannelSftp.LsEntry entry : vv) {
                    String remoteFileName = entry.getFilename();
                    log.info("校验文件名：" + remoteFileName);

                    String path = localPath.endsWith(File.separator) ? localPath : localPath + File.separator;
                    File file = new File(path + remoteFileName);
                    log.info("保存校对文件的本地路径为:" + file.getAbsolutePath());

                    if (entry.getAttrs().isDir()) {
                        // 如果是目录，则递归下载
                        String newRemotePath = remotePath + remoteFileName;
                        String newLocalPath = localPath + remoteFileName;
                        downloadFiles.addAll(recursiveDownload(newRemotePath, newLocalPath));
                    } else {
                        log.info("开始下载 " + remoteFileName + " ~~");
                        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                            this.channelSftp.get(remoteFileName, fileOutputStream);
                            log.info("下载完成 ~~");
                            downloadFiles.add(remoteFileName);
                        } catch (IOException e) {
                            log.error("下载文件时出现异常：" + e.getMessage());
                        }
                    }
                }
            } else {
                log.info("对应的目录" + remotePath + "不存在！");
            }
        } catch (Exception e) {
            log.error("下载文件时出现异常：" + e.getMessage(), e);
        }
        return downloadFiles;
    }

    /**
     * 打开或者进入指定目录
     * @param directory 目录路径
     * @param sftp sftp通道
     * @return true 打开成功，false 打开失败
     */
    public boolean openDir(String directory, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            log.info("cd " + directory + " ok");
            return true;
        } catch (SftpException e) {
            log.error(e + "");
            return false;
        }
    }

    /**
     * 下载单个文件
     * <br/>
     * <br/>
     * 从sftp上下载文件到指定目录
     *
     * @param remotePath     远程服务器文件路径：/test/
     * @param remoteFileName sftp服务器文件名：test.txt
     * @param localFilePath  下载到本地的文件路径：D:/test/
     * @return flag 下载是否成功， true成功，false下载失败
     */
    public boolean downloadFile(String remotePath, String remoteFileName, String localFilePath) {
        if (ObjectUtil.isEmpty(remotePath)
                || ObjectUtil.isEmpty(remoteFileName)
                || ObjectUtil.isEmpty(localFilePath)){
            log.error("远程服务器文件路径、文件名、本地文件路径不能为空");
            return false;
        }
        try {
            this.isOpenConn();
        } catch (Exception e) {
            log.error("SFTP连接异常: {}", e.getMessage());
            return false;
        }

        try {
            remotePath = addTrailingSlash(remotePath);
            this.channelSftp.cd(remotePath);
            log.info("成功切换到远程目录: {}", this.channelSftp.pwd());

            ChannelSftp.LsEntry remoteFile = findRemoteFile(remotePath, remoteFileName);
            if (remoteFile == null) {
                log.info("远程文件不存在");
                disconnect();
                return false;
            }

            localFilePath = addTrailingSlash(localFilePath);
            Path localPath = Paths.get(localFilePath);
            createLocalDir(localPath);

            Path localFile = localPath.resolve(remoteFileName);

            try (InputStream input = this.channelSftp.get(remoteFileName);
                 OutputStream output = new FileOutputStream(localFile.toFile())) {
                log.info("下载远程文件 [{}] 到本地路径: {}", remoteFileName, localFilePath);
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = input.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
                log.info("下载远程文件 [{}] 成功", remoteFileName);
                return true;
            } catch (IOException e) {
                log.error("文件操作异常: {}", e.getMessage());
            }
        } catch (SftpException e) {
            log.error("SFTP操作异常: {}", e.getMessage());
        } finally {
            this.disconnect();
        }
        return false;
    }

    /**
     * 下载单个文件
     * <br/>
     * <br/>
     * 从sftp上下载文件到指定目录
     *
     * @param remotePath     远程服务器文件路径：/test/
     * @param remoteFileName sftp服务器文件名：test.txt
     * @param localFilePath  下载到本地的文件路径：D:/test/
     * @param localFileName  下载到本地之后文件的名称：test.txt
     * @return flag 下载是否成功， true成功，false下载失败
     */
    public boolean downloadFile(String remotePath, String remoteFileName, String localFilePath,String localFileName) {
        if (ObjectUtil.isEmpty(remotePath)
                || ObjectUtil.isEmpty(remoteFileName)
                || ObjectUtil.isEmpty(localFilePath)){
            log.error("下载远程文件失败，参数不能为空");
            return false;
        }
        try {
            this.isOpenConn();
        } catch (Exception e) {
            log.error("SFTP连接异常: {}", e.getMessage());
            return false;
        }
        FileOutputStream fieloutput = null;
        try {
            remotePath = addTrailingSlash(remotePath);
            this.channelSftp.cd(remotePath);
            log.info("成功切换到远程目录: {}", this.channelSftp.pwd());

            ChannelSftp.LsEntry remoteFile = findRemoteFile(remotePath, remoteFileName);
            if (remoteFile == null) {
                log.info("远程文件不存在");
                disconnect();
                return false;
            }
            localFilePath = addTrailingSlash(localFilePath);
            if (ObjectUtil.isEmpty(localFileName)) {
                localFileName = remoteFileName;
            }
            File file = new File(localFilePath+ localFileName);
            try {
                fieloutput = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                log.error("文件输出流异常: {}", e.getMessage());
                throw new RuntimeException(e);
            }
            this.channelSftp.get(remotePath + remoteFileName, fieloutput);
            log.info("文件下载成功");
        } catch (SftpException e) {
            log.error("SFTP操作异常: {}", e.getMessage());
        }  finally {
            this.disconnect();
        }
        return true;
    }

    /**
     * 查找远程目录下的文件
     * @param remotePath 远程目录
     * @param remoteFileName 远程文件名
     * @return 远程文件
     * @throws SftpException 异常
     */
    private ChannelSftp.LsEntry findRemoteFile(String remotePath, String remoteFileName) throws SftpException {
        Vector<ChannelSftp.LsEntry> files = channelSftp.ls(remotePath);
        for (ChannelSftp.LsEntry file : files) {
            if (file.getFilename().equals(remoteFileName)) {
                return file;
            }
        }
        return null;
    }

    /**
     * 创建本地目录
     * @param localPath 本地目录
     */
    private void createLocalDir(Path localPath) {
        if (!Files.exists(localPath)) {
            try {
                Files.createDirectories(localPath);
                log.info("创建本地目录: {}", localPath);
            } catch (IOException e) {
                log.error("创建本地目录失败: {}", e.getMessage());
            }
        }
    }

    /**
     * 判断sftp服务器上的目录是否存在
     * @param remoteDir 远程目录
     * @return true 存在，false 不存在
     */
    private boolean isDirExists(String remoteDir) {
        try {
            SftpATTRS attrs = this.channelSftp.lstat(remoteDir);
            return attrs.isDir();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 创建sftp服务器文件夹
     * @param dirPath 要创建的目录路径
     */
    private void createRemoteDir(String dirPath){
        try {
            String[] folders = dirPath.split("/");
            for (String folder : folders) {
                if (!folder.isEmpty()) {
                    try {
                        this.channelSftp.cd(folder);
                    } catch (Exception e) {
                        this.channelSftp.mkdir(folder);
                        this.channelSftp.cd(folder);
                    }
                }
            }
        } catch (Exception e){
            log.error("创建远程目录失败: {}", e.getMessage());
            throw new RuntimeException("创建远程目录失败,原因：{}",e);
        }
    }

    /**
     * 删除文件
     *
     * @param remotePath  要删除文件所在目录
     * @param deleteFile 要删除的文件
     */
    public void delete(String remotePath, String deleteFile) {
        if (ObjectUtil.isEmpty(remotePath)
                ||ObjectUtil.isEmpty(deleteFile)) {
            log.error("远程路径或删除文件不能为空");
            throw new RuntimeException("远程路径或删除文件不能为空");
        }
        try {
            this.isOpenConn();
        } catch (Exception e) {
            log.error("sftp连接异常,{}",e.getMessage());
            throw new RuntimeException("sftp连接异常,原因：{}",e);
        }
        try {
            log.info("开始删除文件！");
            this.channelSftp.cd(remotePath);
            this.channelSftp.rm(deleteFile);
        } catch (SftpException e) {
            log.error("删除文件失败！");
            throw new RuntimeException("删除文件失败,原因：{}", e);
        } finally {
            this.disconnect();
            log.info("删除文件成功！");
        }
    }

    /**
     * 检测路径是否以"/"结尾
     *      如果不是以"/"结尾，则自动添加"/"；
     *      如果是以"/"结尾，则不做任何处理。
     *
     * @param path 路径
     * @return 处理后的路径
     */
    public static String addTrailingSlash(String path) {
        if (!path.endsWith("/")) {
            path = path + "/";
        }
        return path;
    }
}
