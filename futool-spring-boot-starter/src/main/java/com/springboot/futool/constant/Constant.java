package com.springboot.futool.constant;

/**
 * @description 常量类
 * @author liuc
 * @date 2021/8/24 14:58
 * @since JDK1.8
 * @version V1.0
 */
public class Constant {
    /**
     * 逗号
     */
    public static final String COMMA = ",";
    /**
     * 句号
     */
    public static final String PERIOD = ".";
    /**
     * 竖杠
     */
    public static final String VERTICAL_BAR = "|";
    /**
     * 空字符串
     */
    public static final String NULLSTR = "";
    /**
     * 分号
     */
    public static final char SEMICOLON = ';';
    /**
     * 下划线
     */
    public static final String UNDERLINE = "_";
    /**
     * 等于号
     */
    public static final String EQUAL = "=";

    /** 字符常量：空格符 ' ' */

    public static final char SPACE = ' ';

    /** 字符常量：制表符 \t */

    public static final char TAB = ' ';

    /** 字符常量：点 . */

    public static final char DOT = '.';

    /** 字符常量：斜杠 / */

    public static final char SLASH = '/';

    /** 字符常量：反斜杠 \ */

    public static final char BACKSLASH = '\\';

    /** 字符常量：回车符 \r */

    public static final char CR = '\r';

    /** 字符常量：换行符 \n */

    public static final char LF = '\n';

    /** 字符常量：连接符 - */

    public static final char DASHED = '-';

    /** 字符常量：花括号(左) { */

    public static final char DELIM_START = '{';

    /** 字符常量：花括号(右) } */

    public static final char DELIM_END = '}';

    /** 字符常量：中括号(左) [ */

    public static final char BRACKET_START = '[';

    /** 字符常量：中括号(右) ] */

    public static final char BRACKET_END = ']';

    /** 字符常量：双引号 : */

    public static final char DOUBLE_QUOTES = '"';

    /** 字符常量：单引号 ' */

    public static final char SINGLE_QUOTE = '\'';

    /** 字符常量：与 & */

    public static final char AMP = '&';

    /** 字符常量：冒号 : */

    public static final char COLON = ':';

    /** 字符常量：艾特 @ */

    public static final char AT = '@';

    public static final String IS_NUMERIC = "[0-9]*";

    /**
     * 列表默认页码: 1
     */
    public static final int COMMON_PAGE_NUM = 1;

    /**
     * 列表默认显示数: 10
     */
    public static final int COMMON_PAGE_SIZE = 10;
    /**
     * 字符集
     */
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String CHARSET_GBK= "GBK";
}
