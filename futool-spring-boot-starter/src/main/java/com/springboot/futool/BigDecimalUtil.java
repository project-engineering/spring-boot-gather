package com.springboot.futool;

import com.springboot.futool.regex.RegexUtil;
import lombok.extern.slf4j.Slf4j;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;

/**
 * 用于高精确处理常用的数学运算
 * 由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精确的浮点数运算，包括加减乘除和四舍五入及类型转换。
 * @author liuc
 * @date 2021/8/24 10:55
 */
@Slf4j
public class BigDecimalUtil {

    /**
     * 提供精确的加法运算
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */
    public static BigDecimal add(Object v1, Object v2) {
        return add(v1, v2, 2);
    }

    /**
     * 提供精确的加法运算
     *
     * @param v1    被加数
     * @param v2    加数
     * @param scale 保留scale 位小数
     * @return 两个参数的和
     */
    public static BigDecimal add(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        if (UtilValidate.isEmpty(v1)) {
            v1 = BigDecimal.ZERO;
        }
        if (UtilValidate.isEmpty(v2)) {
            v2 = BigDecimal.ZERO;
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.add(b2).setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */
    public static BigDecimal sub(Object v1, Object v2) {
        return sub(v1, v2, 2);
    }

    /**
     * 提供精确的减法运算
     *
     * @param v1    被减数
     * @param v2    减数
     * @param scale 保留scale 位小数
     * @return 两个参数的差
     */
    public static BigDecimal sub(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        if (UtilValidate.isEmpty(v1)) {
            v1 = BigDecimal.ZERO;
        }
        if (UtilValidate.isEmpty(v2)) {
            v2 = BigDecimal.ZERO;
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.subtract(b2).setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @return 两个参数的积
     */
    public static BigDecimal mul(Object v1, Object v2) {
        return mul(v1, v2, 2);
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1    被乘数
     * @param v2    乘数
     * @param scale 保留scale 位小数
     * @return 两个参数的积
     */
    public static BigDecimal mul(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        if (UtilValidate.isEmpty(v1)) {
            v1 = BigDecimal.ZERO;
        }
        if (UtilValidate.isEmpty(v2)) {
            v2 = BigDecimal.ZERO;
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.multiply(b2).setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 提供精确的除法运算
     *
     * @param v1 被除数
     * @param v2 除数
     * @return 两个参数的商
     */
    public static BigDecimal div(Object v1, Object v2) {
        return div(v1, v2, 2);
    }

    /**
     * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
     * 定精度，以后的数字四舍五入
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 表示需要精确到小数点以后几位
     * @return 两个参数的商
     */
    public static BigDecimal div(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (b2.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalArgumentException("Divider cannot be equal to zero");
        }
        if (b2.abs().compareTo(new BigDecimal("1e-" + scale)) < 0) {
            throw new IllegalArgumentException("Divider is too close to zero");
        }
        return b1.divide(b2, scale, RoundingMode.HALF_UP);
    }

    /**
     * 提供精确的小数位四舍五入处理
     *
     * @param v     需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static BigDecimal round(Object v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = objToBigDecimal(v);
        return b.setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 取余数
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 小数点后保留几位
     * @return 余数
     */
    public static BigDecimal remainderStr(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (BigDecimal.ZERO.compareTo(b2)== 0) {
            throw new IllegalArgumentException(
                    "Divider cannot be equal to zero");
        }
        return b1.remainder(b2).setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 取余数  BigDecimal
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 小数点后保留几位
     * @return 余数
     */
    public static BigDecimal remainderBigDecimal(Object v1, Object v2, int scale) {
        return remainderStr(v1, v2, scale);
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于v2 则 返回true 否则false
     */
    public static boolean compareGreaterThan(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        int bj = b1.compareTo(b2);
        return  bj > 0;
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于v2 则 返回true 否则false
     */
    public static int compare(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.compareTo(b2);
    }

    /**
     * Object转BigDecimal类型
     *
     * @param value 要转换的参数
     * @return BigDecimal 转换后输出的参数
     */
    public static BigDecimal objToBigDecimal(Object value) {
        BigDecimal ret;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof Integer) {
                ret = BigDecimal.valueOf((Integer) value);
            } else if (value instanceof Double) {
                ret = BigDecimal.valueOf((Double) value);
            } else if (value instanceof Float) {
                ret = new BigDecimal(Float.toString((Float) value));
            } else if (value instanceof String) {
                String stringValue = ((String) value).trim();
                if (RegexUtil.isNumeric(stringValue)) {
                    ret = new BigDecimal(stringValue);
                } else {
                    ret = null;
                }
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = BigDecimal.valueOf(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass()
                        + " into a BigDecimal.");
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * Object转为BigDecimal类型并四舍五入保留N位小数
     *
     * @param obj 要转换的参数
     * @param scale 保留的小数位数
     * @return BigDecimal 转换后输出的参数
     */
    public static BigDecimal objToBigDecimalScale(Object obj,int scale) {
        BigDecimal bd = BigDecimal.ZERO;
        if (UtilValidate.isNotEmpty(obj)) {
            bd = objToBigDecimal(obj).setScale(scale, RoundingMode.HALF_UP);
        }
        return bd;
    }

    /**
     * BigDecimal转换为String类型并避免输出科学计数法
     *
     * @param bd 要转换的参数
     * @return String 转换后输出的参数
     */
    public static String bigDecimalToStr(BigDecimal bd) {
        String str = null;
        if (UtilValidate.isEmpty(bd)) {
            return null;
        }
        if (UtilValidate.isNotEmpty(bd)) {
            str = bd.stripTrailingZeros().toPlainString();
        }
        return str;
    }

    /**
     * BigDecimal转换为Integer
     *
     * @param bd 要转换的参数
     * @return Integer 转换后输出的参数
     */
    public static Integer bigDecimalToInteger(BigDecimal bd) {
        int inte = 0;
        if (UtilValidate.isNotEmpty(bd)) {
            inte = bd.intValue();
        }
        return inte;
    }

    /**
     * 判断BigDecimal类型值是否大于0，是返回true、否返回false
     *
     * @param bd BigDecimal
     * @return boolean
     */
    public static boolean isGreaterThanZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() > 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断BigDecimal类型值是否小于0，是返回true、否返回false
     *
     * @param bd BigDecimal
     * @return boolean
     */
    public static boolean isLessThanZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() < 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断BigDecimal类型值是否小于0，是返回true、否返回false
     * @param bd BigDecimal
     * @return boolean
     */
    public static boolean isEqualZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() == 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 小数点后N位截取
     *
     * @param bigDecimal 需要截取的BigDecimal
     * @param scale 保留几位小数
     * @return String
     */
    public static String getBigDecimalPoint(String bigDecimal, int scale) {
        if (scale < 0 || !bigDecimal.contains(".")) {
            return bigDecimal;
        }
        if (scale == 0) {
            return bigDecimal.split("\\.")[0];
        }
        BigDecimal b = new BigDecimal(bigDecimal);
        BigDecimal one = BigDecimal.ONE;
        return b.divide(one, scale, RoundingMode.HALF_UP).doubleValue() + "";
    }

    /**
     * 将Bigdecial以财务形式显示,千分位表示，保留N位小数
     * @param number 需要转换的数据
     * @param i 保留N位，如保留一位小数，输入1，结果是1,000,000.0这种格式
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/19 16:00
     */
    public static String bigDecimalFormatToFinance(BigDecimal number,Integer i){
        StringBuilder sb = new StringBuilder("#,###");
        if (i > 0) {
            sb.append(".");
            while(--i >= 0){
                sb.append("0");
            }
        }
        NumberFormat nf = new DecimalFormat(sb.toString());
        return  nf.format(number).equals("0")?"":nf.format(number);
    }

    /**
     * 逗号‘,’千位符的String转Bigdecimal
     * 如1,000,000 -> 1000000
     * 1,000,000.0 -> 1000000.0
     * 1,000,000.00 -> 1000000.00
     * @param str 需要转换的字符串
     * @return java.math.BigDecimal
     * @author liuc
     * @date 2021/11/19 16:07
     */
    public static BigDecimal financeFormatToBigDecimal(String str){
        DecimalFormat format = new DecimalFormat();
        format.setParseBigDecimal(true);
        ParsePosition position = new ParsePosition(0);
        return (BigDecimal) format.parse(str, position);
    }

    /**
     * Object 小数转为百分比字符串并保留两位小数
     * @param obj 对象
     * @return java.lang.String
     * @author liuc
     * @date 2021/12/8 11:03
     */
    public static String objToPercent(Object obj){
        DecimalFormat df = new DecimalFormat("0.00%");
        BigDecimal d= objToBigDecimal(obj);
        return df.format(d);
    }

    /**
     * 百分比字符串数字转BigDecimal
     * 例如：10.01% -> 0.01001
     *      100% -> 1
     * @param str 需要转BigDecimal的百分比字符串
     * @return java.math.BigDecimal BigDecimal结果
     * @author liuc
     * @date 2021/12/8 13:56
     */
    public static BigDecimal percentToBigDecimal(String str){
        BigDecimal bd = null;
        if (UtilValidate.isNotEmpty(str)) {
            str = str.replace("%","");
            if (RegexUtil.isNumeric(str)) {
                bd = div(str,"100");
            }
        }
        return bd;
    }

    /**
     * 百分比计算
     * @param obj 金额
     * @param percent 百分比数
     * @return BigDecimal 默认四舍五入
     */
    public static BigDecimal multiplyPercent(Object obj, BigDecimal percent) {
        if (objToBigDecimal(obj).compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        if (defaultZero(percent).compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        return objToBigDecimal(obj).multiply(percent).divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
    }
    /**
     * 如果num为Null，默认为0
     * @param num 数值
     * @return BigDecimal
     */
    public static BigDecimal defaultZero(BigDecimal num) {
        if (num == null) {
            return BigDecimal.ZERO;
        }
        return num;
    }

    /**
     * 求最小数值
     * @param num 数值
     * @return BigDecimal
     */
    public static BigDecimal min(BigDecimal... num) {
        if (UtilValidate.isEmpty(num)) {
            return null;
        }
        return Arrays.stream(num).min(BigDecimal::compareTo).get();
    }

    /**
     * 求最大数值
     * @param num 数值
     * @return BigDecimal
     */
    public static BigDecimal max(BigDecimal... num) {
        if (UtilValidate.isEmpty(num)) {
            return null;
        }
        return Arrays.stream(num).max(BigDecimal::compareTo).get();
    }

    /**
     * 转成str
     * @param obj 对象
     * @return String
     */
    public static String objToStr(Object obj) {
        if (UtilValidate.isEmpty(obj)) {
            throw new IllegalArgumentException("对象不能为空");
        }
        if (!RegexUtil.isNumeric(obj.toString())) {
            throw new IllegalArgumentException("["+obj+"]不是一个数字对象");
        }
    	return bigDecimalToStr(objToBigDecimal(obj));
    }

    /**
     * 将Object中的BigDecimal字段乘以给定的数
     * 计算出的结果如果还是无限接近于0的数（小数点超过6位小数，如 0.0000001），会以科学计数法的形式输出，如有必要，请自行转换为String
     * @param object 需要操作的对象
     * @param multiplier 乘数
     * @param excludedFields 不需要乘以的BigDecimal字段名称
     */
    public static void multiplyBigDecimalsForObject(Object object, BigDecimal multiplier, String... excludedFields) {
        if (object == null) {
            throw new IllegalArgumentException("Object must not be null.");
        }
        if (multiplier == null) {
            throw new IllegalArgumentException("Multiplier must not be null.");
        }

        Set<String> excludedFieldSet = new HashSet<>(Arrays.asList(excludedFields));

        if (object instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) object;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if (entry.getValue() instanceof BigDecimal value) {
                    BigDecimal newValue = value.multiply(multiplier);
                    entry.setValue(newValue);
                }
            }
        } else {
            Class<?> clazz = object.getClass();
            for (Field field : clazz.getDeclaredFields()) {
                if (field.getType().equals(BigDecimal.class) && !excludedFieldSet.contains(field.getName())) {
                    try {
                        field.setAccessible(true);
                        BigDecimal value = (BigDecimal) field.get(object);
                        if (value != null) {
                            BigDecimal newValue = value.multiply(multiplier);
                            field.set(object, newValue);
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException("Failed to reflect BigDecimal field", e);
                    } finally {
                        field.setAccessible(false);
                    }
                }
            }
        }
    }

/**
     * 将List中的BigDecimal字段乘以给定的数
     * 计算出的结果如果还是无限接近于0的数（小数点超过6位小数，如 0.0000001），会以科学计数法的形式输出，如有必要，请自行转换为String
     * @param list          需要操作的List对象
     * @param multiplier    乘数
     * @param excludeFields 不需要乘以的BigDecimal字段名称
     */
    public static void multiplyBigDecimalFieldsForList(List<Object> list, BigDecimal multiplier, String... excludeFields) {
        // 将排除字段名称转换为HashSet以提高查找效率
        Set<String> excludeFieldsSet = new HashSet<>(Arrays.asList(excludeFields));

        for (Object obj : list) {
            if (obj != null) {
                // 获取对象的所有字段
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field field : fields) {
                    // 检查字段是否为BigDecimal类型且不在排除列表中
                    if (BigDecimal.class.isAssignableFrom(field.getType()) && !excludeFieldsSet.contains(field.getName())) {
                        try {
                            // 设置字段可访问
                            field.setAccessible(true);
                            // 获取字段值并乘以乘数
                            BigDecimal value = (BigDecimal) field.get(obj);
                            if (value != null) {
                                BigDecimal newValue = value.multiply(multiplier);
                                field.set(obj, newValue);
                            }
                        } catch (IllegalAccessException e) {
                            // 异常处理，可以根据实际情况记录日志或抛出自定义异常
                            throw new RuntimeException("Failed to reflect BigDecimal field", e);
                        }
                    }
                }
            }
        }
    }

    /**
     * 将对象中的BigDecimal字段除以给定的数
     * 计算出的结果如果还是无限接近于0的数（小数点超过6位小数，如 0.0000001），会以科学计数法的形式输出，如有必要，请自行转换为String
     * @param object        需要操作的对象
     * @param divisor       除数
     * @param excludedFields 不需要除以的BigDecimal字段名称
     */
    public static void divideBigDecimalsForObject(Object object, BigDecimal divisor, String... excludedFields) {
        if (object == null) {
            throw new IllegalArgumentException("Object must not be null.");
        }
        if (divisor == null || divisor.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalArgumentException("Divisor must not be null and must not be zero.");
        }

        Set<String> excludedFieldSet = new HashSet<>(Arrays.asList(excludedFields));

        Class<?> clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.getType().equals(BigDecimal.class) && !excludedFieldSet.contains(field.getName())) {
                try {
                    field.setAccessible(true);
                    BigDecimal value = (BigDecimal) field.get(object);
                    if (value != null) {
                        // 设置精度和舍入模式，这里假设我们使用两位小数和四舍五入模式
                        BigDecimal newValue = value.divide(divisor, 2, RoundingMode.HALF_UP);
                        field.set(object, newValue);
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Failed to reflect BigDecimal field", e);
                } finally {
                    field.setAccessible(false);
                }
            }
        }
    }

    /**
     * 将List中的BigDecimal字段除以给定的数
     * 计算出的结果如果还是无限接近于0的数（小数点超过6位小数，如 0.0000001），会以科学计数法的形式输出，如有必要，请自行转换为String
     * @param list          需要操作的List对象
     * @param divisor       除数
     * @param excludeFields 不需要除以的BigDecimal字段名称
     */
    public static void divideBigDecimalFieldsForList(List<Object> list, BigDecimal divisor, String... excludeFields) {
        // 检查除数是否为0，如果是，则抛出异常
        if (BigDecimal.ZERO.equals(divisor)) {
            throw new ArithmeticException("Divisor cannot be zero.");
        }
        // 将排除字段名称转换为HashSet以提高查找效率
        Set<String> excludeFieldsSet = new HashSet<>(Arrays.asList(excludeFields));

        for (Object obj : list) {
            if (obj != null) {
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field field : fields) {
                    // 检查字段是否为BigDecimal类型且不在排除列表中
                    if (BigDecimal.class.isAssignableFrom(field.getType()) && !excludeFieldsSet.contains(field.getName())) {
                        try {
                            // 设置字段可访问
                            field.setAccessible(true);
                            // 获取字段值并除以除数
                            BigDecimal value = (BigDecimal) field.get(obj);
                            if (value != null) {
                                // 设置精度和舍入模式，这里假设我们使用两位小数和四舍五入模式
                                BigDecimal newValue = value.divide(divisor, 2, RoundingMode.HALF_UP);
                                field.set(obj, newValue);
                            }
                        } catch (IllegalAccessException e) {
                            // 抛出运行时异常，可以根据实际情况记录日志或进行其他异常处理
                            throw new RuntimeException("Failed to reflect BigDecimal field", e);
                        }
                    }
                }
            }
        }
    }
}
