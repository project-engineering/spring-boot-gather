package com.springboot.futool;

import com.springboot.futool.constant.Constant;
import com.springboot.futool.regex.RegexUtil;
import jakarta.annotation.Nullable;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StringUtils;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 字符串处理工具类
 * @author liuc
 * @date 2021/8/24 14:57
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class StringUtil {
    /***
     * 判断字符串是否都是数字
     * @param str 字符串
     * @return boolean
     */
    public static boolean isNumeric(String str){
        Pattern pattern = Pattern.compile(Constant.IS_NUMERIC);
        return pattern.matcher(str).matches();
    }

    /**
     * 字符串的数字部分加1操作，例如01加1得到的是02
     */
    public static String addOneForStr(String str){
        if(str != null){
            int fIndex = -1;
            for(int i = 0; i < str.length(); i++){
                char c = str.charAt(i);
                if (c >= '0' && c <= '9'){
                    fIndex = i;
                    break;
                }
            }
            if(fIndex == -1){
                return str;
            }
            String substring = str.substring(fIndex);
            int num = Integer.parseInt(substring) + 1;
            String zeroStr = addZeroForLeft(substring, String.valueOf(num));
            String preStr = str.substring(0, fIndex);
            return preStr + zeroStr + num;
        }
        return null;
    }

    /**
     * 字符串左补0
     */
    public static String addZeroForLeft(String sourceStr, String numberStr){
        StringBuilder sb = new StringBuilder();
        int len = sourceStr.length() - numberStr.length();
        sb.append("0".repeat(Math.max(0, len)));
        return sb.toString();
    }

    /**
     * 字符串左补0
     * @param str 原始字符串
     * @param targetLength 目标长度
     * @return java.lang.String 补齐后的字符串
     */
    public static String addZeroToLeft(String str, int targetLength) {
        if (str == null || str.length() >= targetLength) {
            return str;
        }

        return String.format("%0" + targetLength + "d", Integer.parseInt(str));
    }

    /**
     * 去除null以及空字符串
     * @param s 字符串
     * @return java.lang.String 截取后的字符串
     */
    public static String trimBlank(String s) {
        if (s == null) {
            return "";
        } else {
            return s.trim().replace("null", "");
        }
    }

    /**
     * 将文件名中的汉字转为UTF8编码的串,以便下载时能正确显示另存的文件名
     * @param s 文件名
     * @return java.lang.String UTF8编码的串
     */
    public static String toUtf8(String s) {
        StringBuilder sb = new StringBuilder(s.length() * 3);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c <= 255) {
                sb.append(c);
            } else {
                byte[] b = String.valueOf(c).getBytes(StandardCharsets.UTF_8);
                for (byte b1 : b) {
                    int k = b1 & 0xFF;
                    sb.append('%').append(Integer.toHexString(k).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

    /**
     * 去空格
     * @param str 字符串
     * @return java.lang.String 截取后的字符串
     */
    public static String trim(String str) {
        return (str == null ? "" : str.trim());
    }


    /**
     * 驼峰命名转为下划线命名
     * 例如：helloWorld->HELLO_WORLD
     * @param str 驼峰命名的字符串
     * @param flag 是否全部转成下划线命名小写，true-转成小写，false-转成大写
     * @return java.lang.String 转换后的字符串
     */
    public static String convertToUnderline(String str, boolean flag) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            if (Character.isUpperCase(currentChar)) {
                result.append(Constant.UNDERLINE);
            }
            result.append(currentChar);
        }

        String convertedStr = result.toString();

        return flag ? convertedStr.toLowerCase() : convertedStr.toUpperCase();
    }

    /**
     * 将字符串中的下划线去掉并转换为驼峰命名法
     * 例如：当smallCamel为true时，HELLO_WORLD->helloWorld
     * 当smallCamel为false时，HELLO_WORLD->HelloWorld
     * @param line 源字符串
     * @param smallCamel 是否为小驼峰 ，true-小驼峰，false-大驼峰
     * @return 转换后的字符串
     */
    public static String convertToCamelCase(String line, boolean smallCamel) {
        StringBuilder camelCaseString = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            char currentChar = line.charAt(i);
            if (currentChar == '_') {
                smallCamel = false;
            } else {
                if (!smallCamel) {
                    camelCaseString.append(Character.toUpperCase(currentChar));
                    smallCamel = true;
                } else {
                    camelCaseString.append(Character.toLowerCase(currentChar));
                }
            }
        }
        return camelCaseString.toString();
    }

    /**
     * 字符串转List
     * @param str 字符串
     * @return java.util.List<java.lang.String> 字符串列表
     */
    public static List<String> stringToList(String str) {
        if (str == null || str.isEmpty()) {
            return new ArrayList<>();
        }
        return Arrays.asList(str.split(Constant.COMMA));
    }

    /**
     * List转String,以逗号隔开
     * @param list 字符串列表
     * @return java.lang.String 转换后的字符串
     */
    public static String listToString(List<?> list) {
        if (list != null && !list.isEmpty()) {
            return org.apache.commons.lang3.StringUtils.join(list.toArray(), Constant.COMMA);
        }
        return null;
    }

    /**
     * 替换字符串
     * @param inString 原字符串
     * @param oldPattern 旧的字符串
     * @param newPattern 新的字符串
     * @return java.lang.String 替换后的字符串
     */
    public static String replace(String inString, String oldPattern, @Nullable String newPattern) {
        if (!StringUtils.hasLength(inString) || !StringUtils.hasLength(oldPattern) || newPattern == null) {
            return inString;
        }

        int pos = 0;
        int patLen = oldPattern.length();
        int len = inString.length();
        StringBuilder sb = new StringBuilder(len + 16);

        int index = inString.indexOf(oldPattern);
        while (index != -1) {
            sb.append(inString, pos, index).append(newPattern);
            pos = index + patLen;
            if (pos >= len) {
                break;
            }
            index = inString.indexOf(oldPattern, pos);
        }

        sb.append(inString, pos, len);

        return sb.toString();
    }

    public static boolean hasLength(@Nullable String str) {
        return (str != null && !str.isEmpty());
    }

    /**
     * Object转换为String
     * @param o 对象
     * @return java.lang.String 转换后的字符串
     */
    public static String objToStr(Object o) {
        return Objects.toString(o, "");
    }

    /**
     * 判定输入的是否是汉字
     * @param c 被校验的字符
     * @return true代表是汉字
     **/
    public static boolean checkChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
    }

    /**
     * 校验String是否全是中文
     * @param name 被校验的字符串
     * @return true代表全是汉字
     **/
    public static boolean checkNameChinese(String name) {
        boolean res = true;
        //转换为数组
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            //逐个判断是否为中文
            if (!checkChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;
    }

    /**
     * 校验String是否全是中文
     * @param name 被校验的字符串
     * @return true代表全是汉字
     **/
    public static boolean checkNameCheseNoSpeSymbol(String name) {
        boolean res = true;
        if (isContainChinessSymbol(name)) {
            return false;
        }
        if (validateLegalString(name)) {
            return false;
        }
        //转换为数组
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            //逐个判断是否为中文
            if (!checkChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;
    }


    /**
     * 该函数判断一个字符串是否包含标点符号（中文英文标点符号）。
     * 原理是原字符串做一次清洗，清洗掉所有标点符号。
     * 此时，如果原字符串包含标点符号，那么清洗后的长度和原字符串长度不同。返回true。
     * 如果原字符串未包含标点符号，则清洗后长度不变。返回false。
     * @param s 被校验的字符串
     * @return 返回true 包含标点符号
     **/
    public static boolean isContainChinessSymbol(String s) {
        boolean b = false;
        String tmp = s;
        tmp = tmp.replaceAll("\\p{P}", "");
        if (s.length() != tmp.length()) {
            b = true;
        }
        return b;
    }

    /**
     * 校验一个字符是否是汉字
     * @param c 被校验的字符
     * @return true代表是汉字
     **/
    public static boolean isChineseChar(char c) {
        return String.valueOf(c).getBytes(StandardCharsets.UTF_8).length > 1;
    }

    /**
     * 验证字符串内容是否包含下列非法字符
     * `~!#%^&*=+\\|{};:'\",<>/?○●★☆☉♀♂※¤╬の〆
     * @param content 被校验字符串内容
     * @return true 代表包含非法字符
     **/
    public static boolean validateLegalString(String content) {
        boolean flag = false;
        String illegal = "`~!#%^&*=+\\|{};:'\",<>/?○●★☆☉♀♂※¤╬の〆";
        for (int i = 0; i < content.length(); i++) {
            for (int j = 0; j < illegal.length(); j++) {
                if (content.charAt(i) == illegal.charAt(j)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 包括零的正整数字符串左侧补N个0
     * @param sourceStr 需要左侧补0的包括零的正整数字符串
     * @param number 补零之后数字的总长度
     * @return java.lang.String 补齐后的字符串
     */
    public static String strLeftFillZero(String sourceStr, String number){
        if (UtilValidate.isEmpty(sourceStr)) {
            return null;
        }
        boolean flag = RegexUtil.isPositiveIntIncludesZero(sourceStr);
        if (!flag) {
            throw new RuntimeException("请输入包括零的正整数字符串");
        }
        if (!RegexUtil.isPositiveIntIncludesZero(number)) {
            throw new RuntimeException("请输入补0后的字符串长度!!!");
        }
        if (sourceStr.length() > Integer.parseInt(number)) {
            throw new RuntimeException("补0后的字符串长度需要小于等于补零之后数字的总长度!!!");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(sourceStr);
        int temp = Integer.parseInt(number) - sb.length();
        if (temp > 0) {
            while (sb.length() < Integer.parseInt(number)) {  //若长度不足进行补零
                sb.insert(0, "0");
            }
        }
        return sb.toString();
    }

    /**
     * 左侧删除0
     * @param sourceStr 需要左侧删除0的字符串
     * @return java.lang.String 删除0后的字符串
     */
    public static String strLeftRemoveZero(String sourceStr){
        if (UtilValidate.isEmpty(sourceStr)) {
            return null;
        }
        return sourceStr.replaceAll("^(0+)","");
    }

    /**
     * 字符串首字母小写
     * @param str 字符串
     * @return java.lang.String 首字母小写的字符串
     */
    public static String lowerFirst(String str){
        char[] cs = str.toCharArray();
        cs[0] += 32;
        return String.valueOf(cs);
    }
    /**
     * 字符串首字母大写
     * @param str 字符串
     * @return java.lang.String 首字母大写的字符串
     */
    public static String upperFirst(String str){
        // 进行字母的ascii编码前移，效率要高于截取字符串进行转换的操作
        char[] cs=str.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }
}
