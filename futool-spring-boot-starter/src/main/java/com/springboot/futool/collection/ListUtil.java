package com.springboot.futool.collection;

import com.springboot.futool.BigDecimalUtil;
import com.springboot.futool.DateUtil;
import com.springboot.futool.StringUtil;
import com.springboot.futool.UtilValidate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * list工具类
 */
public class ListUtil {
    /***
     * 通过遍历两个List按照str字段相等取出towList存放到新的List中并返回
     * @param oneList 源数据
     * @param towList 目标数据
     * @param str 字段名
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    public static List<Map<String, Object>> compareListHitData(List<Map<String, Object>> oneList, List<Map<String, Object>> towList, String str) {
        List<Map<String, Object>> resultList = oneList.stream().map(map -> towList.stream()
                .filter(m -> Objects.equals(m.get(str), map.get(str)))
                .findFirst().map(m -> {
                    map.putAll(m);
                    return m;
                }).orElse(null))
                .toList();
        return resultList.stream().filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /***
     * 按照str字段将后面一个list合并到前面的list里
     * 将m2合并到m1里，相同的key对应的value，以m2的为准
     * @param m1 源数据
     * @param m2 目标数据
     * @param key 字段名
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    public static List<Map<String, Object>> mergeList(List<Map<String, Object>> m1, List<Map<String, Object>> m2, String key) {
        m1.addAll(m2);
        Set<String> set = new HashSet<String>(8);
        //暂存所有key
        //合并
        //如果没有的key赋值null
        return m1.stream().filter(map -> map.get(key) != null).collect(Collectors.groupingBy(o->{
            //暂存所有key
            set.addAll(o.keySet());
            return o.get(key);
        })).values().stream().map(maps -> {
            //合并
            Map<String, Object> map = maps.stream().flatMap(m -> {
                return m.entrySet().stream();
            }).collect(Collectors.toMap(Map.Entry::getKey, t -> t.getValue() == null ? "" : t.getValue(), (a, b) -> b));
            //如果没有的key赋值null
            set.forEach(k -> {
                if (!map.containsKey(k)) {
                    map.put(k, null);
                }
            });
            return map;
        }).sorted((map1, map2)->{
            return map1.get(key).toString().compareTo(map2.get(key).toString());
        }).collect(Collectors.toList());
    }

    /**
     * 按照key字段将后面List集合合并到前面的List集合里，如果key值相同，就按最后一个List集合中的值为准
     * @param key 按某个字段合并
     * @param lists 需要合并的list集合数组
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @SafeVarargs
    public static List<Map<String, Object>> mergeList(String key, List<Map<String, Object>>... lists) {
        return new ArrayList<>(Stream.of(lists)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(
                        map -> map.get(key),  // 根据指定的key作为键
                        Function.identity(),   // 值保持不变
                        (map1, map2) -> {      // 如果key相同，则合并两个Map
                            map1.putAll(map2);
                            return map1;
                        }
                ))
                .values());
    }


    /**
     * 通过指定的key获取该key对应的value的list集合，并去重
     * @param list 需要获取的list集合
     * @param key 指定的key
     * @return java.util.List<java.lang.Object>
     */
    public static List<Object> getValueListFromKey(List<Map<String,Object>> list,String key){
        List<Object> list1 = list.stream().flatMap(each -> each.entrySet().stream())
                .filter(each -> each.getKey().contains(key)).map(Map.Entry::getValue)
                .toList();
        return list1.stream().distinct().filter(Objects::nonNull)
                .filter(e -> e!="")
                .collect(Collectors.toList());
    }

    /**
     * 指定去重的列
     */
    public static List<Map<String, Object>> uniqueList(List<Map<String, Object>> list, String key) {
        List<Map<String,Object>> resultList = new ArrayList<>();
        if (UtilValidate.isEmpty(list)||UtilValidate.isEmpty(key)) {
            return resultList;
        }
        resultList = list.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(

                                () ->new TreeSet<>(Comparator.comparing(m->(String)m.get(key)))
                        ),ArrayList::new
                )
        );
        return resultList;
    }

    /**
     * 获取重复的key
     */
    public static Set getMultiKeys(List<Map<String, String>> rows) {
        return rows.stream().collect(Collectors.toMap(e -> {
            return e.get("column1") + "_" + e.get("column2");
        }, e-> {
            List result = new ArrayList();
            result.add(e);
            return result;
        }, (a, b) -> {
            a.addAll(b);
            return a;
        })).entrySet().stream().filter(entry -> entry.getValue().size() > 1)
                .collect(HashSet::new, (list, entry)->{
                    list.add(entry.getKey());
                }, AbstractCollection::addAll);
    }

    /**
     * 移除list中的null值和空字符串
     * @param list 需要移除的list集合
     * @return java.util.List<java.lang.String>
     */
    public static List<String> getListNoNull(List<String> list){
        return list.stream().filter(Objects::nonNull)
                .filter(e -> !e.isEmpty())
                .collect(Collectors.toList());
    }

    /**
     * 循环截取某列表进行分页
     * @param dataList 分页数据
     * @param pageSize 页面大小
     * @param pageNo 当前页面
     * @return java.util.List<java.lang.String> 返回分页后的列表
     */
    public static List<Object> pageList(List<Object> dataList,int pageNo,int pageSize) {
        List<Object> currentPageList = new ArrayList<Object>();
        if (dataList != null && !dataList.isEmpty()) {
            currentPageList = dataList.stream().skip((long) (pageNo - 1) *pageSize).limit(pageSize).collect(Collectors.toList());
        }
        return currentPageList;
    }

    /**
     * List<Map<String, Object> 按照sort字段排序，order:desc-降序排序，asc-升序排序
     * @param mapList 需要排序的list集合
     * @param sort 排序字段
     * @param order 排序方式
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回排序后的list集合
     */
    public static List<Map<String, Object>> sortListMap(List<Map<String, Object>> mapList, final Object sort, final String order) {
        mapList.sort(new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                BigDecimal bd1 = BigDecimalUtil.objToBigDecimal(o1.get(sort));
                BigDecimal bd2 = BigDecimalUtil.objToBigDecimal(o2.get(sort));
                if ("DESC".equalsIgnoreCase(order)) {
                    return bd2.compareTo(bd1);
                } else {
                    return bd1.compareTo(bd2);
                }
            }
        });
        return mapList;
    }

    /**
     * 根据map中的某个key 去除List中重复的map
     *
     * @param list 需要去重的list集合
     * @param mapKey map中的key
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回去重后的list集合
     */
    public static List<Map<String, Object>> removeRepeatMapByKey(List<Map<String, Object>> list, String mapKey) {
        if (UtilValidate.isEmpty(list)) {
            return null;
        }
        // 把list中的数据转换成msp,去掉同一id值多余数据，保留查找到第一个id值对应的数据
        List<Map<String, Object>> listMap = new ArrayList<>();
        Map<String, Map<String,Object>> msp = new HashMap<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            Map<String,Object> map = list.get(i);
            String id = (String) map.get(mapKey);
            map.remove(mapKey);
            msp.put(id, map);
        }
        // 把msp再转换成list,就会得到根据某一字段去掉重复的数据的List<Map>
        Set<String> mspKey = msp.keySet();
        for (String key : mspKey) {
            Map<String,Object> newMap = msp.get(key);
            newMap.put(mapKey, key);
            listMap.add(newMap);
        }
        return listMap;
    }

    /**
     * 该方法可根据指定字段对List<Map>中的数据去重，存在重复的，保存第一组Map
     * @param originMapList 原始数据
     * @param keys 去重的字段
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回去重后的list集合
     */
    public static List<Object> deleteDuplicatedMapFromListByKeys(List<Map<String,Object>> originMapList, List<Object> keys) {
        if (UtilValidate.isEmpty(originMapList)) {
            return null;
        }
        Map<String,Object> tempMap = new HashMap<>(8);
        for (Map<String,Object> originMap : originMapList) {
            StringBuilder objHashCode = new StringBuilder();
            for (Object key : keys) {
                String value = originMap.get(key) != null ? originMap.get(key).toString() : "";
                objHashCode.append(value.hashCode());
            }
            tempMap.put(objHashCode.toString(), originMap);
        }
        return new ArrayList<Object>(tempMap.values());
    }

    /**
     * 该方法查找两个List<Map>中的数据差集
     * @param originMapList 原始数据
     * @param newMapList 新数据
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回差集的list集合
     */
    public static List<Map<String,Object>> differenceMapByList(List<Map<String,Object>> originMapList,List<Map<String,Object>> newMapList){
        if(UtilValidate.isEmpty(originMapList)) return null;
        if (UtilValidate.isEmpty(newMapList)) return null;
        List<Map<String,Object>> retList = new ArrayList<>();
        List<Map<String,Object>> listAll = new ArrayList<>();
        listAll.addAll(originMapList);
        listAll.addAll(newMapList);
        //过滤相同的集合
        List<Map<String,Object>> listSameTemp = new ArrayList<>();
        originMapList.forEach(a -> {
            if(newMapList.contains(a))
                listSameTemp.add(a);
        });
        retList = listSameTemp.stream().distinct().collect(Collectors.toList());
        //去除重复的，留下两个集合中的差集
        listAll.removeAll(retList);
        retList = listAll;
        return retList;
    }

    /**
     * 获取大于等于某个值的List
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回大于等于某个值的List
     */
    public static List<Map<String, Object>> filterListGreaterThanOrEqualValue(List<Map<String,Object>> list, String key , int value){
        return list.stream().filter(map -> (int)map.get(key) >=value).collect(Collectors.toList());
    }

    /**
     * 获取大于等于某个值的List
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回大于等于某个值的List
     */
    public static List<Map<String, Object>> filterListGreaterThanOrEqualValue(List<Map<String,Object>> list, String key , BigDecimal value){
        return list.stream().filter(map -> BigDecimalUtil.compare(map.get(key),value) >=0).collect(Collectors.toList());
    }

    /**
     * 根据key过滤value为指定值的List集合
     * @param list 需要过滤的list集合
     * @param key 指定key
     * @param value 指定value
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回过滤后的list集合
     */
    public static List<Map<String,Object>> filterIf(List<Map<String,Object>> list, String key , String value,boolean flag){
        List<Map<String,Object>> finshList = new ArrayList<>();
        if (flag) {
            finshList = list.stream().filter(map -> map.get(key).equals(value)).collect(Collectors.toList());
        } else {
            finshList = list.stream().filter(map -> !map.get(key).equals(value)).collect(Collectors.toList());
        }
        return finshList;
    }

    /**
     * 根据key是否删除value为指定值的list集合
     * @param list 需要删除的list集合
     * @param key 指定key
     * @param value 指定value
     * @param flag true-是；false-否
     */
    public static void removeIf(List<Map<String,Object>> list, String key , String value,boolean flag){
        if (flag) {
            list.removeIf(map->map.get(key).equals(value));
        } else {
            list.removeIf(map->!map.get(key).equals(value));
        }
    }

    /**
     * 获取在某个时间段之间的List集合，包含开始时间和结束时间
     * @param list 需要获取的list集合
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param dateKey 某个时间字段
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回在某个时间段之间的List集合
     */
    public static List<Map<String,Object>> getListOfDateRange(List<Map<String,Object>> list,Object startDate,Object endDate,String dateKey){
        List<Map<String,Object>> resultList = new ArrayList<>();
        if (UtilValidate.isNotEmpty(startDate)&&UtilValidate.isNotEmpty(endDate)) {
            resultList = list.stream().filter(m->UtilValidate.isNotEmpty(m.get(dateKey))
                    && DateUtil.convertObjToLdt(m.get(dateKey)).isAfter(DateUtil.convertObjToLdt(startDate).minusNanos(1))
                    && DateUtil.convertObjToLdt(m.get(dateKey)).isBefore(DateUtil.convertObjToLdt(endDate).plusNanos(1))).collect(Collectors.toList());
        }
        if (UtilValidate.isNotEmpty(startDate)&&UtilValidate.isEmpty(endDate)) {
            resultList = list.stream().filter(m->UtilValidate.isNotEmpty(m.get(dateKey))
                    && DateUtil.convertObjToLdt(m.get(dateKey)).isAfter(DateUtil.convertObjToLdt(startDate).minusNanos(1))).collect(Collectors.toList());
        }
        if (UtilValidate.isEmpty(startDate)&&UtilValidate.isNotEmpty(endDate)) {
            resultList = list.stream().filter(m->UtilValidate.isNotEmpty(m.get(dateKey))
                    && DateUtil.convertObjToLdt(m.get(dateKey)).isBefore(DateUtil.convertObjToLdt(endDate).plusNanos(1))).collect(Collectors.toList());
        }
        return resultList;
    }

    /**
     * 按某个字段求和
     * @param list 需要求和的list集合
     * @param key 求和的字段
     * @return java.math.BigDecimal 返回求和后的BigDecimal值
     */
    public static BigDecimal sumByKey(List<Map<String,Object>> list,String key){
        //BigDecimal求和
        return list.stream().map(m -> BigDecimalUtil.objToBigDecimal(m.get(key)))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 按某个字段求平均值
     * @param list 需要求平均值的list集合
     * @param key 求平均值的字段
     * @return java.math.BigDecimal 返回求平均后的BigDecimal值
     */
    public static BigDecimal averageByKey(List<Map<String,Object>> list,String key){
        return list.stream().map(m->BigDecimalUtil.objToBigDecimal(m.get(key))).reduce(BigDecimal.ZERO,BigDecimal::add)
                .divide(BigDecimalUtil.objToBigDecimal(list.size()),2, RoundingMode.HALF_UP);
    }


    /**
     * 将 key 转换为小写或者大写 key
     *
     * @param mapList 原始集合
     * @param flag 是否将key全部转成小写，true-转成小写；false-转成大写
     * @return 转换后的集合
     */
    public static List<Map<String, Object>> coverKeyToLowerOrUpperCase(List<Map<String, Object>> mapList,boolean flag) {
        List<Map<String, Object>> returnList = new ArrayList<>(mapList.size());
        mapList.forEach(oneMap -> {
            Map<String, Object> returnMap = new HashMap<>(oneMap.size());
            if (flag) {
                // 转为小写
                oneMap.forEach((key, val) -> returnMap.put(key.toLowerCase(), val));
            } else {
                // 转为大写
                oneMap.forEach((key, val) -> returnMap.put(key.toUpperCase(), val));
            }
            returnList.add(returnMap);
        });
        return returnList;
    }

    /**
     * 将驼峰key转下划线
     *
     * @param mapList 下划线 key 的map
     * @param flag 是否将所有字母转成小写，true-转成小写；false-转成大写
     * @return 驼峰 key 的map
     */
    public static List<Map<String, Object>> coverKeysToUnderline(List<Map<String, Object>> mapList,boolean flag) {
        List<Map<String, Object>> returnList = new ArrayList<>(mapList.size());
        mapList.forEach(oneMap -> {
            Map<String, Object> returnMap = new HashMap<>(oneMap.size());
            // 转为下划线
            oneMap.forEach((key, val) -> returnMap.put(StringUtil.convertToUnderline(key,flag), val));
            returnList.add(returnMap);
        });
        return returnList;
    }

    /**
     * 将下划线key转驼峰
     *
     * @param mapList 下划线 key 的map
     * @return 驼峰 key 的map
     */
    public static List<Map<String, Object>> coverKeysToCamelCase(List<Map<String, Object>> mapList) {
        return coverKeysToCamelCase(mapList, true);
    }

    /**
     * 将下划线key转驼峰
     *
     * @param mapList 下划线 key 的map
     * @param flag 是否将所有字母转成小写，true-转成小写；false-转成大写
     * @return 驼峰 key 的map
     */
    public static List<Map<String, Object>> coverKeysToCamelCase(List<Map<String, Object>> mapList, boolean flag) {
        List<Map<String, Object>> returnList = new ArrayList<>(mapList.size());
        mapList.forEach(oneMap -> {
            Map<String, Object> returnMap = new HashMap<>(oneMap.size());
            // 转为驼峰
            oneMap.forEach((key, val) -> returnMap.put(StringUtil.convertToCamelCase(key,flag), val));
            returnList.add(returnMap);
        });
        return returnList;
    }

    /**
     * 根据传入的多个key将List<Map<String, Object>>中的key对应的value进行拼接，并
     * 判断是否相同，如果相同就进行去重处理，保留最先的map，其他的都会被移除
     * @param data 需要去重的List<Map<String, Object>>集合
     * @param keyList 根据多个key去重的集合
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回去重后的list集合
     */
    public static List<Map<String, Object>> distinctList(List<Map<String, Object>> data, List<String> keyList) {
        return new ArrayList<>(data.stream()
                .collect(Collectors.toMap(
                        map -> Arrays.stream(keyList.toArray(String[]::new))
                                .map(key -> map.getOrDefault(key, ""))
                                .reduce("", (value1, value2) -> value1 + "_" + value2),
                        map -> map,
                        (map1, map2) -> map1))
                .values());
    }


    /**
     * 根据传入的多个key将List<Map<String, Object>>中的key对应的value进行拼接，再根据拼接后的值进行分组
     * @param data 需要分组的List<Map<String, Object>>集合
     * @param keyList 需要根据N个key来进行分组
     * @return java.util.Map<java.lang.String,java.util.List<java.util.Map<java.lang.String,java.lang.Object>>> 返回分组后的map集合
     */
    public static Map<String, List<Map<String, Object>>> groupByKeys(List<Map<String, Object>> data, List<String> keyList) {
        return data.stream()
                .collect(Collectors.groupingBy(
                        map -> Arrays.stream(keyList.toArray(String[]::new))
                                .map(key -> String.valueOf(map.get(key)))
                                .collect(Collectors.joining("_"))));
    }

    /**
     * 过滤出List<Map<String,Object>>中多个key重复的数据
     * 1、首先使用Stream的collect方法将数据按照指定的多个key分组，得到一个Map，其中key是原始数据中这些字段的值组成的列表，value是所有指定key相同的数据组成的列表。
     * 2、然后使用Stream的filter方法过滤出列表长度大于1的分组，即重复的数据所在的分组。
     * 3、接着使用Stream的flatMap方法将多个分组的列表合并成一个Stream。
     * 4、最后使用Stream的collect方法将所有重复的数据收集到一个新的List中。
     * 这样，duplicatedData列表中就包含了原始数据中多个key重复的数据。
     * @param data 需要过滤的List<Map<String, Object>>集合
     * @param keyList 需要根据N个key来进行过滤
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>> 返回复atedByKeys后的list集合
     */
    public static List<Map<String,Object>> duplicatedByKeys(List<Map<String, Object>> data, List<String> keyList){
        return data.stream()
                .collect(Collectors.groupingBy(map -> Arrays.stream(keyList.toArray(String[]::new))
                        .map(map::get)
                        .collect(Collectors.toList())))
                .values().stream()
                .filter(list -> list.size() > 1)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * 根据一个或多个key进行分组，然后将分组后的列表重新返回
     * @param list 待分组的list集合
     * @param keys 分组的key
     * @return java.util.List<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>> 返回分组后的list集合
     */
    public static List<Map<String, Object>> groupByKeys(List<Map<String, Object>> list, String... keys) {
        if (keys == null || keys.length == 0 || list.isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, List<Map<String, Object>>> groupedMap;
        if (keys.length == 1) {
            groupedMap = list.stream()
                    .collect(Collectors.groupingBy(m -> (String) m.getOrDefault(keys[0], "")));

        } else {
            groupedMap = list.stream()
                    .collect(Collectors.groupingBy(m -> {
                        StringBuilder sb = new StringBuilder();
                        for (String key : keys) {
                            sb.append(m.getOrDefault(key, "")).append("-");
                        }
                        return sb.toString();
                    }));
        }

        return groupedMap.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * 根据对象集合中的某个字段，将对象分成符合条件和不符合条件的两个列表。
     * @param <T> 对象类型
     * @param <U> 字段类型
     * @param list 待分类的对象集合
     * @param conditionList 条件对象集合
     * @param fieldExtractor 字段提取器函数
     * @return 分类后的结果，包含两个列表，符合条件和不符合条件的对象列表
     * 使用示例：
     * List<ReconciliationStatement> reconciliationStatementList = new ArrayList<>();
     * ReconciliationStatement reconciliationStatement = new ReconciliationStatement();
     * reconciliationStatement.setOutTradeNo("123456789");
     * reconciliationStatement.setTradeStatus("SUCCESS");
     * reconciliationStatementList.add(reconciliationStatement);
     * List<ReconciliationFileRecord> reconciliationFileRecordList = new ArrayList<>();
     * ReconciliationFileRecord reconciliationFileRecord = new ReconciliationFileRecord();
     * reconciliationFileRecord.setOutTradeNo("123456789");
     * reconciliationFileRecord.setDate("2021-11-11");
     * reconciliationFileRecordList.add(reconciliationFileRecord);
     * List<ReconciliationStatement> intersectionList = new ArrayList<>();
     * List<ReconciliationStatement> diffList = new ArrayList<>();
     * ListUtil.partitionByField(reconciliationStatementList, reconciliationFileRecordList.stream()
     *                  .map(ReconciliationFileRecord::getOutTradeNo).collect(Collectors.toList())
     *                 ,intersectionList, diffList,ReconciliationStatement::getOutTradeNo);
     */
    public static <T, U> void partitionByField(List<T> list,List<U> conditionList, List<T> sameList,List<T> diffList, Function<T, U> fieldExtractor) {
        sameList.addAll(list.stream()
                .filter(item -> conditionList.contains(fieldExtractor.apply(item)))
                .toList());

        diffList.addAll(list.stream()
                .filter(item -> !conditionList.contains(fieldExtractor.apply(item)))
                .toList());
    }
}
