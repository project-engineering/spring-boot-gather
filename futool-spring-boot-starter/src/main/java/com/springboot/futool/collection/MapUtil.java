package com.springboot.futool.collection;

import com.springboot.futool.StringUtil;
import com.springboot.futool.UtilValidate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class MapUtil {

    /**
     * 将Map中的key首字母转小写
     * @param sourceMap 源map
     * @return 转换后的map
     */
    public static Map<String,Object> keyInitialToLowCase(Map<String,Object> sourceMap){
        Map<String,Object> result = new HashMap<>();
        if (UtilValidate.isEmpty(sourceMap)) {
            return result;
        }
        Set<Map.Entry<String,Object>> entrySet = sourceMap.entrySet();
        for (Map.Entry<String,Object> entry:entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //首字母转成小写
            result.put(StringUtil.lowerFirst(key),value);
        }
        return result;
    }

    /**
     * 将Map中的key首字母转大写
     * @param sourceMap 源map
     * @return 转换后的map
     */
    public static Map<String,Object> keyInitialToUpperCase(Map<String,Object> sourceMap){
        Map<String,Object> result = new HashMap<>();
        if (UtilValidate.isEmpty(sourceMap)) {
            return result;
        }
        Set<Map.Entry<String,Object>> entrySet = sourceMap.entrySet();
        for (Map.Entry<String,Object> entry:entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //首字母转成小写
            result.put(StringUtil.upperFirst(key),value);
        }
        return result;
    }
}
