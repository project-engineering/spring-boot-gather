package com.springboot.futool;

import lombok.extern.log4j.Log4j2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MAC地址工具
 *
 * @author liuc
 * @version V1.0
 * @date 2021/12/14 20:04
 * @since JDK1.8
 */
@Log4j2
public class MacUtil {

    /**
     * 获取当前操作系统名称. return 操作系统名称 例如:windows,Linux,Unix等.
     */
    public static String getOsName() {
        return System.getProperty("os.name").toLowerCase();
    }

    /**
     * 获取Unix网卡的mac地址.
     *
     * @return mac地址
     */
    public static String getUnixMacAddress() {
        try {
            Process process = Runtime.getRuntime().exec("/sbin/ifconfig eth0");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.trim().startsWith("ether")) {
                    return line.substring(6, 23);
                }
            }

            bufferedReader.close();
        } catch (IOException e) {
            log.error("获取MAC地址失败,原因：{}",e.getMessage());
        }

        return null;
    }

    /**
     * 获取Linux网卡的mac地址.
     *
     * @return mac地址
     */
    public static String getLinuxMacAddress() {
        String macAddress = null;
        try {
            Process process = Runtime.getRuntime().exec("ifconfig");
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("ether")) {
                    Pattern pattern = Pattern.compile("([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})");
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        macAddress = matcher.group(0);
                        break;
                    }
                }
            }
            process.waitFor();
            reader.close();
        } catch (IOException | InterruptedException e) {
            log.error("获取MAC地址失败,原因：{}",e.getMessage());
            throw new RuntimeException(e);
        }

        return macAddress;
    }

    /**
     * 获取widnows网卡的mac地址.
     *
     * @return mac地址
     */
    public static String getWindowsMacAddress() {
        String mac = null;

        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            // 遍历网卡，找到mac地址，如果找到多个，只返回第一个
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                byte[] hardwareAddress = networkInterface.getHardwareAddress();
                // mac地址有可能是null
                if (hardwareAddress != null && hardwareAddress.length == 6) {
                    StringBuilder macBuilder = new StringBuilder();
                    for (byte b : hardwareAddress) {
                        macBuilder.append(String.format("%02X%s", b, "-"));
                    }
                    // 删除最后一个-号
                    if (!macBuilder.isEmpty()) {
                        macBuilder.deleteCharAt(macBuilder.length() - 1); // Remove the last '-'
                        mac = macBuilder.toString();
                        break;
                    }
                }
            }
        } catch (SocketException e) {
            // 在生产环境中，应该记录错误日志并返回适当的错误信息
            log.error("获取widnows网卡的mac地址失败，原因: {}", e.getMessage());
            throw new RuntimeException(e);
        }
        return mac;
    }

    public static String getMac() {
        String os = getOsName();
        String mac;
        if (os.startsWith("windows")) {
            mac = getWindowsMacAddress();
        } else if (os.startsWith("linux")) {
            mac = getLinuxMacAddress();
        } else {
            mac = getUnixMacAddress();
        }
        return mac == null ? "" : mac;
    }

}
