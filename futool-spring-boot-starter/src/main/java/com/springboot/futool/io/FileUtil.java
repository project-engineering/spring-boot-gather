package com.springboot.futool.io;

import com.springboot.futool.UtilValidate;
import com.springboot.futool.exception.BusinessException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StopWatch;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 文件工具类
 */
@Log4j2
public class FileUtil {
    private static final int BUFFER_SIZE = 1024;
    private static final long B = 1;
    private static final long K = B<<10;
    private static final long M = K<<10;
    private static final long G = M<<10;
    private static final long T = G<<10;

    /**
     * 将list字符串写入txt文件
     * @param list 字符串列表
     * @param path 文件存放路径
     * @param fileName 文件名
     * @return 写入文件成功返回true，否则返回false
     */
    public static boolean writeFileToContext(List<String> list, String path, String fileName) {
        if (StringUtils.isEmpty(path) || list == null || list.isEmpty()) {
            log.error("Path is empty or list is null/empty");
            return false;
        }

        File filePath = new File(path);
        if (!filePath.exists() && !filePath.mkdirs()) {
            log.error("Failed to create directory: {}", path);
            return false;
        }

        File file = new File(filePath, fileName);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                String line = list.get(i);
                writer.write(line);
                if (i < size - 1) {
                    writer.newLine();
                }
            }
            log.info("Write to file successful: {}", file.getAbsolutePath());
            return true;
        } catch (IOException e) {
            log.error("Failed to write to file: {}", e.getMessage());
            return false;
        }
    }

    /**
     * 删除目标文件夹及以下的所有文件夹及文件 （慎用）
     * @param filePath 需要删除的文件夹的路径
     * @param isForceDelete 是否强制删除，true-强制删除 false-不强制删除(如果文件夹下面还是文件夹，那就不删除)
     *     递归的执行效率很低，并且对资源的占用情况随着任务规模的扩大，对资源的占用将呈几何式增长么，
     *     你想一下，如果目标文件夹下面存在大量的层级比较深的文件和文件夹时，这时候的执行效率是比较
     *     低的，而且很占资源。一旦你选择了递归算法，我觉得执行效率没啥改变了，但是资源占用方面可以
     *     有所改变，因此可以从这方面考虑下。这时候不用疑惑，java的GC线程虽然是实时的在检测着，但
     *     是一旦系统规模大了，难免有些照应不过了，因此有些垃圾对象可能会删除的有点迟，这里可以查一
     *     下，gc回收垃圾的机制有一种是查询对象是否还有被引用，一旦没有被引用，则立即启用回收，准备回收
     */
    public static void delete(String filePath, boolean isForceDelete) {
        File file = new File(filePath);
        if (file.exists()) {
            if (file.isDirectory()) {
                String[] paths = file.list();
                if (paths != null) {
                    for (String path : paths) {
                        delete(filePath + File.separator + path, isForceDelete);
                    }
                }
            }
            if (!file.delete() && isForceDelete) {
                throw new RuntimeException("Failed to delete file: " + filePath);
            }
        }
    }

    /**
     * 删除文件或者目录，如果目录不为空，则递归删除 （慎用）
     * @param filePath 你想删除的文件的路径
     *
     */
    public static void delete(String filePath) {
        delete(new File(filePath));
    }

    /**
     *
     * 删除文件或者目录，如果目录不为空，则递归删除
     * @param file filepath 你想删除的文件
     *
     */
    public static void delete(File file) {
        log.info("开始删除文件或目录：{}", file.getAbsolutePath());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        if (!file.exists()) {
            log.error("文件或目录不存在: {}", file);
            return;
        }
        Path path = file.toPath();
        try {
            if (Files.isDirectory(path)) {
                Files.walkFileTree(path, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } else {
                Files.delete(path);
            }
        } catch (IOException e) {
            log.error("删除文件或目录失败: {}, {}", file, e.getMessage());
            throw new RuntimeException("删除文件或目录失败: " + file + ", " + e.getMessage(), e);
        }
        log.info("删除文件或目录成功，耗时：{} ms", stopWatch.getTotalTimeMillis());
    }

    /**
     * 将从input处读取的字节数据通过output写到文件
     * @param input 输入流
     * @param output 输出流
     */
    private static void copyFile(InputStream input, OutputStream output){
        try (input; output) {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            log.error("拷贝文件失败：",e);
            throw new BusinessException("拷贝文件失败：" + e.getMessage());
        }
    }

    /**
     * 将文件从src复制到des
     * @param src 源文件
     *             例如：/home/temp/1.txt
     * @param des 目标文件
     *             例如：/home/temp/2.txt
     */
    public static void copyFile(String src, String des) {
        copyFile(new File(src), new File(des));
    }

    /**
     * 复制文件
     * @param sourceFile 源文件
     * @param targetFile 目标文件路径
     */
    public static void copyFile(File sourceFile, File targetFile) {
        log.info("开始复制文件，源文件：{}，目标文件路径：{}", sourceFile.getAbsolutePath(), targetFile);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        if (!sourceFile.exists()) {
            log.error("源文件不存在：{}", sourceFile.getAbsolutePath());
            throw new BusinessException("源文件不存在：" + sourceFile.getAbsolutePath());
        }
        File targetDir = targetFile.getParentFile();

        if (targetDir != null && !targetDir.exists()) {
            log.info("目标文件夹不存在，正在创建：{}", targetDir.getAbsolutePath());
            boolean mkdirsResult = targetDir.mkdirs();
            if (!mkdirsResult) {
                log.error("创建目标文件夹失败：{}", targetDir.getAbsolutePath());
                throw new BusinessException("创建目标文件夹失败：" + targetDir.getAbsolutePath());
            }
        }

        try {
            Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("文件复制失败, 原因：", e);
            throw new BusinessException("文件复制失败, 原因：" + e.getMessage());
        }
        log.info("文件复制成功，目标文件路径：{}, 耗时：{} ms", targetFile.getAbsolutePath(), stopWatch.getTotalTimeMillis());
    }

    /**
     * 拷贝文件夹
     * @param src 源目录
     * @param des 目标目录
     */
    public static void copyDirectory(String src, String des){
        copyDirectory(new File(src), new File(des));
    }

    /**
     * 拷贝文件夹
     * @param src 源目录
     * @param des 目标目录
     */
    public static void copyDirectory(File src, File des) {
        log.info("拷贝源目录: {}至目标目录：{}",src.getAbsolutePath(),des.getAbsolutePath());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            if (!src.exists()) {
                throw new FileNotFoundException(src.getAbsolutePath() + " 原目录不存在。");
            }

            if (!des.exists()) {
                log.info("目标目录不存在，创建目标目录：{}",des.getAbsolutePath());
                boolean success = des.mkdirs();
                if (!success) {
                    throw new RuntimeException("创建目标目录失败：" + des.getAbsolutePath());
                } else {
                    log.info("目标目录创建成功，耗时：{} ms", stopWatch.getTotalTimeMillis());
                }
            }

            final Path sourcePath = src.toPath();
            final Path targetPath = des.toPath();

            Files.walkFileTree(sourcePath, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    Path targetDir = targetPath.resolve(sourcePath.relativize(dir));
                    Files.copy(dir, targetDir, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Path targetFile = targetPath.resolve(sourcePath.relativize(file));
                    Files.copy(file, targetFile, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            log.error("拷贝目录失败: {}", e.getMessage());
            throw new RuntimeException("拷贝目录失败: " + e.getMessage());
        }
        stopWatch.stop();
        log.info("拷贝目录成功，耗时：{} ms", stopWatch.getTotalTimeMillis());
    }

    /**
     * 将文件大小（以字节为单位）转换为人类可读的单位（如KB、MB、GB等）
     * @param length 文件大小（以字节为单位）
     * @return 人类可读的单位
     */
    public static String toUnits(long length) {
        if (length < 0) {
            length = 0;
        }

        String[] units = {"B", "KB", "MB", "GB", "TB"};
        int unitIndex = 0;

        while (length >= 1024 && unitIndex < units.length - 1) {
            length /= 1024;
            unitIndex++;
        }

        return String.format("%d %s", length, units[unitIndex]);
    }

    /**
     * 获取文件大小
     * @param file 文件
     * @return 文件大小（以字节为单位）
     */
    private static long getTotalLength(File file) {
        long cnt = 0;
        if (file.isDirectory()) {
            File[] fileList = file.listFiles();
            if (fileList != null) {
                for (File f : fileList) {
                    cnt += getTotalLength(f);
                }
            }
        } else {
            cnt += file.length();
        }
        return cnt;
    }

    /**
     * 获取文件大小
     * @param filepath 文件路径
     * @return 文件大小（以字节为单位）
     */
    public static long getTotalLength(String filepath){
        return getTotalLength(new File(filepath));
    }


    /**
     * 根据文件路径，如果文件路径有就不生成，没有就创建路径且生成文件
     * @param path 文件路径
     * @return java.io.File
     * @author liuc
     * @date 2021/11/6 15:05
     * @throws IOException 异常
     */
    public static File getFile(String path) throws IOException {
        File file = new File(path);
        FileUtils.forceMkdirParent(file);
        FileUtils.touch(file);
        log.info("文件已创建完成!");
        return file;
    }

    /**
     * 根据文件全路径判断文件是否存在，例如/home/temp/1.txt
     * @param filePath 文件全路径
     * @return boolean 存在返回true,不存在返回false
     */
    public static boolean isExists(String filePath){
        File file = new File(filePath);
        return file.exists();
    }


    /**
     * File转byte[]数组
     *
     * @param fileFullPath 文件全路径
     * @return byte[] 字节数组
     */
    public static byte[] fileToByte(String fileFullPath) {
        if (fileFullPath == null || fileFullPath.isEmpty()) {
            return null;
        }
        return fileToByte(new File(fileFullPath));
    }

    /**
     * File转byte[]数组
     *
     * @param file 文件
     * @return byte[] 字节数组
     */
    public static byte[] fileToByte(File file) {
        if (file == null) {
            return null;
        }

        try (FileInputStream fileInputStream = new FileInputStream(file);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[BUFFER_SIZE]; // 增加缓冲区的大小
            int bytesRead;
            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            log.error("file to byte error，{}", e.getMessage());
        }
        return null;
    }

    /**
     * byte[]数组转File
     *
     * @param bytes 字节数组
     * @param fileFullPath 文件全路径
     * @return File 文件
     */
    public static File byteToFile(byte[] bytes, String fileFullPath) {
        if (bytes == null || fileFullPath == null || fileFullPath.isEmpty()) {
            return null;
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(fileFullPath)) {
            fileOutputStream.write(bytes);
            return new File(fileFullPath);
        } catch (IOException e) {
            log.error("byte to file error，{}", e.getMessage());
        }
        return null;
    }

    /**
     * 将byte[]写入文件，并放到指定文件夹下
     * @param data 待写入文件的byte[]信息
     * @param filePath 文件夹路径
     * @param fileName 文件名
     * @return boolean 成功返回true，失败返回false
     */
    public static boolean writeBytesToFile(byte[] data, String filePath, String fileName){
        return writeBytesToFile(data, filePath, fileName, StandardCharsets.UTF_8);
    }

    /**
     * 将byte[]写入文件，并放到指定文件夹下
     * @param data 待写入文件的byte[]信息
     * @param filePath 文件夹路径
     * @param fileName 文件名
     * @param charset 字符集
     * @return boolean 成功返回true，失败返回false
     */
    public static boolean writeBytesToFile(byte[] data, String filePath, String fileName, Charset charset) {
        if (UtilValidate.isEmpty(data) || UtilValidate.isEmpty(filePath) || UtilValidate.isEmpty(fileName)) {
            log.info("参数不能为空");
            return false;
        }

        Path folder = Paths.get(filePath);
        try {
            // 创建文件夹（如果不存在）
            if (!Files.exists(folder)) {
                log.info("文件夹不存在，准备创建文件夹: {}", folder);
                Files.createDirectories(folder);
            } else {
                log.info("文件夹已存在: {}", folder);
            }
            log.info("准备写入文件");
            // 写入文件
            File file = new File(folder + File.separator + fileName);
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(fos, charset);
            writer.write(new String(data, charset));
            writer.close();
            log.info("文件写入成功：{}", file);
            return true;
        } catch (IOException e) {
            log.error("写入文件时发生错误, 原因：", e);
            return false;
        }
    }

    /**
     * 将字符串写入文件，并放到指定文件夹下
     * @param content 待写入文件的字符串信息
     * @param filePath 文件夹路径
     * @param fileName 文件名
     * @return boolean 成功返回true，失败返回false
     */
    public static boolean writeStringToFile(String content, String filePath, String fileName){
        return writeStringToFile(content, filePath, fileName, StandardCharsets.UTF_8);
    }

    /**
     * 将字符串写入文件，并放到指定文件夹下
     * @param content 待写入文件的字符串信息
     * @param filePath 文件夹路径
     * @param fileName 文件名
     * @param charset 字符集
     * @return boolean 成功返回true，失败返回false
     */
    public static boolean writeStringToFile(String content, String filePath, String fileName, Charset charset) {
        if (UtilValidate.isEmpty(content) || UtilValidate.isEmpty(filePath) || UtilValidate.isEmpty(fileName)) {
            log.info("参数不能为空");
            return false;
        }

        Path folder = Paths.get(filePath);
        try {
            // 创建文件夹（如果不存在）
            if (!Files.exists(folder)) {
                log.info("文件夹不存在，准备创建文件夹: {}", folder);
                Files.createDirectories(folder);
            } else {
                log.info("文件夹已存在: {}", folder);
            }
            log.info("准备写入文件");
            // 写入文件
            File file = new File(folder + File.separator + fileName);
            if (UtilValidate.isEmpty(charset)) {
                charset = StandardCharsets.UTF_8;
                log.info("使用默认字符集: {}", charset);
            }
            BufferedWriter writer = Files.newBufferedWriter(file.toPath(), charset);
            writer.write(content);
            writer.close();

            log.info("文件写入成功：{}", file);
            return true;
        } catch (IOException e) {
            log.error("写入文件时发生错误, 原因：", e);
            return false;
        }
    }

    /**
     * 下载文件
     * @param downloadUrl 下载地址
     * @param path 保存路径 filePath+fileName
     */
    public static Map<String, Object> downloadFile(String downloadUrl, String path) {
        log.info("开始下载文件，下载地址：{}，保存路径：{}", downloadUrl, path);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        HttpURLConnection connection = null;
        Map<String, Object> map = new HashMap<>();
        try {
            URL url = new URL(downloadUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // 使用try-with-resources自动关闭资源
            try (InputStream inputStream = new BufferedInputStream(connection.getInputStream())) {
                // 创建不存在的父目录
                Path parentDir = Path.of(path).getParent();
                if (parentDir != null) {
                    Files.createDirectories(parentDir);
                }

                // 使用Files.copy简化文件复制过程
                Files.copy(inputStream, Path.of(path), StandardCopyOption.REPLACE_EXISTING);

                map.put("filePath", Objects.requireNonNull(parentDir).toString());
                map.put("fileName", Path.of(path).getFileName().toString());
            }
        } catch (IOException e) {
            log.error("文件下载失败,原因：{}", e.getMessage());
            throw new BusinessException("文件下载失败,原因：" + e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("文件下载成功，保存路径：{}", map);
        }
        stopWatch.stop();
        log.info("文件下载成功，耗时：{} ms", stopWatch.getTotalTimeMillis());
        return map;
    }

    /**
     * 创建文件夹
     * @param directoryPath 文件夹路径
     */
    public static void mkdir(String directoryPath){
        Path path = Paths.get(directoryPath);
        if (!path.toFile().exists()) {
            boolean mkdirsResult = path.toFile().mkdirs();
            if (!mkdirsResult) {
                log.error("创建文件夹失败：{}", directoryPath);
                throw new BusinessException("创建文件夹失败：" + directoryPath);
            }
        }
    }

    public static void main(String[] args) {
        String filePath = "E:\\data\\test";
        String fileName = "testfile.txt";
        String content = "中文内容测试";

        // 将字符串内容转换为字节数组，使用 UTF-8 编码
        byte[] data = content.getBytes(StandardCharsets.UTF_8);

        // 调用写入文件的方法
        boolean result = writeBytesToFile(data, filePath, fileName);

        if (result) {
            System.out.println("文件写入成功");
        } else {
            System.out.println("文件写入失败");
        }
    }
}
