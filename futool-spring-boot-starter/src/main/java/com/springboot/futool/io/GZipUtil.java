package com.springboot.futool.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

/**
 * GZip压缩工具类.
 */
@Log4j2
public class GZipUtil {
    public static final String GZIP_ENCODE_UTF_8 = "UTF-8";
    /**
     * 使用TAR算法进行压缩.
     * @param tarFileBytesMap 待压缩文件的Map集合.
     * @return 压缩后的TAR文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByTar(Map<String, byte[]> tarFileBytesMap) {
        if (tarFileBytesMap == null || tarFileBytesMap.isEmpty()) {
            return new byte[0];
        }
        try (ByteArrayOutputStream tarBaos = new ByteArrayOutputStream();
             TarArchiveOutputStream tarTaos = new TarArchiveOutputStream(tarBaos)) {
            for (Map.Entry<String, byte[]> fileEntry : tarFileBytesMap.entrySet()) {
                TarArchiveEntry tarTae = new TarArchiveEntry(fileEntry.getKey());
                tarTae.setSize(fileEntry.getValue().length);
                tarTaos.putArchiveEntry(tarTae);
                tarTaos.write(fileEntry.getValue());
                tarTaos.closeArchiveEntry();
            }
            // 将压缩后的数据返回
            return tarBaos.toByteArray();
        } catch (IOException e) {
            log.error("压缩TAR文件时发生异常,原因：{}", e.getMessage());
            throw new RuntimeException("压缩TAR文件时发生异常,原因：{}",e);
        }
    }

    /**
     * 使用TAR算法进行解压.
     * @param sourceTarFileBytes TAR文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，返回Map集合长度为0.
     */
    public static Map<String, byte[]> unCompressByTar(byte[] sourceTarFileBytes) {
        Map<String, byte[]> targetFilesFolderMap = new HashMap<>();

        if (sourceTarFileBytes == null || sourceTarFileBytes.length == 0) {
            log.info("sourceTarFileBytes is null or empty.");
            return targetFilesFolderMap;
        }

        try (ByteArrayInputStream sourceTarBais = new ByteArrayInputStream(sourceTarFileBytes);
             TarArchiveInputStream sourceTarTais = new TarArchiveInputStream(sourceTarBais)) {
            TarArchiveEntry sourceTarTae;
            while ((sourceTarTae = sourceTarTais.getNextTarEntry()) != null) {
                // 将解压后的文件数据放入 Map 中
                targetFilesFolderMap.put(sourceTarTae.getName(), IOUtils.toByteArray(sourceTarTais));
            }
        } catch (IOException e) {
            log.error("解压TAR文件时发生异常,原因：{}", e.getMessage());
            throw new RuntimeException("解压TAR文件时发生异常,原因：{}",e);
        }
        return targetFilesFolderMap;
    }

    /**
     * 使用GZIP算法进行压缩.
     * @param sourceFileBytes 待压缩文件的Map集合.
     * @return 压缩后的GZIP文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByGZip(byte[] sourceFileBytes) {
        if (sourceFileBytes == null || sourceFileBytes.length == 0) {
            System.out.println("sourceFileBytes is null or empty.");
            return new byte[0]; // 返回空字节数组表示压缩失败
        }

        try (ByteArrayOutputStream gzipBaos = new ByteArrayOutputStream();
             GzipCompressorOutputStream gzipGcos = new GzipCompressorOutputStream(gzipBaos)) {
            // 将源文件数据写入 Gzip 输出流中进行压缩
            gzipGcos.write(sourceFileBytes);
            gzipGcos.close(); // 关闭输出流以确保数据写入完成
            return gzipBaos.toByteArray();
        } catch (IOException e) {
            log.error("压缩文件时发生异常,原因：{}", e.getMessage());
            throw new RuntimeException("压缩文件时发生异常,原因：{}",e);
        }
    }

    /**
     * 使用GZIP算法进行解压.
     * @param sourceGZipFileBytes GZIP文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] unCompressByGZip(byte[] sourceGZipFileBytes){
        // 变量定义.
        try (ByteArrayOutputStream gzipBaos = new ByteArrayOutputStream();
             ByteArrayInputStream sourceGZipBais = new ByteArrayInputStream(sourceGZipFileBytes);
             GzipCompressorInputStream sourceGZipGcis = new GzipCompressorInputStream(sourceGZipBais)) {
            // 采用commons-compress提供的方式进行解压.
            IOUtils.copy(sourceGZipGcis, gzipBaos);
            return gzipBaos.toByteArray();
        } catch (IOException e) {
            log.error("解压缩失败,原因：{}" ,e.getMessage());
            throw new RuntimeException("解压缩失败,原因：{}",e);
        }
    }

    /**
     * 将字符串压缩为GZIP字节数组
     * @param str
     * @return
     */
    public static byte[] compress(String str){
        return compress(str,GZIP_ENCODE_UTF_8);
    }

    /**
     * 将字符串压缩为GZIP字节数组
     * @param str 需要压缩的字符串
     * @param encoding 压缩使用的字节编码
     * @return
     */
    public static byte[] compress(String str, String encoding) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Input string is null or empty.");
        }
        if (encoding == null || !Charset.isSupported(encoding)) {
            throw new IllegalArgumentException("Unsupported encoding: " + encoding);
        }

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             GZIPOutputStream gzip = new GZIPOutputStream(out)) {
            byte[] data = str.getBytes(encoding);
            gzip.write(data, 0, data.length);
            // 完成数据写入
            gzip.finish();
            return out.toByteArray();
        } catch (IOException e) {
            log.error("压缩失败,原因：{}",e.getMessage());
            // 可以根据实际需求选择如何处理异常，这里选择将异常包装成运行时异常抛出
            throw new RuntimeException("压缩失败,原因：{}", e);
        }
    }

    /**
     * 解压并返回字符串
     * @param bytes 需要解压的字节数组
     * @return String 解压后返回的字符串
     * @throws IOException
     */
    public static String unCompressToString(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            throw new IllegalArgumentException("Input byte array is null or empty.");
        }

        byte[] result = unCompressByGZip(bytes);
        return new String(result);
    }
}
