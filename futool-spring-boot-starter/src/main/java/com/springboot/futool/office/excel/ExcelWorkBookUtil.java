package com.springboot.futool.office.excel;

import com.springboot.futool.DateUtil;
import com.springboot.futool.UtilValidate;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.FileMagic;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

@Log4j2
public class ExcelWorkBookUtil {
    /**
     * excel模板文件路径
     */
    public static final String EXCEL_MODEL_PATH = "classpath:template/";
    /**
     * excel模板编号赋值键
     */
    public static final String EXCEL_ROWNUM_KEY = "rn";

    /**
     * 检测是否存在指定Sheet
     * @param path
     * @param sheet
     * @return
     */
    public static boolean checkSheetExist(String path,String sheet){
        File f = new File(path);
        return checkSheetExist(f,sheet);
    }

    /**
     *
     * @param path
     * @return
     */
    public static boolean checkSheetExit(String path){
        File f = new File(path);
        return checkSheetExist(f);
    }

    /**
     * 检查是否存在指定Sheet
     * @param file
     * @param sheet
     * @return
     */
    @SneakyThrows
    public static boolean checkSheetExist(File file, String sheet){
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(file);
            if (checkExcelIs2007(file)) {
                return checkSheetExistXls2007(inputStream,sheet);
            } else {
                return checkSheetExistXls(inputStream,sheet);
            }
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
    }

    /**
     * 检测是否存在指定excel文件
     * @param file
     * @return
     */
    @SneakyThrows
    public static boolean checkSheetExist(File file){
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(file);
            if (checkExcelIs2007(file)) {
                return checkSheetExistXls2007(inputStream);
            } else {
                return checkSheetExistXls(inputStream);
            }
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
    }

    /**
     * 检测是否是2007版本的excel文件
     * @param path
     * @return
     */
    public static boolean checkExcelIs2007(String path){
        File f = new File(path);
        return checkExcelIs2007(f);
    }

    /**
     * 检测是否是2007版本的excel文件
     * @param file
     * @return
     */
    @SneakyThrows
    public static boolean checkExcelIs2007(File file){
        InputStream inputStream = new FileInputStream(file);
        return checkExcelIs2007(inputStream);
    }

    /**
     * 检测文件格式是否是xls或者xlsx
     * @param inputStream
     * @return
     */
    @SneakyThrows
    public static boolean checkExcelIs2007(InputStream inputStream){
        InputStream is = FileMagic.prepareToCheckMagic(inputStream);
        FileMagic fm = FileMagic.valueOf(is);
        switch (fm) {
            case OLE2:
                return false;
            case OOXML:
                return true;
            default:
                throw new IOException("文件格式无法识别，请使用xls,xlsx格式");
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls2007(InputStream inputStream , String sheet){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return (book.getSheetIndex(sheet)>=0);
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls2007(InputStream inputStream){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return book.sheetIterator().hasNext();
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls(InputStream inputStream, String sheet){
        HSSFWorkbook book = null;
        try {
            book = new HSSFWorkbook(inputStream);
            return (book.getSheetIndex(sheet)>=0);
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls(InputStream inputStream){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return book.sheetIterator().hasNext();
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    /**
     * 通过文件名获取文件路径
     * @param fileName
     * @return
     */
    public static String getPathByName(String fileName){
        return "G:\\home\\"+ DateUtil.convertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_3)+"\\"+fileName;
    }

    /**
     * 根据模板的路径获取模板文件对象
     * classpath根目录下：/template/report.xlsx
     */
    private static File getTemplateFile(String template) throws IOException {
        Resource resource =new ClassPathResource("/template/"+template);
        return  resource.getFile();
    }

    /**
     * 读取excel模板，并导出excel
     * 模板最后一行必须是取值代码行，如：${name}
     * @param modelFileName 模板excel文件名，注意不是路径，仅仅是文件名
     * @param fileName 导出的文件名，注意不是路径，仅仅是文件名
     * @param param 接口滴哦用参数 包含动态表头参数（接口调用后会删除） Map<String,Object> headerParam
     * @return
     */
    @SneakyThrows
    public static Map<String,Object> export(String modelFileName, String fileName, Map<String,Object> param){
        //读取模板文件
        File modelFile = getTemplateFile(modelFileName);
        if (!modelFile.exists()) {
            log.error("未找到模板文件，模板路径["+modelFileName+"]");
            throw new IOException("未找到模板文件");
        }
        String path = getPathByName(fileName).replace(fileName,"");
        File pathFile = new File(path);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }
        //生成结果文件
        String filePath = getPathByName(fileName);
        File file = new File(filePath);
        if (!file.exists()) {
            //如果文件不存在就创建
            file.createNewFile();
        }
        //处理文件内容
        XSSFWorkbook workbook = null;
        try(FileInputStream fileInputStream = new FileInputStream(modelFile);
            FileOutputStream stream = new FileOutputStream(filePath)){
            workbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            if (UtilValidate.isNotEmpty(param.get("headerParam"))) {
                exportHeader(sheet, (Map<String, Object>) param.get("headerParam"));
                param.remove("headerParam");
            }
            export(sheet,param);
            workbook.write(stream);
        }catch (IOException e){
            log.error("写入文件失败，原因："+e.getMessage());
            throw new RuntimeException("写入文件失败");
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        Map<String,Object> successMap = new HashMap<>();
        successMap.put("fileName",fileName);
        successMap.put("filePath",filePath);
        return successMap;
    }

    private static void export(XSSFSheet sheet,Map<String,Object> param){
        int lastRowNum = sheet.getLastRowNum();
        //获取参数行
        Row row = sheet.getRow(lastRowNum);
        Iterator<Cell> iterator = row.iterator();
        ArrayList<String> arrayList = new ArrayList<>();
        while (iterator.hasNext()){
            Cell next = iterator.next();
            arrayList.add(next.getStringCellValue());
        }
        boolean isEmpty = false;
        int rowCount = lastRowNum,colCount;
        List<Map<String,Object>> pageList = (List<Map<String, Object>>) param.get("pageList");
        if (UtilValidate.isEmpty(pageList)) {
            isEmpty = true;
        } else {
            /**
             * 写入文件
             */
            //处理行
            for (Map<String,Object> info:pageList) {
                colCount = 0;
                if (lastRowNum == rowCount) {
                    //处理列
                    for (String key:arrayList) {
                        Object value;
                        if (UtilValidate.isEmpty(key)) {
                            value = "";
                        } else if (key.contains("${")){
                            String k = key.replace("${","").replace("}","");
                            if (UtilValidate.areEqual(EXCEL_ROWNUM_KEY,k)) {
                                //excel编号，特殊处理
                                value = rowCount - lastRowNum + 1;
                            } else {
                                value = info.get(k);
                            }
                        } else {
                            value = key;
                        }
                        row.getCell(colCount++).setCellValue(null == value?"":value+"");
                    }
                    rowCount++;
                } else {
                    Row row2 = sheet.createRow(rowCount);
                    row2.setHeight(row.getHeight());
                    row2.setHeightInPoints(row.getHeightInPoints());
                    row2.setZeroHeight(row.getZeroHeight());
                    row2.setRowStyle(row.getRowStyle());
                    //处理列
                    for (String key:arrayList) {
                        Object value;
                        if (UtilValidate.isEmpty(key)) {
                            value = "";
                        } else if (key.contains("${")){
                            String k = key.replace("${","").replace("}","");
                            if (UtilValidate.areEqual(EXCEL_ROWNUM_KEY,k)) {
                                //excel编号，特殊处理
                                value = rowCount - lastRowNum + 1;
                            } else {
                                value = info.get(k);
                            }
                        } else {
                            value = key;
                        }
                        Cell createCell = row2.createCell(colCount);
                        createCell.setCellStyle(row.getCell(colCount).getCellStyle());
                        createCell.setCellValue(null == value?"":value+"");
                        colCount++;
                    }
                    rowCount++;
                }
            }
        }
        //如果上送的数据为空，需要把模板的最后一行（取数表达式行），给删除掉
        if (isEmpty) {
            sheet.removeRow(row);
        }
    }

    /**
     * 导出表头中的内容
     * @param sheet
     * @param param
     */
    private static void exportHeader(XSSFSheet sheet,Map<String,Object> param){
        try {
            int lastRowNum = sheet.getLastRowNum();
            for (int index = 0; index < lastRowNum; index++) {
                Row row = sheet.getRow(index);
                Iterator<Cell> iterator = row.iterator();
                int colCount = 0;
                while (iterator.hasNext()){
                    Cell next = iterator.next();
                    String key = next.getStringCellValue();
                    Object value;
                    if (UtilValidate.isEmpty(key)) {
                        value = "";
                    } else if (key.contains("${")){
                        String k = key.replace("${","").replace("}","");
                        if (UtilValidate.areEqual(EXCEL_ROWNUM_KEY,k)) {
                            //excel编号，特殊处理
                            value = index + 1;
                        } else {
                            value = param.get(k);
                        }
                    } else {
                        value = key;
                    }
                    row.getCell(colCount++).setCellValue(null == value?"":value+"");
                }
            }
        } catch (Exception e){
            log.error("写入文件失败，原因："+e.getMessage());
            throw new RuntimeException("写入文件失败");
        }
    }


    /**
     * 读取excel内容
     * @param file
     * @param ignoreRow
     * @return
     */
    public static HashMap<String, ArrayList<ArrayList<String>>> readExcel(File file, int ignoreRow) {
        if (file.getName().toLowerCase().endsWith(".xlsx")) {
            return readExcelForXlsx(file, ignoreRow);
        } else if (file.getName().toLowerCase().endsWith(".xls")) {
            return readExcelForXls(file, ignoreRow);
        }
        return null;
    }
    /**
     * 读取Excel xlsx后缀名文件数据
     *
     * @param file
     */
    private static HashMap<String, ArrayList<ArrayList<String>>> readExcelForXlsx(File file, int ignoreRow) {
        HashMap<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
        if (!file.exists()) {
            log.error("{}文件不存在", file.getName());
            return null;
        }
        int rowSize = 0;
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
            XSSFWorkbook workbook = null;
            try {
                workbook = new XSSFWorkbook(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            XSSFCell cell = null;
            for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);


                ArrayList<ArrayList<String>> lists = new ArrayList<>();
                for (int rowIndex = ignoreRow; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                    XSSFRow row = sheet.getRow(rowIndex);
                    if (null == row) {
                        continue;
                    }

                    int tempRowSize = row.getLastCellNum() + 1;
                    if (tempRowSize > rowSize) {
                        rowSize = tempRowSize;
                    }

                    ArrayList<String> list = new ArrayList<>();
                    int col = 0;

                    for (int colIndex = 0; colIndex <= row.getLastCellNum(); colIndex++) {
                        cell = row.getCell(colIndex);
                        String value = "";
                        if (cell != null) {
                            CellType cellType = cell.getCellType();

                            switch (cellType) {
                                case NUMERIC:
                                    if (isCellDateFormatted(cell)) {
                                        value = String.valueOf(cell.getDateCellValue());
                                    } else {
                                        value = String.valueOf(new DecimalFormat("0").format(cell.getNumericCellValue()));
                                    }
                                    break;
                                case STRING:
                                    value = String.valueOf(cell.getStringCellValue());
                                    break;
                                case FORMULA:
                                    value = String.valueOf(cell.getCellFormula());
                                    break;
                                case BLANK:
                                    value = "";
                                    break;
                                case BOOLEAN:
                                    value = String.valueOf(cell.getBooleanCellValue());
                                    break;
                                case ERROR:
                                    value = String.valueOf(cell.getErrorCellValue());
                                    break;
                                default:
                                    value = "";
                            }
                            if (StringUtils.isNotBlank(value)) {
                                list.add(value);
                            } else {
                                col++;
                            }
                        }
                    }
                    if (col == row.getRowNum()) {
                        continue;
                    }
                    if (list.size() > 0) {
                        lists.add(list);
                    }
                }

                map.put("sheet" + sheetIndex, lists);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    /**
     * 读取excel xls后缀名文件
     *
     * @param file
     * @param ignoreRow
     * @return
     */
    private static HashMap<String, ArrayList<ArrayList<String>>> readExcelForXls(File file, int ignoreRow) {
        HashMap<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
        if (!file.exists()) {
            log.error("{}文件不存在", file.getName());
            return null;
        }
        int rowSize = 0;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            HSSFWorkbook workbook = new HSSFWorkbook(bufferedInputStream);
            HSSFCell cell = null;
            for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
                HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
                ArrayList<ArrayList<String>> lists = new ArrayList<>();
                for (int rowIndex = ignoreRow; rowIndex < sheet.getLastRowNum(); rowIndex++) {
                    HSSFRow row = sheet.getRow(rowIndex);
                    if (null == row) {
                        continue;
                    }

                    int tempRowSize = row.getLastCellNum() + 1;
                    if (tempRowSize > rowSize) {
                        rowSize = tempRowSize;
                    }
                    ArrayList<String> list = new ArrayList<>();
                    int col = 0;
                    for (int colIndex = 0; colIndex < row.getLastCellNum(); colIndex++) {
                        cell = row.getCell(colIndex);
                        String value = "";
                        if (cell != null) {
                            CellType cellType = cell.getCellType();

                            switch (cellType) {
                                case NUMERIC:
                                    if (isCellDateFormatted(cell)) {
                                        value = String.valueOf(cell.getDateCellValue());
                                    } else {
                                        value = String.valueOf(new DecimalFormat("0").format(cell.getNumericCellValue()));
                                    }
                                    break;
                                case STRING:
                                    value = String.valueOf(cell.getStringCellValue());
                                    break;
                                case FORMULA:
                                    value = String.valueOf(cell.getCellFormula());
                                    break;
                                case BLANK:
                                    value = "";
                                    break;
                                case BOOLEAN:
                                    value = String.valueOf(cell.getBooleanCellValue());
                                    break;
                                case ERROR:
                                    value = String.valueOf(cell.getErrorCellValue());
                                    break;
                                default:
                                    value = "";
                            }
                            if (StringUtils.isNotBlank(value)) {
                                list.add(value);
                            } else {
                                col++;
                            }
                        }
                    }
                    if (col == row.getRowNum()) {
                        continue;
                    }
                    if (list.size() > 0) {
                        lists.add(list);
                    }
                }
                map.put("sheet" + sheetIndex, lists);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static boolean isCellDateFormatted(Cell cell) {
        if (cell == null) return false;
        boolean bDate = false;

        double d = cell.getNumericCellValue();
        if (isValidExcelDate(d)) {
            CellStyle style = cell.getCellStyle();
            if (style == null) return false;
            int i = style.getDataFormat();
            String f = style.getDataFormatString();
            bDate = isADateFormat(i, f);
        }
        return bDate;
    }

    public static boolean isADateFormat(int formatIndex, String formatString) {
        if (isInternalDateFormat(formatIndex)) {
            return true;
        }

        if ((formatString == null) || (formatString.length() == 0)) {
            return false;
        }

        String fs = formatString;
        //下面这一行是自己手动添加的 以支持汉字格式wingzing
        fs = fs.replaceAll("[\"|\']","").replaceAll("[年|月|日|时|分|秒|毫秒|微秒]", "");

        fs = fs.replaceAll("\\\\-", "-");

        fs = fs.replaceAll("\\\\,", ",");

        fs = fs.replaceAll("\\\\.", ".");

        fs = fs.replaceAll("\\\\ ", " ");

        fs = fs.replaceAll(";@", "");

        fs = fs.replaceAll("^\\[\\$\\-.*?\\]", "");

        fs = fs.replaceAll("^\\[[a-zA-Z]+\\]", "");

        return (fs.matches("^[yYmMdDhHsS\\-/,. :]+[ampAMP/]*$"));
    }

    public static boolean isInternalDateFormat(int format) {
        switch (format) { case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 45:
            case 46:
            case 47:
                return true;
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44: } return false;
    }

    public static boolean isValidExcelDate(double value) {
        return (value > -4.940656458412465E-324D);
    }
}
