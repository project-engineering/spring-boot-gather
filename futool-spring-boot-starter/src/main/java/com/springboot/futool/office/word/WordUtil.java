package com.springboot.futool.office.word;

import cn.afterturn.easypoi.word.WordExportUtil;
import cn.hutool.core.util.ObjectUtil;
import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import com.springboot.futool.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.wp.usermodel.HeaderFooterType;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.*;
import java.util.*;

/**
 * Word工具类
 */
@Log4j2
public class WordUtil {
    private static final String TEMPLATE_PATH = "templates/word/";
    private static final String TEMPLATE_FILE = "template.docx";
    /**
     * 导出word存放的路径
     */
    private static String LOCAL_PATH;
    /**
     * 导出word的文件名
     */
    private static String FILE_NAME;
    /**
     * 页眉
     */
    private static String HEADER;
    /**
     * 页脚
     */
    private static String FOOTER;

    /**
     * 2003- 版本的word
     */
    private static final String word2003L = ".doc";
    /**
     * 2007+ 版本的word
     */
    private final static String word2007U = ".docx";
    /**
     * pdf 文件后缀
     */
    private final static String PDF = ".pdf";
    public static void export(Map<String, Object> map, String url, File tempFile) {
        try {
            XWPFDocument doc = WordExportUtil.exportWord07(url, map);
            FileOutputStream fos = new FileOutputStream(tempFile);
            doc.write(fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 按照word模板导出word
     * @param template
     * @param localPath
     * @param fileName
     * @param map
     */
    public static void export(String template, String localPath, String fileName, Map<String, Object> map) {
        try {
            String url = getTemplateFile(template).getAbsolutePath();
            String filePath;
            if (ObjectUtil.isNotEmpty(localPath)) {
                filePath = localPath + File.separator + fileName;
            } else {
                filePath = getPathByName(fileName);
            }
            File file = new File(filePath);
            if (!file.exists()) {
                // 创建文件夹
                File parentDir = file.getParentFile();
                if (!parentDir.exists() && !parentDir.mkdirs()) {
                    log.error("创建文件夹失败，路径[{}]", parentDir.getAbsolutePath());
                    throw new IOException("创建文件夹失败");
                }

                if (!file.createNewFile()) {
                    log.error("创建文件失败，路径[{}]", file.getAbsolutePath());
                    throw new IOException("创建文件失败");
                }
            }

            XWPFDocument doc = WordExportUtil.exportWord07(url, map);

            // 添加页眉
            addHeader(doc,"我是页眉");

            // 添加页脚
            addFooter(doc);

            FileOutputStream fos = new FileOutputStream(filePath);
            doc.write(fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static File getTemplateFile(String template) throws IOException {
        Resource resource =new ClassPathResource("/template/word/"+template);
        return  resource.getFile();
    }

    /**
     * 通过文件名获取文件路径
     * @param fileName
     * @return
     */
    public static String getPathByName(String fileName){
        return "E:\\data\\"+ DateUtil.convertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_3)+"\\"+fileName;
    }

    private static void addHeader(XWPFDocument doc,String headerText) {
        XWPFHeader header = doc.createHeader(HeaderFooterType.DEFAULT);
        XWPFParagraph paragraphHeader = header.getParagraphArray(0);
        if (paragraphHeader == null) {
            paragraphHeader = header.createParagraph();
        }
        paragraphHeader.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runHeader = paragraphHeader.createRun();
        runHeader.setText(headerText);
    }

    /**
     * 添加页脚
     * @param doc
     */
    private static void addFooter(XWPFDocument doc) {
        // 添加页脚
        XWPFFooter footer = doc.createFooter(HeaderFooterType.DEFAULT);
        XWPFParagraph paragraphFooter = footer.createParagraph();
        paragraphFooter.setAlignment(ParagraphAlignment.CENTER);

        // 添加 "第" 字
        XWPFRun runFirst = paragraphFooter.createRun();
        runFirst.setText("第");

        // 添加页码
        XWPFRun pageNumberRun = paragraphFooter.createRun();
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.BEGIN);
        pageNumberRun.getCTR().addNewInstrText().setStringValue("PAGE \\* MERGEFORMAT");
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.SEPARATE);
        pageNumberRun.getCTR().addNewFldChar().setFldCharType(STFldCharType.END);
        // 添加 "页" 字
        XWPFRun runLast = paragraphFooter.createRun();
        runLast.setText("页");
    }

    /**
     * 将Word文档转换为PDF
     *
     * <p>
     * 支持 Word2003、Word2007+ 版本的Word文档转换为PDF
     * <p>
     * 注意：
     *    通过documents4j 实现word转pdf -- linux 环境 需要有 libreoffice 服务
     *
     * @param inputWordFile word文件地址 如 /root/example.doc
     * @param outputPdfFile pdf文件存放地址 如 /root/example.pdf
     */
    public static void convertWordToPdf(String inputWordFile, String outputPdfFile) {
        log.info("[documents4J] 开始将word文件转换为pdf文件");
        if (ObjectUtil.isEmpty(inputWordFile) || ObjectUtil.isEmpty(outputPdfFile)){
            throw new IllegalArgumentException("输入参数不能为空");
        }
        if (!inputWordFile.endsWith(word2003L) &&!inputWordFile.endsWith(word2007U)) {
            throw new IllegalArgumentException("输入文件格式不正确，仅支持doc和docx格式");
        }
        if (!outputPdfFile.endsWith(PDF)) {
            throw new IllegalArgumentException("输出文件格式不正确，仅支持pdf格式");
        }
        InputStream docxInputStream = null;
        OutputStream outputStream = null;
        try {
            // Word文档输入流
            docxInputStream = new FileInputStream(inputWordFile);

            // 转换后pdf生成地址
            outputStream = new FileOutputStream(outputPdfFile);
            // 创建转换器
            IConverter converter = LocalConverter.builder().build();
            // 转换
            if (inputWordFile.endsWith(word2003L)) {
                converter.convert(docxInputStream)
                        .as(DocumentType.DOC)
                        .to(outputStream)
                        .as(DocumentType.PDF).execute();
            }
            if (inputWordFile.endsWith(word2007U)) {
                converter.convert(docxInputStream)
                        .as(DocumentType.DOCX)
                        .to(outputStream)
                        .as(DocumentType.PDF)
                        .execute();
            }
            // 关闭转换器
            converter.shutDown();
            // 关闭输出流
            outputStream.close();
            // 关闭输入流
            docxInputStream.close();
        } catch (Exception e) {
            log.info("[documents4J] word转pdf失败:{}" + e.getMessage());
            throw new RuntimeException("[documents4J] word转pdf失败:{}",e);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (docxInputStream != null) {
                    docxInputStream.close();
                }
            } catch (IOException e) {
                log.info("[documents4J] word转pdf关闭流失败:{}" + e.getMessage());
                throw new RuntimeException("[documents4J] word转pdf关闭流失败:{}",e);
            }
        }
        log.info("[documents4J] word转pdf成功:{}",outputPdfFile);
    }
}
