package com.springboot.futool.office.excel;

import com.springboot.futool.UtilValidate;
import com.springboot.futool.collection.ListUtil;
import com.springboot.futool.exception.BusinessException;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.FileMagic;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import com.springboot.futool.DateUtil;

@Log4j2
public class ExcelUtil {
    /**
     * excel模板文件路径
     */
    public static final String EXCEL_MODEL_PATH = "classpath:template/";
    /**
     * excel模板编号赋值键
     */
    public static final String EXCEL_ROWNUM_KEY = "rn";

    private static final Map<String, String> cellValueCache = new HashMap<>();

    /**
     * 检测是否存在指定Sheet
     * @param path
     * @param sheet
     * @return
     */
    public static boolean checkSheetExist(String path,String sheet){
        File f = new File(path);
        return checkSheetExist(f,sheet);
    }

    /**
     *
     * @param path
     * @return
     */
    public static boolean checkSheetExit(String path){
        File f = new File(path);
        return checkSheetExist(f);
    }

    /**
     * 检查是否存在指定Sheet
     * @param file
     * @param sheet
     * @return
     */
    @SneakyThrows
    public static boolean checkSheetExist(File file, String sheet){
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(file);
            if (checkExcelIs2007(file)) {
                return checkSheetExistXls2007(inputStream,sheet);
            } else {
                return checkSheetExistXls(inputStream,sheet);
            }
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
    }

    /**
     * 检测是否存在指定excel文件
     * @param file
     * @return
     */
    @SneakyThrows
    public static boolean checkSheetExist(File file){
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(file);
            if (checkExcelIs2007(file)) {
                return checkSheetExistXls2007(inputStream);
            } else {
                return checkSheetExistXls(inputStream);
            }
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
    }

    /**
     * 检测是否是2007版本的excel文件
     * @param path
     * @return
     */
    public static boolean checkExcelIs2007(String path){
        File f = new File(path);
        return checkExcelIs2007(f);
    }

    /**
     * 检测是否是2007版本的excel文件
     * @param file
     * @return
     */
    @SneakyThrows
    public static boolean checkExcelIs2007(File file){
        InputStream inputStream = new FileInputStream(file);
        return checkExcelIs2007(inputStream);
    }

    /**
     * 检测文件格式是否是xls或者xlsx
     * @param inputStream
     * @return
     */
    @SneakyThrows
    public static boolean checkExcelIs2007(InputStream inputStream){
        InputStream is = FileMagic.prepareToCheckMagic(inputStream);
        FileMagic fm = FileMagic.valueOf(is);
        switch (fm) {
            case OLE2:
                return false;
            case OOXML:
                return true;
            default:
                throw new IOException("文件格式无法识别，请使用xls,xlsx格式");
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls2007(InputStream inputStream , String sheet){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return (book.getSheetIndex(sheet)>=0);
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls2007(InputStream inputStream){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return book.sheetIterator().hasNext();
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls(InputStream inputStream, String sheet){
        HSSFWorkbook book = null;
        try {
            book = new HSSFWorkbook(inputStream);
            return (book.getSheetIndex(sheet)>=0);
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    @SneakyThrows
    public static boolean checkSheetExistXls(InputStream inputStream){
        XSSFWorkbook book = null;
        try {
            book = new XSSFWorkbook(inputStream);
            return book.sheetIterator().hasNext();
        } finally {
            if (book == null) {
                book.close();
            }
        }
    }

    /**
     * 通过文件名获取文件路径
     * @param fileName
     * @return
     */
    public static String getPathByName(String fileName){
        return "E:\\home\\"+ DateUtil.convertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_3)+"\\"+fileName;
    }

    /**
     * 根据模板的路径获取模板文件对象
     * classpath根目录下：/template/report.xlsx
     */
    private static File getTemplateFile(String template) throws IOException {
        Resource resource =new ClassPathResource("/template/"+template);
        return  resource.getFile();
    }

    /**
     * 读取excel模板，并导出excel
     * 模板最后一行必须是取值代码行，如：${name}
     * @param modelFileName excel模板名
     * @param fileName 导出的文件名，注意不是路径，仅仅是文件名
     * @param param 接口滴哦用参数 包含动态表头参数（接口调用后会删除） Map<String,Object> headerParam
     *  包含动态表头参数（接口调用后会删除） Map<String,Object> param
     * @param columnsToMerge 需要合并的列，如：{"0","1"}
     * @param groupBy 将需要合并的列按照某个值进行分组后再合并，如：{"key"}
     * @return
     */
    @SneakyThrows
    public static Map<String, Object> export(String modelFileName, String fileName, Map<String, Object> param, String[] columnsToMerge,String groupBy) {
        log.info("导出excel，模板路径[{}]",modelFileName);
        File modelFile = getTemplateFile(modelFileName);
        if (!modelFile.exists()) {
            log.error("未找到模板文件，模板路径[" + modelFileName + "]");
            throw new IOException("未找到模板文件");
        }

        String filePath = getPathByName(fileName);
        File file = new File(filePath);
        if (!file.exists()) {
            // 创建文件夹
            File parentDir = file.getParentFile();
            if (!parentDir.exists() && !parentDir.mkdirs()) {
                log.error("创建文件夹失败，路径[{}]",parentDir.getAbsolutePath());
                throw new IOException("创建文件夹失败");
            }

            if (!file.createNewFile()) {
                log.error("创建文件失败，路径[{}]",file.getAbsolutePath());
                throw new IOException("创建文件失败");
            }
        }

        XSSFWorkbook workbook = null;
        try (FileInputStream fileInputStream = new FileInputStream(modelFile);
             FileOutputStream stream = new FileOutputStream(filePath)) {
            workbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);

            if (UtilValidate.isNotEmpty(param.get("headerParam"))) {
                exportHeader(sheet, (Map<String, Object>) param.get("headerParam"));
                param.remove("headerParam");
            }

            export(sheet, param, columnsToMerge,groupBy);
            workbook.write(stream);
        } catch (IOException e) {
            log.error("写入文件失败，原因：" + e.getMessage());
            throw new RuntimeException("写入文件失败");
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error("关闭workbook失败，原因：" + e.getMessage());
                }
            }
        }

        Map<String, Object> successMap = new HashMap<>();
        successMap.put("fileName", fileName);
        successMap.put("filePath", filePath);
        log.info("导出成功，文件路径为：{}",filePath);
        return successMap;
    }

    /**
     * 导出数据部分，并合并指定列中数据重复的单元格
     * @param sheet
     * @param param
     * @param columnsToMerge
     */
    private static void export(XSSFSheet sheet, Map<String, Object> param, String[] columnsToMerge,String groupBy) {
        List<Map<String, Object>> pageList = (List<Map<String, Object>>) param.get("pageList");
        if (UtilValidate.isEmpty(pageList)) {
            return;
        }

        // 获取参数行
        int lastRowNum = sheet.getLastRowNum();
        Row row = sheet.getRow(lastRowNum);
        List<String> headerList = new ArrayList<>();
        for (Cell cell : row) {
            headerList.add(cell.getStringCellValue());
        }

        int rowCount = lastRowNum + 1;
        if (StringUtils.isNotEmpty(groupBy)) {
            pageList = ListUtil.groupByKeys(pageList,groupBy);
        }
        for (Map<String, Object> info : pageList) {
            boolean hasData = false;
            for (String key : headerList) {
                // 判断是否为动态列
                if (key.contains("${")) {
                    String k = key.replace("${", "").replace("}", "");
                    if (!UtilValidate.areEqual(EXCEL_ROWNUM_KEY, k)) {
                        if (info.get(k) != null) {
                            hasData = true;
                            break;
                        }
                    }
                } else {
                    hasData = true;
                    break;
                }
            }
            if (hasData) {
                // 创建新行
                Row newRow = sheet.createRow(rowCount);
                // 设置行高、行宽等属性
                newRow.setHeight(row.getHeight());
                newRow.setHeightInPoints(row.getHeightInPoints());
                newRow.setZeroHeight(row.getZeroHeight());
                newRow.setRowStyle(row.getRowStyle());

                int colCount = 0;
                for (String key : headerList) {
                    Object value;
                    if (UtilValidate.isEmpty(key)) {
                        value = "";
                    } else if (key.contains("${")) {
                        String k = key.replace("${", "").replace("}", "");
                        if (UtilValidate.areEqual(EXCEL_ROWNUM_KEY, k)) {
                            // 处理特殊列
                            value = rowCount - lastRowNum + 1;
                        } else {
                            value = info.get(k);
                        }
                    } else {
                        value = key;
                    }

                    Cell newCell = newRow.createCell(colCount);
                    Cell oldCell = row.getCell(colCount);
                    newCell.setCellStyle(oldCell.getCellStyle());
                    newCell.setCellValue(null == value ? "" : value.toString());
                    colCount++;
                }
                rowCount++;
            }
        }

        // 合并单元格
        if (UtilValidate.isNotEmpty(columnsToMerge)) {
            try {
                for (String columnToMerge : columnsToMerge) {
                    int columnIndex = Integer.parseInt(columnToMerge);
                    mergeCells(sheet, columnIndex, lastRowNum + pageList.size(), 100);
                }
            } catch (Exception e) {
                log.error("合并单元格时发生异常", e);
                throw new BusinessException("合并单元格时发生异常,异常原因：{}", e);
            }
        }

        // 删除模板的取数表达式行
        deleteTemplateRow(sheet, row.getRowNum());
    }

    /**
     * 合并指定列相同值的单元格
     * @param sheet
     * @param columnIndex
     * @param numRows
     * @param batchSize
     */
    public static void mergeCells(Sheet sheet, int columnIndex, int numRows, int batchSize) {
        List<Map.Entry<String, CellRangeAddress>> entriesToMerge = new ArrayList<>();
        int batchCount = 0;
        Row prevRow = sheet.getRow(0);
        // 获取指定列所有单元格的数据，并放到缓存中
        String prevCellValue = getCellValue(prevRow.getCell(columnIndex));
        // 遍历所有行
        for (int i = 1; i <= numRows; i++) {
            Row currentRow = sheet.getRow(i);

            if (currentRow == null) {
                currentRow = sheet.createRow(i);
            }

            Cell currentCell = currentRow.getCell(columnIndex);
            // 获取当前单元格的值
            String currentCellValue = getCellValue(currentCell);
            // 如果当前单元格的值与上一行的值相同，则将其与上一行合并
            if (currentCellValue.equals(prevCellValue)) {
                // 合并单元格
                if (entriesToMerge.isEmpty()) {
                    CellRangeAddress newRange = new CellRangeAddress(prevRow.getRowNum(), currentRow.getRowNum(), columnIndex, columnIndex);
                    entriesToMerge.add(new AbstractMap.SimpleEntry<>(prevCellValue, newRange));
                } else {
                    // 更新最后一个合并单元格的结束行
                    Map.Entry<String, CellRangeAddress> lastEntry = entriesToMerge.get(entriesToMerge.size() - 1);
                    CellRangeAddress lastRange = lastEntry.getValue();
                    if (lastRange.getLastRow() + 1 == currentRow.getRowNum()) {
                        // 更新最后一个合并单元格的结束行
                        lastRange.setLastRow(currentRow.getRowNum());
                    } else {
                        // 创建新的合并单元格
                        CellRangeAddress newRange = new CellRangeAddress(prevRow.getRowNum(), currentRow.getRowNum(), columnIndex, columnIndex);
                        entriesToMerge.add(new AbstractMap.SimpleEntry<>(prevCellValue, newRange));
                    }
                }
            }

            prevRow = currentRow;
            prevCellValue = currentCellValue;

            batchCount++;
            // 达到批处理大小或最后一行时进行合并
            if (batchCount >= batchSize || i == numRows) {
                // 合并单元格
                mergeEntries(sheet, entriesToMerge);
                // 清空临时列表
                entriesToMerge.clear();
                // 重置批处理计数器
                batchCount = 0;
            }
        }
    }

    /**
     * 合并单元格
     *
     * @param sheet
     * @param entries
     */
    private static void mergeEntries(Sheet sheet, List<Map.Entry<String, CellRangeAddress>> entries) {
        for (Map.Entry<String, CellRangeAddress> entry : entries) {
            CellRangeAddress range = entry.getValue();
            boolean overlapping = false;
            // 检查合并单元格是否重叠
            for (CellRangeAddress existingRange : sheet.getMergedRegions()) {
                if (range.isInRange(existingRange.getFirstRow(), existingRange.getFirstColumn())
                        || range.isInRange(existingRange.getLastRow(), existingRange.getLastColumn())
                        || existingRange.isInRange(range.getFirstRow(), range.getFirstColumn())) {
                    overlapping = true;
                    break;
                }
            }
            // 如果没有重叠，则合并单元格
            if (!overlapping && range.getLastRow() - range.getFirstRow() >= 1) {
                sheet.addMergedRegion(range);
            }
        }
    }

    /**
     * 获取单元格的数据，并放到缓存中
     * @param cell
     * @return
     */
    private static String getCellValue(Cell cell) {
        if (cell == null) {
            return "";
        }

        String cellValue = cell.getStringCellValue();
        if (cellValueCache.containsKey(cellValue)) {
            return cellValueCache.get(cellValue);
        } else {
            cellValueCache.put(cellValue, cellValue);
            return cellValue;
        }
    }

    /**
     * 删除模板的取数表达式行
     * @param sheet
     * @param rowNumber
     */
    private static void deleteTemplateRow(XSSFSheet sheet, int rowNumber) {
        sheet.shiftRows(rowNumber + 1, sheet.getLastRowNum(), -1);
        Row row = sheet.getRow(sheet.getLastRowNum());
        if (row != null) {
            sheet.removeRow(row);
        }
    }

    /**
     * 导出表头中的内容
     * @param sheet
     * @param param
     */
    private static void exportHeader(XSSFSheet sheet,Map<String,Object> param){
        try {
            int lastRowNum = sheet.getLastRowNum();
            for (int index = 0; index < lastRowNum; index++) {
                Row row = sheet.getRow(index);
                Iterator<Cell> iterator = row.iterator();
                int colCount = 0;
                while (iterator.hasNext()){
                    Cell next = iterator.next();
                    String key = next.getStringCellValue();
                    Object value;
                    if (UtilValidate.isEmpty(key)) {
                        value = "";
                    } else if (key.contains("${")){
                        String k = key.replace("${","").replace("}","");
                        if (UtilValidate.areEqual(EXCEL_ROWNUM_KEY,k)) {
                            //excel编号，特殊处理
                            value = index + 1;
                        } else {
                            value = param.get(k);
                        }
                    } else {
                        value = key;
                    }
                    row.getCell(colCount++).setCellValue(null == value?"":value+"");
                }
            }
        } catch (Exception e){
            log.error("写入文件失败，原因："+e.getMessage());
            throw new RuntimeException("写入文件失败");
        }
    }


    /**
     * 读取excel内容
     * @param file
     * @param ignoreRow
     * @return
     */
    public static HashMap<String, ArrayList<ArrayList<String>>> readExcel(File file, int ignoreRow) {
        if (file.getName().toLowerCase().endsWith(".xlsx")) {
            return readExcelForXlsx(file, ignoreRow);
        } else if (file.getName().toLowerCase().endsWith(".xls")) {
            return readExcelForXls(file, ignoreRow);
        }
        return null;
    }
    /**
     * 读取Excel xlsx后缀名文件数据
     *
     * @param file
     */
    private static HashMap<String, ArrayList<ArrayList<String>>> readExcelForXlsx(File file, int ignoreRow) {
        HashMap<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
        if (!file.exists()) {
            log.error("{}文件不存在", file.getName());
            return null;
        }
        int rowSize = 0;
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
            XSSFWorkbook workbook = null;
            try {
                workbook = new XSSFWorkbook(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            XSSFCell cell = null;
            for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);


                ArrayList<ArrayList<String>> lists = new ArrayList<>();
                for (int rowIndex = ignoreRow; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                    XSSFRow row = sheet.getRow(rowIndex);
                    if (null == row) {
                        continue;
                    }

                    int tempRowSize = row.getLastCellNum() + 1;
                    if (tempRowSize > rowSize) {
                        rowSize = tempRowSize;
                    }

                    ArrayList<String> list = new ArrayList<>();
                    int col = 0;

                    for (int colIndex = 0; colIndex <= row.getLastCellNum(); colIndex++) {
                        cell = row.getCell(colIndex);
                        String value = "";
                        if (cell != null) {
                            CellType cellType = cell.getCellType();

                            switch (cellType) {
                                case NUMERIC:
                                    if (isCellDateFormatted(cell)) {
                                        value = String.valueOf(cell.getDateCellValue());
                                    } else {
                                        value = String.valueOf(new DecimalFormat("0").format(cell.getNumericCellValue()));
                                    }
                                    break;
                                case STRING:
                                    value = String.valueOf(cell.getStringCellValue());
                                    break;
                                case FORMULA:
                                    value = String.valueOf(cell.getCellFormula());
                                    break;
                                case BLANK:
                                    value = "";
                                    break;
                                case BOOLEAN:
                                    value = String.valueOf(cell.getBooleanCellValue());
                                    break;
                                case ERROR:
                                    value = String.valueOf(cell.getErrorCellValue());
                                    break;
                                default:
                                    value = "";
                            }
                            if (UtilValidate.isNotEmpty(value)) {
                                list.add(value);
                            } else {
                                col++;
                            }
                        }
                    }
                    if (col == row.getRowNum()) {
                        continue;
                    }
                    if (list.size() > 0) {
                        lists.add(list);
                    }
                }

                map.put("sheet" + sheetIndex, lists);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    /**
     * 读取excel xls后缀名文件
     *
     * @param file
     * @param ignoreRow
     * @return
     */
    private static HashMap<String, ArrayList<ArrayList<String>>> readExcelForXls(File file, int ignoreRow) {
        HashMap<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
        if (!file.exists()) {
            log.error("{}文件不存在", file.getName());
            return null;
        }
        int rowSize = 0;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            HSSFWorkbook workbook = new HSSFWorkbook(bufferedInputStream);
            HSSFCell cell = null;
            for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
                HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
                ArrayList<ArrayList<String>> lists = new ArrayList<>();
                for (int rowIndex = ignoreRow; rowIndex < sheet.getLastRowNum(); rowIndex++) {
                    HSSFRow row = sheet.getRow(rowIndex);
                    if (null == row) {
                        continue;
                    }

                    int tempRowSize = row.getLastCellNum() + 1;
                    if (tempRowSize > rowSize) {
                        rowSize = tempRowSize;
                    }
                    ArrayList<String> list = new ArrayList<>();
                    int col = 0;
                    for (int colIndex = 0; colIndex < row.getLastCellNum(); colIndex++) {
                        cell = row.getCell(colIndex);
                        String value = "";
                        if (cell != null) {
                            CellType cellType = cell.getCellType();

                            switch (cellType) {
                                case NUMERIC:
                                    if (isCellDateFormatted(cell)) {
                                        value = String.valueOf(cell.getDateCellValue());
                                    } else {
                                        value = String.valueOf(new DecimalFormat("0").format(cell.getNumericCellValue()));
                                    }
                                    break;
                                case STRING:
                                    value = String.valueOf(cell.getStringCellValue());
                                    break;
                                case FORMULA:
                                    value = String.valueOf(cell.getCellFormula());
                                    break;
                                case BLANK:
                                    value = "";
                                    break;
                                case BOOLEAN:
                                    value = String.valueOf(cell.getBooleanCellValue());
                                    break;
                                case ERROR:
                                    value = String.valueOf(cell.getErrorCellValue());
                                    break;
                                default:
                                    value = "";
                            }
                            if (UtilValidate.isNotEmpty(value)) {
                                list.add(value);
                            } else {
                                col++;
                            }
                        }
                    }
                    if (col == row.getRowNum()) {
                        continue;
                    }
                    if (list.size() > 0) {
                        lists.add(list);
                    }
                }
                map.put("sheet" + sheetIndex, lists);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 判断是否是日期格式
     * @param cell
     * @return
     */
    public static boolean isCellDateFormatted(Cell cell) {
        if (cell == null) return false;
        boolean bDate = false;

        double d = cell.getNumericCellValue();
        if (isValidExcelDate(d)) {
            CellStyle style = cell.getCellStyle();
            if (style == null) return false;
            int i = style.getDataFormat();
            String f = style.getDataFormatString();
            bDate = isADateFormat(i, f);
        }
        return bDate;
    }

    /**
     * 判断是否是有效日期
     * @param formatIndex
     * @param formatString
     * @return
     */
    public static boolean isADateFormat(int formatIndex, String formatString) {
        if (isInternalDateFormat(formatIndex)) {
            return true;
        }

        if ((formatString == null) || (formatString.length() == 0)) {
            return false;
        }

        String fs = formatString;
        //下面这一行是自己手动添加的 以支持汉字格式wingzing
        fs = fs.replaceAll("[\"|\']","").replaceAll("[年|月|日|时|分|秒|毫秒|微秒]", "");

        fs = fs.replaceAll("\\\\-", "-");

        fs = fs.replaceAll("\\\\,", ",");

        fs = fs.replaceAll("\\\\.", ".");

        fs = fs.replaceAll("\\\\ ", " ");

        fs = fs.replaceAll(";@", "");

        fs = fs.replaceAll("^\\[\\$\\-.*?\\]", "");

        fs = fs.replaceAll("^\\[[a-zA-Z]+\\]", "");

        return (fs.matches("^[yYmMdDhHsS\\-/,. :]+[ampAMP/]*$"));
    }

    /**
     * 判断是否是内部日期格式
     * @param format
     * @return
     */
    public static boolean isInternalDateFormat(int format) {
        switch (format) { case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 45:
            case 46:
            case 47:
                return true;
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44: } return false;
    }

    /**
     * 判断是否是合法的Excel日期格式
     * @param value
     * @return
     */
    public static boolean isValidExcelDate(double value) {
        return (value > -4.940656458412465E-324D);
    }
}

