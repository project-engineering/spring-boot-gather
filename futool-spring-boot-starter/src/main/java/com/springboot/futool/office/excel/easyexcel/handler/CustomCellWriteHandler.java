package com.springboot.futool.office.excel.easyexcel.handler;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.fastjson2.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.util.ObjectUtils;
import org.springframework.util.PropertyPlaceholderHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 自定义单元格写入处理器
 */
public class CustomCellWriteHandler implements CellWriteHandler {
    /**
     * 添加超链接的字段的下标
     */
    private int[] mergeColumnIndex;
    /**
     * 从第几行开始添加，0代表标题行
     */
    private int mergeRowIndex;
    private JSONObject headTitle;

    PropertyPlaceholderHelper placeholderHelper = new PropertyPlaceholderHelper("${", "}");

    public CustomCellWriteHandler(JSONObject headTitle) {
        this.headTitle = headTitle;
    }

    public CustomCellWriteHandler(int mergeRowIndex, int[] mergeColumnIndex) {
        this.mergeRowIndex = mergeRowIndex;
        this.mergeColumnIndex = mergeColumnIndex;
    }

    /**
     * 动态设置表头字段
     * @param writeSheetHolder
     * @param writeTableHolder
     * @param row
     * @param head
     * @param columnIndex
     * @param relativeRowIndex
     * @param isHead
     */
    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row, Head head, Integer columnIndex, Integer relativeRowIndex, Boolean isHead) {
        // 动态设置表头字段
        if (!ObjectUtils.isEmpty(head)) {
            List<String> headNameList = head.getHeadNameList();
            if (CollectionUtils.isNotEmpty(headNameList)) {
                ArrayList<Properties> propertiesList = new ArrayList<>();
                for (String key : headTitle.keySet()) {
                    Properties properties = new Properties();
                    properties.setProperty(key, headTitle.getString(key));
                    propertiesList.add(properties);
                }
                for (int i = 0; i < headNameList.size(); i++) {
                    for (Properties properties : propertiesList) {
                        //表头中如果有${}设置的单元格，则可以自定义赋值。 根据构造方法传入的jsonObject对象
                        headNameList.set(i, placeholderHelper.replacePlaceholders(headNameList.get(i), properties));
                    }
                }
            }
        }
    }


    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer integer, Boolean aBoolean) {

    }

    @Override
    public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, WriteCellData<?> cellData, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

    }

    /**
     * 动态添加超链接
     * @param writeSheetHolder
     * @param writeTableHolder
     * @param cellDataList
     * @param cell
     * @param head
     * @param relativeRowIndex
     * @param isHead
     */
    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        //当前行
        int curRowIndex = cell.getRowIndex();
        //当前列
        int curColIndex = cell.getColumnIndex();

        if (curRowIndex >= mergeRowIndex) {
            for (int i = 0; i < mergeColumnIndex.length; i++) {
                if (curColIndex == mergeColumnIndex[i]) {
                    mergeWithPrevRow(writeSheetHolder, cell, curRowIndex, curColIndex);
                    break;
                }
            }
        }
    }

    private void mergeWithPrevRow(WriteSheetHolder writeSheetHolder, Cell cell, int curRowIndex, int curColIndex) {
        int sheetNo = writeSheetHolder.getSheetNo();
        if (0 == sheetNo) {
            Row row =  writeSheetHolder.getSheet().getRow(curRowIndex);
            Cell prevCell = row.getCell(1);
            String prevCellValue = prevCell.getStringCellValue();
            //获得超链接，当前单元格的内容就是一个超链接
            if (StrUtil.isNotBlank(prevCellValue)) {
                CreationHelper createHelper = writeSheetHolder.getSheet().getWorkbook().getCreationHelper();
                //创建超链接
                //HyperlinkType.DOCUMENT 表示文档中的位置
                Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                //设置超链接的地址
                hyperlink.setAddress("'"+prevCellValue+"'!A1");
                //添加超链接
                cell.setHyperlink(hyperlink);
            }
        }else {
            Row row =  writeSheetHolder.getSheet().getRow(curRowIndex);
            Cell prevCell = row.getCell(0);
            String prevCellValue = prevCell.getStringCellValue();
            //获得超链接，当前单元格的内容就是一个超链接
            if (StrUtil.isNotBlank(prevCellValue)) {
                CreationHelper createHelper = writeSheetHolder.getSheet().getWorkbook().getCreationHelper();
                //创建超链接
                //HyperlinkType.DOCUMENT 表示文档中的位置
                Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                //设置超链接的地址
                hyperlink.setAddress("'目录'!A1");
                //添加超链接
                cell.setHyperlink(hyperlink);
            }
        }

    }
}