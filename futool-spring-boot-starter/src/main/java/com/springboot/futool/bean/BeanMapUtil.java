package com.springboot.futool.bean;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ObjectUtils;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 利用反射对Java对象和Map相互转化
 */
@Log4j2
public class BeanMapUtil {
	
	/**
	 * 将Map转化为Object返回泛型类型
	 * @param map 源数据
	 * @param beanClass 目标对象类型
	 * @return T 目标对象
	 */
	public static <T> T mapToBean(Map<String, Object> map, Class<T> beanClass){
		Object obj = mapToObject(map, beanClass);
		return beanClass.cast(obj);
	}

	
	/**
	 * 将Map转化为Object
	 * @param map 源数据
	 * @param beanClass 目标对象类型
	 * @return Object 目标对象
	 */
	public static <T> T mapToObject(Map<String, Object> map, Class<T> beanClass) {
		if (ObjectUtils.isEmpty(map)) {
			return null;
		}
		try {
			T obj = beanClass.getDeclaredConstructor().newInstance();
			BeanUtils.populate(obj, map);
			return obj;
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("转换失败,原因：{}", e.getMessage());
			throw new RuntimeException("转换失败,原因：{}", e);
		}
	}
	
	/**
	 * 将Object转化为Map
	 * @param obj 源数据
	 * @return Map<String,Object> 目标Map
	 */
	public static Map<String, Object> objToMap(Object obj) {
		if (ObjectUtils.isEmpty(obj)) {
			return null;
		}
		try {
			return Arrays.stream(obj.getClass().getDeclaredFields())
					.peek(field -> field.setAccessible(true))
					.collect(Collectors.toMap(Field::getName, field -> {
						try {
							return field.get(obj);
						} catch (IllegalAccessException e) {
							log.error("获取字段值失败,原因：{}",e.getMessage());
							throw new RuntimeException("获取字段值失败,原因：{}", e);
						}
					}));
		} catch (Exception e) {
			log.error("转换失败,原因：{}" ,e.getMessage());
			throw new RuntimeException("转换失败,原因：{}", e);
		}
	}

	/**
	 * 将Object转化为Map
	 * @param obj 源数据
	 * @return Map<String,Object> 目标Map
	 */
	public static Map<String, Object> beanToMap(Object obj) {
		if (ObjectUtils.isEmpty(obj)) {
			return null;
		}

		Map<String, Object> map = new HashMap<>();
		Field[] declaredFields = obj.getClass().getDeclaredFields();
		// 添加需要排除的字段名
		Set<String> excludedFields = new HashSet<>(List.of("serialVersionUID"));

		for (Field field : declaredFields) {
			if (excludedFields.contains(field.getName())) {
				// 跳过需要排除的字段
				continue;
			}

			field.setAccessible(true);
			try {
				map.put(field.getName(), field.get(obj));
			} catch (IllegalAccessException e) {
				throw new IllegalArgumentException("无法访问字段值: " + field.getName(), e);
			}
		}
		return map;
	}

}
