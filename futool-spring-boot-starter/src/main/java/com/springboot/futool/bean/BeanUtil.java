package com.springboot.futool.bean;

import com.springboot.futool.UtilValidate;
import com.springboot.futool.annotation.Convert;
import com.springboot.futool.converter.Converter;
import lombok.extern.log4j.Log4j2;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Bean工具类
 */
@Log4j2
public class BeanUtil {
    /**
     * 将对象转换为List<Map>
     * @param list 待转换的对象列表
     * @param clazz 对象类型
     * @return 转换后的List<Map>
     */
    public static <T> List<Map<String, Object>> convertList(List<T> list, Class<T> clazz) {
        if (UtilValidate.isEmpty(list) || UtilValidate.isEmpty(clazz)) {
            throw new IllegalArgumentException("list or clazz cannot be null");
        }

        List<Map<String, Object>> mapList = new ArrayList<>();

        for (T obj : list) {
            Map<String, Object> map = new HashMap<>();
            Field[] fields = clazz.getDeclaredFields();

            for (Field field : fields) {
                // 设置字段可访问
                field.setAccessible(true);
                String fieldName = field.getName();
                // 获取字段的值
                Object value = getFieldValue(obj, field);
                // 添加到map中
                map.put(fieldName, value);
            }
            // 添加到list中
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * 获取字段的值
     * @param obj 对象
     * @param field 字段
     * @return 字段的值
     */
    private static <T> Object getFieldValue(T obj, Field field) {
        try {
            // 设置字段可访问
            field.setAccessible(true);
            if (field.isAnnotationPresent(Convert.class)) {
                Convert annotation = field.getAnnotation(Convert.class);
                Converter converter = annotation.value().getDeclaredConstructor().newInstance();
                return converter.convert(field.get(obj));
            } else {
                return field.get(obj);
            }
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            log.error("获取字段值失败,原因：{}", e.getMessage());
            throw new RuntimeException("获取字段值失败,原因：{}", e);
        }
    }
}
