package com.springboot.futool.converter;

import com.springboot.futool.enums.IdTypeEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * 下载excel时，idType码值转换成对应的中文描述
 */
@Slf4j
public class IdTypeConverter implements Converter {
    @Override
    public String convert(Object value) {
        return IdTypeEnum.getDesc((String) value);
    }
}