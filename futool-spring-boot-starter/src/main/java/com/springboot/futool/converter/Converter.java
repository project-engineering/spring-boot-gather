package com.springboot.futool.converter;

/**
 * 类型转换器
 */
public interface Converter {
    Object convert(Object value);
}