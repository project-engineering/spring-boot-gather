package com.springboot.futool;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Log4j2
class BigDecimalUtilTest {

    @Test
    void bigDecimalFormatToFinance() {
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),0));
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),1));
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),2));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1000000.0"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000.0"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000.00"));
    }

    @Test
    void objToPercent() {
        System.out.println(BigDecimalUtil.objToPercent(0.1));
        System.out.println(BigDecimalUtil.objToPercent(1));
        System.out.println(BigDecimalUtil.objToPercent(10));
        System.out.println(BigDecimalUtil.objToPercent(0.23444));
        System.out.println(BigDecimalUtil.objToPercent(0.23555));
        System.out.println(BigDecimalUtil.objToPercent("0.23555"));
        double a = 2.23;
        System.out.println(BigDecimalUtil.objToPercent(a));
        float b = 0.21f;
        System.out.println(BigDecimalUtil.objToPercent(b));
    }

    @Test
    void percentToBigDecimal() {
        System.out.println(BigDecimalUtil.percentToBigDecimal("100%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("10%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("-1%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("+2%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("+2.01%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("null"));
        System.out.println(BigDecimalUtil.percentToBigDecimal(null));
        System.out.println(BigDecimalUtil.percentToBigDecimal(""));
        System.out.println(BigDecimalUtil.percentToBigDecimal(" "));
    }

    @Test
    void objToBigDecimal() {
        System.out.println(BigDecimalUtil.objToBigDecimal(0.1));
        System.out.println(BigDecimalUtil.objToBigDecimal(1));
        System.out.println(BigDecimalUtil.objToBigDecimal(Integer.parseInt("1")));
        System.out.println(BigDecimalUtil.objToBigDecimal(10));
        System.out.println(BigDecimalUtil.objToBigDecimal(0.23444));
        System.out.println(BigDecimalUtil.objToBigDecimal(0.23555));
        System.out.println(BigDecimalUtil.objToBigDecimal(0.21f));
        System.out.println(BigDecimalUtil.objToBigDecimal(0.23557d));
        System.out.println(BigDecimalUtil.objToBigDecimal(Long.valueOf(100L)));
        System.out.println(BigDecimalUtil.objToBigDecimal("-1.2"));
        System.out.println(BigDecimalUtil.objToBigDecimal("a"));
        System.out.println(BigDecimalUtil.objToBigDecimal(" "));
        System.out.println(BigDecimalUtil.objToBigDecimal(""));
        System.out.println(BigDecimalUtil.objToBigDecimal(null));
    }

    @Test
    void div(){
        System.out.println(BigDecimalUtil.div("2.5","3.6",2));
        System.out.println(BigDecimalUtil.div("5","2",1));
        System.out.println(BigDecimalUtil.div("5","3"));
        System.out.println(BigDecimalUtil.div("5","0.00000",1));
        System.out.println(BigDecimalUtil.div("5","-0.00000",1));
    }

    @Test
    void objToStr(){
        System.out.println(BigDecimalUtil.objToStr(0.1));
        System.out.println(BigDecimalUtil.objToStr(1));
        System.out.println(BigDecimalUtil.objToStr(Integer.parseInt("1")));
        System.out.println(BigDecimalUtil.objToStr(10));
        System.out.println(BigDecimalUtil.objToStr(0.23444));
        System.out.println(BigDecimalUtil.objToStr(0.23555));
        System.out.println(BigDecimalUtil.objToStr(0.21f));
        System.out.println(BigDecimalUtil.objToStr(0.23557d));
        System.out.println(BigDecimalUtil.objToStr(Long.valueOf(100L)));
        System.out.println(BigDecimalUtil.objToStr("-1.2"));
//        System.out.println(BigDecimalUtil.objToStr("a"));
//        System.out.println(BigDecimalUtil.objToStr(" "));
//        System.out.println(BigDecimalUtil.objToStr(""));
//        System.out.println(BigDecimalUtil.objToStr(null));
        System.out.println(BigDecimalUtil.objToStr(new BigDecimal("1000000")));
        System.out.println(BigDecimalUtil.objToStr(new BigDecimal("1000000.0")));
        System.out.println(BigDecimalUtil.objToStr(new BigDecimal("0.0000000000001")));
        System.out.println(BigDecimalUtil.objToStr(new BigDecimal("-0.0000000000001")));
        System.out.println(BigDecimalUtil.objToStr(new BigDecimal("1E+12")));
    }

    @Test
    void multiplyBigDecimalsForObject(){
        Map<String, Object> map = new HashMap<>();
        map.put("a", new BigDecimal("1000000"));
        map.put("b", new BigDecimal("0.0000000000001"));
        map.put("c", new BigDecimal("-0.0000000000001"));
        map.put("d", new BigDecimal("1E+12"));
        BigDecimalUtil.multiplyBigDecimalsForObject(map, new BigDecimal("2"));
        log.info(JsonUtil.toJson(map));

        Order order = new Order();
        order.setOrderId("1");
        order.setOrderAmount(new BigDecimal("1000000"));
        order.setOrderDiscount(new BigDecimal("0.0000000000001"));
        BigDecimalUtil.multiplyBigDecimalsForObject(order, new BigDecimal("3"));

        log.info(JsonUtil.toJson(order));
    }
}