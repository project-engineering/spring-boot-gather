package com.springboot.futool.encrypt.base64;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import java.util.Arrays;

public class Base64UtilTest {
    @SneakyThrows
    @Test
    void encodeAndDecode(){
        //String---->byte[] 调用getBytes方法
        String str1 = "abc123中国";
        byte[] bytes = str1.getBytes();
        System.out.println(Arrays.toString(bytes));

        byte[] gbks = str1.getBytes("gbk");
        System.out.println(Arrays.toString(gbks));
        //byte[]---->String 调用String构造器
//        使用默认的字符集解码
        String str2 = new String(bytes);
        System.out.println(str2);

        String str3 = new String(gbks);
        System.out.println(str3);//乱码，原因：编码集和解码集不一样

        String str4 = new String(gbks,"gbk");
        System.out.println(str4);//没有乱码，原因：编码集和解码集一样
    }
}
