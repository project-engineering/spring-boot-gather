package com.springboot.futool.encrypt.md5;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

public class MD5UtilTest {
    @SneakyThrows
    @Test
    void test(){
        //获得password的加密后的密码：
        String password ="123456";
        String pwdInDb = MD5Util.getEncryptedPwd("123456");
        System.out.println(pwdInDb);
        //输入值与加密值校验方法：
        boolean res= MD5Util.validPassword(password, pwdInDb);
        System.out.println("res:"+res);
    }
}
