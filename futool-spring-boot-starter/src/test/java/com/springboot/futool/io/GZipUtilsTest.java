package com.springboot.futool.io;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class GZipUtilsTest {
    @SneakyThrows
    @Test
    void gzip(){
        Map<String, byte[]> fileBytesMap;

        fileBytesMap = new HashMap<>();
        // 设置文件列表.
        File dirFile = new File("E:\\data\\test");
        for (File file : dirFile.listFiles()) {
            fileBytesMap.put(file.getName(), FileUtils.readFileToByteArray(file));
        }
        byte[] ramBytes = GZipUtil.compressByTar(fileBytesMap);
        log.info("ramBytes.length:{}", ramBytes.length);
        ramBytes = GZipUtil.compressByGZip(ramBytes);
        FileUtils.writeByteArrayToFile(new File("E:\\data\\test\\ram.tar.gz"), ramBytes);

        ramBytes = GZipUtil.unCompressByGZip(ramBytes);
        fileBytesMap = GZipUtil.unCompressByTar(ramBytes);
        log.info(fileBytesMap.size());
    }
}
