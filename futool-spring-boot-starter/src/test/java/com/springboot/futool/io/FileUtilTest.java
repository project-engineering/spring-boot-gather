package com.springboot.futool.io;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Log4j2
public class FileUtilTest {
    @Test
    void writeFileToContext(){
        List<String> contentList = List.of("Line 1", "Line 2", "Line 3");
        String path = "E:\\data\\test\\";
        String fileName = "output.txt";

        if (FileUtil.writeFileToContext(contentList, path, fileName)) {
            log.info("File write successful!");
        } else {
            log.error("File write failed!");
        }
    }

    @Test
    void deleteFile(){
        String filePath= "E:\\data\\test";
        boolean isForceDelete = true;
        FileUtil.delete(filePath, isForceDelete);
        log.info("File deletion completed!");
    }

    @Test
    void copyDirectory(){
        File srcDir = new File("E:\\data\\invoice");
        File desDir = new File("E:\\data\\invoice1");

        FileUtil.copyDirectory(srcDir, desDir);
    }

    @Test
    void fileToByte(){

        byte[] fileBytes = FileUtil.fileToByte("E:\\data\\test\\output.txt");

        if (fileBytes != null) {
            log.info("{}",fileBytes);
            log.info("File converted to byte array successfully!");
        } else {
            log.error("Failed to convert file to byte array.");
        }
    }
    @Test
    void fileToByte1(){
        File file = new File("E:\\data\\test\\output.txt"); // 替换为你的文件路径

        byte[] fileBytes = FileUtil.fileToByte(file);

        if (fileBytes != null) {
            log.info("{}",fileBytes);
            log.info("File converted to byte array successfully!");
        } else {
            log.error("Failed to convert file to byte array.");
        }
    }

    @Test
    void byteToFile(){
        byte[] bytes = {65, 66, 67, 68, 69}; // 示例字节数组
        String fileFullPath = "E:\\data\\test\\test.txt"; // 替换为你想保存文件的路径

        File file = FileUtil.byteToFile(bytes, fileFullPath);

        if (file != null) {
            log.info("File created successfully at: " + file.getAbsolutePath());
        } else {
            log.error("Failed to create file.");
        }
    }

    @Test
    void toUnits(){
        long fileSize = 1234567890L; // 文件大小为 1234567890 字节

        String readableSize = FileUtil.toUnits(fileSize);

        log.info("文件大小为：{}",readableSize);
    }

    @Test
    void copyFile(){
        FileUtil.copyFile("E:\\data\\export1.pdf","E:\\data\\copy1.pdf");
    }

    @Test
    void downloadFile(){
        String downloadUrl = "https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png";
        String savePath = "E:\\data\\test\\download.png";
        FileUtil.downloadFile(downloadUrl, savePath);
    }
}
