package com.springboot.futool;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

@Log4j2
public class MacUtilTest {
    @Test
    void getUnixMacAddress(){
        // 测试获取Unix MAC地址
        String macAddress = MacUtil.getUnixMacAddress();
        log.info("Unix MAC地址: " + macAddress);
    }

    @Test
    void getWindowsMacAddress(){
        // 测试获取Windows MAC地址
        String macAddress = MacUtil.getWindowsMacAddress();
        log.info("Windows MAC地址: " + macAddress);
    }
}
