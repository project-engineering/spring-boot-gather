package com.springboot.futool.office.excel;

import com.springboot.futool.DateUtil;
import com.springboot.futool.office.excel.ExcelWorkBookUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelWorkBookUtilTest {
    @Test
    void export(){
        Map<String,Object> param = new HashMap<>();
        List<Map<String,Object>> pageList = new ArrayList<>();
        Map<String,Object> headerParam = new HashMap<>();
        headerParam.put("p_etl_dt_md","2023-10");
        headerParam.put("p_etl_dt_yd","2022-12");
        headerParam.put("p_etl_dt_qyd","2021-12");
        param.put("headerParam",headerParam);
        param.put("pageList",pageList);
        ExcelWorkBookUtil.export("test.xlsx","test"+ DateUtil.convertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_14)+".xlsx",param);
    }

    @Test
    public void testExcelRead(){
        HashMap<String, ArrayList<ArrayList<String>>> excelReadMap = ExcelWorkBookUtil.readExcel(new File("G:\\home\\2023-11-22\\开关插座表.xlsx"), 1);
        if(excelReadMap != null){
            excelReadMap.entrySet().stream().forEach(entry -> {
                entry.getValue().stream().forEach(col -> {
                    col.stream().forEach(System.out::println);
                });
            });
        }
    }
}
