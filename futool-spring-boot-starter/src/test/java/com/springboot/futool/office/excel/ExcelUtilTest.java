package com.springboot.futool.office.excel;

import com.springboot.futool.DateUtil;
import com.springboot.futool.office.excel.ExcelUtil;
import com.springboot.futool.office.excel.ExcelWorkBookUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.*;

@Slf4j
public class ExcelUtilTest {
    @Test
    void export(){
        Map<String,Object> param = new HashMap<>();
        List<Map<String,Object>> pageList = new ArrayList<>();
        Map<String,Object> headerParam = new HashMap<>();
        headerParam.put("p_etl_dt_md","2023-10");
        headerParam.put("p_etl_dt_yd","2022-12");
        headerParam.put("p_etl_dt_qyd","2021-12");
        param.put("headerParam",headerParam);
        param.put("pageList",pageList);
        ExcelUtil.export("test.xlsx","test"+ DateUtil.convertObjToString(DateUtil.getCurrDateTime(),DateUtil.DATE_FORMATTER_14)+".xlsx",param,null,null);
    }

    @Test
    public void testExcelRead(){
        HashMap<String, ArrayList<ArrayList<String>>> excelReadMap = ExcelWorkBookUtil.readExcel(new File("G:\\home\\2023-11-22\\开关插座表.xlsx"), 1);
        if(excelReadMap != null){
            excelReadMap.entrySet().stream().forEach(entry -> {
                entry.getValue().stream().forEach(col -> {
                    col.stream().forEach(System.out::println);
                });
            });
        }
    }

    @Test
    public void testExport(){
        List<Map<String, Object>> dataList = new ArrayList<>();
        String[] columnIndexToMerge = {"0","1"}; // Specify the column index to merge
        Random random = new Random();

        for (int i = 0; i < 10000; i++) {
            Map<String, Object> map = new HashMap<>();

            // 生成随机的key和value
            String custId = "" + random.nextInt(100);  // 使用随机数作为key
            Object custName = "" + random.nextInt(1000);  // 使用随机数作为value
            map.put("custId", custId);
            map.put("custName", custName);
            dataList.add(map);
        }
        Map<String,Object> param = new HashMap<>();
        param.put("pageList",dataList);
        ExcelUtil.export("mergeTest.xlsx","exportTest.xlsx",param,columnIndexToMerge,"custId");
    }
}