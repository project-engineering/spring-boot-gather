package com.springboot.futool;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;
import java.util.*;

/**
 * @author liuc
 * @date 2024-07-04 15:16
 */
public class ObjectUtilTest {

    @Test
    public void convertNullToEmptyString(){
        Person obj = new Person("张三", null,null, null,null,
                null,null,null,null,null,null,null, null, null, null, null, null,null);
        System.out.println("Before conversion:");
        System.out.println("Name: " + obj.getName());
        System.out.println("Age: " + obj.getAge());
        System.out.println("Address: " + obj.getAddress());
        System.out.println("Friends: " + obj.getFriends());
        System.out.println("Attributes: " + obj.getAttributes());

        ObjectUtil.convertNullToEmptyString(obj);

        System.out.println("\nAfter conversion:");
        System.out.println(JsonUtil.toJson(obj));
    }
    @Test
    public void trimWhitespace() {
        Person person = new Person("   John Doe   ", "  123 Main Street  ",null,null,null,
                null,null,null,null,null,null,null, null, null, null, null, null,null);
        System.out.println("Before trimming:");
        System.out.println("Name: [" + person.getName() + "]");
        System.out.println("Address: [" + person.getAddress() + "]");

        ObjectUtil.trimWhitespace(person);

        System.out.println("\nAfter trimming:");
        System.out.println("Name: [" + person.getName() + "]");
        System.out.println("Address: [" + person.getAddress() + "]");
    }

    @Data
    @AllArgsConstructor
    class Person{
        private String name;
        private String address;
        private Hashtable<String, String> hashtable;
        private Integer age;
        private List<String> friends;
        private Map<String, Object> attributes;
        private Double salary;
        private Long balance;
        private Date joiningDate;
        private Boolean isMarried;
        private Character gender;
        private Byte b;
        private Short s;
        private Queue<String> queue;
        private Deque<String> deque;
        private Set<String> set;
        private Stack<String> stack;
        private Vector<String> vector;
    }
}
