package com.springboot.futool;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liuc
 * @date 2024-09-06 22:25
 */
@Data
public class Order {
    private String orderId;
    private BigDecimal orderAmount;
    private BigDecimal orderDiscount;
}
