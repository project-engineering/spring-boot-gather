package com.springboot.futool.bean;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Log4j2
public class BeanUtilTest {
    @Test
    void convertList(){
        // 创建一个对象列表
        List<Person> personList = new ArrayList<>();
        Person person1 = new Person.Builder()
                .name("Alice")
                .age(25)
                .idType("1")
                .idNo("3242342342434234")
                .build();
        personList.add(person1);
        Person person2 = new Person.Builder()
                .name("Bob")
                .age(30)
                .idType("3")
                .idNo("234234234")
                .build();
        personList.add(person2);

        // 转换为 Map 列表
        List<Map<String, Object>> mapList = BeanUtil.convertList(personList, Person.class);

        // 输出结果
        for (Map<String, Object> map : mapList) {
            log.info(map);
        }
    }
}
