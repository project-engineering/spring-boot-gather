package com.springboot.futool.bean;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class BeanMapUtilTest {
    @Test
    void mapToBean(){
        Map<String, Object> map = new HashMap<>();
        map.put("name", "John");
        map.put("age", 25);

        Person person = BeanMapUtil.mapToBean(map, Person.class);
        log.info(person.getName()); // 输出: John
        log.info(person.getAge()); // 输出: 25
    }

    @Test
    void objToMap(){
        Person person = new Person.Builder()
                .name("John")
                .age(25)
                .build();
        Map<String, Object> map = BeanMapUtil.objToMap(person);
        log.info(map.get("name")); // 输出: John
        log.info(map.get("age")); // 输出: 25
    }

    @Test
    public void testBeanToMap() {
        // 创建一个测试对象
        Person person = new Person.Builder()
                .name("Alice")
                .age(25)
                .build();

        // 调用 beanToMap 方法转换为 Map
        Map<String, Object> map = BeanMapUtil.beanToMap(person);

        // 验证转换结果
        log.info(map.get("name")); // 输出: John
        log.info(map.get("age")); // 输出: 25
    }
}
