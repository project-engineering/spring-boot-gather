package com.springboot.futool.bean;

import com.springboot.futool.annotation.Convert;
import com.springboot.futool.converter.IdTypeConverter;
import lombok.Data;

@Data
public class Person {
    private String name;
    private int age;
    @Convert(IdTypeConverter.class)
    private String idType;
    private String idNo;

    private Person (Builder builder){
        this.name = builder.name;
        this.age = builder.age;
        this.idType = builder.idType;
        this.idNo = builder.idNo;
    }

    public static class Builder{
        private String name;
        private int age;
        private String idType;
        private String idNo;

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder age(int age){
            this.age = age;
            return this;
        }

        public Builder idType(String idType){
            this.idType = idType;
            return this;
        }

        public Builder idNo(String idNo){
            this.idNo = idNo;
            return this;
        }

        public Person build(){
            return new Person(this);
        }
    }
}
