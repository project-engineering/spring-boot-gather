package com.springboot.futool.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.springboot.futool.JsonUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/1 19:55
 * @since JDK1.8
 */
@Log4j2
public class JsonUtilTest {
    @Test
    public void testJsonToMap() {
        String json = "{\"sex\":\"male\",\"name\":\"Steven\"}";
        //第一种写法
//        Map<String, Object> map = JsonUtil.fromJson(json,Map.class);
        //第二种写法
        Map<String, Object> map = JsonUtil.fromJson(json,new TypeReference<Map>() {});
        // {sex=male, name=Steven}
        log.info(map);
    }

    @Test
    public void testJsonToMapList() {
        String jsonMapList = "[{\"sex\":\"male\",\"name\":\"Steven\"},{\"sex\":\"null\",\"name\":\"Allen\"}]";
        //第一种写法
        List<Map<String, Object>> mapList = JsonUtil.fromJson(jsonMapList, List.class);
        //第二种写法
//        List<Map<String, Object>> mapList = JsonUtil.fromJson(jsonMapList, new TypeReference<>() {});
        // [{sex=male, name=Steven}, {sex=female, name=Allen}]
        System.out.println(mapList);
    }

    @Test
    public void testMapToJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Steven");
        map.put("sex", "male");
        String json = JsonUtil.toJson(map);
        // {"sex":"male","name":"Steven"}
        log.info(json);
    }

    @Test
    public void testMapListToJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Steven");
        map.put("sex", "male");
        Map<String, Object> map1 = new HashMap<>();
        map1.put("name", "Allen");
        map1.put("sex", "female");
        List<Map<String, Object>> mapList = new ArrayList<>();
        mapList.add(map);
        mapList.add(map1);
        String s = JsonUtil.toJson(mapList);
        // [{"sex":"male","name":"Steven"},{"sex":"female","name":"Allen"}]
        log.info(s);
    }

    @Test
    public void arrayToJson() {
        String[] array = {"Hello", "5", "World", "6"};
        String json = JsonUtil.toJson(array);
        // ["Hello","5","World","6"]
        log.info(json);
    }

    @Test
    public void testObjectToJson1() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Steven");
        map.put("sex", "male");
        map.put("address", "");
        String s = JsonUtil.toJson(map);
        // {"address":"","sex":"male","name":"Steven"}
        log.info(s);
    }

    @Test
    public void testObjectToJson2() {
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Steven");
        map.put("sex", null);
        map.put("address", "");
        Map<String, Object> map2 = new HashMap<>();
        map2.put("name", "zhangsan");
        map2.put("sex", "男");
        map2.put("address", "安徽安庆");
        list.add(map);
        list.add(map2);
        String s = JsonUtil.toJson(list,true);
        // [{"address":"","name":"Steven"},{"address":"安徽安庆","sex":"男","name":"zhangsan"}]
        log.info(s);
    }

    @Test
    public void testToJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Steven");
        map.put("sex", null);
        map.put("address", "");
        String s = JsonUtil.toJson(map,true);
        // {"address":"","name":"Steven"}
        log.info(s);
    }
}
