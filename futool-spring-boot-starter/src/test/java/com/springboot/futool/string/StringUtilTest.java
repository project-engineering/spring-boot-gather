package com.springboot.futool.string;

import com.springboot.futool.StringUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.*;

@Log4j2
public class StringUtilTest {
    @Test
    public void testAddOneForStr(){
        String str = "01";
        String s = StringUtil.addOneForStr(str);
        //02
        System.out.println(s);
    }

    @Test
    void addZeroForLeft(){
        String str = "123";
        String s = StringUtil.addZeroToLeft(str, 9);
        //001
        log.info(s);
    }

    @Test
    public void testStringToList(){
        String str = "0,1,23";
        List<String> list = StringUtil.stringToList(str);
        //[0, 1, 23]
        System.out.println(list);
    }

    @Test
    public void testListToString(){
        List<String> list1 = new ArrayList<String>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        //a,b,c
        System.out.println(StringUtil.listToString(list1));
        //1,2,3
        System.out.println(StringUtil.listToString(list2));
    }

    @Test
    public void testConvertToCamelCase() {
        String str = "HELLO_WORLD";
        String s = StringUtil.convertToCamelCase(str,true);
        //HelloWorld
        System.out.println(s);

        str = "hello_world";
        System.out.println(StringUtil.convertToCamelCase(str,false));
        str = "HELLO_WORLD";
        System.out.println(StringUtil.convertToCamelCase(str,false));
    }

    @Test
    public void testHumpToUnderline() {
        String str1 = "helloWorld";
        //HELLO_WORLD
        System.out.println(StringUtil.convertToUnderline(str1,true));
        System.out.println(StringUtil.convertToUnderline(str1,false));
    }

    @Test
    public void testStrLeftFillZero(){
        System.out.println(StringUtil.strLeftFillZero("10000000000","1"));
    }

    @Test
    public void testStrLeftRemoveZero(){
        System.out.println(StringUtil.strLeftRemoveZero("000000145345"));
    }

    @Test
    void objToStr(){
        Map<String,Object> map = new HashMap<>();
        map.put("A",1);
        log.info(StringUtil.objToStr(map));
        List<Map<String,Object>> list = new ArrayList<>();
        map.put("B",2);
        list.add(map);
        log.info(StringUtil.objToStr(list));
        Set<String> set = new HashSet<>();
        set.add("hh");
        log.info(StringUtil.objToStr(set));
        String[] strArr = new String[2];
        strArr[0] = "1";
        strArr[1] = "2";
        log.info(StringUtil.objToStr(strArr));

        log.info(StringUtil.objToStr(123));
        log.info(StringUtil.objToStr(123.0));
        log.info(StringUtil.objToStr(123L));
        log.info(StringUtil.objToStr(123F));
        log.info(StringUtil.objToStr(123D));
        log.info(StringUtil.objToStr(new BigDecimal("43234.22")));
        //这个会失真，建议使用上面的方式
        log.info(StringUtil.objToStr(new BigDecimal(43234.22)));
        log.info(StringUtil.objToStr(null));
    }

    @Test
    void toUtf8(){
        String input = "测试字符串";
        String utf8String = StringUtil.toUtf8(input);
        log.info("UTF-8编码结果：{}",utf8String);
    }

    @Test
    void replace(){
        String input = "Hello, World! Hello, Universe!";
        String oldPattern = "Hello";
        String newPattern = "Hi";

        log.info("Original String: {}" ,input);
        String replacedString = StringUtil.replace(input, oldPattern, newPattern);
        log.info("Replaced String: {}" , replacedString);
    }
}