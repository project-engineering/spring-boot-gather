package com.springboot.futool.transfer;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.util.Iterator;
import java.util.Vector;

@Log4j2
public class SftpUtilTest {
    //ftp服务器地址
    public String hostname = "192.168.8.128";
    //ftp服务器端口号默认为22
    public Integer port = 22 ;
    //ftp登录账号
    public String username = "lcsftp";
    //ftp登录密码
    public String password = "123456";

    //ftp服务器地址
//    public String hostname = "172.168.13.1";
//    //ftp服务器端口号默认为22
//    public Integer port = 22 ;
//    //ftp登录账号
//    public String username = "gp";
//    //ftp登录密码
//    public String password = "rabbit@2023Y";

    @Test
    void testUpload() {
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        // 测试上传文件
        sftp.uploadFile("E:\\东北再担保\\Document\\09-二代征信数据报送\\二代征信\\","人民银行征信系统数据采集规范  抵（质）押物信息（二代试行）201911.pdf"
                , "/upload","1人民银行征信系统数据采集规范  抵（质）押物信息（二代试行）201911.pdf");
    }

    @Test
    void bacthUploadFile() {
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        // 测试批量上传文件
        sftp.batchUploadFile("E:\\东北再担保\\Document\\09-二代征信数据报送\\", "/upload/二代征信数据报送");
    }

    @Test
    void listFiles(){
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        try {
            Vector<?> vector = sftp.listFiles("/upload");
            // 使用迭代器遍历 Vector
            Iterator<?> iterator = vector.iterator();
            while (iterator.hasNext()) {
                log.info(iterator.next());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void downloadFile(){
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        // 测试下载文件
        sftp.downloadFile("/upload","1人民银行征信系统数据采集规范  抵（质）押物信息（二代试行）201911.pdf", "E:\\data\\");
    }

    @Test
    void downloadFile1(){
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        // 测试下载文件
        sftp.downloadFile("/upload","1人民银行征信系统数据采集规范  抵（质）押物信息（二代试行）201911.pdf", "E:\\data\\","test.pdf");
    }

    @Test
    void download() {
        SftpUtil sftp = new SftpUtil.Builder()
                .host(hostname)
                .port(port)
                .username(username)
                .password(password)
                .timeout(5*1000)
                .build();
        // 测试下载文件
        sftp.batchDownLoadFile("/upload", "E:\\data\\test\\");
    }
}
