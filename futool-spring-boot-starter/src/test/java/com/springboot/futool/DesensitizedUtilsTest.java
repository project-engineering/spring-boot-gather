package com.springboot.futool;

import org.junit.jupiter.api.Test;

public class DesensitizedUtilsTest {
    @Test
    void desensitizedIdCard() {
        System.out.println(DesensitizedUtils.idCardNum("51111220060325886X"));
    }

    @Test
    void desensitizedPhoneNum() {
        System.out.println(DesensitizedUtils.mobilePhone("15927161554"));
    }

    @Test
    void desensitizedFullName() {
        System.out.println(DesensitizedUtils.desensitizedFullName("张三"));
        System.out.println(DesensitizedUtils.desensitizedFullName("诸葛亮"));
        System.out.println(DesensitizedUtils.desensitizedFullName("东方不败"));
        System.out.println(DesensitizedUtils.desensitizedFullName("巴合提亚尔"));
    }

    @Test
    void desensitizedAddress() {
        System.out.println(DesensitizedUtils.address("山东省青岛市开平路53号国棉四厂二宿舍1号楼2单元204户甲"));
    }
}
