package com.springboot.limit.aop;

import com.google.common.collect.Maps;
import com.springboot.baseline.enums.ResultCode;
import com.springboot.limit.annotation.TokenBucketLimiter;
import com.springboot.limit.util.IpUtil;
import com.springboot.limit.util.JoinPointUtil;
import com.google.common.util.concurrent.RateLimiter;
import com.springboot.baseline.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
import java.util.Map;

@Aspect
@Component
@Slf4j
public class GuavaLimiterAop {

    /**
     * 不同的接口，不同的流量控制
     * map的key为 Limiter.key
     */
    private final Map<String, RateLimiter> limitMap = Maps.newConcurrentMap();

    @Pointcut("@annotation(com.springboot.limit.annotation.TokenBucketLimiter)")
    public void aspect() {
    }

    @Around(value = "aspect()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("准备限流");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 或者url(存在map集合的key)
        String ip = IpUtil.getIp();
        Method method = signature.getMethod();
        // 获取自定义注解
        TokenBucketLimiter limit = (TokenBucketLimiter) JoinPointUtil.getLimiter(joinPoint,TokenBucketLimiter.class);
        if (limit != null) {
            //key作用：不同的接口，不同的流量控制
            String key=limit.key();
            RateLimiter rateLimiter = null;
            //验证缓存是否有命中key
            // 判断map集合中是否有创建有创建好的令牌桶
            if (!limitMap.containsKey(ip+"-"+key)) {
                // 创建令牌桶
                rateLimiter = RateLimiter.create(limit.permitsPerSecond());
                limitMap.put(ip+"-"+key, rateLimiter);
                log.info("<<=================  请求{},创建令牌桶,容量{} 成功!!!", ip, limit.permitsPerSecond());
            }
            log.info(ip+"-"+key);
            rateLimiter = limitMap.get(ip+"-"+key);
            // 获取令牌
            boolean acquire = rateLimiter.tryAcquire(limit.timeout(), limit.timeunit());

            if (!acquire) {
                return ResultUtils.error(ResultCode.LIMIT_ERROR.getCode(),limit.msg());
            }
        }
        return joinPoint.proceed();
    }
}
