package com.springboot.limit.aop;

import com.springboot.baseline.enums.ResultCode;
import com.springboot.baseline.util.ResultUtils;
import com.springboot.limit.annotation.FixedWindowRateLimiter;
import com.springboot.limit.util.IpUtil;
import com.springboot.limit.util.JoinPointUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * 固定窗口限流算法
 */
@Aspect
@Component
@Slf4j
public class FixedWindowRateLimiterAop {
    public static final String KEY = "fixedWindowRateLimiter:";
    @Resource
    private RedissonClient redissonClient;

    @Value("${spring.limit.path}")
    private String path;

    @Pointcut("@annotation(com.springboot.limit.annotation.FixedWindowRateLimiter)")
    public void aspect() {
    }

    @Around(value = "aspect()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("准备限流");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 或者url(存在map集合的key)
        String ip = IpUtil.getIp();
        Method method = signature.getMethod();
        // 获取自定义注解
        FixedWindowRateLimiter fixedWindowRateLimiter = (FixedWindowRateLimiter) JoinPointUtil.getLimiter(joinPoint,FixedWindowRateLimiter.class);
        if (fixedWindowRateLimiter != null) {
            //加分布式锁，防止并发情况下窗口初始化时间不一致问题
            RLock rLock = redissonClient.getLock(KEY + "LOCK:" + path);
            try {
                rLock.lock(fixedWindowRateLimiter.timeout(), fixedWindowRateLimiter.timeunit());
                String redisKey = KEY + path;
                RAtomicLong counter = redissonClient.getAtomicLong(redisKey);
                //计数
                long count = counter.incrementAndGet();

                //触发限流
                long limit = 0L;
                if (fixedWindowRateLimiter.timeunit() == TimeUnit.MILLISECONDS) {
                    limit = fixedWindowRateLimiter.timeout()/1000;
                }
                if (fixedWindowRateLimiter.timeunit() == TimeUnit.SECONDS) {
                    limit = fixedWindowRateLimiter.timeout();
                } if (fixedWindowRateLimiter.timeunit() == TimeUnit.MINUTES) {
                    limit = fixedWindowRateLimiter.timeout()*60;
                }
                if (fixedWindowRateLimiter.timeunit() == TimeUnit.MINUTES) {
                    limit = fixedWindowRateLimiter.timeout()*60*60;
                }
                //如果为1的话，就说明窗口刚初始化
                if (count == 1) {
                    //直接设置过期时间，作为窗口
                    counter.expire(limit, fixedWindowRateLimiter.timeunit());
                }
                if (count > limit) {
                    //触发限流的不记在请求数量中
                    counter.decrementAndGet();
                    return ResultUtils.error(ResultCode.LIMIT_ERROR.getCode(),fixedWindowRateLimiter.msg());
//                    return true;
                }

                return ResultUtils.success();
            } finally {
                RLock lock = redissonClient.getLock(KEY + "LOCK:" + path);
                if(lock.isLocked()&&lock.isHeldByCurrentThread()){
                    lock.unlock();
                }
            }
        }
        return joinPoint.proceed();
    }
}
