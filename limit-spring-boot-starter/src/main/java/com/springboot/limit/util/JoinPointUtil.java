package com.springboot.limit.util;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

public class JoinPointUtil {
    /**
     * 获取注解对象
     * @param joinPoint 对象
     * @return 注解对象
     */
    public static Object getLimiter(final JoinPoint joinPoint,final Class<? extends Annotation> annotation) {
        Method[] methods = joinPoint.getTarget().getClass().getDeclaredMethods();
        String name = joinPoint.getSignature().getName();
        if (!StringUtils.isEmpty(name)) {
            for (Method method : methods) {
                if (method.isAnnotationPresent(annotation)) {
                    Annotation annotInstance = method.getAnnotation(annotation);
                    if (!Objects.isNull(annotInstance) && name.equals(method.getName())) {
                        return annotInstance;
                    }
                }
//                Object annotation = method.getAnnotation(defineAnnotation.getClass());
//                if (!Objects.isNull(annotation) && name.equals(method.getName())) {
//                    return annotation;
//                }
            }
        }
        return null;
    }
}
