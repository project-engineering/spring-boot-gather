import request from '@/utils/request'

export interface User {
  id: number
  username: string
  nickname?: string
  avatar?: string
  email?: string
  phone?: string
  gender?: number
  status: 0 | 1
  createTime?: string
  updateTime?: string
  roleIds?: number[]
  statusLoading?: boolean
}

export interface UserQuery {
  pageNum: number
  pageSize: number
  username?: string
  nickname?: string
  phone?: string
  status?: number
}

export interface UserDTO {
  id?: number
  username: string
  password?: string
  nickname?: string
  email?: string
  phone?: string
  gender?: number
  status?: number
  roleIds?: number[]
}

// 获取用户列表
export function getUserList(params: UserQuery) {
  return request<{
    records: User[]
    total: number
    size: number
    current: number
    pages: number
  }>({
    url: '/api/users',
    method: 'get',
    params
  })
}

// 添加用户
export function addUser(data: UserData) {
  return request({
    url: '/api/users',
    method: 'post',
    data
  })
}

// 修改用户
export function updateUser(id: number, data: UserData) {
  return request({
    url: `/api/users/${id}`,
    method: 'put',
    data
  })
}

// 删除用户
export function deleteUser(id: number) {
  return request({
    url: `/api/users/${id}`,
    method: 'delete'
  })
}

// 修改用户状态
export function updateUserStatus(id: number, status: number) {
  return request<void>({
    url: `/api/users/${id}/status`,
    method: 'put',
    params: { status }
  })
}

// 获取用户角色
export function getUserRoles(userId: number) {
  return request({
    url: `/api/users/roles/${userId}`,
    method: 'get'
  })
}