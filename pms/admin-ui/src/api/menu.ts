import request from '@/utils/request'

export interface MenuItem {
  id: number
  parentId: number
  menuName: string
  menuType: string
  path: string
  component: string
  perms: string
  icon: string
  sortOrder: number
  children?: MenuItem[]
}

export interface MenuForm {
  id?: number
  parentId: number
  menuName: string
  menuType: string
  path?: string
  component?: string
  perms?: string
  icon?: string
  sortOrder?: number
}

// 获取菜单列表
export function getMenuList() {
  return request<MenuItem[]>({
    url: '/api/menus/list',
    method: 'get'
  })
}

// 获取菜单树
export function getMenuTree() {
  return request<MenuItem[]>({
    url: '/api/menus/tree',
    method: 'get'
  })
}

// 获取菜单详情
export function getMenu(id: number) {
  return request<MenuItem>({
    url: `/api/menus/${id}`,
    method: 'get'
  })
}

// 添加菜单
export function addMenu(data: MenuForm) {
  return request({
    url: '/api/menus',
    method: 'post',
    data
  })
}

// 更新菜单
export function updateMenu(id: number, data: MenuForm) {
  return request({
    url: `/api/menus/${id}`,
    method: 'put',
    data
  })
}

// 删除菜单
export function deleteMenu(id: number) {
  return request({
    url: `/api/menus/${id}`,
    method: 'delete'
  })
}

// 导出所有方法
export default {
  getMenuList,
  getMenuTree,
  getMenu,
  addMenu,
  updateMenu,
  deleteMenu
} 