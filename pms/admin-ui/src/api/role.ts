import request from '@/utils/request'

interface RoleQuery {
  pageNum: number
  pageSize: number
  roleName?: string
  roleCode?: string
  status?: number
}

interface RoleData {
  id?: number
  roleName: string
  roleCode: string
  description?: string
  sortOrder?: number
}

// 获取角色列表
export function getRoleList(params?: RoleQuery) {
  return request({
    url: '/api/roles',
    method: 'get',
    params
  })
}

// 添加角色
export function addRole(data: RoleData) {
  return request({
    url: '/api/roles',
    method: 'post',
    data
  })
}

// 修改角色
export function updateRole(id: number, data: RoleData) {
  return request({
    url: `/api/roles/${id}`,
    method: 'put',
    data
  })
}

// 删除角色
export function deleteRole(id: number) {
  return request({
    url: `/api/roles/${id}`,
    method: 'delete'
  })
}

// 获取角色菜单
export function getRoleMenus(roleId: number) {
  return request({
    url: `/api/roles/${roleId}/menus`,
    method: 'get'
  })
}

// 分配角色菜单
export function assignRoleMenus(roleId: number, menuIds: number[]) {
  return request({
    url: `/api/roles/${roleId}/menus`,
    method: 'put',
    data: menuIds
  })
} 