import type { RouteRecordRaw } from 'vue-router'

/**
 * 过滤路由
 * @param routes 路由配置
 * @returns 过滤后的路由
 */
export function filterRoutes(routes: RouteRecordRaw[]) {
  return routes.filter(route => {
    if (route.meta?.hidden) {
      return false
    }
    
    if (route.children) {
      route.children = filterRoutes(route.children)
    }
    
    return true
  })
}

/**
 * 格式化路由
 * @param routes 路由配置
 * @returns 格式化后的路由
 */
export function formatRoutes(routes: RouteRecordRaw[]) {
  return routes.map(route => {
    const formattedRoute = {
      ...route,
      menuName: route.meta?.title || '',
      icon: route.meta?.icon
    }
    
    if (route.children) {
      formattedRoute.children = formatRoutes(route.children)
    }
    
    return formattedRoute
  })
} 