import { defineStore } from 'pinia'
import { getMenuTree } from '@/api/menu'
import type { MenuItem } from '@/api/menu'
import type { RouteRecordRaw } from 'vue-router'
import router from '@/router'
import Layout from '@/layout/index.vue'

// 导入所有视图组件
const modules = import.meta.glob('@/views/**/*.vue')

export const useMenuStore = defineStore('menu', {
  state: () => ({
    menuList: [] as MenuItem[]
  }),

  actions: {
    async loadMenuTree() {
      try {
        // 获取动态菜单
        const { data } = await getMenuTree()
        
        // 过滤按钮类型的菜单
        this.menuList = data.filter(item => item.menuType === 'MENU')
        
        // 生成路由
        this.generateRoutes()
      } catch (error) {
        console.error('Failed to load menu tree:', error)
        this.menuList = []
      }
    },

    // 生成动态路由
    generateRoutes() {
      // 递归生成路由配置
      const generateRoutesFromMenu = (menus: MenuItem[]): RouteRecordRaw[] => {
        return menus.map(menu => {
          // 处理组件路径
          const getComponent = (component: string) => {
            if (!component) return null
            // 构造完整的组件路径
            const componentPath = `/src/views/${component}.vue`
            return modules[componentPath]
          }

          const route: RouteRecordRaw = {
            path: menu.path,
            name: menu.path.split('/').pop()?.replace(/^\S/, s => s.toUpperCase()),
            component: menu.parentId === 0 ? Layout : getComponent(menu.component),
            meta: {
              title: menu.menuName,
              icon: menu.icon
            }
          }

          if (menu.children?.length) {
            route.children = generateRoutesFromMenu(menu.children)
          }

          return route
        })
      }

      // 生成路由配置
      const routes = generateRoutesFromMenu(this.menuList)
      
      // 添加到路由
      routes.forEach(route => {
        if (!router.hasRoute(route.name!)) {
          router.addRoute(route)
        }
      })
    }
  }
}) 