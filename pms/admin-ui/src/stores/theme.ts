import { defineStore } from 'pinia'

interface ThemeVariables {
  [key: string]: string
}

interface Theme {
  name: string
  variables: ThemeVariables
}

export const themes: Theme[] = [
  {
    name: 'light',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#409eff',
      '--el-color-success': '#67c23a',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#409eff',
      '--menu-text-color': '#ffffff',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#66b1ff',
      '--menu-active-bg-color': '#66b1ff',
      '--header-bg-color': '#ffffff',
      '--header-text-color': '#409eff',
      '--header-border-color': '#d8dce5',
      '--main-bg-color': '#f0f2f5',
      '--border-color': '#dcdfe6',
      
      // 组件颜色
      '--el-bg-color': '#ffffff',
      '--el-text-color-primary': '#303133',
      '--el-text-color-regular': '#606266',
      '--el-text-color-secondary': '#909399',
      '--el-border-color': '#dcdfe6',
      '--el-border-color-light': '#e4e7ed',
      '--el-fill-color-blank': '#ffffff'
    }
  },
  {
    name: 'dark',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#409eff',
      '--el-color-success': '#67c23a',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#1f1f1f',
      '--menu-text-color': '#bfbfbf',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#141414',
      '--menu-active-bg-color': '#409eff',
      '--header-bg-color': '#1f1f1f',
      '--header-text-color': '#ffffff',
      '--header-border-color': '#303030',
      '--main-bg-color': '#141414',
      '--border-color': '#303030',
      
      // 组件颜色
      '--el-bg-color': '#141414',
      '--el-text-color-primary': '#ffffff',
      '--el-text-color-regular': '#d0d0d0',
      '--el-text-color-secondary': '#a3a6ad',
      '--el-border-color': '#303030',
      '--el-border-color-light': '#404040',
      '--el-fill-color-blank': '#1f1f1f'
    }
  },
  {
    name: 'blue',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#2b85e4',
      '--el-color-success': '#67c23a',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#2b85e4',
      '--menu-text-color': '#ffffff',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#539eeb',
      '--menu-active-bg-color': '#539eeb',
      '--header-bg-color': '#ffffff',
      '--header-text-color': '#2b85e4',
      '--header-border-color': '#d8dce5',
      '--main-bg-color': '#f0f2f5',
      '--border-color': '#dcdfe6',
      
      // 组件颜色
      '--el-bg-color': '#ffffff',
      '--el-text-color-primary': '#303133',
      '--el-text-color-regular': '#606266',
      '--el-text-color-secondary': '#909399',
      '--el-border-color': '#dcdfe6',
      '--el-border-color-light': '#e4e7ed',
      '--el-fill-color-blank': '#ffffff'
    }
  },
  {
    name: 'green',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#19be6b',
      '--el-color-success': '#19be6b',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#19be6b',
      '--menu-text-color': '#ffffff',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#47cb89',
      '--menu-active-bg-color': '#47cb89',
      '--header-bg-color': '#ffffff',
      '--header-text-color': '#19be6b',
      '--header-border-color': '#d8dce5',
      '--main-bg-color': '#f0f2f5',
      '--border-color': '#dcdfe6',
      
      // 组件颜色
      '--el-bg-color': '#ffffff',
      '--el-text-color-primary': '#303133',
      '--el-text-color-regular': '#606266',
      '--el-text-color-secondary': '#909399',
      '--el-border-color': '#dcdfe6',
      '--el-border-color-light': '#e4e7ed',
      '--el-fill-color-blank': '#ffffff'
    }
  },
  {
    name: 'peach',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#ed9ec7',
      '--el-color-success': '#67c23a',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#ed9ec7',
      '--menu-text-color': '#ffffff',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#f1b1d3',
      '--menu-active-bg-color': '#f1b1d3',
      '--header-bg-color': '#ffffff',
      '--header-text-color': '#ed9ec7',
      '--header-border-color': '#d8dce5',
      '--main-bg-color': '#f0f2f5',
      '--border-color': '#dcdfe6',
      
      // 组件颜色
      '--el-bg-color': '#ffffff',
      '--el-text-color-primary': '#303133',
      '--el-text-color-regular': '#606266',
      '--el-text-color-secondary': '#909399',
      '--el-border-color': '#dcdfe6',
      '--el-border-color-light': '#e4e7ed',
      '--el-fill-color-blank': '#ffffff'
    }
  },
  {
    name: 'purple',
    variables: {
      // Element Plus 主题色
      '--el-color-primary': '#722ed1',
      '--el-color-success': '#67c23a',
      '--el-color-warning': '#e6a23c',
      '--el-color-danger': '#f56c6c',
      '--el-color-info': '#909399',
      
      // 布局颜色
      '--menu-bg-color': '#722ed1',
      '--menu-text-color': '#ffffff',
      '--menu-active-text-color': '#ffffff',
      '--menu-hover-bg-color': '#8f54db',
      '--menu-active-bg-color': '#8f54db',
      '--header-bg-color': '#ffffff',
      '--header-text-color': '#722ed1',
      '--header-border-color': '#d8dce5',
      '--main-bg-color': '#f0f2f5',
      '--border-color': '#dcdfe6',
      
      // 组件颜色
      '--el-bg-color': '#ffffff',
      '--el-text-color-primary': '#303133',
      '--el-text-color-regular': '#606266',
      '--el-text-color-secondary': '#909399',
      '--el-border-color': '#dcdfe6',
      '--el-border-color-light': '#e4e7ed',
      '--el-fill-color-blank': '#ffffff'
    }
  }
]

export const useThemeStore = defineStore('theme', {
  state: () => ({
    currentTheme: localStorage.getItem('theme') || 'light'
  }),

  actions: {
    initTheme() {
      this.setTheme(this.currentTheme)
    },

    setTheme(themeName: string) {
      const theme = themes.find(t => t.name === themeName)
      if (theme) {
        // 应用主题变量
        Object.entries(theme.variables).forEach(([key, value]) => {
          document.documentElement.style.setProperty(key, value)
        })
        
        // 更新状态和本地存储
        this.currentTheme = themeName
        localStorage.setItem('theme', themeName)
        
        // 处理暗黑模式
        if (themeName === 'dark') {
          document.documentElement.classList.add('dark')
          document.documentElement.setAttribute('data-theme', 'dark')
        } else {
          document.documentElement.classList.remove('dark')
          document.documentElement.setAttribute('data-theme', 'light')
        }
      }
    }
  }
}) 