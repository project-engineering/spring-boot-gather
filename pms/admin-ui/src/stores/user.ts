import { defineStore } from 'pinia'
import { login, logout, getUserInfo } from '@/api/auth'

export const useUserStore = defineStore('user', {
  state: () => ({
    token: localStorage.getItem('token') || '',
    userInfo: null as UserInfo | null,
    permissions: [] as string[]
  }),
  
  actions: {
    async login(loginData) {
      try {
        const { data } = await login(loginData)
        this.token = data
        localStorage.setItem('token', data)
        await this.getUserInfo()
        return data
      } catch (error) {
        this.token = null
        localStorage.removeItem('token')
        throw error
      }
    },
    
    async getUserInfo() {
      try {
        const { data } = await getUserInfo()
        this.userInfo = data
        if (data.permissions && Array.isArray(data.permissions)) {
          this.permissions = data.permissions
          console.log('用户权限:', this.permissions)
        } else {
          console.warn('用户权限数据格式不正确:', data.permissions)
          this.permissions = []
        }
        localStorage.setItem('userInfo', JSON.stringify(data))
        return data
      } catch (error) {
        console.error('获取用户信息失败:', error)
        this.userInfo = null
        this.permissions = []
        localStorage.removeItem('userInfo')
        throw error
      }
    },
    
    async logout() {
      try {
        await logout()
      } finally {
        this.token = null
        this.userInfo = null
        this.permissions = []
        localStorage.removeItem('token')
        localStorage.removeItem('userInfo')
      }
    }
  }
}) 