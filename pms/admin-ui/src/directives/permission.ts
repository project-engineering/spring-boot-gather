import type { Directive } from 'vue'
import { useUserStore } from '@/stores/user'

export const permission: Directive = {
  mounted(el, binding) {
    const { value } = binding
    const userStore = useUserStore()
    const permissions = userStore.permissions

    if (value && value.length > 0) {
      const hasPermission = permissions.some(permission => {
        return permission === value
      })

      if (!hasPermission) {
        el.parentNode?.removeChild(el)
      }
    }
  }
} 