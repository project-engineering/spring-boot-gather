const fs = require('fs')
const path = require('path')

function renameJsToTs(dir) {
  const files = fs.readdirSync(dir)
  
  files.forEach(file => {
    const filePath = path.join(dir, file)
    const stat = fs.statSync(filePath)
    
    if (stat.isDirectory()) {
      renameJsToTs(filePath)
    } else if (file.endsWith('.js')) {
      const newPath = filePath.replace('.js', '.ts')
      fs.renameSync(filePath, newPath)
      console.log(`Renamed: ${filePath} -> ${newPath}`)
    }
  })
}

// 从src目录开始递归重命名
renameJsToTs(path.join(__dirname, 'src')) 