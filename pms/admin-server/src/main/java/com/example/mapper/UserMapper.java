package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.SysUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
    
    @Select("SELECT DISTINCT m.perms FROM sys_user_role ur " +
            "LEFT JOIN sys_role_menu rm ON ur.role_id = rm.role_id " +
            "LEFT JOIN sys_menu m ON rm.menu_id = m.id " +
            "WHERE ur.user_id = #{userId} AND m.menu_type = 'BUTTON' AND m.status = 1")
    List<String> selectPermissionsByUserId(@Param("userId") Long userId);
    
    // 查询用户角色ID列表
    List<Long> selectRoleIdsByUserId(@Param("userId") Long userId);
    
    // 删除用户角色关系
    int deleteUserRoles(@Param("userId") Long userId);
    
    // 批量插入用户角色关系
    int insertUserRoles(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);
} 