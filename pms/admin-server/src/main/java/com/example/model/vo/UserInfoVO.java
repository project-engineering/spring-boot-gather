package com.example.model.vo;

import lombok.Data;
import lombok.Builder;

import java.util.List;

@Data
@Builder
public class UserInfoVO {
    private Long userId;
    private String username;
    private String nickname;
    private String avatar;
    private List<String> permissions;
} 