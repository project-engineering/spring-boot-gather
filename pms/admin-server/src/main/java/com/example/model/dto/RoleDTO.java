package com.example.model.dto;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;

@Data
public class RoleDTO {
    @NotBlank(message = "角色名称不能为空")
    private String roleName;
    
    @NotBlank(message = "角色编码不能为空")
    private String roleCode;
    
    private String description;
    private Integer sortOrder;
} 