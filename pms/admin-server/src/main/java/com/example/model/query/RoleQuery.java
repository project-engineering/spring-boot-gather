package com.example.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleQuery extends PageQuery {
    private String roleName;
    private String roleCode;
    private Integer status;
} 