package com.example.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MenuDTO {
    private Long id;
    
    @NotNull(message = "父菜单ID不能为空")
    private Long parentId;
    
    @NotBlank(message = "菜单名称不能为空")
    private String menuName;
    
    @NotBlank(message = "菜单类型不能为空")
    private String menuType;
    
    private String path;
    
    private String component;
    
    private String perms;
    
    private String icon;
    
    private Integer sortOrder;
} 