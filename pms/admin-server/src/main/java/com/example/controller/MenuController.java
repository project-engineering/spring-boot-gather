package com.example.controller;

import com.example.common.api.Result;
import com.example.entity.SysMenu;
import com.example.model.dto.MenuDTO;
import com.example.service.MenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@Tag(name = "菜单管理")
@RestController
@RequestMapping("/api/menus")
@RequiredArgsConstructor
public class MenuController {

    private final MenuService menuService;

    @Operation(summary = "获取菜单列表")
    @GetMapping("/list")
    public Result<List<SysMenu>> list() {
        return Result.success(menuService.list());
    }

    @Operation(summary = "获取菜单树")
    @GetMapping("/tree")
    public Result<List<SysMenu>> getMenuTree() {
        return Result.success(menuService.getCurrentUserMenuTree());
    }

    @Operation(summary = "获取菜单详情")
    @GetMapping("/{id}")
    public Result<SysMenu> getMenu(@PathVariable Long id) {
        return Result.success(menuService.getById(id));
    }

    @Operation(summary = "添加菜单")
    @PostMapping
    public Result<Void> addMenu(@RequestBody @Valid MenuDTO menuDTO) {
        menuService.saveMenu(menuDTO);
        return Result.success();
    }

    @Operation(summary = "更新菜单")
    @PutMapping("/{id}")
    public Result<Void> updateMenu(@PathVariable Long id, @RequestBody @Valid MenuDTO menuDTO) {
        menuDTO.setId(id);
        menuService.updateMenu(menuDTO);
        return Result.success();
    }

    @Operation(summary = "删除菜单")
    @DeleteMapping("/{id}")
    public Result<Void> deleteMenu(@PathVariable Long id) {
        menuService.removeById(id);
        return Result.success();
    }
} 