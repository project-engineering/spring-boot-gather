package com.example.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.common.api.Result;
import com.example.entity.SysRole;
import com.example.entity.SysUser;
import com.example.model.dto.UserDTO;
import com.example.model.query.UserQuery;
import com.example.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Tag(name = "用户管理")
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    
    private final UserService userService;
    
    @Operation(summary = "获取用户列表")
    @GetMapping
    public Result<IPage<SysUser>> list(UserQuery query) {
        log.info("查询用户列表，参数：{}", query);
        IPage<SysUser> page = userService.getUserPage(query);
        log.info("查询结果：total={}, pages={}, current={}, size={}, records.size={}", 
            page.getTotal(), page.getPages(), page.getCurrent(), page.getSize(), 
            page.getRecords() != null ? page.getRecords().size() : 0);
        return Result.success(page);
    }
    
    @Operation(summary = "添加用户")
    @PostMapping
    public Result<Void> add(@RequestBody @Valid UserDTO userDTO) {
        log.info("添加用户：{}", userDTO);
        userService.addUser(userDTO);
        return Result.success();
    }
    
    @Operation(summary = "修改用户")
    @PutMapping("/{id}")
    public Result<Void> update(@PathVariable Long id, @RequestBody @Valid UserDTO userDTO) {
        log.info("修改用户，id：{}，参数：{}", id, userDTO);
        userService.updateUser(id, userDTO);
        return Result.success();
    }
    
    @Operation(summary = "删除用户")
    @DeleteMapping("/{id}")
    public Result<Void> delete(@PathVariable Long id) {
        log.info("删除用户，id：{}", id);
        userService.removeById(id);
        return Result.success();
    }
    
    @PutMapping("/{id}/status")
    @PreAuthorize("@ss.hasPermission('system:user:status')")
    public Result<Void> updateStatus(@PathVariable Long id, @RequestParam Integer status) {
        log.info("修改用户状态，id：{}，status：{}", id, status);
        userService.updateStatus(id, status);
        return Result.success();
    }

    @GetMapping("/roles/{userId}")
    public Result getUserRoles(@PathVariable Long userId) {
        List<SysRole> roles = userService.getUserRoles(userId);
        return Result.success(roles);
    }
} 