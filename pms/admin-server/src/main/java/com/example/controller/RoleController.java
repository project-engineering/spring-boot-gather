package com.example.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.common.api.Result;
import com.example.entity.SysRole;
import com.example.model.dto.RoleDTO;
import com.example.model.query.RoleQuery;
import com.example.service.RoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Slf4j
@Tag(name = "角色管理")
@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor
public class RoleController {
    
    private final RoleService roleService;
    
    @Operation(summary = "获取角色列表")
    @GetMapping
    public Result<IPage<SysRole>> list(RoleQuery query) {
        log.info("查询角色列表，参数：{}", query);
        return Result.success(roleService.getRolePage(query));
    }
    
    @Operation(summary = "添加角色")
    @PostMapping
    public Result<Void> add(@RequestBody @Valid RoleDTO roleDTO) {
        log.info("添加角色：{}", roleDTO);
        roleService.addRole(roleDTO);
        return Result.success();
    }
    
    @Operation(summary = "修改角色")
    @PutMapping("/{id}")
    public Result<Void> update(@PathVariable Long id, @RequestBody @Valid RoleDTO roleDTO) {
        log.info("修改角色，id：{}，参数：{}", id, roleDTO);
        roleService.updateRole(id, roleDTO);
        return Result.success();
    }
    
    @Operation(summary = "删除角色")
    @DeleteMapping("/{id}")
    public Result<Void> delete(@PathVariable Long id) {
        log.info("删除角色，id：{}", id);
        roleService.removeById(id);
        return Result.success();
    }
    
    @Operation(summary = "获取角色菜单")
    @GetMapping("/{id}/menus")
    public Result<List<Long>> getRoleMenus(@PathVariable Long id) {
        log.info("获取角色菜单，id：{}", id);
        return Result.success(roleService.getRoleMenuIds(id));
    }
    
    @Operation(summary = "分配角色菜单")
    @PutMapping("/{id}/menus")
    public Result<Void> assignMenus(@PathVariable Long id, @RequestBody List<Long> menuIds) {
        log.info("分配角色菜单，id：{}，menuIds：{}", id, menuIds);
        roleService.assignMenus(id, menuIds);
        return Result.success();
    }
} 