package com.example.controller;

import com.example.common.api.Result;
import com.example.model.dto.LoginDTO;
import com.example.model.vo.LoginUser;
import com.example.security.SecurityUtils;
import com.example.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "认证管理")
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @Operation(summary = "登录")
    @PostMapping("/login")
    public Result<String> login(@RequestBody @Valid LoginDTO loginDTO) {
        String token = authService.login(loginDTO);
        return Result.success(token);
    }

    @Operation(summary = "获取用户信息")
    @GetMapping("/info")
    public Result<LoginUser> getUserInfo() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        return Result.success(loginUser);
    }

    @Operation(summary = "退出登录")
    @PostMapping("/logout")
    public Result<Void> logout() {
        authService.logout();
        return Result.success();
    }
} 