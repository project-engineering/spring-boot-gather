package com.example.service.impl;

import com.example.common.exception.BusinessException;
import com.example.entity.SysUser;
import com.example.model.dto.LoginDTO;
import com.example.model.vo.UserInfoVO;
import com.example.service.AuthService;
import com.example.service.UserService;
import com.example.common.api.ResultCode;
import com.example.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final UserService userService;

    @Override
    public String login(LoginDTO loginDTO) {
        log.info("正在验证用户凭证，用户名：{}", loginDTO.getUsername());
        try {
            log.debug("开始认证过程...");
            Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword())
            );
            log.debug("认证成功，生成用户凭证");
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            log.debug("用户详情：{}", userDetails.getUsername());
            return jwtUtils.generateToken(userDetails);
        } catch (BadCredentialsException e) {
            log.error("用户名或密码错误，用户名：{}", loginDTO.getUsername());
            throw new BusinessException(ResultCode.LOGIN_ERROR.getMessage());
        } catch (Exception e) {
            log.error("登录过程发生异常：", e);
            throw new BusinessException(ResultCode.ERROR.getMessage());
        }
    }

    @Override
    public UserInfoVO getUserInfo() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        SysUser user = userService.getByUsername(username);
        if (user == null) {
            throw new BusinessException("用户不存在");
        }

        return UserInfoVO.builder()
                .userId(user.getId())
                .username(user.getUsername())
                .nickname(user.getNickname())
                .avatar(user.getAvatar())
                .permissions(userService.getUserPermissions(user.getId()))
                .build();
    }

    @Override
    public void logout() {
        SecurityContextHolder.clearContext();
    }
} 