package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.SysMenu;
import com.example.model.dto.MenuDTO;
import java.util.List;

public interface MenuService extends IService<SysMenu> {
    
    /**
     * 获取当前用户的菜单树
     */
    List<SysMenu> getCurrentUserMenuTree();
    
    /**
     * 保存菜单
     */
    void saveMenu(MenuDTO menuDTO);
    
    /**
     * 更新菜单
     */
    void updateMenu(MenuDTO menuDTO);
} 