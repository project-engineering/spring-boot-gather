package com.example.service;

import com.example.model.dto.LoginDTO;
import com.example.model.vo.UserInfoVO;

public interface AuthService {
    String login(LoginDTO loginDTO);
    UserInfoVO getUserInfo();
    void logout();
}