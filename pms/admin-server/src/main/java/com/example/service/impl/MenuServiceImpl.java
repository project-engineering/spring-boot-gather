package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.SysMenu;
import com.example.mapper.MenuMapper;
import com.example.model.dto.MenuDTO;
import com.example.service.MenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, SysMenu> implements MenuService {
    
    @Override
    public List<SysMenu> getCurrentUserMenuTree() {
        List<SysMenu> menus = list();
        return buildTree(menus);
    }
    
    @Override
    public void saveMenu(MenuDTO menuDTO) {
        SysMenu menu = new SysMenu();
        BeanUtils.copyProperties(menuDTO, menu);
        save(menu);
    }
    
    @Override
    public void updateMenu(MenuDTO menuDTO) {
        SysMenu menu = new SysMenu();
        BeanUtils.copyProperties(menuDTO, menu);
        updateById(menu);
    }
    
    private List<SysMenu> buildTree(List<SysMenu> menus) {
        List<SysMenu> tree = new ArrayList<>();
        Map<Long, List<SysMenu>> childrenMap = new HashMap<>();
        
        // 按父ID分组
        for (SysMenu menu : menus) {
            childrenMap.computeIfAbsent(menu.getParentId(), k -> new ArrayList<>()).add(menu);
        }
        
        // 构建树
        for (SysMenu menu : menus) {
            if (menu.getParentId() == 0) {
                menu.setChildren(childrenMap.get(menu.getId()));
                tree.add(menu);
            }
        }
        
        return tree;
    }
} 