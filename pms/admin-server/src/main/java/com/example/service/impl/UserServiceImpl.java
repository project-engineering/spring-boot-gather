package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.SysRole;
import com.example.entity.SysUser;
import com.example.mapper.MenuMapper;
import com.example.mapper.UserMapper;
import com.example.mapper.UserRoleMapper;
import com.example.model.dto.UserDTO;
import com.example.model.query.UserQuery;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements UserService {

    private final UserMapper userMapper;
    private final MenuMapper menuMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private UserRoleMapper userRoleMapper;

    public UserServiceImpl(UserMapper userMapper, MenuMapper menuMapper, @Lazy PasswordEncoder passwordEncoder) {
        this.userMapper = userMapper;
        this.menuMapper = menuMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public SysUser getByUsername(String username) {
        return userMapper.selectOne(
            new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, username)
                .eq(SysUser::getDeleted, 0)
        );
    }

    @Override
    public List<String> getUserPermissions(Long userId) {
        return menuMapper.selectPermsByUserId(userId);
    }

    @Override
    public IPage<SysUser> getUserPage(UserQuery query) {
        // 创建分页对象
        Page<SysUser> page = new Page<>(query.getPageNum(), query.getPageSize(), true);  // 设置 searchCount 为 true
        
        // 构建查询条件
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<SysUser>()
                .like(StringUtils.isNotBlank(query.getUsername()), SysUser::getUsername, query.getUsername())
                .like(StringUtils.isNotBlank(query.getNickname()), SysUser::getNickname, query.getNickname())
                .like(StringUtils.isNotBlank(query.getPhone()), SysUser::getPhone, query.getPhone())
                .eq(query.getStatus() != null, SysUser::getStatus, query.getStatus())
                .eq(SysUser::getDeleted, 0)
                .orderByDesc(SysUser::getCreateTime);
        
        // 执行分页查询
        return baseMapper.selectPage(page, wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addUser(UserDTO userDTO) {
        // 检查用户名是否已存在
        long count = count(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, userDTO.getUsername()));
        if (count > 0) {
            throw new RuntimeException("用户名已存在");
        }

        // 创建用户
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userDTO, user);
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setStatus(1);
        save(user);

        // 保存用户角色关系
        if (userDTO.getRoleIds() != null && !userDTO.getRoleIds().isEmpty()) {
            userMapper.insertUserRoles(user.getId(), userDTO.getRoleIds());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(Long id, UserDTO userDTO) {
        // 检查用户是否存在
        SysUser user = getById(id);
        if (user == null) {
            throw new RuntimeException("用户不存在");
        }

        // 检查用户名是否已被其他用户使用
        long count = count(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, userDTO.getUsername())
                .ne(SysUser::getId, id));
        if (count > 0) {
            throw new RuntimeException("用户名已存在");
        }

        // 更新用户信息
        BeanUtils.copyProperties(userDTO, user);
        if (StringUtils.isNotBlank(userDTO.getPassword())) {
            user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        }
        updateById(user);

        // 更新用户角色关系
        userMapper.deleteUserRoles(id);
        if (userDTO.getRoleIds() != null && !userDTO.getRoleIds().isEmpty()) {
            userMapper.insertUserRoles(id, userDTO.getRoleIds());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(Long id, Integer status) {
        SysUser user = new SysUser();
        user.setId(id);
        user.setStatus(status);
        updateById(user);
    }

    @Override
    public List<Long> getUserRoleIds(Long userId) {
        return userMapper.selectRoleIdsByUserId(userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUserRoles(Long userId, List<Long> roleIds) {
        // 先删除原有关系
        userMapper.deleteUserRoles(userId);
        // 添加新的关系
        if (roleIds != null && !roleIds.isEmpty()) {
            userMapper.insertUserRoles(userId, roleIds);
        }
    }

    @Override
    public List<SysRole> getUserRoles(Long userId) {
        return userRoleMapper.selectRolesByUserId(userId);
    }
} 