package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.entity.SysRole;
import com.example.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.model.dto.UserDTO;
import com.example.model.query.UserQuery;

import java.util.List;

public interface UserService extends IService<SysUser> {
    SysUser getByUsername(String username);
    List<String> getUserPermissions(Long userId);
    IPage<SysUser> getUserPage(UserQuery query);
    void addUser(UserDTO userDTO);
    void updateUser(Long id, UserDTO userDTO);
    void updateStatus(Long id, Integer status);
    List<Long> getUserRoleIds(Long userId);
    void updateUserRoles(Long userId, List<Long> roleIds);
    List<SysRole> getUserRoles(Long userId);
} 