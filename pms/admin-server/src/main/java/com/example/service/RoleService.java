package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.SysRole;
import com.example.model.dto.RoleDTO;
import com.example.model.query.RoleQuery;

import java.util.List;

public interface RoleService extends IService<SysRole> {
    IPage<SysRole> getRolePage(RoleQuery query);
    void addRole(RoleDTO roleDTO);
    void updateRole(Long id, RoleDTO roleDTO);
    List<Long> getRoleMenuIds(Long roleId);
    void assignMenus(Long roleId, List<Long> menuIds);
} 