package com.example.security;

import org.springframework.stereotype.Component;
import java.util.List;

@Component("ss")
public class SecurityService {
    
    public boolean hasPermission(String permission) {
        // 获取当前用户权限列表
        List<String> permissions = SecurityUtils.getLoginUser().getPermissions();
        return permissions != null && permissions.contains(permission);
    }
} 