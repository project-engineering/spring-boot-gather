package com.example.security;

import com.example.model.vo.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
    
    /**
     * 获取当前登录用户
     */
    public static LoginUser getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new RuntimeException("用户未登录");
        }
        Object principal = authentication.getPrincipal();
        if (principal instanceof LoginUser) {
            return (LoginUser) principal;
        }
        throw new RuntimeException("获取用户信息失败");
    }
    
    /**
     * 获取当前登录用户ID
     */
    public static Long getUserId() {
        return getLoginUser().getId();
    }
    
    /**
     * 获取当前登录用户名
     */
    public static String getUsername() {
        return getLoginUser().getUsername();
    }
} 