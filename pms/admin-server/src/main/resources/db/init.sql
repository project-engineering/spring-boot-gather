-- 创建数据库
CREATE DATABASE IF NOT EXISTS admin_db DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE admin_db;

-- 用户表
CREATE TABLE IF NOT EXISTS sys_user (
    id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    username VARCHAR(50) NOT NULL COMMENT '用户名',
    password VARCHAR(100) NOT NULL COMMENT '密码',
    nickname VARCHAR(50) COMMENT '昵称',
    avatar VARCHAR(200) COMMENT '头像',
    email VARCHAR(100) COMMENT '邮箱',
    phone VARCHAR(20) COMMENT '手机号',
    gender TINYINT DEFAULT 0 COMMENT '性别：0-未知，1-男，2-女',
    status TINYINT DEFAULT 1 COMMENT '状态：0-禁用，1-正常',
    deleted TINYINT DEFAULT 0 COMMENT '是否删除：0-未删除，1-已删除',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (id),
    UNIQUE KEY uk_username (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统用户表';

-- 角色表
CREATE TABLE IF NOT EXISTS sys_role (
    id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    role_name VARCHAR(50) NOT NULL COMMENT '角色名称',
    role_code VARCHAR(50) NOT NULL COMMENT '角色编码',
    description VARCHAR(200) COMMENT '角色描述',
    sort_order INT DEFAULT 0 COMMENT '排序',
    status TINYINT DEFAULT 1 COMMENT '状态：0-禁用，1-正常',
    deleted TINYINT DEFAULT 0 COMMENT '是否删除：0-未删除，1-已删除',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
    PRIMARY KEY (id),
    UNIQUE KEY uk_role_code (role_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- 菜单表
CREATE TABLE IF NOT EXISTS sys_menu (
    id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    parent_id BIGINT DEFAULT 0 COMMENT '父菜单ID',
    menu_name VARCHAR(50) NOT NULL COMMENT '菜单名称',
    menu_type VARCHAR(10) COMMENT '菜单类型：MENU-菜单，BUTTON-按钮',
    path VARCHAR(200) COMMENT '路由路径',
    component VARCHAR(200) COMMENT '组路径',
    perms VARCHAR(100) COMMENT '权限标识',
    icon VARCHAR(100) COMMENT '图标',
    sort_order INT DEFAULT 0 COMMENT '排序',
    status TINYINT DEFAULT 1 COMMENT '状态：0-禁用，1-正常',
    deleted TINYINT DEFAULT 0 COMMENT '是否删除：0-未删除，1-已删除',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

-- 用户角色关联表
CREATE TABLE IF NOT EXISTS sys_user_role (
    user_id BIGINT NOT NULL COMMENT '用户ID',
    role_id BIGINT NOT NULL COMMENT '角色ID',
    PRIMARY KEY (user_id, role_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关联表';

-- 角色菜单关联表
CREATE TABLE IF NOT EXISTS sys_role_menu (
    role_id BIGINT NOT NULL COMMENT '角色ID',
    menu_id BIGINT NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (role_id, menu_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单关联表';

-- 插入初始数据
INSERT INTO sys_user (username, password, nickname, status)
VALUES ('admin', '$2a$10$6OOLMiYFlTerz9y6wDdm8e9U1QFHgEm8koKYivlWvqWJNOIdGWgGe', '管理员', 1);

INSERT INTO sys_role (role_name, role_code, description, sort_order)
VALUES ('超级管理员', 'ROLE_ADMIN', '系统超级管理员', 1);

INSERT INTO sys_user_role (user_id, role_id)
VALUES (1, 1);

-- 插入菜单数据
INSERT INTO sys_menu (id, parent_id, menu_name, menu_type, path, component, perms, icon, sort_order)
VALUES 
(1, 0, '首页', 'MENU', '/dashboard', 'dashboard/index', 'dashboard:view', 'HomeFilled', 1),
(2, 0, '系统管理', 'MENU', '/system', 'Layout', null, 'Setting', 2),
(3, 2, '用户管理', 'MENU', '/system/user', 'system/user/index', 'system:user:list', 'User', 1),
(4, 2, '角色管理', 'MENU', '/system/role', 'system/role/index', 'system:role:list', 'UserFilled', 2),
(5, 2, '菜单管理', 'MENU', '/system/menu', 'system/menu/index', 'system:menu:list', 'Menu', 3);

-- 插入按钮权限
INSERT INTO sys_menu (parent_id, menu_name, menu_type, perms, sort_order)
VALUES 
(3, '用户新增', 'BUTTON', 'system:user:add', 1),
(3, '用户编辑', 'BUTTON', 'system:user:edit', 2),
(3, '用户删除', 'BUTTON', 'system:user:delete', 3);

-- 添加用户管理相关权限
INSERT INTO sys_menu (parent_id, menu_name, menu_type, path, component, perms, icon, sort_order, status) VALUES
(3, '修改用户状态', 'BUTTON', '', '', 'system:user:status', '', 4, 1);

-- 为超级管理员角色分配所有菜单权限
INSERT INTO sys_role_menu (role_id, menu_id)
SELECT 1, id FROM sys_menu;

-- 修改菜单表结构
ALTER TABLE sys_menu 
MODIFY COLUMN menu_type VARCHAR(10) COMMENT '菜单类型：MENU-菜单，BUTTON-按钮',
CHANGE COLUMN permission perms VARCHAR(100) COMMENT '权限标识';

-- 检查菜单权限是否存在
SELECT * FROM sys_menu WHERE perms = 'system:user:status';

-- 如果不存在，添加权限
INSERT INTO sys_menu (parent_id, menu_name, menu_type, perms, sort_order, status)
SELECT 
  id,  -- 用户管理的菜单ID
  '修改用户状态', 
  'BUTTON',
  'system:user:status',
  4,
  1
FROM sys_menu 
WHERE menu_name = '用户管理' AND menu_type = 'MENU';

-- 为超级管理员角色分配该权限
INSERT INTO sys_role_menu (role_id, menu_id)
SELECT DISTINCT 
    1,  -- 超级管理员角色ID
    m.id
FROM sys_menu m
WHERE m.perms = 'system:user:status'
AND NOT EXISTS (
    SELECT 1 FROM sys_role_menu rm 
    WHERE rm.role_id = 1 AND rm.menu_id = m.id
);

-- 检查权限是否正确分配
SELECT m.* 
FROM sys_menu m
JOIN sys_role_menu rm ON m.id = rm.menu_id
WHERE rm.role_id = 1 AND m.perms = 'system:user:status';