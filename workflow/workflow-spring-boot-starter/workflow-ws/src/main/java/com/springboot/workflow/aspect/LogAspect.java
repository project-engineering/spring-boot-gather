package com.springboot.workflow.aspect;

import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.util.IPUtils;
import com.springboot.workflow.entity.system.log.SysLogErrorInfo;
import com.springboot.workflow.entity.system.log.SysLogInfo;
import com.springboot.workflow.system.service.log.SysLogErrorInfoService;
import com.springboot.workflow.system.service.log.SysLogInfoService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * 切面处理类，操作日志异常日志记录处理
 */
@Aspect
@Component
@Log4j2
public class LogAspect {
    /** 换行符 */
    private static final String LINE_SEPARATOR = System.lineSeparator();
    /**
     * 操作版本号
     * 项目启动时从命令行传入，例如：java -jar xxx.war --version=201902
     */
    @Value("${spring.application.version}")
    private String version;

    /**
     * 统计请求的处理时间
     */
    ThreadLocal<Long> startTime = new ThreadLocal<>();

    @Resource
    private SysLogInfoService sysLogInfoService;

    @Resource
    private SysLogErrorInfoService sysLogErrorInfoService;

    /** 以自定义 @WebLog 注解为切点 */
    @Pointcut("@annotation(com.springboot.workflow.common.annotation.WebLog)")
    public void webLog() {}

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());
        // 开始打印请求日志
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();

        // 获取 @WebLog 注解的描述信息
        String methodDescription = getAspectLogDescription(joinPoint);
        // 打印请求相关参数
        log.info("=========================================="+joinPoint.getSignature().getName() +" Start ==========================================");
        // 打印请求 url
        log.info("URL            : {}", request.getRequestURL().toString());
        // 打印描述信息
        log.info("Description    : {}", methodDescription);
        // 打印 Http method
        log.info("HTTP Method    : {}", request.getMethod());
        // 打印调用 controller 的全路径以及执行方法
        log.info("Class Method   : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        // 打印请求的 IP
        log.info("IP             : {}", request.getRemoteAddr());
        // 打印请求入参
        log.info("Request Args   : {}", JSONUtil.toJsonStr(joinPoint.getArgs()));
    }

    /**
     * 正常返回通知，拦截用户操作日志，连接点正常执行完成后执行， 如果连接点抛出异常，则不会执行
     */
    @AfterReturning(value = "webLog()", returning = "keys")
    public void doAfterReturning(JoinPoint joinPoint, Object keys) {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) Objects.requireNonNull(requestAttributes).resolveReference(RequestAttributes.REFERENCE_REQUEST);

        SysLogInfo logInfo = SysLogInfo.builder().build();
        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();

            // 获取切入点所在的方法
            Method method = signature.getMethod();

            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();

            // 获取操作
            WebLog log = method.getAnnotation(WebLog.class);
            if (log.saveLog()) {
                logInfo.setModule(log.modul());
                logInfo.setType(log.type());
                logInfo.setMessage(log.desc());

                logInfo.setId(IdUtil.simpleUUID());
                logInfo.setMethod(className + "." + method.getName()); // 请求的方法名
                logInfo.setReqParam(JSONUtil.toJsonStr(converMap(Objects.requireNonNull(request).getParameterMap()))); // 请求参数
                logInfo.setResParam(JSONUtil.toJsonStr(keys)); // 返回结果
//            logInfo.setUserId(SecurityUserUtils.getUser().getId()); // 请求用户ID
//            logInfo.setUserName(SecurityUserUtils.getUser().getUsername()); // 请求用户名称
                logInfo.setIp(IPUtils.getIpAddr(request)); // 请求IP
                logInfo.setUri(request.getRequestURI()); // 请求URI
                logInfo.setCreateTime(LocalDateTime.now()); // 创建时间
                logInfo.setVersion(version); // 操作版本
                logInfo.setTakeUpTime((System.currentTimeMillis() - startTime.get())+ "ms"); // 耗时
                sysLogInfoService.save(logInfo);
            }
        } catch (Exception e) {
            log.error("操作日志记录异常", e);
            throw new BusinessException("操作日志记录异常");
        }
    }

    /**
     * 环绕
     * @param proceedingJoinPoint 切点
     * @return Object 返回值
     * @throws Throwable 异常
     */
    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        log.info("Response Args  : {}", JSONUtil.toJsonStr(result));
        // 执行耗时
        log.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        // 接口结束后换行，方便分割查看
        log.info("==========================================="+proceedingJoinPoint.getSignature().getName()+" End ===========================================" + LINE_SEPARATOR);
        return result;
    }

    /**
     * 异常返回通知，用于拦截异常日志信息 连接点抛出异常后执行，并记录异常日志
     */
    @AfterThrowing(pointcut = "webLog()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) Objects.requireNonNull(requestAttributes).resolveReference(RequestAttributes.REFERENCE_REQUEST);

        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();

            // 获取切入点所在的方法
            Method method = signature.getMethod();

            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            WebLog log = method.getAnnotation(WebLog.class);
            if (log.saveLog()) {
                sysLogErrorInfoService.save(
                        SysLogErrorInfo.builder()
                                .id(UUID.randomUUID().toString())
                                .reqParam(JSONUtil.toJsonStr(converMap(Objects.requireNonNull(request).getParameterMap()))) // 请求参数
                                .method(className + "." + method.getName()) // 请求方法名
                                .name(e.getClass().getName()) // 异常名称
                                .message(stackTraceToString(e.getClass().getName(), e.getMessage(), e.getStackTrace())) // 异常信息
//                            .userId(SecurityUserUtils.getUser().getId()) // 操作员ID
//                            .userName(SecurityUserUtils.getUser().getUsername()) // 操作员名称
                                .uri(request.getRequestURI()) // 操作URI
                                .ip(IPUtils.getIpAddr(request)) // 操作员IP
                                .version(version) // 版本号
                                .createTime(LocalDateTime.now()) // 发生异常时间
                                .build()
                );
            }
        } catch (Exception e2) {
            log.error("操作异常日志记录异常", e2);
            throw new BusinessException("操作异常日志记录异常");
        }
    }

    /**
     * 转换request 请求参数
     * @return Map<String, String>
     */
    public Map<String, String> converMap(Map<String, String[]> paramMap) {
        Map<String, String> rtnMap = new HashMap<String, String>();
        for (String key : paramMap.keySet()) {
            rtnMap.put(key, paramMap.get(key)[0]);
        }
        return rtnMap;
    }

    /**
     * 转换异常信息为字符串
     */
    public String stackTraceToString(String exceptionName, String exceptionMessage, StackTraceElement[] elements) {
        StringBuilder strbuff = new StringBuilder();
        for (StackTraceElement stet : elements) {
            strbuff.append(stet).append("<br/>");
        }
        return exceptionName + ":" + exceptionMessage + "<br/>" + strbuff.toString();
    }


    /**
     * 获取切面注解的描述
     *
     * @param joinPoint 切点
     * @return 描述信息
     */
    public String getAspectLogDescription(JoinPoint joinPoint) {
        try {
            String targetName = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            Object[] arguments = joinPoint.getArgs();
            Class<?> targetClass = Class.forName(targetName);
            Method[] methods = targetClass.getMethods();
            StringBuilder description = new StringBuilder();
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                    Class[] clazzs = method.getParameterTypes();
                    if (clazzs.length == arguments.length) {
                        description.append(method.getAnnotation(WebLog.class).desc());
                        break;
                    }
                }
            }
            return description.toString();
        } catch (Exception e) {
            log.error("获取切面注解的描述异常",e);
            throw new BusinessException(e.getMessage());
        }
    }
}
