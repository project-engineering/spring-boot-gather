package com.springboot.workflow.controller.activiti;

import com.springboot.workflow.activiti.service.ProcessHistoryService;
import com.springboot.workflow.api.activiti.finished.param.FinishedListParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 已办任务
 */
@Tag(name = "已办任务", description = "已办任务")
@RequestMapping("/api/processFinished")
@RestController
public class ProcessFinishedController {

    @Resource
    private ProcessHistoryService processHistoryService;

    /**
     * 查看我的已办任务，包括已完成和未完成的流程
     *
     * @param param 参数
     */
    @Operation(summary = "已办任务")
    @WebLog(modul = "已办任务", type = Constants.SELECT, desc = "查看我的已办任务")
    @PostMapping("/list")
    public Result list(@RequestBody FinishedListParam param) {
        return processHistoryService.queryPage(param);
    }

}
