package com.springboot.workflow.controller.activiti;

import com.springboot.workflow.activiti.service.ProcessDefinitionService;
import com.springboot.workflow.api.activiti.definition.params.DefinitionListParam;
import com.springboot.workflow.api.activiti.definition.params.DeployProcessParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

/**
 * 流程定义控制器
 */
@Tag(name = "流程定义", description = "流程定义")
@RequestMapping("/api/processDefinition")
@RestController
public class ProcessDefinitionController {
    @Resource
    private ProcessDefinitionService processDefinitionService;

    @Operation(summary = "流程定义分页查询", description = "流程定义分页查询")
    @WebLog(modul = "流程管理", type = Constants.SELECT, desc = "流程定义分页查询")
    @PostMapping("/list")
    public Result list(@RequestBody DefinitionListParam param) {
        return processDefinitionService.queryPage(param);
    }

    /**
     * 部署流程
     *
     * @param param 参数
     */
    @Operation(summary = "部署流程")
    @WebLog(modul = "流程定义", type = Constants.ADD, desc = "部署流程",saveLog = true)
    @PostMapping("deployProcess")
    public Result deployProcess(@Valid @RequestBody DeployProcessParam param) {
        processDefinitionService.deployProcess(param);
        return Result.success();
    }

    /**
     * 获取流程定义xml
     *
     * @param deploymentId 部署id
     */
    @Operation(summary = "获取流程定义xml")
    @WebLog(modul = "流程定义", type = Constants.SELECT, desc = "获取流程定义xml")
    @GetMapping("/getDefinitionXml/{deploymentId}")
    public Result getDefinitionXml(@PathVariable("deploymentId") String deploymentId) {
        return Result.success(processDefinitionService.getDefinitionXml(deploymentId));
    }

    /**
     * 获取流程定义详情
     *
     * @param deploymentId 部署id
     */
    @Operation(summary = "获取流程定义详情")
    @WebLog(modul = "流程定义", type = Constants.SELECT, desc = "获取流程定义详情")
    @GetMapping("/getDefinitionInfo/{deploymentId}")
    public Result getDefinitionInfo(@PathVariable("deploymentId") String deploymentId) {
        return Result.success(processDefinitionService.getDefinitionInfo(deploymentId));
    }

    /**
     * 更新流程定义状态 激活或者挂起
     *
     * @param deploymentId 部署id
     */
    @Operation(summary = "更新流程定义状态 激活或者挂起")
    @WebLog(modul = "流程定义", type = Constants.UPDATE, desc = "更新流程定义状态 激活或者挂起",saveLog = true)
    @PutMapping("/updateState/{deploymentId}")
    public Result updateState(@PathVariable("deploymentId") String deploymentId) {
        processDefinitionService.updateState(deploymentId);
        return Result.success();
    }


    /**
     * 删除
     *
     * @param deploymentId 主键
     */
    @Operation(summary = "删除")
    @WebLog(modul = "流程定义", type = Constants.DELETE, desc = "删除流程定义信息",saveLog = true)
    @DeleteMapping("/delete/{deploymentId}")
    public Result delete(@PathVariable("deploymentId") String deploymentId) {
        processDefinitionService.delete(deploymentId);
        return Result.success();
    }
}
