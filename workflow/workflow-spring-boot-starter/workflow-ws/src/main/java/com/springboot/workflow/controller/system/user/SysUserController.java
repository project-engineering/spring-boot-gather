package com.springboot.workflow.controller.system.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.springboot.workflow.api.system.RegisterParam;
import com.springboot.workflow.api.system.user.GetUserInfoParam;
import com.springboot.workflow.api.system.user.UpdatePwdParam;
import com.springboot.workflow.api.system.user.UserParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.entity.system.user.SysUser;
import com.springboot.workflow.system.service.user.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "用户管理", description = "用户管理")
@RestController
@RequestMapping("/api/user")
public class SysUserController {
    @Resource
    private SysUserService sysUserService;

    @PreAuthorize("hasAnyAuthority('sys:user:add')")
    @Operation(summary = "新增用户信息")
    @WebLog(modul = "用户管理", type = Constants.ADD, desc = "新增用户信息")
    @PostMapping
    public Result add(@RequestBody SysUser sysUser){
        if (sysUserService.addUser(sysUser)) {
            return Result.success("新增用户成功!");
        }
        return Result.fail("新增用户失败!");
    }

    @PreAuthorize("hasAnyAuthority('sys:user:update')")
    @Operation(summary = "修改用户信息")
    @WebLog(modul = "用户管理", type = Constants.UPDATE, desc = "修改用户信息")
    @PutMapping
    public Result update(@RequestBody SysUser sysUser){
        if (sysUserService.updateUser(sysUser)) {
            return Result.success("修改用户成功!");
        }
        return Result.fail("修改用户失败!");
    }

    @PreAuthorize("hasAnyAuthority('sys:user:delete')")
    @Operation(summary = "删除角色信息")
    @WebLog(modul = "用户管理", type = Constants.DELETE, desc = "删除角色信息")
    @DeleteMapping("/{userId}")
    public Result delete(@PathVariable("userId") String userId){
        if (sysUserService.deleteUser(userId)) {
            return Result.success("删除用户成功!");
        }
        return Result.fail("删除用户失败!");
    }

    @Operation(summary = "分页查询用户信息")
    @WebLog(modul = "用户管理", type = Constants.SELECT, desc = "分页查询用户信息")
    @PostMapping("/list")
    public Result list(@RequestBody UserParam param){
        IPage<SysUser> list = sysUserService.getList(param);
        return Result.success(list,list.getTotal());
    }

    @PreAuthorize("hasAnyAuthority('sys:user:reset')")
    @Operation(summary = "重置密码")
    @WebLog(modul = "用户管理", type = Constants.SELECT, desc = "重置密码")
    @PostMapping("/resetPwd/{userId}")
    public Result resetPwd(@PathVariable("userId") String userId){
        if (sysUserService.resetPwd(userId)>0) {
            return Result.success("重置密码成功!");
        }
        return Result.fail("重置密码失败!");
    }

    @Operation(summary = "注册")
    @PostMapping("/register")
    public Result register(@RequestBody RegisterParam registerParam){
        sysUserService.register(registerParam);
        return Result.success("注册成功!");
    }

    @Operation(summary = "修改密码")
    @PostMapping("/updatePwd")
    public Result<Void> updatePwd(@RequestBody UpdatePwdParam param){
        sysUserService.updatePwd(param);
        return Result.success();
    }

    @Operation(summary = "获取用户信息")
    @WebLog(modul = "用户管理", type = Constants.SELECT, desc = "获取用户信息")
    @PostMapping("/getUserInfo")
    public Result getUserInfo(@RequestBody GetUserInfoParam param){
         return Result.success(sysUserService.getUserInfo(param.getUserId()));
    }
}
