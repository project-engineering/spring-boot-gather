package com.springboot.workflow.controller.activiti;

import com.springboot.workflow.activiti.service.TableService;
import com.springboot.workflow.api.activiti.table.params.QueryTableListParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "数据库管理")
@RequestMapping("/api/table")
@RestController
public class  TableController {
    @Resource
    private TableService tableService;
    /**
     * 获取组件类型
     *
     * @return 结果
     */
    @Operation(summary = "获取组件类型")
    @WebLog(modul = "数据库管理", type = Constants.SELECT, desc = "获取组件类型")
    @GetMapping("getWidgetDataType")
    public Result getWidgetDataType() {
        return Result.success(tableService.getWidgetDataType());
    }

    /**
     * 表名称
     *
     * @param param 数据库表信息
     * @return 结果
     */
    @Operation(summary = "获取表名称")
    @WebLog(modul = "数据库管理", type = Constants.SELECT, desc = "获取表名称")
    @PostMapping("list")
    public Result list(@RequestBody QueryTableListParam param) {
        return Result.success(tableService.tableList(param.getTableName()));
    }

    /**
     * 数据库表结构
     *
     * @param tableName 表名称或表备注
     * @return 表结构信息
     */
    @Operation(summary = "获取数据库表结构")
    @WebLog(modul = "数据库管理", type = Constants.SELECT, desc = "获取数据库表结构")
    @GetMapping("tableColumns")
    public Result tableColumns(@RequestParam String tableName,
                                              @RequestParam(required = false) String columnKey) {
        return Result.success(tableService.tableColumns(tableName, columnKey));
    }
}
