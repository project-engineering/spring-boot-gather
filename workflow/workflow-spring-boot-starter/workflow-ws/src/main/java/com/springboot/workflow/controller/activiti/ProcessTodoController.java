package com.springboot.workflow.controller.activiti;

import com.springboot.workflow.activiti.service.ProcessTodoService;
import com.springboot.workflow.api.activiti.todo.params.TodoCompleteParam;
import com.springboot.workflow.api.activiti.todo.params.TodoListParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "代办任务")
@RequestMapping("/api/processTodo")
@RestController
public class ProcessTodoController {
    @Resource
    private ProcessTodoService processTodoService;

    /**
     * 查看我代办的流程
     *
     * @param param 参数
     */
    @Operation(summary = "查看我代办的流程")
    @WebLog(modul = "代办任务", type = Constants.SELECT, desc = "查看我代办的流程")
    @PostMapping("/list")
    public Result list(@RequestBody TodoListParam param) {
        return processTodoService.queryPage(param);
    }

    /**
     * 获取节点表单
     * @param taskId 任务id
     * @return 表单数据
     */
    @Operation(summary = "获取节点表单")
    @WebLog(modul = "代办任务", type = Constants.SELECT, desc = "获取节点表单")
    @GetMapping("getNodeForm/{taskId}")
    public Result getNodeForm(@PathVariable("taskId") String taskId){
        return Result.success(processTodoService.getNodeForm(taskId));
    }

    /**
     * 审批节点
     *
     * @param param 参数
     */
    @Operation(summary = "审批节点")
    @WebLog(modul = "代办任务", type = Constants.ADD, desc = "审批节点")
    @PostMapping("complete")
    public Result complete(@RequestBody TodoCompleteParam param) {
        processTodoService.complete(param);
        return Result.success();
    }
}
