package com.springboot.workflow.controller.system.dept;

import com.springboot.workflow.api.system.dept.params.AddSysDeptParam;
import com.springboot.workflow.api.system.dept.params.QuerySysDeptPageParam;
import com.springboot.workflow.api.system.dept.params.UpdateSysDeptParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.system.service.dept.SysDeptService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/dept")
@RestController
@Tag(name = "部门管理", description = "部门管理相关接口")
public class SysDeptController {
    @Resource
    private SysDeptService deptService;

    @Operation(summary = "分页查询部门信息")
    @WebLog(modul = "部门管理", type = Constants.SELECT, desc = "分页查询部门信息")
    @PostMapping("list")
    public Result list(@RequestBody QuerySysDeptPageParam param) {
        return Result.success(deptService.queryPage(param));
    }

    @Operation(summary = "部门详情")
    @WebLog(modul = "部门管理", type = Constants.SELECT, desc = "部门详情")
    @GetMapping("/info/{deptId}")
    public Result info(@PathVariable("deptId") String deptId) {
        return Result.success(deptService.getById(deptId));
    }

    @Operation(summary = "新增部门信息")
    @WebLog(modul = "部门管理", type = Constants.ADD, desc = "新增部门信息")
    @PostMapping("/add")
    public Result add(@RequestBody AddSysDeptParam param) {
        deptService.saveSysDept(param);
        return Result.success();
    }

    @Operation(summary = "修改部门信息")
    @WebLog(modul = "部门管理", type = Constants.UPDATE, desc = "修改部门信息",saveLog = true)
    @PutMapping("/update")
    public Result update(@RequestBody UpdateSysDeptParam param) {
        deptService.updateSysDept(param);
        return Result.success();
    }

    @Operation(summary = "删除部门信息")
    @WebLog(modul = "部门管理", type = Constants.DELETE, desc = "删除部门信息")
    @DeleteMapping("/delete/{deptId}")
    public Result delete(@PathVariable("deptId")  String deptId) {
        deptService.deleteSysDept(deptId);
        return Result.success();
    }
}
