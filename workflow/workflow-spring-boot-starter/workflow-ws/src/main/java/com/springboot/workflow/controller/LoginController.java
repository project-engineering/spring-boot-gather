package com.springboot.workflow.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.ObjectUtil;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.springboot.workflow.api.system.LoginDTO;
import com.springboot.workflow.api.system.LoginParam;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.system.service.user.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Log4j2
@Tag(name = "登录接口")
@RestController
public class LoginController {
    @Resource
    private DefaultKaptcha defaultKaptcha;

    @Resource
    private SysUserService userService;

    @Operation(description = "登录")
    @PostMapping("/login")
    public Result<LoginDTO> login(HttpServletRequest request, @RequestBody LoginParam param) {
        //验证码校验
        String captcha = request.getSession().getAttribute("captcha").toString();
        if (ObjectUtil.isEmpty(captcha)) {
            throw new BusinessException("验证码已失效!");
        }
        if (!captcha.equalsIgnoreCase(param.getCaptcha())) {
            throw new BusinessException("验证码错误!");
        }
        return Result.success(userService.login(param));
    }

    @Operation(description = "获取验证码")
    @GetMapping("/getCaptcha")
    public Result<String> getCaptcha(HttpServletRequest request) {
        //获取验证码字符
        String text = defaultKaptcha.createText();
        log.info("验证码：{}",text);
        //将验证码存入session
        request.getSession().setAttribute("captcha", text);
        //生成验证码图片
        BufferedImage image = defaultKaptcha.createImage(text);
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", outputStream);
            String base64 = Base64.encode(outputStream.toByteArray());
            //将图片转为base64字符串
            String img = "data:image/jpeg;base64," + base64.replaceAll("\r\n", "");
            return Result.success(img);
        } catch (IOException e) {
            log.error("生成验证码图片失败", e);
            throw new BusinessException("生成验证码图片失败!");
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                log.error("关闭输出流失败", e);
            }
        }
    }

}
