package com.springboot.workflow.controller.system;

import com.springboot.workflow.api.system.menu.SaveMenuParam;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.system.service.SysRoleMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色菜单表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "角色菜单表")
@RestController
@RequestMapping("/api/roleMenu")
public class SysRoleMenuController {
    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @Operation(summary = "给角色分配菜单")
    @PostMapping("/saveRoleMenu")
    public Result<Void> saveRoleMenu(@RequestBody SaveMenuParam saveMenuParam) {
        sysRoleMenuService.saveRoleMenu(saveMenuParam);
        return Result.success();
    }
}
