package com.springboot.workflow.controller.activiti;

import com.springboot.workflow.activiti.service.ProcessHistoryService;
import com.springboot.workflow.activiti.service.ProcessStartService;
import com.springboot.workflow.api.activiti.start.params.StartListParam;
import com.springboot.workflow.api.activiti.start.params.StartProcessParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "流程启动")
@RequestMapping("/api/processStart")
@RestController
public class ProcessStartController {
    @Resource
    private ProcessStartService processStartService;

    @Resource
    private ProcessHistoryService processHistoryService;

    /**
     * 我发起的任务列表
     *
     * @param param 参数
     */
    @Operation(summary = "我发起的任务列表")
    @WebLog(modul = "流程启动", type = Constants.SELECT, desc = "查看我发起的任务列表")
    @PostMapping("/list")
    public Result list(@RequestBody StartListParam param) {
        return processStartService.queryPage(param);
    }

    /**
     * 启动流程
     *
     * @param param 启动流程参数
     * @return 结果
     */
    @Operation(summary = "启动流程")
    @WebLog(modul = "流程启动", type = Constants.ADD, desc = "启动流程",saveLog = true)
    @PostMapping("/start")
    public Result start(@RequestBody StartProcessParam param) {
        processStartService.startProcess(param);
        return Result.success();
    }

    /**
     * 查询审批进度
     *
     * @param instanceId 流程实例id
     * @return 审批记录
     */
    @Operation(summary = "查询审批进度")
    @WebLog(modul = "流程启动", type = Constants.SELECT, desc = "查询审批进度")
    @GetMapping("/getHistoryRecord/{instanceId}")
    public Result getHistoryRecord(@PathVariable("instanceId") String instanceId) {
        return Result.success(processHistoryService.getHistoryRecord(instanceId));
    }

    /**
     * 查询流程图信息(高亮信息)
     *
     * @param instanceId 流程实例id
     * @return 流程图信息
     */
    @Operation(summary = "查询流程图信息(高亮信息)")
    @WebLog(modul = "流程启动", type = Constants.SELECT, desc = "查询流程图信息(高亮信息)")
    @GetMapping("/getHighlightNodeInfo/{instanceId}")
    public Result getHighlightNodeInfo(@PathVariable("instanceId") String instanceId) {
        return Result.success(processHistoryService.getHighlightNodeInfo(instanceId));
    }

    /**
     * 获取主表单信息
     *
     * @param instanceId 流程实例id
     * @return 主表单数据
     */
    @Operation(summary = "获取主表单信息")
    @WebLog(modul = "流程启动", type = Constants.SELECT, desc = "获取主表单信息")
    @GetMapping("/getMainFormInfo/{instanceId}")
    public Result getMainFormInfo(@PathVariable("instanceId") String instanceId) {
        return Result.success(processHistoryService.getMainFormInfo(instanceId));
    }

    /**
     * 删除流程实例
     *
     * @param instanceId 流程实例id
     */
    @Operation(summary = "删除流程实例")
    @WebLog(modul = "流程启动", type = Constants.DELETE, desc = "删除流程实例",saveLog = true)
    @DeleteMapping("/delete/{instanceId}")
    public Result delete(@PathVariable("instanceId") String instanceId) {
        processStartService.delete(instanceId);
        return Result.success();
    }
}
