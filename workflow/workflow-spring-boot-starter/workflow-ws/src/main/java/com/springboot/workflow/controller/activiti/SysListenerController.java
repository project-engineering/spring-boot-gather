package com.springboot.workflow.controller.activiti;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.springboot.workflow.activiti.service.SysListenerService;
import com.springboot.workflow.api.activiti.listener.param.SysListenerListParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.entity.activiti.SysListener;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "执行监听器")
@RestController
@RequestMapping("/api/listener")
public class SysListenerController {
    @Resource
    private SysListenerService listenerService;

    @Operation(summary = "分页查询执行监听器信息")
    @WebLog(modul = "执行监听器", type = Constants.SELECT, desc = "分页查询执行监听器信息")
    @PostMapping("/list")
    public Result list(@RequestBody SysListenerListParam param){
        IPage<SysListener> list = listenerService.queryPage(param);
        return Result.success(list,list.getTotal());
    }

    /**
     * 保存
     *
     * @param model 数据
     */
    @Operation(summary = "保存执行监听器")
    @WebLog(modul = "执行监听器", type = Constants.ADD, desc = "保存执行监听器")
    @PostMapping("/add")
    public Result add(@RequestBody SysListener model) {
        listenerService.addListener(model);
        return Result.success();
    }

    /**
     * 修改
     */
    @Operation(summary = "修改执行监听器信息")
    @WebLog(modul = "执行监听器", type = Constants.UPDATE, desc = "修改执行监听器信息")
    @PostMapping("/update")
    public Result update(@RequestBody SysListener model) {
        listenerService.updateListener(model);
        return Result.success();
    }



    /**
     * 删除
     *
     * @param listenerId 主键
     */
    @Operation(summary = "删除执行监听器")
    @WebLog(modul = "执行监听器", type = Constants.DELETE, desc = "删除执行监听器")
    @DeleteMapping("/delete/{listenerId}")
    public Result delete(@PathVariable String listenerId) {
        listenerService.deleteListener(listenerId);
        return Result.success();
    }
}
