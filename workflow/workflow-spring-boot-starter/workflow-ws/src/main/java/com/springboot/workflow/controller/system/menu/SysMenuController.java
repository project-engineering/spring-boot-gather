package com.springboot.workflow.controller.system.menu;

import com.springboot.workflow.api.system.menu.AssignTreeDTO;
import com.springboot.workflow.api.system.menu.AssignTreeParam;
import com.springboot.workflow.api.system.menu.GetMenuListParam;
import com.springboot.workflow.common.annotation.WebLog;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.entity.system.menu.SysMenu;
import com.springboot.workflow.system.service.menu.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "菜单管理")
@RestController
@RequestMapping("/api/menu")
public class SysMenuController {
    @Resource
    private SysMenuService sysMenuService;

    @Operation(summary = "新增菜单")
    @WebLog(modul = "菜单管理", type = Constants.ADD, desc = "新增菜单")
    @PostMapping("/addMenu")
    public Result addMenu(@RequestBody SysMenu sysMenu) {
        sysMenuService.saveMenu(sysMenu);
        return Result.success();
    }

    @Operation(summary = "修改菜单")
    @WebLog(modul = "菜单管理", type = Constants.UPDATE, desc = "修改菜单")
    @PutMapping("/updateMenu")
    public Result updateMenu(@RequestBody SysMenu sysMenu) {
        sysMenuService.updateMenu(sysMenu);
        return Result.success();
    }

    @Operation(summary = "删除菜单")
    @WebLog(modul = "菜单管理", type = Constants.DELETE, desc = "删除菜单")
    @DeleteMapping("/deleteMenu/{menuId}")
    public Result deleteMenu(@PathVariable("menuId")String menuId) {
        sysMenuService.deleteMenu(menuId);
        return Result.success();
    }

    @Operation(summary = "查询菜单列表")
    @WebLog(modul = "菜单管理", type = Constants.SELECT, desc = "查询菜单列表")
    @GetMapping("/list")
    public Result<List<SysMenu>> list() {
        List<SysMenu> list = sysMenuService.listMenu();
        List<SysMenu> treeList = sysMenuService.buildTree(list,"0");
        return Result.success(treeList);
    }

    @Operation(summary = "查询上级菜单")
    @WebLog(modul = "菜单管理", type = Constants.SELECT, desc = "查询上级菜单")
    @GetMapping("/getParentMenuList")
    public Result<List<SysMenu>> getParentMenuList() {
        List<SysMenu> parentList = sysMenuService.getParentMenuList();
        return Result.success(parentList);
    }

    @Operation(summary = "查询菜单树")
    @WebLog(modul = "菜单管理", type = Constants.SELECT, desc = "查询菜单树")
    @PostMapping("/getAssignTree")
    public Result<AssignTreeDTO> getAssignTree(@RequestBody AssignTreeParam assignTreeParam) {
        return Result.success(sysMenuService.getAssignTree(assignTreeParam));
    }


    @Operation(summary = "获取菜单")
    @WebLog(modul = "菜单管理", type = Constants.SELECT, desc = "获取菜单")
    @PostMapping("/getMenuList")
    public Result getMenuList(@RequestBody GetMenuListParam param) {
        return Result.success(sysMenuService.getMenuList(param.getUserId()));
    }
}
