package com.springboot.workflow.config.security.handler;

import com.alibaba.fastjson2.JSONObject;
import com.springboot.workflow.common.exception.CustomerAuthenticationException;
import com.springboot.workflow.common.response.Result;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 自定义认证失败的处理器
 */
@Component
public class LoginFailureHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        int code =500;
        String message = "";
        if (e instanceof AccountExpiredException) {
            message = "账号已过期，请联系管理员";
        } else if (e instanceof BadCredentialsException) {
            message = "用户名或密码错误";
        } else if (e instanceof CredentialsExpiredException) {
            message = "密码已过期，请联系管理员";
        } else if (e instanceof DisabledException) {
            message = "账号已被禁用，请联系管理员";
        } else if (e instanceof LockedException) {
            message = "账号已被锁定，请联系管理员";
        } else if (e instanceof AuthenticationCredentialsNotFoundException) {
            message = "用户名或密码不能为空";
        } else if (e instanceof InternalAuthenticationServiceException) {
            message = "用户名错误或不存在，登录失败，请联系管理员";
        } else if (e instanceof CustomerAuthenticationException) {
            code = 600;
            message = e.getMessage();
        } else if (e instanceof InsufficientAuthenticationException) {
            message = "无权限访问资源";
        } else {
            message = "登录失败，请联系管理员";
        }
        String res = JSONObject.toJSONString(new Result(code, message, null));
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream out = response.getOutputStream();
        out.write(res.getBytes(StandardCharsets.UTF_8));
        out.flush();
        out.close();
    }
}
