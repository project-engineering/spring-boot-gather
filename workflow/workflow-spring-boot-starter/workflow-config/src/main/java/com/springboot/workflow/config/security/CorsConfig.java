package com.springboot.workflow.config.security;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Collections;

@Configuration
public class CorsConfig {

    @Bean
    public FilterRegistrationBean<CorsFilter> filterRegistrationBean() {
        CorsConfiguration config = new CorsConfiguration();
        //1、设置允许跨域请求的域名
        config.setAllowedOriginPatterns(Collections.singletonList("*"));
        //2、设置允许跨域请求的请求头
        config.addAllowedHeader(CorsConfiguration.ALL);
        //3、设置允许跨域请求的方法
        config.addAllowedMethod(CorsConfiguration.ALL);
        //4、允许凭证
        config.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        CorsFilter filter = new CorsFilter(source);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(filter);
        //小于0的值，表示该Filter的执行顺序，值越小，执行越早
        bean.setOrder(-101);
        return bean;
    }
}
