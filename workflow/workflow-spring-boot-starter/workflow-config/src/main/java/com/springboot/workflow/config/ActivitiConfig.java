package com.springboot.workflow.config;

import org.activiti.engine.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Activiti配置类
 * 提供了流程引擎，流程定义和部署存储库的访问的服务，运行时数据的访问的服务，任务和流程实例的访问的服务，历史数据的访问的服务，以及流程引擎管理的访问的服务
 */
@Configuration
public class ActivitiConfig {

    /**
     * 提供对流程定义和部署存储库的访问的服务
     * 例如，可以用于查询流程定义，部署流程定义，删除流程定义等
     * @param processEngine 流程引擎
     * @return 流程定义和部署存储库的访问的服务
     */
    @Bean
    public RepositoryService repositoryService(ProcessEngine processEngine) {
        return processEngine.getRepositoryService();
    }

    /**
     * 提供对运行时数据的访问的服务
     * 例如，可以用于启动流程实例，查询流程实例的变量，设置流程实例的变量等
     * @param processEngine 流程引擎
     * @return 运行时数据的访问的服务
     */
    @Bean
    public RuntimeService runtimeService(ProcessEngine processEngine) {
        return processEngine.getRuntimeService();
    }

    /**
     * 提供对任务和流程实例的访问的服务
     * 例如，可以用于完成任务，查询流程实例的当前状态等
     * @param processEngine 流程引擎
     * @return 任务和流程实例的访问的服务
     */
    @Bean
    public TaskService taskService(ProcessEngine processEngine) {
        return processEngine.getTaskService();
    }

    /**
     * 提供对历史数据的访问的服务
     * 例如，可以用于查询流程实例的历史记录
     * @param processEngine 流程引擎
     * @return 历史数据的访问的服务
     */
    @Bean
    public HistoryService historyService(ProcessEngine processEngine) {
        return processEngine.getHistoryService();
    }

    /**
     * 用于流程引擎上的管理和维护操作的服务
     * 例如，可以用于启动和关闭流程引擎，创建和删除流程定义，以及执行流程实例的操作
     * @param processEngine 流程引擎
     * @return 流程引擎管理的访问的服务
     */
    @Bean
    public ManagementService managementService(ProcessEngine processEngine) {
        return processEngine.getManagementService();
    }

}
