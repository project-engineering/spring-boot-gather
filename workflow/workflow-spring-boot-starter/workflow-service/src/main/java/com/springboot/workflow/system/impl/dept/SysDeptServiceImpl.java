package com.springboot.workflow.system.impl.dept;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.api.system.dept.params.AddSysDeptParam;
import com.springboot.workflow.api.system.dept.params.QuerySysDeptPageParam;
import com.springboot.workflow.api.system.dept.params.UpdateSysDeptParam;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.entity.system.dept.SysDept;
import com.springboot.workflow.mapper.system.dept.SysDeptMapper;
import com.springboot.workflow.system.service.dept.SysDeptService;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {
    @Resource
    private SysDeptMapper sysDeptMapper;

    /**
     * 部门分页列表
     *
     * @param param 分页参数
     * @return 列表
     */
    @Override
    public IPage<SysDept> queryPage(QuerySysDeptPageParam param) {
        int pageNo = param.getPageNo();
        if (ObjectUtil.isEmpty(pageNo)) {
            pageNo = 1;
        }
        int pageSize = param.getPageSize();
        if (ObjectUtil.isEmpty(pageSize)) {
            pageSize = 10;
        }
        //构造分页对象
        IPage<SysDept> page = new Page<>(pageNo,pageSize);
        LambdaQueryWrapper<SysDept> queryWrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotEmpty(param.getDeptId())) {
            queryWrapper.eq(SysDept::getDeptId, param.getDeptId());
        }
        if (ObjectUtil.isNotEmpty(param.getDeptName())) {
            queryWrapper.like(SysDept::getDeptName, param.getDeptName());
        }
        if (ObjectUtil.isNotEmpty(param.getLeader())) {
            queryWrapper.eq(SysDept::getLeader, param.getLeader());
        }
        if (ObjectUtil.isNotEmpty(param.getPhone())) {
            queryWrapper.eq(SysDept::getPhone, param.getPhone());
        }
        if (ObjectUtil.isNotEmpty(param.getEmail())) {
            queryWrapper.eq(SysDept::getEmail, param.getEmail());
        }
        if (ObjectUtil.isNotEmpty(param.getIsSys())) {
            queryWrapper.eq(SysDept::getIsSys, param.getIsSys());
        }
        queryWrapper.orderByDesc(SysDept::getCreateTime);
        //执行查询
        return sysDeptMapper.selectPage(page,queryWrapper);
    }

    @Override
    public void saveSysDept(AddSysDeptParam param) {
        SysDept dept = new SysDept();
        if (StringUtils.isNotEmpty(param.getDeptId())) {
            dept = sysDeptMapper.selectById(param.getDeptId());
            if (ObjectUtil.isNotEmpty(dept)) {
                throw new BusinessException("部门已存在!");
            }
        } else {
            BeanUtil.copyProperties(param, dept);
            dept.setDeptId(IdUtil.simpleUUID());
            sysDeptMapper.insert(dept);
        }
    }

    @Override
    public void deleteSysDept(String deptId) {
        sysDeptMapper.deleteById(deptId);
    }

    @Override
    public void updateSysDept(UpdateSysDeptParam param) {
        if (StringUtils.isNotEmpty(param.getDeptId())) {
            SysDept dept = sysDeptMapper.selectById(param.getDeptId());
            if (ObjectUtil.isEmpty(dept)) {
                throw new BusinessException("部门不存在!");
            }
            BeanUtil.copyProperties(param, dept);
            sysDeptMapper.updateById(dept);
        }
    }
}