package com.springboot.workflow.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.workflow.entity.activiti.SysDeployNode;

/**
 * 流程部署节点数据
 *
 * @author liuguofeng
 * @email liuguofeng-java@qq.com
 * @date 2023-11-28 14:50:18
 */
public interface SysDeployNodeService extends IService<SysDeployNode> {

}

