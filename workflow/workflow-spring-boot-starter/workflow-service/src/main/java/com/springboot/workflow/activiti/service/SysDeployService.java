package com.springboot.workflow.activiti.service;

import com.springboot.workflow.entity.activiti.SysDeploy;
import java.util.Map;

/**
 * 流程部署详情
 */
public interface SysDeployService {
    /**
     * 插入数据到绑定数据库表中
     *
     * @param instanceId 实例id
     * @param deployId   部署id
     * @param activityId 流程定义节点唯一标识
     * @param variables  流程变量
     */
    void saveData(String instanceId, String deployId, String activityId, Map<String, Object> variables);

    boolean save(SysDeploy sysDeploy);

    void remove(String deploymentId);

    SysDeploy getById(String deploymentId);
}
