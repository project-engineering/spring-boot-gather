package com.springboot.workflow.activiti.service;

import com.springboot.workflow.api.activiti.finished.dto.HistoryRecordDto;
import com.springboot.workflow.api.activiti.finished.param.FinishedListParam;
import com.springboot.workflow.common.response.Result;
import java.util.List;
import java.util.Map;

/**
 * 流程历史相关
 */
public interface ProcessHistoryService {

    /**
     * 已办任务列表
     *
     * @param param 参数
     * @return 结果
     */
    Result queryPage(FinishedListParam param);

    /**
     * 查询审批进度
     *
     * @param instanceId 流程实例id
     * @return 审批记录
     */
    List<HistoryRecordDto> getHistoryRecord(String instanceId);

    /**
     * 查询流程图信息(高亮信息)
     *
     * @param instanceId 流程实例id
     * @return 流程图信息
     */
    Map<String, Object> getHighlightNodeInfo(String instanceId);

    /**
     * 获取主表单信息
     *
     * @param instanceId 流程实例id
     * @return 主表单数据
     */
    Map<String, Object> getMainFormInfo(String instanceId);

}
