package com.springboot.workflow.activiti.service;

import com.springboot.workflow.api.activiti.start.params.StartListParam;
import com.springboot.workflow.api.activiti.start.params.StartProcessParam;
import com.springboot.workflow.common.response.Result;

/**
 *  流程启动
 */
public interface ProcessStartService {
    /**
     * 我发起的任务列表
     *
     * @param param 参数
     * @return 结果
     */
    Result queryPage(StartListParam param);

    /**
     * 启动流程
     *
     * @param param    启动流程参数
     */
    void startProcess(StartProcessParam param);

    /**
     * 删除流程实例
     *
     * @param instanceId 流程实例id
     */
    void delete(String instanceId);
}
