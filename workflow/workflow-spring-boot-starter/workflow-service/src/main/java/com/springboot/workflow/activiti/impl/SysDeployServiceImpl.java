package com.springboot.workflow.activiti.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.activiti.service.SysDeployNodeService;
import com.springboot.workflow.activiti.service.SysDeployService;
import com.springboot.workflow.activiti.service.TableService;
import com.springboot.workflow.entity.activiti.SysDeploy;
import com.springboot.workflow.entity.activiti.SysDeployNode;
import com.springboot.workflow.mapper.activiti.SysDeployMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class SysDeployServiceImpl extends ServiceImpl<SysDeployMapper, SysDeploy> implements SysDeployService {
    @Resource
    private SysDeployNodeService deployNodeService;

    @Resource
    private TableService tableService;
    @Resource
    private SysDeployMapper sysDeployMapper;

    /**
     * 插入数据到绑定数据库表中
     *
     * @param instanceId 实例id
     * @param deployId   部署id
     * @param activityId 流程定义节点唯一标识
     * @param variables  流程变量
     */
    @Override
    public void saveData(String instanceId, String deployId, String activityId, Map<String, Object> variables) {
        SysDeploy sysDeploy = this.getById(deployId);
        // 如果没有找到数据就说明这个流程没有绑定数据库表
        if (sysDeploy == null) return;
        SysDeployNode deployNode = deployNodeService.getOne(new LambdaQueryWrapper<SysDeployNode>()
                .eq(SysDeployNode::getActivityId, activityId)
                .eq(SysDeployNode::getDeployId, deployId));
        if (deployNode != null && deployNode.getColumns() != null) {
            // 更新数据
            tableService.saveOrUpdateData(instanceId, sysDeploy.getTableName(),
                    deployNode.getColumns(), variables);
        }
    }

    @Override
    public boolean save(SysDeploy entity) {
        return super.save(entity);
    }

    @Override
    public void remove(String deploymentId) {
        sysDeployMapper.deleteById(deploymentId);
    }

    @Override
    public SysDeploy getById(String deploymentId) {
        return sysDeployMapper.selectById(deploymentId);
    }
}
