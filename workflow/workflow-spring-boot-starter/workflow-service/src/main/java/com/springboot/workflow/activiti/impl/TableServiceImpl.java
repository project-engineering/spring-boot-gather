package com.springboot.workflow.activiti.impl;

import com.alibaba.fastjson2.JSON;
import com.springboot.workflow.activiti.service.TableService;
import com.springboot.workflow.api.activiti.table.params.TableInfoParam;
import com.springboot.workflow.common.constants.ColumnKeyType;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.handler.NodeColumnItem;
import com.springboot.workflow.config.DbConfig;
import com.springboot.workflow.entity.activiti.TableColumns;
import com.springboot.workflow.entity.activiti.TableInfo;
import com.springboot.workflow.mapper.activiti.TableMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TableServiceImpl implements TableService {
    @Resource
    private DbConfig dbConfig;
    @Override
    public Map<String, Object> getWidgetDataType() {
        Map<String, Object> map = new HashMap<>();
        map.put("widgetDataType", dbConfig.getWidgetDataType());
        map.put("widgetDefaultDataType", dbConfig.getDefaultWidgetDataType());
        return map;
    }

    @Override
    public List<TableInfo> tableList(String tableName) {
        return dbConfig.getTableMapper().tableList(tableName);
    }

    @Override
    public List<TableColumns> tableColumns(String tableName, String columnKey) {
        return dbConfig.getTableMapper().tableColumns(tableName, columnKey);
    }

    /**
     * 创建表结构
     *
     * @param tableInfo 表信息
     */
    @Override
    public void createTable(TableInfoParam tableInfo) {
        // 查询表是否存在
        List<TableInfo> tableInfos = tableList(tableInfo.getTableName());
        // 如果存在就返回
        if (!tableInfos.isEmpty()) return;

        dbConfig.getTableMapper().createTable(tableInfo.getTableName(),
                tableInfo.getTableComment(),
                tableInfo.getTableName() + "_id",
                tableInfo.getColumns());
    }

    /**
     * 保存或更新数据
     *
     * @param id        主键id
     * @param tableName 表名
     * @param columns   节点绑定的表字段
     * @param variables 流程变量
     */
    @Override
    public void saveOrUpdateData(String id, String tableName, List<NodeColumnItem> columns, Map<String, Object> variables) {
        if (columns.isEmpty()) return;
        TableColumns primaryColumns = tableColumns(tableName, ColumnKeyType.PRIMARY_KEY)
                .stream().findAny().orElse(null);
        if (primaryColumns == null) throw new BusinessException("未找到表主键！");
        // 如果没有设置主表单或者主表单没有绑定字段就只插入id
        Map<String, Object> listData = new HashMap<>();
        listData.put(primaryColumns.getColumnName(), id);
        // 找到流程变量的数据,把数据更新到表中
        for (NodeColumnItem column : columns) {
            String columnName = column.getColumnName();
            Object value = variables.get(columnName);
            if (value == null) continue;
            // 如果是list类型,转成字符串保存到数据库
            if (value instanceof List) {
                value = JSON.toJSONString(value);
            }
            listData.put(columnName, value);
        }
        long exist = dbConfig.getTableMapper().exist(primaryColumns.getColumnName(), id, tableName);
        if (exist > 0) {
            dbConfig.getTableMapper().updateDataById(primaryColumns.getColumnName(), id, tableName, listData);
        } else {
            dbConfig.getTableMapper().insertData(tableName, listData);
        }
    }
}
