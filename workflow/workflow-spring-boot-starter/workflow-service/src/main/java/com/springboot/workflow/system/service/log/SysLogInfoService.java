package com.springboot.workflow.system.service.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.workflow.entity.system.log.SysLogInfo;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-17
 */
public interface SysLogInfoService extends IService<SysLogInfo> {
}
