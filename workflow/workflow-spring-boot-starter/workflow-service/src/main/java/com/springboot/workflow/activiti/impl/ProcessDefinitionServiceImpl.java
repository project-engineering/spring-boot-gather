package com.springboot.workflow.activiti.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.springboot.workflow.activiti.service.ProcessDefinitionService;
import com.springboot.workflow.activiti.service.SysDeployNodeService;
import com.springboot.workflow.activiti.service.SysDeployService;
import com.springboot.workflow.activiti.service.TableService;
import com.springboot.workflow.api.activiti.definition.dto.DefinitionListDto;
import com.springboot.workflow.api.activiti.definition.params.DefinitionListParam;
import com.springboot.workflow.api.activiti.definition.params.DeployProcessParam;
import com.springboot.workflow.api.activiti.definition.params.FormJsonsParam;
import com.springboot.workflow.api.activiti.definition.params.NodeColumnsParam;
import com.springboot.workflow.api.activiti.table.params.TableInfoParam;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.entity.activiti.*;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * 流程定义实现类
 */
@Log4j2
@Service
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private SysDeployNodeService deployNodeService;
    @Resource
    private SysDeployService deployService;
    @Resource
    private TableService tableService;

    @Override
    public Result queryPage(DefinitionListParam param) {
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                .orderByProcessDefinitionId()
                .orderByProcessDefinitionKey().desc()
                .orderByProcessDefinitionVersion().desc();
        query.processDefinitionNameLike("%" + param.getDefinitionName() + "%");
        query.processDefinitionKeyLike("%" + param.getDefinitionKey() + "%");
        if (param.isActive()) query.active();
        List<ProcessDefinition> list =
                query.listPage(param.getPageNo() - 1, param.getPageSize());
        List<DefinitionListDto> resultList = new ArrayList<>();
        for (ProcessDefinition item : list) {
            DefinitionListDto vo = new DefinitionListDto();
            Deployment deployment = repositoryService.createDeploymentQuery()
                    .deploymentId(item.getDeploymentId())
                    .singleResult();
            vo.setDeploymentTime(deployment.getDeploymentTime());
            BeanUtils.copyProperties(item, vo);
            // 获取主表单
            SysDeployNode mainForm = deployNodeService.getOne(new LambdaQueryWrapper<SysDeployNode>()
                    .eq(SysDeployNode::getDeployId, item.getDeploymentId())
                    .eq(SysDeployNode::getIsMainFrom, 1));
            if (mainForm != null) vo.setFormJson(mainForm.getFormJson());
            resultList.add(vo);
        }
        return Result.success(resultList, query.count());
    }

    @Override
    public void deployProcess(DeployProcessParam param) {
        // 部署xml
        Deployment deploy = repositoryService.createDeployment().disableBpmnValidation()
                .addString("index.bpmn", param.getXml())
                .deploy();

        Date date = new Date();
        // 流程部署详情
        TableInfoParam tableInfo = param.getTableInfo();
        if (tableInfo != null) {
            // 创建表
            tableService.createTable(tableInfo);

            // 部署表信息
            SysDeploy sysDeploy = new SysDeploy();
            sysDeploy.setTableName(tableInfo.getTableName());
            sysDeploy.setTableComment(tableInfo.getTableComment());
            sysDeploy.setDeployId(deploy.getId());
            sysDeploy.setCreateTime(date);
            deployService.save(sysDeploy);
        }

        // 保存节点数据
        List<FormJsonsParam> formJsons = param.getFormJsonList();
        List<SysDeployNode> list = new ArrayList<>();
        for (FormJsonsParam formJson : formJsons) {
            SysDeployNode deployNode = new SysDeployNode() {{
                setNodeId(IdUtil.simpleUUID());
                setDeployId(deploy.getId());
                setActivityId(formJson.getActivityId());
                setFormJson(formJson.getFormJson());
                setIsMainFrom(formJson.getIsMainFrom());
                setCreateTime(date);
            }};
            param.getNodeColumns().stream().filter(t -> t.getActivityId()
                            .equals(formJson.getActivityId())).findAny()
                    .ifPresent(t -> deployNode.setColumns(t.getColumns()));
            list.add(deployNode);
        }
        deployNodeService.saveBatch(list);
    }

    /**
     * 获取流程定义xml
     *
     * @param deploymentId 部署id
     * @return 流程xml字符串
     */
    @Override
    public String getDefinitionXml(String deploymentId) {
        InputStream is = null;
        try {
            is = repositoryService.getResourceAsStream(deploymentId, "index.bpmn");
            byte[] bytes = new byte[is.available()];
            while (is.read(bytes) != -1) ;
            return new String(bytes);
        } catch (IOException ex) {
            log.error("获取流程xml失败", ex);
            throw new BusinessException("获取流程图失败");
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ex) {
                log.error("关闭流失败", ex);
            }
        }
    }

    /**
     * 获取流程定义详情
     *
     * @param deploymentId 部署id
     * @return 流程xml字符串和流程表单
     */
    @Override
    public Map<String, Object> getDefinitionInfo(String deploymentId) {
        // 获取xml
        Map<String, Object> result = new HashMap<>();
        String xml = getDefinitionXml(deploymentId);
        result.put("xml", xml);
        // 获取节点表单
        List<SysDeployNode> list = deployNodeService.list(new LambdaQueryWrapper<SysDeployNode>()
                .select(SysDeployNode::getActivityId,
                        SysDeployNode::getFormJson,
                        SysDeployNode::getColumns,
                        SysDeployNode::getIsMainFrom)
                .eq(SysDeployNode::getDeployId, deploymentId));

        // 获取节点动态表单
        List<FormJsonsParam> formJsonList = new ArrayList<>();
        for (SysDeployNode deployNode : list) {
            FormJsonsParam formJsonsDto = new FormJsonsParam();
            BeanUtils.copyProperties(deployNode, formJsonsDto);
            formJsonList.add(formJsonsDto);
        }
        result.put("formJsonList", formJsonList);

        SysDeploy sysDeploy = deployService.getById(deploymentId);
        if (sysDeploy != null) {
            // 表结构信息
            TableInfoParam tableInfo = new TableInfoParam();
            tableInfo.setTableName(sysDeploy.getTableName());
            tableInfo.setTableComment(sysDeploy.getTableComment());
            List<TableColumns> tableColumns = tableService.tableColumns(sysDeploy.getTableName(), null);
            List<TableColumnsEntity> columns = new ArrayList<>();
            for (TableColumns tableColumn : tableColumns) {
                TableColumnsEntity tableColumnsVo = new TableColumnsEntity();
                BeanUtils.copyProperties(tableColumn, tableColumnsVo);
                columns.add(tableColumnsVo);
            }
            tableInfo.setColumns(columns);
            result.put("tableInfo", tableInfo);

            // 获取节点绑定数据库表字段的数据
            List<NodeColumnsParam> nodeColumnsVos = new ArrayList<>();
            for (SysDeployNode deployNode : list) {
                if (deployNode.getColumns() == null) continue;
                NodeColumnsParam nodeColumnsVo = new NodeColumnsParam();
                nodeColumnsVo.setActivityId(deployNode.getActivityId());
                nodeColumnsVo.setColumns(deployNode.getColumns());
                nodeColumnsVos.add(nodeColumnsVo);
            }
            result.put("nodeColumns", nodeColumnsVos);
        }
        return result;
    }

    /**
     * 更新流程定义状态 激活或者挂起
     *
     * @param deploymentId 部署id
     */
    @Override
    public void updateState(String deploymentId) {
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                .deploymentId(deploymentId)
                .singleResult();
        if (definition == null) throw new BusinessException("未知流程");
        boolean isSuspended = repositoryService.isProcessDefinitionSuspended(definition.getId());
        if (isSuspended) {
            repositoryService.activateProcessDefinitionById(definition.getId());
        } else {
            repositoryService.suspendProcessDefinitionById(definition.getId());
        }
    }

    /**
     * 删除流程
     *
     * @param deploymentId 部署id
     */
    @Transactional
    @Override
    public void delete(String deploymentId) {
        deployNodeService.remove(new LambdaQueryWrapper<SysDeployNode>()
                .eq(SysDeployNode::getDeployId, deploymentId));
        deployService.remove(deploymentId);
        repositoryService.deleteDeployment(deploymentId, true);
    }
}
