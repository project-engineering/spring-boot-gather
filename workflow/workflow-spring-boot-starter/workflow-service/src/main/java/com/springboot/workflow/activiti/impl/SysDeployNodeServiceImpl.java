package com.springboot.workflow.activiti.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.activiti.service.SysDeployNodeService;
import com.springboot.workflow.entity.activiti.SysDeployNode;
import com.springboot.workflow.mapper.activiti.SysDeployNodeMapper;
import org.springframework.stereotype.Service;

@Service
public class SysDeployNodeServiceImpl extends ServiceImpl<SysDeployNodeMapper, SysDeployNode> implements SysDeployNodeService {

}