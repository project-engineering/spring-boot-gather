package com.springboot.workflow.activiti.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.activiti.service.SysListenerService;
import com.springboot.workflow.api.activiti.listener.param.SysListenerListParam;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.entity.activiti.SysListener;
import com.springboot.workflow.mapper.activiti.SysListenerMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class SysListenerServiceImpl  extends ServiceImpl<SysListenerMapper, SysListener> implements SysListenerService {
    @Resource
    private SysListenerMapper sysListenerMapper;

    @Override
    public IPage<SysListener> queryPage(SysListenerListParam param) {
        int pageNo = param.getPageNo();
        if (ObjectUtil.isEmpty(pageNo)) {
            pageNo = 1;
        }
        int pageSize = param.getPageSize();
        if (ObjectUtil.isEmpty(pageSize)) {
            pageSize = 10;
        }
        //构造分页对象
        IPage<SysListener> page = new Page<>(pageNo,pageSize);
        //构造查询条件
        QueryWrapper<SysListener> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(param.getListenerName())){
            queryWrapper.lambda().eq(SysListener::getListenerName,param.getListenerName());
        }
        queryWrapper.lambda().orderByDesc(SysListener::getCreateTime);
        //执行查询
        return sysListenerMapper.selectPage(page,queryWrapper);
    }

    @Override
    public SysListener getById(String listenerId) {
        return sysListenerMapper.selectById(listenerId);
    }

    @Override
    public void addListener(SysListener model) {
        if (StringUtils.isNotEmpty(model.getListenerId())) {
            SysListener listener = getById(model.getListenerId());
            if (listener == null) throw new BusinessException("未知数据");
            if (listener.getIsSys() == 1) throw new BusinessException("系统内置不能修改!");
        }

        try {
            Class.forName(model.getJavaClass());
        } catch (ClassNotFoundException e) {
            throw new BusinessException("监听器类路径不存在!");
        }
        model.setListenerId(IdUtil.simpleUUID());
        sysListenerMapper.insert(model);
    }

    @Override
    public void deleteListener(String id) {
        SysListener listener = getById(id);
        if (listener == null) throw new BusinessException("未知数据");
        if (listener.getIsSys() == 1) throw new BusinessException("系统内置不能删除!");
        sysListenerMapper.deleteById(id);
    }

    @Override
    public void updateListener(SysListener model) {
        if (StringUtils.isEmpty(model.getListenerId())) throw new BusinessException("未知数据");
        SysListener listener = getById(model.getListenerId());
        if (listener == null) throw new BusinessException("未知数据");
        if (listener.getIsSys() == 1) throw new BusinessException("系统内置不能修改!");
        try {
            Class.forName(model.getJavaClass());
        } catch (ClassNotFoundException e) {
            throw new BusinessException("监听器类路径不存在!");
        }
        sysListenerMapper.updateById(model);
    }

}
