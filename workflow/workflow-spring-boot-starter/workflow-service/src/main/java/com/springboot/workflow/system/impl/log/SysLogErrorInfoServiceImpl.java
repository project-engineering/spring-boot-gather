package com.springboot.workflow.system.impl.log;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.entity.system.log.SysLogErrorInfo;
import com.springboot.workflow.mapper.system.log.SysLogErrorInfoMapper;
import com.springboot.workflow.system.service.log.SysLogErrorInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 异常信息表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysLogErrorInfoServiceImpl extends ServiceImpl<SysLogErrorInfoMapper, SysLogErrorInfo> implements SysLogErrorInfoService {

}
