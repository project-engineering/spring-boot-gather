package com.springboot.workflow.activiti.service;

import com.springboot.workflow.api.activiti.definition.params.DefinitionListParam;
import com.springboot.workflow.api.activiti.definition.params.DeployProcessParam;
import com.springboot.workflow.common.response.Result;
import java.util.Map;

/**
 * 流程定义服务
 */
public interface ProcessDefinitionService {
    /**
     * 流程管理列表
     *
     * @param param 参数
     * @return 列表
     */
    Result queryPage(DefinitionListParam param);

    /**
     * 部署流程
     *
     * @param param 参数
     */
    void deployProcess(DeployProcessParam param);

    /**
     * 获取流程定义xml
     *
     * @param deploymentId 部署id
     * @return 流程xml字符串
     */
    String getDefinitionXml(String deploymentId);

    /**
     * 获取流程定义详情
     *
     * @param deploymentId 部署id
     * @return 流程xml字符串和流程表单
     */
    Map<String, Object> getDefinitionInfo(String deploymentId);

    /**
     * 更新流程定义状态 激活或者挂起
     *
     * @param deploymentId 部署id
     */
    void updateState(String deploymentId);

    /**
     * 删除流程
     *
     * @param deploymentId 部署id
     */
    void delete(String deploymentId);
}
