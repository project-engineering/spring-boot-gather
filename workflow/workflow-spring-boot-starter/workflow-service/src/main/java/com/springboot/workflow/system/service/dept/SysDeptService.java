package com.springboot.workflow.system.service.dept;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.workflow.api.system.dept.params.AddSysDeptParam;
import com.springboot.workflow.api.system.dept.params.QuerySysDeptPageParam;
import com.springboot.workflow.api.system.dept.params.UpdateSysDeptParam;
import com.springboot.workflow.entity.system.dept.SysDept;

/**
 * 部门表
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 部门分页列表
     *
     * @param param 查询参数
     * @return 列表
     */
    IPage<SysDept> queryPage(QuerySysDeptPageParam param);

    void saveSysDept(AddSysDeptParam param);

    void deleteSysDept(String deptId);

    void updateSysDept(UpdateSysDeptParam param);
}

