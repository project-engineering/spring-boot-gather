package com.springboot.workflow.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.workflow.api.system.menu.SaveMenuParam;
import com.springboot.workflow.entity.system.SysRoleMenu;
import java.util.List;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    List<SysRoleMenu> getRoleMenuListByRoleIdList(List<String> roleIdList);
    void saveRoleMenu(SaveMenuParam saveMenuParam);
}
