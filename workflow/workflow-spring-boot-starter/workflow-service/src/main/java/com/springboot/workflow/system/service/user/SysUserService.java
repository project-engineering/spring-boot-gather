package com.springboot.workflow.system.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.workflow.api.system.LoginDTO;
import com.springboot.workflow.api.system.LoginParam;
import com.springboot.workflow.api.system.RegisterParam;
import com.springboot.workflow.api.system.user.UpdatePwdParam;
import com.springboot.workflow.api.system.user.UserInfoDTO;
import com.springboot.workflow.api.system.user.UserParam;
import com.springboot.workflow.entity.system.user.SysUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 新增用户
     * @param sysUser
     */
    boolean addUser(SysUser sysUser);
    /**
     * 修改用户
     * @param sysUser
     */
    boolean updateUser(SysUser sysUser);
    /**
     * 删除用户
     * @param userId
     */
    boolean deleteUser(String userId);
    IPage<SysUser> getList(UserParam param);

    /**
     * 密码重置
     *
     * @param userId
     * @return
     */
    int resetPwd(String userId);

    /**
     * 根据用户名查询用户
     * @param username 用户信息
     * @return SysUser
     */
    SysUser getUserByUsername(String username);

    /**
     * 注册用户
     * @param registerParam 注册参数
     */
    void register(RegisterParam registerParam);

    /**
     * 登录
     * @param param 登录参数
     */
    LoginDTO login(LoginParam param);
    SysUser getUserByUserId(String userId);

    void updatePwd(UpdatePwdParam param);

    UserInfoDTO getUserInfo(String userId);
}
