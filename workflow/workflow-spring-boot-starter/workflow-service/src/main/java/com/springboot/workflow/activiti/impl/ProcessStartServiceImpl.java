package com.springboot.workflow.activiti.impl;

import com.springboot.workflow.activiti.service.ProcessStartService;
import com.springboot.workflow.activiti.service.SysDeployService;
import com.springboot.workflow.api.activiti.start.dto.StartListDto;
import com.springboot.workflow.api.activiti.start.params.StartListParam;
import com.springboot.workflow.api.activiti.start.params.StartProcessParam;
import com.springboot.workflow.common.constants.ActivityType;
import com.springboot.workflow.common.constants.Constants;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.response.Result;
import jakarta.annotation.Resource;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProcessStartServiceImpl implements ProcessStartService {
    @Resource
    private RepositoryService repositoryService;

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private HistoryService historyService;

    @Resource
    private TaskService taskService;

    @Resource
    private SysDeployService deployService;

    /**
     * 我发起的任务列表
     *
     * @param param 参数
     * @return 结果
     */
    @Override
    public Result queryPage(StartListParam param) {
        HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery()
                .startedBy(param.getUserId())
                .orderByProcessInstanceStartTime()
                .desc();

        // 根据业务key查询 注意是等于不是模糊查询
        if (StringUtils.isNoneEmpty(param.getBusinessKey())) {
            query.processInstanceBusinessKey(param.getBusinessKey());
        }
        // 根据流程名称查询 注意是等于不是模糊查询
        if (StringUtils.isNoneEmpty(param.getDefinitionName())) {
            query.processDefinitionName(param.getDefinitionName());
        }
        // 根据流程key查询 注意是等于不是模糊查询
        if (StringUtils.isNoneEmpty(param.getDefinitionKey())) {
            query.processDefinitionKey(param.getDefinitionKey());
        }

        List<HistoricProcessInstance> list = query
                .listPage(param.getPageNo() - 1, param.getPageSize());
        List<StartListDto> resultList = new ArrayList<>();
        for (HistoricProcessInstance item : list) {
            // 设置流程实例
            StartListDto vo = new StartListDto();
            vo.setId(item.getId());
            vo.setBusinessKey(item.getBusinessKey());
            vo.setStartTime(item.getStartTime());
            vo.setEndTime(item.getEndTime());

            // 流程定义信息
            ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(item.getProcessDefinitionId())
                    .singleResult();
            vo.setDefinitionName(definition.getName());
            vo.setDefinitionKey(definition.getKey());
            vo.setDefinitionVersion(definition.getVersion());

            // 获取任务处理节点
            Task task = taskService.createTaskQuery()
                    .processInstanceId(item.getId())
                    .singleResult();
            if (task != null) {
                vo.setTaskId(task.getId());
                vo.setTaskName(task.getName());
                vo.setStatus(1);
            } else {
                // 任务处理完成在history获取
                List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                        .processInstanceId(item.getId())
                        .orderByHistoricTaskInstanceEndTime()
                        .desc()
                        .list();
                HistoricTaskInstance historicTask = historicTaskInstances.get(0);
                vo.setTaskId(historicTask.getId());
                vo.setTaskName(historicTask.getName());
                vo.setStatus(2);
            }
            resultList.add(vo);
        }
        return Result.success(resultList, query.count());
    }

    /**
     * 启动流程
     *
     * @param param    启动流程参数
     */
    @Transactional
    @Override
    public void startProcess(StartProcessParam param) {
        // 获取相关数据
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(param.getDefinitionId()).singleResult();
        if(definition.isSuspended()) throw new BusinessException("流程已挂起,不能启动!");

        // 设置流程发起人用户Id
        Authentication.setAuthenticatedUserId(param.getUserId());
        Map<String, Object> variables = param.getVariables();
        // 设置发起人用户id
        // 如果节点审批人,设置的是发起人,则审批节点的 assignee="${initiator}"
        variables.put(Constants.PROCESS_INITIATOR, param.getUserId());
        ProcessInstance instance = runtimeService.startProcessInstanceById(param.getDefinitionId(), param.getBusinessKey(), variables);

        // 获取开始事件
        HistoricActivityInstance activityInstance = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(instance.getId()).list()
                .stream().filter(t -> t.getActivityType().equals(ActivityType.START_EVENT))
                .findAny().orElse(null);
        // 保存数据
        assert activityInstance != null;
        deployService.saveData(instance.getId(), definition.getDeploymentId(),
                activityInstance.getActivityId(), variables);
    }


    /**
     * 删除流程实例
     *
     * @param instanceId 流程实例id
     */
    @Override
    public void delete(String instanceId) {
        // 查询历史数据
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(instanceId)
                .singleResult();
        if (historicProcessInstance.getEndTime() != null) {
            historyService.deleteHistoricProcessInstance(historicProcessInstance.getId());
            return;
        }
        // 删除流程实例
        runtimeService.deleteProcessInstance(instanceId, null);
        // 删除历史流程实例
        historyService.deleteHistoricProcessInstance(instanceId);
    }
}
