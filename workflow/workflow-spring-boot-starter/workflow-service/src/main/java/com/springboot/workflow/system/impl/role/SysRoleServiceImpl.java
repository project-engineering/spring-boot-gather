package com.springboot.workflow.system.impl.role;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.workflow.api.system.role.RoleParam;
import com.springboot.workflow.api.system.role.RoleSelectType;
import com.springboot.workflow.entity.system.SysUserRole;
import com.springboot.workflow.entity.system.role.SysRole;
import com.springboot.workflow.mapper.system.role.SysRoleMapper;
import com.springboot.workflow.system.service.SysUserRoleService;
import com.springboot.workflow.system.service.role.SysRoleService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Resource
    private SysRoleMapper mapper;
    @Resource
    private SysUserRoleService userRoleService;

    @Override
    public IPage<SysRole> getList(RoleParam param) {
        //构造分页对象
        IPage<SysRole> page = new Page<>(param.getPageNo(),param.getPageSize());
        //构造查询条件
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(param.getRoleName())){
            queryWrapper.like("role_name",param.getRoleName());
        }
        //执行查询
        return mapper.selectPage(page,queryWrapper);
    }

    @Override
    public List<RoleSelectType> getRoleSelectList() {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        List<SysRole> list = mapper.selectList(queryWrapper);
        List<RoleSelectType> roleSelectTypeList = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(list)) {
            for (SysRole item : list) {
                RoleSelectType roleSelectType = RoleSelectType.builder()
                        .roleId(item.getRoleId())
                        .roleName(item.getRoleName())
                        .build();
                roleSelectTypeList.add(roleSelectType);
            }
        }
        return roleSelectTypeList;
    }

    @Override
    public boolean addRole(SysRole sysRole) {
        sysRole.setRoleId(IdUtil.simpleUUID());
        int i = mapper.insert(sysRole);
        if (i > 0) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public List<SysRole> getRoleListByUserId(String userId) {
        List<SysUserRole> sysUserRoleList = userRoleService.getRoleListByUserId(userId);
        if (ObjectUtil.isNotEmpty(sysUserRoleList)) {
            List<String> roleIdList = sysUserRoleList.stream().map(SysUserRole::getRoleId).toList();
            if (ObjectUtil.isNotEmpty(roleIdList)) {
                LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.in(SysRole::getRoleId,roleIdList);
                return mapper.selectList(queryWrapper);
            }
        }
        return null;
    }
}
