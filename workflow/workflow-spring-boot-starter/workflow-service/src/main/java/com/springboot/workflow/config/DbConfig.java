package com.springboot.workflow.config;

import com.springboot.workflow.common.enums.DbType;
import com.springboot.workflow.common.util.PropertiesUtils;
import com.springboot.workflow.mapper.activiti.TableMapper;
import com.springboot.workflow.mapper.activiti.TableMySQLMapper;
import com.springboot.workflow.mapper.activiti.TablePostgreSQLMapper;
import jakarta.annotation.Resource;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import java.util.Map;
import static com.springboot.workflow.common.enums.DbType.MY_SQL;
import static com.springboot.workflow.common.enums.DbType.POSTGRE_SQL;

@Configuration
public class DbConfig {
    @Value("${system.database}")
    private DbType database;

    @Getter
    private Map<String, String[]> widgetDataType;
    @Getter
    private Map<String, Map.Entry<String, Integer>> defaultWidgetDataType;


    // 数据库操作实现
    @Resource
    private TableMySQLMapper tableMySQLMapper;
    @Resource
    private TablePostgreSQLMapper tablePostgreSQLMapper;

    /**
     * 切换数据库
     */
    @Bean
    @Primary
    public TableMapper getTableMapper() {
        switch (database) {
            case MY_SQL:
                widgetDataType = PropertiesUtils.getWidgetDataType(MY_SQL.getKey());
                defaultWidgetDataType = PropertiesUtils.getDefaultWidgetDataType(MY_SQL.getKey());
                return tableMySQLMapper;
            case POSTGRE_SQL:
                widgetDataType = PropertiesUtils.getWidgetDataType(POSTGRE_SQL.getKey());
                defaultWidgetDataType = PropertiesUtils.getDefaultWidgetDataType(POSTGRE_SQL.getKey());
                return tablePostgreSQLMapper;
            default:
                throw new RuntimeException("不支持当前数据库：" + database);
        }
    }
}
