package com.springboot.workflow.activiti.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.springboot.workflow.activiti.service.ProcessTodoService;
import com.springboot.workflow.activiti.service.SysDeployNodeService;
import com.springboot.workflow.activiti.service.SysDeployService;
import com.springboot.workflow.api.activiti.todo.dto.TodoListDto;
import com.springboot.workflow.api.activiti.todo.params.TodoCompleteParam;
import com.springboot.workflow.api.activiti.todo.params.TodoListParam;
import com.springboot.workflow.common.exception.BusinessException;
import com.springboot.workflow.common.response.Result;
import com.springboot.workflow.entity.activiti.SysDeployNode;
import com.springboot.workflow.entity.system.user.SysUser;
import com.springboot.workflow.system.service.user.SysUserService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.core.el.juel.tree.TreeBuilderException;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代办任务
 */
@Log4j2
@Service
public class ProcessTodoServiceImpl implements ProcessTodoService {
    @Resource
    private TaskService taskService;

    @Resource
    private RepositoryService repositoryService;

    @Resource
    private HistoryService historyService;

    @Resource
    private SysUserService userService;

    @Resource
    private SysDeployNodeService deployNodeService;

    @Resource
    private SysDeployService deployService;

    /**
     * 查看我代办的流程
     *
     * @param param 参数
     */
    @Override
    public Result queryPage(TodoListParam param) {
        List<String> usersGroups = new ArrayList<>();
        usersGroups.add(param.getDeptId());
        TaskQuery query = taskService.createTaskQuery()
                .active()
                .taskCandidateOrAssigned(param.getUserId(), usersGroups)
                .processDefinitionNameLike("%" + param.getDefinitionName() + "%")
                .processDefinitionKeyLike("%" + param.getDefinitionKey() + "%")
                .orderByTaskCreateTime()
                .desc();
        List<Task> list = query
                .listPage(param.getPageNo() - 1, param.getPageSize());

        List<TodoListDto> resultList = new ArrayList<>();
        for (Task task : list) {
            TodoListDto vo = new TodoListDto();
            // 当前流程
            vo.setTaskId(task.getId());
            vo.setTaskName(task.getName());
            vo.setTaskDefinitionKey(task.getTaskDefinitionKey());
            vo.setProcessInstanceId(task.getProcessInstanceId());
            vo.setCreateTime(task.getCreateTime());
            vo.setProcessDefinitionId(task.getProcessDefinitionId());

            // 流程定义
            ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(task.getProcessDefinitionId())
                    .singleResult();
            vo.setDefinitionName(definition.getName());
            vo.setDefinitionKey(definition.getKey());
            vo.setDefinitionVersion(definition.getVersion());

            // 流程发起人
            HistoricProcessInstance instance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId())
                    .singleResult();
            SysUser user = userService.getById((instance.getStartUserId()));
            vo.setStartUserId(user.getUserId());
            vo.setStartUserName(user.getUsername());
            resultList.add(vo);
        }
        return Result.success(resultList, query.count());
    }

    /**
     * 获取节点表单
     *
     * @param taskId 任务id
     * @return 表单数据
     */
    @Override
    public Map<String, Object> getNodeForm(String taskId) {
        // 获取任务
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) throw new BusinessException("未知任务！");
        // 流程定义信息
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(task.getProcessDefinitionId());
        // 获取当前节点 formKey
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
        FlowElement flowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        // 获取表单数据
        SysDeployNode form = deployNodeService.getOne(new LambdaQueryWrapper<SysDeployNode>()
                .eq(SysDeployNode::getDeployId, processDefinition.getDeploymentId())
                .eq(SysDeployNode::getActivityId, flowElement.getId()));
        // 用户没有填写表单
        if (form == null) {
            return new HashMap<>();
        }
        return form.getFormJson();
    }

    /**
     * 节点审批
     *
     * @param param 参数
     */
    @Transactional
    @Override
    public void complete(TodoCompleteParam param) {
        try {
            List<String> usersGroups = new ArrayList<>();
            usersGroups.add(param.getDeptId());
            Task task = taskService.createTaskQuery()
                    .active()
                    .processInstanceId(param.getProcessInstanceId())
                    .taskCandidateOrAssigned(param.getUserId(), usersGroups)
                    .orderByTaskCreateTime()
                    .desc().singleResult();
            if (task == null) throw new BusinessException("未找到审批节点!");
            // 如果没有代理人就拾取任务进行办理
            if (StringUtils.isEmpty(task.getAssignee())) {
                taskService.claim(task.getId(), param.getUserId());
            }

            Map<String, Object> variables = param.getVariables();
            taskService.setVariables(task.getId(), variables);
            taskService.setVariablesLocal(task.getId(), variables);
            taskService.complete(task.getId());

            // 获取相关数据
            ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(task.getProcessDefinitionId()).singleResult();
            HistoricActivityInstance activityInstance = historyService.createHistoricActivityInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId()).list()
                    .stream().filter(t -> t.getTaskId() != null && t.getTaskId().equals(task.getId()))
                    .findAny().orElse(null);
            // 保存数据
            assert activityInstance != null;
            deployService.saveData(task.getProcessInstanceId(), definition.getDeploymentId(),
                    activityInstance.getActivityId(), variables);
        } catch (TreeBuilderException ex) {
            throw new BusinessException("流程条件表达式错误:" + ex.getMessage());
        } catch (ActivitiException ex) {
            throw new BusinessException("Activiti流程异常:" + ex.getMessage());
        } catch (Exception ex) {
            log.error("流程提交异常", ex);
            throw new BusinessException("流程提交未知异常!");
        }
    }
}
