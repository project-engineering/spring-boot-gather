package com.springboot.workflow.listener;

import jakarta.annotation.Resource;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.history.HistoricVariableInstance;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class MyExecutionListener implements ExecutionListener {
    @Resource
    private HistoryService historyService;

    @Override
    public void notify(DelegateExecution execution) {

        // 获取流程变量
        List<HistoricVariableInstance> list = historyService
                .createHistoricVariableInstanceQuery()
                .processInstanceId(execution.getProcessInstanceId()).list();


        System.out.println("execution.getProcessInstanceId() = " + execution.getProcessInstanceId());
        System.out.println("execution.getEventName() = " + execution.getEventName());
        FlowElement currentFlowElement = execution.getCurrentFlowElement();
        System.out.println("currentFlowElement.getName() = " + currentFlowElement.getName());
        System.out.println("currentFlowElement.getDocumentation() = " + currentFlowElement.getDocumentation());
        System.out.println("currentFlowElement.getExecutionListeners() = " + currentFlowElement.getExecutionListeners());

        String currentActivityId = execution.getCurrentActivityId();
        System.out.println("currentActivityId = " + currentActivityId);
    }
}
