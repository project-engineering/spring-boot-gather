package com.springboot.workflow.activiti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.springboot.workflow.api.activiti.listener.param.SysListenerListParam;
import com.springboot.workflow.entity.activiti.SysListener;

public interface SysListenerService {
    /**
     * 执行监听器分页列表
     *
     * @param param 参数
     * @return 列表
     */
    IPage<SysListener> queryPage(SysListenerListParam param);

    SysListener getById(String listenerId);

    void addListener(SysListener model);

    void deleteListener(String id);

    void updateListener(SysListener model);
}
