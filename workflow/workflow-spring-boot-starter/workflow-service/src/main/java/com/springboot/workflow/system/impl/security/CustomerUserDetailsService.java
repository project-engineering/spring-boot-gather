package com.springboot.workflow.system.impl.security;

import cn.hutool.core.util.ObjectUtil;
import com.springboot.workflow.common.exception.CustomerAuthenticationException;
import com.springboot.workflow.entity.system.menu.SysMenu;
import com.springboot.workflow.entity.system.role.SysRole;
import com.springboot.workflow.entity.system.user.SysUser;
import com.springboot.workflow.system.service.menu.SysMenuService;
import com.springboot.workflow.system.service.role.SysRoleService;
import com.springboot.workflow.system.service.user.SysUserService;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CustomerUserDetailsService implements UserDetailsService {
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysMenuService sysMenuService;
    @Resource
    private SysRoleService sysRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 认证
        SysUser user = sysUserService.getUserByUsername(username);
        if (ObjectUtil.isEmpty(user)) {
            throw new CustomerAuthenticationException("用户名或密码错误");
        }
        // 授权
        List<SysMenu> menuList = sysMenuService.getMenuListByUserId(user.getUserId());
        List<String> collect = menuList.stream()
                .map(SysMenu::getCode)
                .filter(ObjectUtil::isNotEmpty)
                .toList();
        String[] authorities = collect.toArray(new String[0]);
        // 查询用户权限
        List<SysRole> roleList = sysRoleService.getRoleListByUserId(user.getUserId());
        List<String> roleTypeList = roleList.stream()
                .map(SysRole::getRoleType)
                .filter(ObjectUtil::isNotEmpty)
                .toList();
        authorities = ArrayUtils.addAll(authorities, roleTypeList.toArray(new String[0]));
        List<GrantedAuthority> authoritiesList = AuthorityUtils.createAuthorityList(authorities);
        user.setAuthorities(authoritiesList);
        return user;
    }

}
