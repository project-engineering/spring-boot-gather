package com.springboot.workflow.activiti.service;

import com.springboot.workflow.api.activiti.todo.params.TodoCompleteParam;
import com.springboot.workflow.api.activiti.todo.params.TodoListParam;
import com.springboot.workflow.common.response.Result;
import java.util.Map;

/**
 * 代办任务
 */
public interface ProcessTodoService {

    /**
     * 查看我代办的流程
     *
     * @param param 参数
     */
    Result queryPage(TodoListParam param);

    /**
     * 获取节点表单
     * @param taskId 任务id
     * @return 表单数据
     */
    Map<String, Object> getNodeForm(String taskId);

    /**
     * 节点审批
     *
     * @param param 参数
     */
    void complete(TodoCompleteParam param);

}
