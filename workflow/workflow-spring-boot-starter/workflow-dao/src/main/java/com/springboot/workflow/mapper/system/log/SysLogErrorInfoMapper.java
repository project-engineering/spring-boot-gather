package com.springboot.workflow.mapper.system.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.system.log.SysLogErrorInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 异常信息表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysLogErrorInfoMapper extends BaseMapper<SysLogErrorInfo> {

}

