package com.springboot.workflow.entity.activiti;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 执行监听器
 */
@Data
@TableName("sys_listener")
public class SysListener implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 监听器id
     */
    @TableId
    private String listenerId;
    /**
     * 监听器名称
     */
    @TableField("listener_name")
    private String listenerName;
    /**
     * java类
     */
    @TableField("java_class")
    private String javaClass;
    /**
     * 是否是系统内置,1:是,0:否
     */
    @TableField("is_sys")
    private Integer isSys;
    /**
     * 备注
     */
    @TableField("remark")
    private String remark;
    /**
     * 创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill =FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime updateTime;

}
