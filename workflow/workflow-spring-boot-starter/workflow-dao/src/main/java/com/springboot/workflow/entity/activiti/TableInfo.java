package com.springboot.workflow.entity.activiti;

import lombok.Data;

/**
 * 表信息
 *
 */
@Data
public class TableInfo {

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 表备注
     */
    private String tableComment;

    /**
     * 创建时间
     */
    private String createTime;

}
