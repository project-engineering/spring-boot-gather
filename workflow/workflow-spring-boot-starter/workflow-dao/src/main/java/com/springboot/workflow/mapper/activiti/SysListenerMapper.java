package com.springboot.workflow.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.activiti.SysListener;
import org.apache.ibatis.annotations.Mapper;

/**
 * 执行监听器
 */
@Mapper
public interface SysListenerMapper extends BaseMapper<SysListener> {

}
