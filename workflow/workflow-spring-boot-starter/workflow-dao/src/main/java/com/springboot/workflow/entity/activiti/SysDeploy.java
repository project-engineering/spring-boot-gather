package com.springboot.workflow.entity.activiti;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 流程部署详情
 */
@Data
@TableName("sys_deploy")
public class SysDeploy implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 部署id
     */
    @TableId
    private String deployId;
    /**
     * 绑定数据库表的名称
     */
    @TableField("table_name")
    private String tableName;
    /**
     * 绑定数据库表的备注
     */
    @TableField("table_comment")
    private String tableComment;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}