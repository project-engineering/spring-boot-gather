package com.springboot.workflow.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.activiti.SysDeployNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * 流程部署节点数据处理层
 */
@Mapper
public interface SysDeployNodeMapper extends BaseMapper<SysDeployNode> {

}