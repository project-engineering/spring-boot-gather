package com.springboot.workflow.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.activiti.TableColumnsEntity;
import com.springboot.workflow.entity.activiti.TableColumns;
import com.springboot.workflow.entity.activiti.TableInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;
import java.util.Map;

@Mapper
public interface TableMapper extends BaseMapper<TableInfo> {
    /**
     * 数据库表信息
     *
     * @param tableName 表名称或表备注
     * @return 表信息
     */
    @SelectProvider(type = SqlProvider.class, method = "selectTableList")
    List<TableInfo> tableList(@Param("tableName") String tableName);

    class SqlProvider {
        public String selectTableList(@Param("tableName") String tableName) {
            return new SQL() {{
                SELECT("table_name, table_comment, create_time");
                FROM("information_schema.TABLES");
                WHERE("TABLE_SCHEMA = (select database())");
                AND();
                WHERE("table_name NOT LIKE 'act_%'");
                AND();
                WHERE("table_name NOT LIKE 'sys_%'");
                if (tableName != null && !tableName.trim().isEmpty()) {
                    AND();
                    WHERE("table_name like concat('%', #{tableName}, '%')");
                }
                ORDER_BY("create_time desc");
            }}.toString();
        }
    }

    /**
     * 数据库表结构
     *
     * @param tableName 表名称或表备注
     * @param columnKey 行键
     * @return 表结构信息
     */
    List<TableColumns> tableColumns(@Param("tableName") String tableName,
                                    @Param("columnKey") String columnKey);

    /**
     * 创建表
     *
     * @param tableName    表名
     * @param tableComment 表备注
     * @param idName       主键名
     * @param columns      字段信息
     */
    void createTable(@Param("tableName") String tableName,
                     @Param("tableComment") String tableComment,
                     @Param("idName") String idName,
                     @Param("columns") List<TableColumnsEntity> columns);


    /**
     * 查询数据是否存在
     *
     * @param idName    主键名称
     * @param idValue   主键值
     * @param tableName 表名
     * @return 0:不存在,>0:存在
     */
    long exist(@Param("idName") String idName,
               @Param("idValue") Object idValue,
               @Param("tableName") String tableName);

    /**
     * 根据表名列名插入数据
     *
     * @param tableName 表名
     * @param listData  表中数据(key=字段名,value=值)
     * @return 结果
     */
    Integer insertData(@Param("tableName") String tableName,
                       @Param("listData") Map<String, Object> listData);

    /**
     * 根据表名列名插入数据
     *
     * @param idName    主键名称
     * @param idValue   主键值
     * @param tableName 表名
     * @param listData  表中数据(key=字段名,value=值)
     * @return 结果
     */
    Integer updateDataById(@Param("idName") String idName,
                           @Param("idValue") Object idValue,
                           @Param("tableName") String tableName,
                           @Param("listData") Map<String, Object> listData);

}
