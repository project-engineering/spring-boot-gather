package com.springboot.workflow.entity.activiti;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@Tag(name = "表结构")
public class TableColumnsEntity {
    /**
     * 行名称
     */
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]*$", message = "绑定字段-字段名称不符合规则")
    private String columnName;
    /**
     * 数据类型
     */
    private String dataType;

    /**
     * 字段长度
     */
    private Integer columnLength;
    /**
     * 行备注
     */
    private String columnComment;
    /**
     * 行健
     */
    private String columnKey;
}
