package com.springboot.workflow.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.activiti.SysDeploy;
import org.apache.ibatis.annotations.Mapper;

/**
 * 流程部署详情
 */
@Mapper
public interface SysDeployMapper extends BaseMapper<SysDeploy> {
}
