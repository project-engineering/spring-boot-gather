package com.springboot.workflow.mapper.system.dept;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.workflow.entity.system.dept.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门表
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
