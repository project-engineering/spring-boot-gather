package com.springboot.workflow.entity.activiti;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.springboot.workflow.common.handler.NodeColumnItem;
import com.springboot.workflow.common.handler.NodeColumnTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@TableName(value = "sys_deploy_node", autoResultMap = true)
public class SysDeployNode implements Serializable {
    private static final long serialVersionUID = 1L;

    @Schema(description = "节点id")
    @TableId(value = "node_id")
    private String nodeId;

    @Schema(description = "部署id")
    @TableField(value = "deploy_id")
    private String deployId;

    @Schema(description = "流程定义节点唯一标识")
    @TableField(value = "activity_id")
    private String activityId;

    @Schema(description = "表单详情")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Object> formJson;

    @Schema(description = "表单详情")
    @TableField(typeHandler = NodeColumnTypeHandler.class)
    private List<NodeColumnItem> columns;

    @Schema(description = "是否是主表单,1:是,2:否")
    @TableField(value = "is_main_from")
    private Integer isMainFrom;

    @Schema(description = "创建时间")
    @TableField(value = "create_time")
    private Date createTime;

}