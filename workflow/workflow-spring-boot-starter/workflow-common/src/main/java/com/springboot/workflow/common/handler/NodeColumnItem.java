package com.springboot.workflow.common.handler;

import lombok.Data;
import java.io.Serializable;

/**
 * 绑定的具体字段
 */
@Data
public class NodeColumnItem implements Serializable {

    /**
     * 表字段名称
     */
    private String columnName;

    /**
     * 行备注
     */
    private String columnComment;
}