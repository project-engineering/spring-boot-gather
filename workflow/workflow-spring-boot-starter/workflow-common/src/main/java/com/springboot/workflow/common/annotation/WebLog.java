package com.springboot.workflow.common.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface WebLog {
    /**
     * 操作模块
     */
    String modul() default "";

    /**
     * 操作类型
     */
    String type() default "";

    /**
     * 操作说明
     */
    String desc() default "";

    /**
     * 是否保存请求参数
     */
    boolean saveLog() default false;
}
