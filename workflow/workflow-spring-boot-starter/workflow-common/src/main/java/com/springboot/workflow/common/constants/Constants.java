package com.springboot.workflow.common.constants;

public class Constants {
    /**
     * 查询操作
     */
    public static final String SELECT = "SELECT";
    /**
     * 新增操作
     */
    public static final String ADD = "ADD";
    /**
     * 修改操作
     */
    public static final String UPDATE = "UPDATE";
    /**
     * 删除操作
     */
    public static final String DELETE = "DELETE";

    /**
     * 文件上传前缀
     */
    public final static String RESOURCE_PREFIX = "upload";

    /**
     * 前端在请求头内传的token,为了模拟登录,
     * 其实Token就是userId
     */
    public final static String TOKEN = "Token";

    /**
     * 发起人key
     */
    public final static String PROCESS_INITIATOR = "initiator";
}
