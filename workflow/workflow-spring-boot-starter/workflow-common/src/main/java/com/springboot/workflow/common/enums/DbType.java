package com.springboot.workflow.common.enums;

import lombok.Getter;

/**
 * 数据库类型
 */
@Getter
public enum DbType {
    /**
     * MySql数据库
     */
    MY_SQL("MySQL"),
    /**
     * PostgreSql数据库
     */
    POSTGRE_SQL("PostgreSQl");

    private final String key;
    DbType(String key) {
        this.key = key;
    }
}

