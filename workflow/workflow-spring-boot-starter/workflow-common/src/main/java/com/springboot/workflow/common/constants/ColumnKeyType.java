package com.springboot.workflow.common.constants;

/**
 * 数据库类型
 */
public class ColumnKeyType {
    /**
     * 主键
     */
    public static final String PRIMARY_KEY = "PRI";

}
