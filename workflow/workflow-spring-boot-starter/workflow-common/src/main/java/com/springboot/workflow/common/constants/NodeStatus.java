package com.springboot.workflow.common.constants;

/**
 * 节点状态
 */
public class NodeStatus {
    /**
     * 已完成的节点
     */
    public final static int EXECUTED = 1;

    /**
     * 未完成的节点
     */
    public final static int UNFINISHED = 2;
}
