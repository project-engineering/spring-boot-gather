package com.springboot.workflow.common.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * @author liuc
 * @date 2024-07-18 9:09
 */
@Schema(description = "分页参数")
@SuperBuilder
@Data
@EqualsAndHashCode()
@ToString(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class Page {
    @Schema(description = "页码")
    public  Integer pageNo;

    @Schema(description = "每页数量")
    public  Integer pageSize;

}
