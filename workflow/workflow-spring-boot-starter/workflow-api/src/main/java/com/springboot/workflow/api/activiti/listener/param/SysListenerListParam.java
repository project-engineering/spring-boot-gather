package com.springboot.workflow.api.activiti.listener.param;

import com.springboot.workflow.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name = "监听器列表入参")
public class SysListenerListParam extends Page {

    @Schema(description = "监听器名称")
    private String listenerName;
}
