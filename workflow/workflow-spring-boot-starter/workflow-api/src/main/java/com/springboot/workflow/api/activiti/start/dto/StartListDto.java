package com.springboot.workflow.api.activiti.start.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Date;

@Data
@Tag(name = "我发起的任务列表返回参数")
public class StartListDto {
    @Schema(description = "流程实例id")
    private String id;

    @Schema(description = "业务key")
    private String businessKey;

    @Schema(description = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @Schema(description = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    @Schema(description = "流程部署名称")
    private String definitionName;

    @Schema(description = "流程版本号")
    private Integer definitionVersion;

    @Schema(description = "流程key")
    private String definitionKey;

    @Schema(description = "任务id")
    private String taskId;

    @Schema(description = "任务名称")
    private String taskName;

    @Schema(description = "任务状态:1:进行中,2:已完成")
    private Integer status;
}
