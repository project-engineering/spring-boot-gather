package com.springboot.workflow.api.activiti.definition.params;

import com.springboot.workflow.common.handler.NodeColumnItem;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import java.util.List;

@Tag(name = "节点绑定数据库表字段的数据")
@Data
public class NodeColumnsParam {

    @NotBlank(message = "流程定义节点唯一标识")
    @Schema(description = "流程定义节点唯一标识")
    private String activityId;

    @Schema(description = "绑定的具体字段")
    private List<NodeColumnItem> columns;
}
