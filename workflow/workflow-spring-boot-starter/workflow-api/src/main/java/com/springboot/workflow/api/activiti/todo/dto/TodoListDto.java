package com.springboot.workflow.api.activiti.todo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Date;

@Data
@Tag(name = "待办列表出差")
public class TodoListDto {

    @Schema(description = "任务id")
    private String taskId;

    @Schema(description = "任务名称")
    private String taskName;

    @Schema(description = "流程实例id")
    private String processInstanceId;

    @Schema(description = "节点key")
    private String taskDefinitionKey;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date createTime;

    @Schema(description = "流程定义id")
    private String processDefinitionId;

    @Schema(description = "流程定义名称")
    private String definitionName;

    @Schema(description = "流程定义key")
    private String definitionKey;

    @Schema(description = "流程定义版本")
    private Integer definitionVersion;

    @Schema(description = "发起人用户id")
    private String startUserId;

    @Schema(description = "发起人用户名称")
    private String startUserName;
}
