package com.springboot.workflow.api.activiti.start.params;

import com.springboot.workflow.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Tag(name = "我发起任务列表入参")
public class StartListParam extends Page {
    @Schema(description = "用户id")
    @NotBlank(message = "用户id不能为空")
    private String userId;

    @Schema(description = "业务key")
    private String businessKey;

    @Schema(description = "流程定义名称")
    private String definitionName;

    @Schema(description = "流程定义key")
    private String definitionKey;
}
