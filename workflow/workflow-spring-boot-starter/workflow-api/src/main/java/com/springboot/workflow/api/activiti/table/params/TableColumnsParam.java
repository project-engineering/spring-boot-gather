package com.springboot.workflow.api.activiti.table.params;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class TableColumnsParam {

    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]*$", message = "绑定字段-字段名称不符合规则")
    @Schema(description = "字段名称")
    private String columnName;
    @Schema(description = "数据类型")
    private String dataType;

    @Schema(description = "字段长度")
    private Integer columnLength;

    @Schema(description = "备注")
    private String columnComment;

    @Schema(description = "健")
    private String columnKey;
}
