package com.springboot.workflow.api.activiti.table.params;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class QueryTableListParam {
    @Schema(description = "表名")
    private String tableName;
}
