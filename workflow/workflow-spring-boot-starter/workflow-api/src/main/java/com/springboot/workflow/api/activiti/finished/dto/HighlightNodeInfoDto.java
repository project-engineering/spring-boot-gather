package com.springboot.workflow.api.activiti.finished.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.List;

/**
 * 高亮节点信息
 */
@Data
@Tag(name = "高亮节点信息")
public class HighlightNodeInfoDto {
    @Schema(description = "流程定义节点唯一标识")
    private String activityId;

    @Schema(description = "状态 1:已完成节点,2:活动的未处理的节点(下一个节点), 参考 {@link NodeStatus}")
    private Integer status;

    @Schema(description = "历史审批记录")
    private List<HistoryRecordDto> historyRecordVo;
}
