package com.springboot.workflow.api.activiti.definition.params;

import com.springboot.workflow.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 流程定义列表参数对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Tag(name = "流程定义列表参数对象")
public class DefinitionListParam extends Page {
    @Schema(description = "流程定义名称")
    private String definitionName;

    @Schema(description = "流程定义key")
    private String definitionKey;

    @Schema(description = "是否激活")
    private boolean isActive;
}
