package com.springboot.workflow.api.activiti.todo.params;

import com.springboot.workflow.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Tag(name = "流程待办列表入参")
public class TodoListParam extends Page {
    @Schema(description = "用户id")
    private String userId;

    @Schema(description = "部门id(组id)")
    private String deptId;

    @Schema(description = "流程定义名称")
    private String definitionName;

    @Schema(description = "流程定义key")
    private String definitionKey;
}
