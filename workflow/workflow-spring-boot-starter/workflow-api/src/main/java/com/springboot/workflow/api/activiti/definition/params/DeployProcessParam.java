package com.springboot.workflow.api.activiti.definition.params;

import com.springboot.workflow.api.activiti.table.params.TableInfoParam;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import java.util.List;

@Data
public class DeployProcessParam {

    @NotBlank(message = "bpmn不能为空")
    @Schema(description = "bpmn xml", example = "bpmn xml")
    private String xml;

    @Schema(description = "表单数据", example = "表单数据")
    private List<FormJsonsParam> formJsonList;

    @Valid
    @Schema(description = "节点绑定字段信息")
    private List<NodeColumnsParam> nodeColumns;

    @Valid
    @Schema(description = "数据库表信息")
    private TableInfoParam tableInfo;
}
