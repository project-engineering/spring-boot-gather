package com.springboot.workflow.api.activiti.table.params;

import com.springboot.workflow.entity.activiti.TableColumnsEntity;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import java.util.List;

@Tag(name = "表信息")
@Data
public class TableInfoParam {
    /**
     * 表名称
     */
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]*$", message = "绑定表名称不符合规则")
    private String tableName;

    /**
     * 表备注
     */
    private String tableComment;

    /**
     * 数据库表信息
     */
    @Valid
    private List<TableColumnsEntity> columns;
}
