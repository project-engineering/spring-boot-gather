package com.springboot.workflow.api.system;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Tag(name = "注册请求参数")
@Data
public class RegisterParam {
    @Schema(description = "用户名", example = "admin")
    @NotEmpty(message = "用户名不能为空")
    private String username;

    @Schema(description = "密码", example = "123456")
    @NotEmpty(message = "密码不能为空")
    @Size(min = 6, max = 20, message = "密码长度必须在6-20之间")
    private String password;

    @Schema(description = "邮箱", example = "admin@example.com")
    private String email;
}
