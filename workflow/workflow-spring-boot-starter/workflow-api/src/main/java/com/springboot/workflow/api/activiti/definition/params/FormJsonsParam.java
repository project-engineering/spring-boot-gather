package com.springboot.workflow.api.activiti.definition.params;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.util.Map;

/**
 * 动态表单数据结构
 */
@Data
public class FormJsonsParam {
    @Schema(description = "流程定义节点唯一标识")
    private String activityId;

    @Schema(description = "表单结构")
    private Map<String, Object> formJson;

    @Schema(description = "是否是主表单")
    private int isMainFrom;
}
