package com.springboot.workflow.api.system.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Tag(name = "菜单路由")
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterDTO {
    @Schema(description = "路由名称")
    private String name;
    @Schema(description = "路由路径")
    private String path;
    @Schema(description = "路由组件")
    private String component;
    @Schema(description = "路由重定向")
    private String redirect;
    @Schema(description = "路由元信息")
    private Meta meta;
    @Schema(description = "子路由")
    private List<RouterDTO> children;

    @Data
    @Builder
    @AllArgsConstructor
    public static class Meta {
        @Schema(description = "页面标题")
        private String title;
        @Schema(description = "页面图标")
        private String icon;
        @Schema(description = "页面权限")
        private Object[] roles;
    }

}
