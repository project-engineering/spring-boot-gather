package com.springboot.workflow.api.system.dept.params;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name = "新增部门信息")
public class AddSysDeptParam {

    @Schema(description = "部门id")
    private String deptId;

    @Schema(description = "部门名称")
    private String deptName;

    @Schema(description = "负责人")
    private String leader;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "是否是系统内置,1:是,0:否")
    private Integer isSys;

}