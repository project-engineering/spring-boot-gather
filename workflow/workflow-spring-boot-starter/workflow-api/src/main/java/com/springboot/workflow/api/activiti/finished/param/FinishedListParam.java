package com.springboot.workflow.api.activiti.finished.param;

import com.springboot.workflow.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Tag(name = "已办任务列表入参")
public class FinishedListParam extends Page {
    @Schema(description = "用户id")
    private String userId;

    @Schema(description = "流程定义名称")
    private String definitionName;

    @Schema(description = "流程定义key")
    private String definitionKey;
}
