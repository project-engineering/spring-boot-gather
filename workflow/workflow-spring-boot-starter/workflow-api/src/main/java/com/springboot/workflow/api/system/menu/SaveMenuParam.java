package com.springboot.workflow.api.system.menu;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Data
@Tag(name = "新增菜单角色关系")
public class SaveMenuParam {
    @Schema(description = "角色ID")
    @NotBlank(message = "角色ID不能为空")
    private String roleId;

    @Schema(description = "菜单ID列表")
    @NotBlank(message = "菜单ID列表不能为空")
    private List<String> menuIdList;
}
