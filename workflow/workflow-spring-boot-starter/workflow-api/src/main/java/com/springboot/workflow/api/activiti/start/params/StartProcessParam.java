package com.springboot.workflow.api.activiti.start.params;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Map;

@Data
@Tag(name = "启动流程实例入参数")
public class StartProcessParam {
    @Schema(description = "当前用户登录id")
    private String userId;

    @Schema(description = "业务id")
    private String businessKey;

    @Schema(description = "流程实例id")
    private String definitionId;

    @Schema(description = "启动流程时的表单参数,用于流程变量和回显")
    private Map<String, Object> variables;
}
