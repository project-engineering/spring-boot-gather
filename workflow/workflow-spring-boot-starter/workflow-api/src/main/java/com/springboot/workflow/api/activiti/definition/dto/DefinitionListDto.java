package com.springboot.workflow.api.activiti.definition.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Date;
import java.util.Map;

@Tag(name = "流程管理列表")
@Data
public class DefinitionListDto {
    @Schema(description = "流程id")
    private String id;

    @Schema(description = "部署id")
    private String deploymentId;

    @Schema(description = "流程名称")
    private String name;

    @Schema(description = "流程key")
    private String key;

    @Schema(description = "流程版本")
    private int version;

    @Schema(description = "流程表单详情")
    private Map<String, Object> formJson;

    @Schema(description = "是否挂起状态")
    private boolean isSuspended;

    @Schema(description = "部署时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deploymentTime;
}
