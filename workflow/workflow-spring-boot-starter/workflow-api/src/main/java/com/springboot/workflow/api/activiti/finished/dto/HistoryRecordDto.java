package com.springboot.workflow.api.activiti.finished.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Date;

@Data
@Tag(name = "流程记录")
public class HistoryRecordDto {
    @Schema(description = "任务名称")
    private String nodeName;

    @Schema(description = "流程定义节点唯一标识")
    private String activityId;

    @Schema(description = "任务开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @Schema(description = "任务结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    @Schema(description = "任务状态 1:已完成节点,2:活动的未处理的节点(下一个节点) 参考{@link NodeStatus}")
    private Integer status;

    @Schema(description = "流程记录身份信息")
    private IdentityDto identity;

    @Schema(description = "动态表单结构数据")
    private Object formJson;

    @Schema(description = "动态表单数据")
    private Object formData;
}
