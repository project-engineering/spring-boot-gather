package com.springboot.workflow.api.activiti.todo.params;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.Map;

@Data
@Tag(name = "审批节点入参")
public class TodoCompleteParam {

    @Schema(description = "用户id")
    private String userId;

    @Schema(description = "部门id(组id)")
    private String deptId;

    @Schema(description = "流程实例id")
    private String processInstanceId;

    @Schema(description = "流程变量")
    private Map<String, Object> variables;
}
