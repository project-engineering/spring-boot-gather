package com.springboot.workflow.api.activiti.finished.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.List;

/**
 * 流程记录身份信息
 *
 */
@Data
@Tag(name = "流程记录身份信息")
public class IdentityDto {

    @Schema(description = "审批人用户名称")
    private String userName;

    @Schema(description = "候选人用户名称")
    private List<String> userNames;

    @Schema(description = "候选组名称")
    private List<String> groupNames;
}
