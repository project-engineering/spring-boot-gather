import http from '@/http';
import {QueryTableListParam} from "@/api/activiti/table/TableModel";

export const getWidgetDataType =()=>{
  return http.get('/api/table/getWidgetDataType')
}

export const getTableList =(param:QueryTableListParam)=>{
  return http.post('/api/table/list',param)
}
