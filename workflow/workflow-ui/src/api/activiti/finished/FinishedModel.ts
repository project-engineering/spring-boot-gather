//列表查询参数类型
export type ListParam = {
  userId:string,
  definitionName:string,
  definitionKey:string,
  pageNo:number,
  pageSize:number,
  total:number
}
