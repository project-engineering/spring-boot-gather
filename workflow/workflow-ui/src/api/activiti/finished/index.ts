import http from '@/http';
import {ListParam} from "@/api/activiti/finished/FinishedModel";

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/api/processFinished/list',param)
}
