import http from '@/http';
import {CompleteParam, ListParam} from "@/api/activiti/todo/TodoModel";
import {UnwrapNestedRefs, UnwrapRef} from "vue";

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/api/processTodo/list',param)
}

//审批
export const getNodeFormApi = (taskId:string)=>{
  return http.get('/api/processTodo/getNodeForm/'+taskId)
}

//提交审批
export const completeApi = (param: UnwrapRef<UnwrapNestedRefs<{ processInstanceId: string; variables: {} }> & {}>)=>{
  return http.post('/api/processTodo/complete',param)
}
