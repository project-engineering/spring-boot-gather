//列表查询参数类型
export type ListParam = {
  userId:string,
  deptId:string,
  definitionName:string,
  definitionKey:string,
  pageNo:number,
  pageSize:number,
  total:number,
}

export type CompleteParam = {
  userId:string,
  deptId:string,
  processInstanceId:string,
  variables: {},
}
