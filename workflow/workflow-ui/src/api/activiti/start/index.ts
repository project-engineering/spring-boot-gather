import http from '@/http';
import {ListParam, StartProcessParam} from "@/api/activiti/start/StartModel";

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/api/processStart/list',param)
}

//启动流程
export const startProcessApi = (param:StartProcessParam) => {
  return http.post('/api/processStart/start',param)
}

//查询审批进度
export const getHistoryRecordApi = (instanceId:string) => {
  return http.get('/api/processStart/getHistoryRecord/'+instanceId)
}

//获取主表单信息
export const getMainFormInfoApi = (instanceId:string) => {
  return http.get('/api/processStart/getMainFormInfo/'+instanceId)
}

//查询流程图信息(高亮信息)
export const getHighlightNodeInfoApi = (instanceId:string) => {
  return http.get('/api/processStart/getHighlightNodeInfo/'+instanceId)
}

//删除流程实例
export const deleteApi = (instanceId:string) => {
  return http.delete('/api/processStart/delete/'+instanceId)
}
