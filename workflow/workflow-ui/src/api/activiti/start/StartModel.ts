//列表查询参数类型
export type ListParam = {
  userId:string,
  businessKey:string,
  definitionName:string,
  definitionKey:string,
  pageNo:number,
  pageSize:number,
  total:number,
}

//启动流程参数类型
export type StartProcessParam = {
  businessKey:string,
  definitionId:string,
  variables:any,
}
