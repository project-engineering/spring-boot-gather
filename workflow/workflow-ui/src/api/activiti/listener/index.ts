import http from '@/http';
import {ListParam} from "@/api/activiti/listener/ListenerModel";

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/api/listener/list',param)
}

//新增
export const addApi = (data:any) => {
  return http.post('/api/listener/add', data)
}

//删除
export const deleteApi = (id:string) => {
  return http.delete(`/api/listener/delete/${id}`)
}

//编辑
export const editApi = (data:any) => {
  return http.post('/api/listener/update', data)
}

