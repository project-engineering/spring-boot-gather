//列表查询参数类型
export type ListParam = {
  listenerName:string,
  pageNo:number,
  pageSize:number,
  total:number
}

export type AddListenerModel = {
  type:string,
  listenerId:string,
  listenerName:string,
  javaClass:string,
  isSys:number,
  remark:string
}
