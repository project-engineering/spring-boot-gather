//列表查询参数类型
export type ListParam = {
  definitionName:string,
  definitionKey:string,
  isActive:boolean,
  pageNo:number,
  pageSize:number,
  total:number
}


export type DeployProcessParam = {
  xml:string,
  formJsonList:any[],
  nodeColumns:any[],
  //表结构对象
  tableInfo:any,
}
