import http from '@/http';
import {DeployProcessParam, ListParam} from "@/api/activiti/definition/DefinitionModel";

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/api/processDefinition/list',param)
}

//部署流程
export const deployProcessApi =(param:DeployProcessParam)=>{
  return http.post('/api/processDefinition/deployProcess',param)
}

//更新流程状态
export const updateStateApi =(deploymentId:string)=>{
  return http.put(`/api/processDefinition/updateState/${deploymentId}`)
}

//获取流程定义
export const getDefinitionInfoApi =(deploymentId:string)=>{
  return http.get(`/api/processDefinition/getDefinitionInfo/${deploymentId}`)
}
//删除
export const deleteApi =(deploymentId:string)=>{
  return http.delete(`/api/processDefinition/delete/${deploymentId}`)
}

//查看流程图
export const getDefinitionXmlApi =(deploymentId:string)=>{
  return http.get(`/api/processDefinition/getDefinitionXml/${deploymentId}`)
}
