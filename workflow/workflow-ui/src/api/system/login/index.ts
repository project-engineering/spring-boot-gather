import http from '@/http';
import { RegisterReq, LoginReq } from "@/interface/user";

/**
 * 注册接口
 * @param param
 */
export const useRegisterApi = (param:RegisterReq) => {
    return http.post('/api/user/register', param)
}

/**
 * 登录接口
 * @param param
 */
export const useLoginApi = (param:LoginReq) => {
    return http.post('/login', param)
}
