//菜单数据类型
export type MenuType ={
    menuId: string,
    parentId: string,
    title: string,
    code: string,
    name: string,
    path: string,
    url: string,
    type: string, // 0-目录，1:菜单，2:按钮
    icon: string,
    parentName: string,
    orderNum: string
}

// 菜单树参数
export type AssignMenuParams = {
    userId: string,
    roleId: string,
}

//获取菜单
export type GetMenuListParam = {
    userId: string
}