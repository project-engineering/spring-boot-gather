//列表查询参数类型
export type ListParam = {
    deptName:string,
    pageNo:number,
    pageSize:number,
    total:number
}

//新增参数类型
export type AddDeptModel = {
  type:string,
  deptId:string,
  deptName: string,
  leader: string,
  phone: string,
  email: string,
  isSys: number,
}
