import http from '@/http';
import {AddDeptModel, ListParam} from "@/api/system/dept/DeptModel";
//新增角色
export const addApi =(param:AddDeptModel)=>{
    console.log(param)
    return http.post('/api/dept/add',param)
}

//列表
export const getListApi =(param:ListParam)=>{
    return http.post('/api/dept/list',param)
}

//详情
export const getDetailApi =(deptId:string)=>{
    return http.get(`/api/dept/info/${deptId}`)
}

//编辑
export const editApi =(param:AddDeptModel)=>{
    return http.put(`/api/dept/update`,param)
}

//删除
export const deleteApi =(userId:string)=>{
    return http.delete(`/api/dept/delete/${userId}`)
}
