//新增角色菜单模型
export type AddRoleMenuModel ={
    roleId: string;
    menuIdList: list<string>;
}