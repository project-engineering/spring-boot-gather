import "element-plus/theme-chalk/display.css";
import "element-plus/theme-chalk/index.css";
import { createApp } from "vue";
//引入Pinia构造函数
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import ElementPlus from "element-plus";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//确定弹框封装
import customConfirm from "@/utils/customConfirm";
import objCopy from "@/utils/objCopy";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import zhCn from "element-plus/dist/locale/zh-cn.mjs";
import axios from "axios";
import "virtual:svg-icons-register";
import piniaPluginpersistedstate from 'pinia-plugin-persist';
//权限验证
import './permission.ts';
//按钮指令
import hasPerm from '@/divective/hasPerm'

//实例化Pinia
const pinia = createPinia()
//pinia数据持久化
pinia.use(piniaPluginpersistedstate);

const app = createApp(App);

app.use(ElementPlus, {
  locale: zhCn,
  size: "default"
});

// vfrom相关依赖引入---------------------------------------------
import "@/components/FormDesigner/styles/index.scss";
import Draggable from "@/../lib/vuedraggable/dist/vuedraggable.umd.js";
import { registerIcon } from "@/components/FormDesigner/utils/el-icons";

import ContainerWidgets from "@/components/FormDesigner/form-widget/container-widget/index";
import ContainerItems from "@/components/FormDesigner/form-render/container-item/index";

import { addDirective } from "@/components/FormDesigner/utils/directive";
import { installI18n } from "@/components/FormDesigner/utils/i18n";
import { loadExtension } from "@/components/FormDesigner/extension/extension-loader";
registerIcon(app);
app.component("draggable", Draggable);
addDirective(app);
installI18n(app);
app.use(ContainerWidgets);
app.use(ContainerItems);
loadExtension(app);
// vfrom相关依赖引入---------------------------------------------

// bpmn.js 相关-------------------------------------------------
import "@/components/BpmnJs/styles/index.scss";
// bpmn.js 相关-------------------------------------------------

app.use(pinia).use(router).mount("#app");
//全局注册图标组件
for(const[key,componet] of Object.entries(ElementPlusIconsVue)){
  app.component(key,componet)
}
//全局挂载弹框
app.config.globalProperties.$confirm = customConfirm
app.config.globalProperties.$objCopy = objCopy
//全局挂载按钮指令
app.config.globalProperties.$hasPerm = hasPerm
window.axios = axios;
