import axios, {
    AxiosInstance,
    AxiosRequestConfig,
    AxiosResponse,
    AxiosRequestHeaders,
    InternalAxiosRequestConfig
} from "axios";
import{ElMessage} from "element-plus";
import { userStore } from "@/store/user";

const baseURL = "http://localhost:8080/workflow/";
//axios请求配置
const config = {
    changeOrigin:true, //允许跨域
    baseURL: baseURL,
    //超时时间60s
    timeout: 60000,
    withCredentials: true, //允许携带cookie
}

//定义返回值类型
export interface Result<T = any> {
    code: number;
    message: string;
    data: T;
}

//定义请求类
class Http {
    //axios实例
    private instance: AxiosInstance;
    //构造函数里面初始化
    constructor(config: AxiosRequestConfig) {
        //实例化axios
        this.instance = axios.create(config);
        //定义拦截器：请求发送之前和请求发送之后处理
        this.interceptors();
    }

    //拦截器
    private interceptors() {
        //请求发送之前处理
        this.instance.interceptors.request.use((config:InternalAxiosRequestConfig) => {
            //可以在这里添加一些公共的请求参数
            const store = userStore();
            //token携带
            let token = store.getToken;
            if (token){
                config.headers!['token'] = token;
            }
            return config;
        }, (error: any) => {
            error.data ={}
            error.data.message =  "服务器异常，请联系管理员！";
            return error;
        })
        //axios请求返回之后的处理
        //请求发送之后处理
        this.instance.interceptors.response.use((res:AxiosResponse)=>{
            if (res.data.code != 200) {
                ElMessage.error(res.data.message||'服务器出错！');
                return Promise.reject(res.data.message||'服务器出错！')
            }else {
                return res.data;
            }
        },(error)=>{
            error.data={}
            if(error && error.response){
                switch (error.response.status){
                    case 400:
                        error.data.message = '错误请求';
                        ElMessage.error(error.data.message)
                        break
                    case 401:
                        error.data.message = '未授权，请重新登录';
                        ElMessage.error(error.data.message)
                        break
                    case 403:
                        error.data.message = '拒绝访问';
                        ElMessage.error(error.data.message)
                        break
                    case 404:
                        error.data.message = '请求错误,未找到该资源';
                        ElMessage.error(error.data.message)
                        break
                    case 405:
                        error.data.message = '请求方法未允许';
                        ElMessage.error(error.data.message)
                        break
                    case 408:
                        error.data.message = '请求超时';
                        ElMessage.error(error.data.message)
                        break
                    case 500:
                        error.data.message = '服务器端出错';
                        ElMessage.error(error.data.message)
                        break
                    case 501:
                        error.data.message = '网络未实现';
                        ElMessage.error(error.data.message)
                        break
                    case 502:
                        error.data.message = '网络错误';
                        ElMessage.error(error.data.message)
                        break
                    case 503:
                        error.data.message = '服务不可用';
                        ElMessage.error(error.data.message)
                        break
                    case 504:
                        error.data.message = '网络超时';
                        ElMessage.error(error.data.message)
                        break
                    case 505:
                        error.data.message = 'http版本不支持该请求';
                        ElMessage.error(error.data.message)
                        break
                    default:
                        error.data.message = `连接错误${error.response.status}`;
                        ElMessage.error(error.data.message)
                }
            } else {
                error.data.message ="连接服务器失败";
                ElMessage.error(error.data.message)
            }
            return Promise.reject(error)
        })
    }

    /** GET请求方法 */
    get<T>(url: string, params?:object) : Promise<T> {
        return this.instance.get(url, { params })
    }

    /** POST请求方法 */
    post<T>(url: string, data?:object) : Promise<T> {
        return this.instance.post(url, data)
    }

    /** PUT请求方法 */
    put<T>(url: string, data?:object) : Promise<T> {
        return  this.instance.put(url, data)
    }

    /**  DELETE请求方法 */
    delete<T>(url: string) : Promise<T> {
        return this.instance.delete(url)
    }

    getBaseUrl() : string {
        return baseURL;
    }
}

export default new Http(config)
