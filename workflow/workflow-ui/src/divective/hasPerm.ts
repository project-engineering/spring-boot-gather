import {userStore} from "../store/user";

//自定义按钮权限指令
export default function hasPerm(value: any) {
    const store = userStore();
    //当前用户用友的权限字段
    const permissions = store.getPermission;
    if (value && value instanceof Array && value.length > 0) {
        const permissionRoles = value;
        //判断当前用户是否有按钮权限
        return permissions.some((role) => {
            return permissionRoles.includes(role);
        });
    } else {
        throw new Error('v-if="global.hasPerm("[\'add\',\'update\']")"');
    }
}