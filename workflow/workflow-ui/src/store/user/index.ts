import {defineStore} from "pinia";
import { getUserInfoApi} from "src/api/system/user";
import {reactive} from "vue";
import {GetUserInfoParam} from "@/api/system/user/UserModel.ts";

const getUserInfoParam = reactive<GetUserInfoParam>({
    userId:''
})
//定义一个store
export const userStore = defineStore('userStore',{
    state: ()=>{
        return {
            userId: '',
            nickName: '',
            token: '',
            permission: []
        }
    },
    getters: {
        getUserId(state){
            return state.userId
        },
        getNickName(state){
            return state.nickName
        },
        getToken(state){
            return state.token
        },
        getPermission(state){
            return state.permission
        }
    },
    actions: {
        setUserId(userId: string) {
            this.userId = userId;
        },
        setNickName(nickName: string) {
            this.nickName = nickName;
        },
        setToken(token: string) {
            this.token = token;
        },
        getUserInfo(){
            return new Promise((resolve, reject) => {
                getUserInfoParam.userId = this.userId;
                getUserInfoApi(getUserInfoParam).then(res => {
                    console.log("获取用户信息成功：",res)
                    if (res && res.code === 200) {
                        //设置权限信息
                        this.permission = res.data.permissions;
                    }
                    resolve(this.permission)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    },
    persist: {
        enabled: true,
        strategies: [
            {
                storage:sessionStorage,
                paths: ['userId', 'nickName','token']
            }
        ]
    }
})
