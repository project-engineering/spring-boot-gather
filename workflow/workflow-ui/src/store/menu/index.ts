import { defineStore} from "pinia";
import { getMenuListApi} from "src/api/system/menu";
import Layout from "@/views/layout/Layout.vue";
import Center from "@/views/layout/center/center.vue";
import {reactive} from "vue";
import {GetMenuListParam} from "@/api/system/menu/MenuModel.ts";

// 导入所有视图组件
const modules = import.meta.glob('../../views/**/*.vue');

const getMenuListParam = reactive<GetMenuListParam>({
    userId:''
})

// 定义一个store
export const menuStore = defineStore('menuStore', {
  state: () => {
    return {
        collapse: false,
        // 菜单列表
        menuList: [{
            path: '/dashboard',
            component: 'Layout',
            name: 'dashboard',
            meta: {
                title: '首页',
                icon: 'HomeFilled',
                roles: ['sys:dashboard']
            },
        }]
    }
  },
  getters: {
      getCollapse() {
        return this.collapse;
      },
      getMenu(state) {
        return state.menuList;
      }
  },
  actions: {
      setCollapse(collapse: boolean) {
          this.collapse = collapse;
      },
      //获取菜单数据
      getMenuList(router:any,userId:string) {
          return new Promise((resolve, reject) => {
              getMenuListParam.userId = userId;
              //调用接口获取菜单数据
              getMenuListApi(getMenuListParam).then(res => {
                  let accessedRoutes;
                  console.log("菜单数据111：",res);
                  if (res && res.code === 200) {
                      //生成路由
                      accessedRoutes = generateRoute(res.data,router) as any;
                      //设置菜单列表
                      this.menuList = this.menuList.concat(accessedRoutes);
                  }
                  resolve(accessedRoutes);
                  console.log("菜单列表：",accessedRoutes);
              }).catch(err => {
                 reject(err);
              });
          });
      }
  }
})

// // 动态生成路由
// export function generateRoute(routes:RouteRecordRaw[],router:any){
//     //路由数据
//     const res:Array<RouteRecordRaw> = [];
//     routes.forEach((route:any) => {
//         const tmp = {...route};
//         const component = tmp.component;
//         //生成路由的component
//         if (route.component) {
//             if (component === 'Layout'){
//                 tmp.component = Layout;
//             } else {
//                 tmp.component = modules[`@/${component}`];
//             }
//         }
//         console.log("tmp.children:",tmp.children);
//         // 有下级路由
//         if (tmp.children && tmp.children.length > 0) {
//             if (route.component != 'Layout') {
//                 tmp.children = Center;
//             }
//             //递归生成子路由
//             tmp.children = generateRoute(tmp.children,router);
//         }
//         //加入路由数据
//         console.log(tmp);
//         res.push(tmp);
//         console.log("11111111111:",res.pop());
//         //加入路由
//         router.addRoute(tmp);
//         console.log("路由数据：",res);
//     })
//     console.log("路由数据11111111：",res);
//     return res;
// }
// 动态生成路由
export function generateRoute(routes, router) {
    const res = [];
    //路由数据
    routes.forEach(route => {
        const tmp = { ...route };
        // console.log("处理前的tmp:", tmp); // 添加调试信息
        //生成路由的component
        const component = tmp.component;
        if (route.component) {
            if (component === 'Layout') {
                tmp.component = Layout;
            } else {
                tmp.component = modules[`../../${component}`];
                if (!tmp.component) {
                    console.error(`组件 ${component} 未找到`); // 检查组件是否有效
                }
            }
        }

        // console.log("处理后的tmp:", tmp); // 添加调试信息
        // 有下级路由
        if (tmp.children && tmp.children.length > 0) {
            if (route.component != 'Layout') {
                tmp.children = Center;
            } else {
                tmp.children = generateRoute(tmp.children, router);
            }
        }
        //加入路由数据
        res.push(tmp);
        //加入路由
        router.addRoute(tmp);
    });
    return res;
}
