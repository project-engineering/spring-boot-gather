import { defineStore } from 'pinia'
import type { RouteLocationNormalized } from 'vue-router'
import router from '@/router'

interface TagView {
  path: string
  name?: string | symbol | null | undefined
  meta?: {
    title?: string
    affix?: boolean
    noCache?: boolean
  }
  title?: string
  fullPath?: string
}

export const useTagsViewStore = defineStore('tagsView', {
  state: () => ({
    visitedViews: [] as TagView[],
    cachedViews: new Set<string>()
  }),

  actions: {
    addView(view: RouteLocationNormalized) {
      this.addVisitedView(view)
      this.addCachedView(view)
      this.saveVisitedViews()
    },

    addVisitedView(view: RouteLocationNormalized) {
      if (this.visitedViews.some(v => v.path === view.path)) return

      const tag: TagView = {
        path: view.path,
        name: view.name,
        meta: view.meta,
        title: view.meta?.title || 'no-name',
        fullPath: view.fullPath
      }

      // 确保首页始终在第一位
      if (view.path === '/dashboard') {
        this.visitedViews.unshift(tag)
      } else {
        this.visitedViews.push(tag)
      }
    },

    addCachedView(view: RouteLocationNormalized) {
      if (view.name === null || view.name === undefined) return
      const name = view.name as string
      if (this.cachedViews.has(name)) return
      if (view.meta?.keepAlive && name !== 'Redirect') {
        this.cachedViews.add(name)
      }
    },

    async delView(path: string) {
      // 移除访问过的视图
      this.delVisitedView(path);

      // 移除缓存的视图
      const view = this.visitedViews.find(v => v.path === path);
      if (view && view.name) {
        this.cachedViews.delete(view.name as string);
      }

      // 保存访问过的视图到本地存储
      this.saveVisitedViews();

      // 如果当前路由是被删除的视图，则重定向到另一个视图
      const currentRoute = router.currentRoute.value;
      if (currentRoute.path === path) {
        const visitedViews = this.visitedViews.filter(v => !v.meta?.affix);
        if (visitedViews.length) {
          const redirectPath = visitedViews[visitedViews.length - 1].path;
          router.push(redirectPath);
        } else {
          // 如果没有其他视图，则重定向到首页或其他指定路由
          router.push('/dashboard');
        }
      }

      return {
        visitedViews: [...this.visitedViews],
        cachedViews: Array.from(this.cachedViews)
      };
      return Promise.resolve();
    },


    delVisitedView(path: string) {
      const index = this.visitedViews.findIndex(v => v.path === path)
      if (index > -1) {
        this.visitedViews.splice(index, 1)
      }
    },

    delCachedView(path: string) {
      this.cachedViews.delete(path)
    },

    delOthersViews(path: string) {
      this.visitedViews = this.visitedViews.filter(v => {
        return v.meta?.affix || v.path === path || v.path === '/dashboard'
      })
      const keepNames = this.visitedViews
        .map(v => v.name)
        .filter((name): name is string => typeof name === 'string')
      this.cachedViews = new Set(keepNames)
      this.saveVisitedViews()
    },

    delAllViews() {
      // 保留首页和固定标签
      this.visitedViews = this.visitedViews.filter(v =>
        v.meta?.affix || v.path === '/dashboard'
      )
      this.cachedViews.clear()
      this.saveVisitedViews()
    },

    // 保存访问过的视图到本地存储
    saveVisitedViews() {
      // 过滤掉不存在的路由
      const validViews = this.visitedViews.filter(view => {
        const route = router.getRoutes().find(r => r.path === view.path)
        return route && !route.meta?.hidden
      })
      localStorage.setItem('visitedViews', JSON.stringify(validViews))
    },

    // 从本地存储恢复访问过的视图
    restoreVisitedViews() {
      const views = localStorage.getItem('visitedViews')
      if (views) {
        const savedViews = JSON.parse(views) as TagView[]

        // 验证并恢复视图
        savedViews.forEach(view => {
          const route = router.getRoutes().find(r => r.path === view.path)
          if (route && !route.meta?.hidden) {
            // 构造完整的路由信息
            const fullRoute: RouteLocationNormalized = {
              path: view.path,
              name: view.name,
              meta: {
                ...route.meta,
                ...view.meta
              },
              fullPath: view.fullPath || view.path,
              matched: [],
              params: {},
              query: {},
              hash: ''
            }
            this.addView(fullRoute)
          }
        })

        // 确保首页在第一位
        const dashboard = this.visitedViews.find(v => v.path === '/dashboard')
        if (dashboard) {
          const index = this.visitedViews.indexOf(dashboard)
          if (index > 0) {
            this.visitedViews.splice(index, 1)
            this.visitedViews.unshift(dashboard)
          }
        }
      }
    }
  }
})
