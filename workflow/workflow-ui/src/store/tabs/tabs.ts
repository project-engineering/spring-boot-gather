import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useTabsStore = defineStore('tabs', () => {
  const tabs = ref([])
  const activeTab = ref('')

  // 添加新标签
  const addTab = (route) => {
    const isExist = tabs.value.some(tab => tab.path === route.path)
    if (!isExist) {
      tabs.value.push({
        title: route.meta.title || '未命名',
        path: route.path,
        name: route.name,
        closable: true
      })
    }
    activeTab.value = route.path
  }

  // 关闭标签
  const closeTab = (targetPath, router) => {
    const targetIndex = tabs.value.findIndex(tab => tab.path === targetPath)
    if (targetIndex === -1) return

    tabs.value.splice(targetIndex, 1)
    if (activeTab.value === targetPath) {
      const nextTab = tabs.value[targetIndex] || tabs.value[targetIndex - 1]
      if (nextTab) {
        router.push(nextTab.path)
      } else {
        router.push('/')
      }
    }
  }

  // 关闭其他标签
  const closeOtherTabs = (path) => {
    tabs.value = tabs.value.filter(tab => tab.path === path || !tab.closable)
    activeTab.value = path
  }

  // 关闭所有标签
  const closeAllTabs = (router) => {
    tabs.value = tabs.value.filter(tab => !tab.closable)
    router.push('/')
  }

  return {
    tabs,
    activeTab,
    addTab,
    closeTab,
    closeOtherTabs,
    closeAllTabs
  }
}) 