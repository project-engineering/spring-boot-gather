export interface LoginReq {
    username: string;
    password: string;
    captcha:string;
    token: string;
}

export interface RegisterReq {
    username: string;
    password: string;
    email: string;
}