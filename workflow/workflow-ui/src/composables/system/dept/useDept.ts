import {ref} from "vue";
import {EditType} from "@/type/BaseEnums";
import {deleteApi} from "@/api/system/dept";
import {ElMessage} from "element-plus";
import {FuncList} from "@/type/BaseType";
import useInstance from "@/hooks/useInstance";
import {AddDeptModel} from "@/api/system/dept/DeptModel";

export default function useDept (getList:FuncList) {
    const {global} = useInstance()
    //新增弹框的ref属性
    //type:0-新增 1-修改 ；row:要编辑的数据
    const addRef = ref<{show:(type:string,row?:AddDeptModel)=>void}>()
    //新增
    const addBtn = () => {
        addRef.value?.show(EditType.ADD)
    }
    //修改
    const editBtn = (row:AddDeptModel) => {
        //显示弹框：父组件调用子组件
        addRef.value?.show(EditType.EDIT,row)

    }
    //删除
    const delBtn = async (deptId:string) => {
        //信息确定
        let confirm = await global.$confirm("确认要删除当前项吗?", "提示");
        if(confirm) {
            let res = await deleteApi(deptId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
                //刷新列表
                getList();
            }
        }
    }

    return {
        addBtn,
        editBtn,
        delBtn,
        addRef,
    }
}
