import {reactive, ref} from "vue";
import {GetRoleListModel, GetRoleListParam, GetUserInfoParam, selectTypeList} from "@/api/system/user/UserModel";
import {getSelectApi,getRoleByUserIdApi} from "../../../api/system/user";

export default function useSelectRole(){
    //角色id
    const roleId = ref('');
    const getRoleListParam = reactive<GetRoleListParam>({
        userId:''
    })
    //角色数据
    const roleData = reactive<selectTypeList>({
        list:[]
    })
    //获取数据
    const listRole = async ()=>{
        let res = await getSelectApi();
        console.log(res)
        if (res && res.code === 200){
            roleData.list = res.data;
        }
    }
    //获取角色id
    const getRoleByUserId = async (userId:string)=>{
        getRoleListParam.userId = userId;
        let res = await getRoleByUserIdApi(getRoleListParam);
        if (res && res.code === 200){
            roleId.value = res.data.roleId;
        }
    }

    return {
        roleData,
        listRole,
        getRoleByUserId,
        roleId
    }
}
