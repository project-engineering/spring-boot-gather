import {ref} from "vue";
import {EditType} from "@/type/BaseEnums";
import {deleteApi} from "@/api/activiti/listener";
import {ElMessage} from "element-plus";
import {FuncList} from "@/type/BaseType";
import useInstance from "@/hooks/useInstance";
import {AddListenerModel} from "@/api/activiti/listener/ListenerModel";

export default function useListener (getList:FuncList) {
    const {global} = useInstance()
    //新增弹框的ref属性
    //type:0-新增 1-修改 ；row:要编辑的数据
    const addRef = ref<{show:(type:string,row?:AddListenerModel)=>void}>()
    //新增
    const addBtn = () => {
        addRef.value?.show(EditType.ADD)
    }
    //修改
    const editBtn = (row:AddListenerModel) => {
        console.log(row)
        //显示弹框：父组件调用子组件
        addRef.value?.show(EditType.EDIT,row)

    }
    //删除
    const delBtn = async (row:AddListenerModel) => {
        //信息确定
        let confirm = await global.$confirm('是否删除该监听器？');
        if(confirm) {
            let res = await deleteApi(row.listenerId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
                //刷新列表
                getList();
            }
        }
    }


    return {
        addBtn,
        editBtn,
        delBtn,
        addRef
    }
}
