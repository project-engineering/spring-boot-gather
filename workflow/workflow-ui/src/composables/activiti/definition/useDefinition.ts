import {deleteApi,updateStateApi} from "@/api/activiti/definition";
import {ElMessage} from "element-plus";
import {FuncList} from "@/type/BaseType";
import useInstance from "@/hooks/useInstance";

export default function useUser (getList:FuncList) {
    const {global} = useInstance()
  //更新流程定义状态 激活或者挂起
  const updateState = async (deploymentId: any) => {
    let res = await updateStateApi(deploymentId);
    if(res && res.code == 200) {
      //信息提示
      ElMessage.success(res.message);
      //刷新列表
      getList();
    }
  }

    //删除
    const delBtn = async (deploymentId: any) => {
        //信息确定
        let confirm = await global.$confirm('确认要删除当前项吗? 流程实例启动的也将被删除,谨慎删除！');
        if(confirm) {
            let res = await deleteApi(deploymentId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
                //刷新列表
                getList();
            }
        }
    }


    return {
      updateState,
      delBtn
    }
}
