DROP TABLE IF EXISTS `kafka_record_ods`;
CREATE TABLE `kafka_record_ods` (
    `id` varchar(60) NOT NULL COMMENT '主键',
    `topic` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订阅主题',
    `partitions` int DEFAULT NULL COMMENT '分区',
    `offset_num` int DEFAULT NULL COMMENT '偏移量',
    `group_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者组',
    `client_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者id',
    `msg` longtext COLLATE utf8mb4_general_ci COMMENT 'kafka消息记录',
    `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '记录状态',
    `receive_time` datetime DEFAULT NULL COMMENT '接收时间',
    `created_stamp` datetime DEFAULT NULL COMMENT '创建时间',
    `last_updated_stamp` datetime DEFAULT NULL COMMENT '最后更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=452 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='kafka消费原始记录表';


DROP TABLE IF EXISTS `kafka_record_ods_his`;
CREATE TABLE `kafka_record_ods_his` (
    `id` varchar(60) NOT NULL COMMENT '主键',
    `topic` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订阅主题',
    `partitions` int DEFAULT NULL COMMENT '分区',
    `offset_num` int DEFAULT NULL COMMENT '偏移量',
    `group_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者组',
    `client_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者id',
    `msg` longtext COLLATE utf8mb4_general_ci COMMENT 'kafka消息记录',
    `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '记录状态',
    `receive_time` datetime DEFAULT NULL COMMENT '接收时间',
    `created_stamp` datetime DEFAULT NULL COMMENT '创建时间',
    `last_updated_stamp` datetime DEFAULT NULL COMMENT '最后更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=452 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='kafka消费历史记录表';