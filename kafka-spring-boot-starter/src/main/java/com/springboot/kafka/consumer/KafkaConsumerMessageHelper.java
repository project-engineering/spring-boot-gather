package com.springboot.kafka.consumer;

import cn.hutool.core.util.IdUtil;
import com.springboot.futool.DateUtil;
import com.springboot.futool.UtilValidate;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.persistence.entity.KafkaRecordOds;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Kafka消费者消息处理工具类
 */
@Log4j2
@Component
@Data
public class KafkaConsumerMessageHelper {
    // 偏移量提交间隔
    private static final int COMMIT_INTERVAL = 1000;
    // 记录已提交的偏移量次数
    private static final AtomicInteger COMMIT_COUNT = new AtomicInteger(0);
    private ConsumerRecords<String,String> records;
    private String groupId;
    private String clientId;
    private KafkaRecordOdsService kafkaRecordOdsService;
    private KafkaConsumer consumer;
    ThreadLocal<Long> THREADLOCAL_OFFSET = new ThreadLocal<>();

    /**
     * 业务处理，保存到表中
     */
    public long save() {
        log.info("开始处理消息...");
        List<KafkaRecordOds> list = new ArrayList<>();
        long offset = 0L;
        if (UtilValidate.isNotEmpty(records)) {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            try {
                for (ConsumerRecord<String,String> record:records) {
                    KafkaRecordOds kafkaRecordOds = new KafkaRecordOds();
                    kafkaRecordOds.setId(IdUtil.getSnowflake(1, 1).nextIdStr());
                    kafkaRecordOds.setTopic(record.topic());
                    kafkaRecordOds.setPartitions(record.partition());
                    kafkaRecordOds.setOffsetNum((int) record.offset());
                    kafkaRecordOds.setGroupId(groupId);
                    kafkaRecordOds.setClientId(clientId);
                    kafkaRecordOds.setMsg(record.value());
                    kafkaRecordOds.setStatus(KafkaConstant.STATUS_0);
                    kafkaRecordOds.setReceiveTime(DateUtil.convertObjToSqlDate(DateUtil.getCurrLocalDateTime()));
                    log.info("====topic:"+record.topic()+",offset:"+record.offset()+",消息："+record.value());
                    list.add(kafkaRecordOds);
                    THREADLOCAL_OFFSET.set(record.offset());
                    // 异步提交位移（每处理一条消息后）
                    consumer.commitAsync(Collections.singletonMap(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset() + 1)), (map, exception) -> {
                        if (exception != null) {
                            log.error("Commit failed: {}" , exception);
                        } else {
                            log.info("Commit succeeded");
                        }
                    });
                }
                kafkaRecordOdsService.save(list);
                offset = THREADLOCAL_OFFSET.get();
            }catch (Exception e) {
                // 异常处理逻辑
                consumer.commitSync();
            } finally {
                // 同步提交位移 一定批次提交一次
                if (COMMIT_COUNT.incrementAndGet() % COMMIT_INTERVAL == 0) {
                    consumer.commitSync();
                    // 重置已提交的偏移量次数
                    COMMIT_COUNT.set(0);
                }
                THREADLOCAL_OFFSET.remove();
            }
            stopWatch.stop();
            log.info("处理["+records.count()+"]条消息，耗时["+stopWatch.getTotalTimeMillis()+"]ms");
        }
        return offset;
    }
}
