package com.springboot.kafka.consumer.constant;

public class KafkaConstant {
    /**
     * 默认消费方式
     */
    public static String CONSUMER_TYEP_0 ="0";
    /**
     * 回拨时间消费
     */
    public static String CONSUMER_TYEP_1 ="1";
    /**
     * 回拨偏移量消费
     */
    public static String CONSUMER_TYEP_2 ="2";
    /**
     * 0-待处理
     */
    public static String STATUS_0 = "0";
    /**
     * 1-处理成功
     */
    public static String STATUS_1 = "1";
    /**
     * 2-处理失败
     */
    public static String STATUS_2 = "2";
    /**
     * 3-处理中
     */
    public static String STATUS_3 = "3";
    /**
     * 4-数据异常不做处理
     */
    public static String STATUS_4 = "4";

    public static final String TEST_TOPIC = "testTopic";
    public static final String TEST_TOPIC_1 = "testTopic1";
    public static final String TEST_TOPIC_2 = "testTopic2";
}
