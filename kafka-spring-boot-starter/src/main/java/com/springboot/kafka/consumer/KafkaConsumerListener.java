package com.springboot.kafka.consumer;

import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.factory.BusinessProcessFactory;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import jakarta.annotation.Resource;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * 实时监听kafka各种主题
 * @author liuc
 */
@Component
public class KafkaConsumerListener implements ApplicationRunner {
    @Resource(name="consumerConf")
    public Properties properties;
    @Resource(name ="executorService")
    public ExecutorService executorService;
    @Resource
    private KafkaRecordOdsService kafkaRecordOdsService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //启动就初始化从kafka主题消费下来的信息对应的后续处理类
        Map<String,Object> map = BusinessProcessFactory.initRecordHandler();
        List<String> topicList = (List<String>) map.get("topicList");
//        List<String> topicList = new ArrayList<>();
//        topicList.add("testTopic1");
        KafkaConsumerWorker kafkaConsumerWorker = new KafkaConsumerWorker();
        kafkaConsumerWorker.setProperties(properties);
        kafkaConsumerWorker.setExecutorService(executorService);
        kafkaConsumerWorker.setKafkaRecordOdsService(kafkaRecordOdsService);
        kafkaConsumerWorker.work(topicList, KafkaConstant.CONSUMER_TYEP_0,null,null);
    }
}
