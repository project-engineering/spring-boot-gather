package com.springboot.kafka.consumer.task;

import com.springboot.futool.UtilValidate;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.factory.BusinessProcessFactory;
import com.springboot.kafka.factory.IBusinessProcessEntrance;
import com.springboot.kafka.persistence.entity.KafkaRecordOds;
import com.springboot.kafka.persistence.entity.KafkaRecordOdsHis;
import com.springboot.kafka.persistence.service.KafkaRecordOdsHisService;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Component
public class KafkaConsumerTask {
    @Resource
    KafkaRecordOdsService kafkaRecordOdsService;
    @Resource
    KafkaRecordOdsHisService kafkaRecordOdsHisService;

    /**
     * 处理从kafka订阅到的不同主题的信息
     * 每600秒执行一次
     * @return
     */
    @Scheduled(cron ="*/600 * * * * ?")
    public void dealKafkaRecordsOdsInfoTask(){
        List<KafkaRecordOds> list = kafkaRecordOdsService.findKafkaRecordOdsByStatusList(KafkaConstant.STATUS_0);
        if (UtilValidate.isNotEmpty(list)) {
            //每1000条做一个批次，分批处理
            int numPerTimes = 1000;
            if (list.size() <= numPerTimes) {
                doDealKafkaRecordsOdsInfoTask(list);
            } else {
                //否则分批处理
                int maxIndex = list.size();
                int maxTimes = maxIndex / numPerTimes;
                maxTimes += (maxIndex%numPerTimes) > 0 ? 1 : 0;
                int currentTimes = 0;
                while (currentTimes < maxTimes) {
                    int fromIndex = numPerTimes * currentTimes;
                    int toIndex = fromIndex + numPerTimes;
                    List<KafkaRecordOds> subList = list.subList(fromIndex,toIndex);
                    doDealKafkaRecordsOdsInfoTask(subList);
                    currentTimes++;
                }
            }
        }
    }

    public void doDealKafkaRecordsOdsInfoTask(List<KafkaRecordOds> list){
        List<String> idList = new ArrayList<>();
        for (KafkaRecordOds kafkaRecordOds:list) {
            idList.add(kafkaRecordOds.getId());
        }
        //更新为处理中
        kafkaRecordOdsService.updateRecordStatus(idList,KafkaConstant.STATUS_3);
        List<Map<String,Object>> peedingList = new ArrayList<>();
        //处理成功的数据id集合
        List<String> successIdList = new ArrayList<>();
        //处理失败的数据id集合
        List<String> failIdList = new ArrayList<>();
        //数据问题不作处理的数据id集合
        List<String> nohandledDataIdList = new ArrayList<>();
        for (KafkaRecordOds kafkaRecordOds:list) {
            String id = kafkaRecordOds.getId();
            String topic = kafkaRecordOds.getTopic();
            String msg = kafkaRecordOds.getMsg();
            Map<String,Object> map = new HashMap<>();
            if (UtilValidate.isNotEmpty(msg) && msg.trim().length() != 0) {
                map.put("id",id);
                map.put("topic",topic);
                map.put("msg",msg);
                peedingList.add(map);
                idList.add(id);
            } else {
                nohandledDataIdList.add(id);
            }
        }
        //按主题分组处理
        Map<String,List<Map<String,Object>>> mapListGroup = peedingList.stream().
                collect(Collectors.groupingBy(m->m.get("topic").toString()));
        IBusinessProcessEntrance entrance = null;
        for (Map.Entry<String,List<Map<String,Object>>> entry:mapListGroup.entrySet()) {
            List<String> processOdsList = new ArrayList<>();
            List<Map<String,Object>> processList = entry.getValue();
            for (Map<String,Object> map:processList) {
                processOdsList.add((String) map.get("id"));
            }
            Map<String,Object> reqMap = new HashMap<>();
            try {
                //根据topic调用不同的实现类
                entrance = BusinessProcessFactory.getServiceImpl(entry.getKey());
                reqMap.put("dataList",processList);
                Map<String,Object> resp = entrance.execute(reqMap);
                successIdList = (List<String>) resp.get("successIdList");
                failIdList = (List<String>) resp.get("failIdList");
            } catch (Exception e) {
                log.error(e);
                //更新为处理失败
                kafkaRecordOdsService.updateRecordStatus(idList,KafkaConstant.STATUS_2);
            }
        }
        if (UtilValidate.isNotEmpty(successIdList)) {
            //更新为处理完成
            kafkaRecordOdsService.updateRecordStatus(successIdList,KafkaConstant.STATUS_1);
        }
        if (UtilValidate.isNotEmpty(failIdList)) {
            //更新为处理失败
            kafkaRecordOdsService.updateRecordStatus(failIdList,KafkaConstant.STATUS_2);
        }
        if (UtilValidate.isNotEmpty(nohandledDataIdList)) {
            //更新为4-数据异常不做处理状态
            kafkaRecordOdsService.updateRecordStatus(nohandledDataIdList,KafkaConstant.STATUS_4);
        }
    }

    /**
     * 将处理成功的kafka信息移到历史表中
     * 每6秒执行一次
     * @return
     */
    @Scheduled(cron ="*/6 * * * * ?")
    public void moveSucessRecordsToHistoryTask(){
        List<KafkaRecordOds> list = kafkaRecordOdsService.findKafkaRecordOdsByStatusList(KafkaConstant.STATUS_1);
        if (UtilValidate.isNotEmpty(list)) {
            //每1000条做一个批次，分批处理
            int numPerTimes = 1000;
            if (list.size() <= numPerTimes) {
                doMoveSucessRecordsToHistoryTask(list);
            } else {
                //否则分批处理
                int maxIndex = list.size();
                int maxTimes = maxIndex / numPerTimes;
                maxTimes += (maxIndex%numPerTimes) > 0 ? 1 : 0;
                int currentTimes = 0;
                while (currentTimes < maxTimes) {
                    int fromIndex = numPerTimes * currentTimes;
                    int toIndex = fromIndex + numPerTimes;
                    List<KafkaRecordOds> subList = list.subList(fromIndex,toIndex);
                    doMoveSucessRecordsToHistoryTask(subList);
                    currentTimes++;
                }
            }
        }
    }

    public void doMoveSucessRecordsToHistoryTask(List<KafkaRecordOds> list){
        List<KafkaRecordOdsHis> peedingList = new ArrayList<>();
        List<String> idList = new ArrayList<>();
        if (UtilValidate.isNotEmpty(list)) {
            for (KafkaRecordOds kafkaRecordOds:list) {
                idList.add(kafkaRecordOds.getId());
                KafkaRecordOdsHis kafkaRecordOdsHis = new KafkaRecordOdsHis();
                kafkaRecordOdsHis.setId(kafkaRecordOds.getId());
                kafkaRecordOdsHis.setTopic(kafkaRecordOds.getTopic());
                kafkaRecordOdsHis.setPartitions(kafkaRecordOds.getPartitions());
                kafkaRecordOdsHis.setOffsetNum(kafkaRecordOds.getOffsetNum());
                kafkaRecordOdsHis.setGroupId(kafkaRecordOds.getGroupId());
                kafkaRecordOdsHis.setClientId(kafkaRecordOds.getClientId());
                kafkaRecordOdsHis.setMsg(kafkaRecordOds.getMsg());
                kafkaRecordOdsHis.setStatus(kafkaRecordOds.getStatus());
                kafkaRecordOdsHis.setReceiveTime(kafkaRecordOds.getReceiveTime());
                peedingList.add(kafkaRecordOdsHis);
            }
            kafkaRecordOdsHisService.save(peedingList);
            kafkaRecordOdsService.deleteByIds(idList);
        }
    }
}
