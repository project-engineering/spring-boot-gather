package com.springboot.kafka.consumer.impl;

import com.springboot.futool.UtilValidate;
import com.springboot.kafka.annotation.KafkaConsumer;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.factory.IBusinessProcessEntrance;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@KafkaConsumer(value = KafkaConstant.TEST_TOPIC_2,desc="处理testTopic2主题")
public class DealTestTopic2 implements IBusinessProcessEntrance {
    @Override
    public Map<String, Object> execute(Map<String, Object> context) throws Exception {
        Map<String,Object> result = new HashMap<>();
        List<Map<String,Object>> dataList = (List<Map<String, Object>>) context.get("dataList");
        if (UtilValidate.isNotEmpty(dataList)) {
            //处理成功的数据id集合
            List<String> successIdList = new ArrayList<>();
            //处理失败的数据id集合
            List<String> failIdList = new ArrayList<>();
            for (Map<String,Object> dataMap:dataList) {
                String id = (String) dataMap.get("id");
                String msg = (String) dataMap.get("msg");
                if (UtilValidate.isNotEmpty(msg)) {
//                    //解析数据
//                    Map<String,Object> jsonMap = JSON.parseObject(msg, new TypeReference<HashMap<String, Object>>() {});
                    log.info(msg);
                }
                successIdList.add(id);
            }
            result.put("successIdList",successIdList);
            result.put("failIdList",failIdList);
        }
        return result;
    }
}
