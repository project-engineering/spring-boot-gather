package com.springboot.kafka.consumer;

import com.springboot.futool.UtilValidate;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import lombok.Data;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

/**
 * kafka工具类
 */
@Data
public class KafkaConsumerWorker {
    public Properties properties;
    public ExecutorService executorService;
    public KafkaRecordOdsService kafkaRecordOdsService;
    public Map<String, KafkaConsumerRunnable> KAFKA_CONSUMER_POOL = new ConcurrentHashMap<>();

    public void work(List<String> topicList, String consumerType, Date startDate, Date endDate) {
        if (UtilValidate.isNotEmpty(topicList)) {
            for (String topic:topicList) {
                KafkaConsumerRunnable kafkaConsumerRunnable = new KafkaConsumerRunnable();
                kafkaConsumerRunnable.setConsumerConfig(properties);
                kafkaConsumerRunnable.setTopic(topic);
                kafkaConsumerRunnable.setKafkaConsumerMessageHelper(new KafkaConsumerMessageHelper());
                kafkaConsumerRunnable.setExecutorService(executorService);
                kafkaConsumerRunnable.setKafkaRecordOdsService(kafkaRecordOdsService);
                if (UtilValidate.areEqual(consumerType, KafkaConstant.CONSUMER_TYEP_1)) {
                    //消费方式consumerType为1-回拨时间消费时
                    if (UtilValidate.isNotEmpty(startDate)) {
                        kafkaConsumerRunnable.setStartDate(startDate);
                        if (UtilValidate.isNotEmpty(endDate)) {
                            kafkaConsumerRunnable.setEndDate(endDate);
                        }
                    } else {
                        break;
                    }
                }
                kafkaConsumerRunnable.setConsumerType(consumerType);
                KAFKA_CONSUMER_POOL.put(topic,kafkaConsumerRunnable);
//                //按照topic创建并启动线程处理
                Thread t = new Thread(KAFKA_CONSUMER_POOL.get(topic),topic);
                t.start();
//                executorService.submit(KAFKA_CONSUMER_POOL.get(topic));
            }

        }
    }
}
