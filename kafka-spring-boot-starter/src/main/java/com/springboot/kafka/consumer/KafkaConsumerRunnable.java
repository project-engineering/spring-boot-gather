package com.springboot.kafka.consumer;

import com.springboot.futool.DateUtil;
import com.springboot.futool.UtilValidate;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 消费者线程
 *
 */
@Log4j2
@Data
public class KafkaConsumerRunnable implements Runnable{
    private ExecutorService executorService;
    private Properties consumerConfig;
    private String consumerType;
    private String topic;
    private KafkaConsumer consumer;
    private Date startDate;
    private Date endDate;
    private KafkaConsumerMessageHelper kafkaConsumerMessageHelper;
    private KafkaRecordOdsService kafkaRecordOdsService;

    @Override
    public void run() {
        Properties p = (Properties) consumerConfig.clone();
        if (UtilValidate.areEqual(consumerType, KafkaConstant.CONSUMER_TYEP_1)) {
            String suffix = "_" + DateUtil.convertObjToString(DateUtil.getCurrLocalDateTime(), DateUtil.DATE_FORMATTER_11);
            updateConsumerConfig(p, topic, suffix);
        } else {
            updateConsumerConfig(p, topic, "");
        }
        //创建一个consumer
        consumer = new KafkaConsumer(p);
        processCommand(consumer, p);
    }

    private void updateConsumerConfig(Properties config, String topic, String suffix) {
        config.put(ConsumerConfig.GROUP_ID_CONFIG, config.get(ConsumerConfig.GROUP_ID_CONFIG) + "_" + topic + suffix);
        config.put(ConsumerConfig.CLIENT_ID_CONFIG, config.get(ConsumerConfig.CLIENT_ID_CONFIG) + "_" + topic + suffix);
    }

    /**
     * 消费主方法
     * @param kafkaConsumer 消费者
     * @param properties 配置信息
     */
    private void processCommand(KafkaConsumer kafkaConsumer,Properties properties){
        try {
            String groupId = (String) properties.get(ConsumerConfig.GROUP_ID_CONFIG);
            String clientId = (String) properties.get(ConsumerConfig.CLIENT_ID_CONFIG);
            long maxPoolIntervalMs = Long.parseLong((String) properties.get(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG));
            //消费方式consumerType为0-默认消费方式时
            if (UtilValidate.areEqual(consumerType,KafkaConstant.CONSUMER_TYEP_0)) {
                executorService.execute(()->{
                    //开始订阅对应主题的kafka消息，并一直消费
                    kafkaConsumer.subscribe(Collections.singletonList(topic));
                    Set<TopicPartition> assignment = new HashSet<>();
                    ConsumerRecords<String,String> records = null;
                    while (assignment.isEmpty()) {
                        kafkaConsumer.poll(Duration.ofMillis(100));
                        //获取到消费者分区信息（有了分区分配信息才能开始消费）
                        assignment = kafkaConsumer.assignment();
                    }
                    //一直获取消费数据
                    while (true){
                        records = kafkaConsumer.poll(Duration.ofMillis(maxPoolIntervalMs));
                        if (!records.isEmpty()) {
                            doProcessRecords(kafkaConsumer, records, groupId, clientId);
                        }
                    }
                });
            }
            //消费方式consumerType为1-回拨时间消费时
            if (UtilValidate.areEqual(consumerType,KafkaConstant.CONSUMER_TYEP_1)) {
                List<PartitionInfo> topicPartitions = kafkaConsumer.partitionsFor(topic);
                if (UtilValidate.isNotEmpty(topicPartitions)) {
                    for (PartitionInfo par:topicPartitions) {
                        processPartition(topic, par, kafkaConsumer, groupId, clientId
                                , maxPoolIntervalMs, DateUtil.convertObjToString(startDate, DateUtil.DATE_FORMATTER_11)
                                , DateUtil.convertObjToString(endDate, DateUtil.DATE_FORMATTER_11));
                    }
                }
            }
            //消费方式consumerType为2-回拨偏移量消费时
            if (UtilValidate.areEqual(consumerType,KafkaConstant.CONSUMER_TYEP_2)) {
                List<PartitionInfo> topicPartitions = kafkaConsumer.partitionsFor(topic);
                if (UtilValidate.isNotEmpty(topicPartitions)) {
                    for (PartitionInfo par:topicPartitions) {
                        Long endOffset = null;
                        TopicPartition topicPartition = new TopicPartition(topic,par.partition());
                        Set<TopicPartition> assignment = new HashSet<>();
                        assignment.add(topicPartition);
                        //指定主题分区
                        kafkaConsumer.assign(Collections.singletonList(topicPartition));
                        //获取该主题分区的消费位置
                        int maxOffset = kafkaRecordOdsService.getMaxOffsetByTopic(topic,par.partition());
                        log.info("主题["+topic+"]的分区["+par.partition()+"]消费的位置为："+maxOffset);
                        if (maxOffset > 0) {
                            maxOffset = maxOffset+1;
                        }
                        Long startOffset = Long.valueOf(maxOffset);
                        if (UtilValidate.isEmpty(endOffset)) {
                            //分区末尾
                            Map<TopicPartition,Long> endOffsetsMap = kafkaConsumer.endOffsets(assignment);
                            if (UtilValidate.isNotEmpty(endOffsetsMap)) {
                                for (TopicPartition tp:assignment) {
                                    endOffset = endOffsetsMap.get(tp);
                                }
                            }
                            log.info("主题["+topic+"]的分区["+par.partition()+"]消费的最后的位置为："+endOffset);
                        }
                        kafkaConsumer.seek(topicPartition,startOffset);
                        processRecords(kafkaConsumer, endOffset, maxPoolIntervalMs, groupId, clientId);
                    }
                }
            }
        } catch (Exception e){
            log.error(e);
            if (kafkaConsumer != null) {
                kafkaConsumer.close();
            }
            //关闭线程池，会等待线程的执行完成
            if (executorService != null) {
                //关闭线程池
                executorService.shutdown();
               // 等待关闭完成, 等待五秒
                try {
                    if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                        log.error("Timed out waiting for consumer threads to shut down, exiting uncleanly!!");
                    }
                } catch (InterruptedException e1) {
                    log.error("Interrupted during shutdown, exiting uncleanly!!");
                }
            }
        }
    }

    /**
     * 消费方式为1-回拨时间消费时，处理每个分区
     * @param topic 主题
     * @param partitionInfo 分区信息
     * @param kafkaConsumer kafka消费者
     * @param groupId 消费者组id
     * @param clientId 消费者id
     * @param maxPoolIntervalMs 最大消费间隔
     * @param startDate 开始时间
     * @param endDate 结束时间
     */
    private void processPartition(String topic, PartitionInfo partitionInfo, KafkaConsumer<String, String> kafkaConsumer, String groupId, String clientId, long maxPoolIntervalMs, String startDate, String endDate) {
        log.info("开始处理主题["+topic+"]的分区["+partitionInfo.partition()+"]");
        Long startOffset = getStartOffset(kafkaConsumer, topic, partitionInfo,maxPoolIntervalMs, startDate);
        Long endOffset = getEndOffset(kafkaConsumer, topic, partitionInfo,maxPoolIntervalMs, endDate);

        if (startOffset != null) {
            TopicPartition topicPartition = new TopicPartition(topic, partitionInfo.partition());
            Set<TopicPartition> assignment = new HashSet<>();
            assignment.add(topicPartition);
            kafkaConsumer.assign(Collections.singletonList(topicPartition));
            kafkaConsumer.seek(topicPartition, startOffset);
            processRecords(kafkaConsumer, endOffset, maxPoolIntervalMs, groupId, clientId);
        }
        log.info("处理主题["+topic+"]的分区["+partitionInfo.partition()+"]结束");
    }

    /**
     * 获取该主题分区的开始消费位置
     * 如果startDate为空，则获取分区的最开始的位置
     * 如果startDate不为空，则获取startDate对应的位置
     * @param kafkaConsumer kafka消费者
     * @param topic 主题
     * @param partitionInfo 分区信息
     * @param maxPoolIntervalMs 最大消费间隔
     * @param startDate 开始时间
     * @return Long 开始消费位置
     */
    private Long getStartOffset(KafkaConsumer<String, String> kafkaConsumer, String topic, PartitionInfo partitionInfo, long maxPoolIntervalMs, String startDate) {
        log.info("开始获取主题["+topic+"]的分区["+partitionInfo.partition()+"]消费的开始位置");
        Long startOffset = getOffsetNum(kafkaConsumer, maxPoolIntervalMs, topic, partitionInfo.partition(), DateUtil.convertObjToUtilDate(startDate));
        if (startOffset == null) {
            Map<TopicPartition, Long> startOffsetMap = kafkaConsumer.beginningOffsets(Collections.singleton(new TopicPartition(topic, partitionInfo.partition())));
            startOffset = startOffsetMap.get(new TopicPartition(topic, partitionInfo.partition()));
        }
        log.info("主题["+topic+"]的分区["+partitionInfo.partition()+"]消费的开始位置为："+startOffset);
        return startOffset;
    }

    /**
     * 获取该主题分区的结束消费位置
     * 如果endDate为空，则获取分区的最后的位置
     * 如果endDate不为空，则获取endDate对应的位置
     * @param kafkaConsumer kafka消费者
     * @param topic 主题
     * @param partitionInfo 分区信息
     * @param maxPoolIntervalMs 最大消费间隔
     * @param endDate 结束时间
     * @return Long 结束消费位置
     */
    private Long getEndOffset(KafkaConsumer<String, String> kafkaConsumer, String topic, PartitionInfo partitionInfo, long maxPoolIntervalMs, String endDate) {
        log.info("开始获取主题["+topic+"]的分区["+partitionInfo.partition()+"]消费的结束位置");
        Long endOffset;
        if (UtilValidate.isEmpty(endDate)) {
            Map<TopicPartition, Long> endOffsetsMap = kafkaConsumer.endOffsets(Collections.singleton(new TopicPartition(topic, partitionInfo.partition())));
            endOffset = endOffsetsMap.get(new TopicPartition(topic, partitionInfo.partition()));
        } else {
            endOffset = getOffsetNum(kafkaConsumer, maxPoolIntervalMs, topic, partitionInfo.partition(), DateUtil.convertObjToUtilDate(endDate));
            if (endOffset == null) {
                Map<TopicPartition, Long> endOffsetsMap = kafkaConsumer.endOffsets(Collections.singleton(new TopicPartition(topic, partitionInfo.partition())));
                endOffset = endOffsetsMap.get(new TopicPartition(topic, partitionInfo.partition()));
            }
        }
        log.info("主题["+topic+"]的分区["+partitionInfo.partition()+"]消费的结束位置为："+endOffset);
        return endOffset;
    }

    /**
     * 获取该主题分区的指定时间对应的offset偏移量
     * @param kafkaConsumer kafka消费者
     * @param endOffset 结束消费位置
     * @param maxPoolIntervalMs 最大消费间隔
     * @param groupId 消费者组id
     * @param clientId 消费者id
     */
    private void processRecords(KafkaConsumer<String, String> kafkaConsumer, Long endOffset, long maxPoolIntervalMs, String groupId, String clientId) {
        long curOffset = 0L;
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(maxPoolIntervalMs));
            if (endOffset == 0) {
                break;
            } else if ((curOffset + 1) == endOffset) {
                break;
            }
            if (!records.isEmpty()) {
                curOffset = doProcessRecords(kafkaConsumer, records, groupId, clientId);
            }
        }
    }

    /**
     * 处理消费数据
     * @param kafkaConsumer kafka消费者
     * @param records 消费数据
     * @param groupId 消费者组id
     * @param clientId 消费者id
     * @return 当前消费到的offset偏移量
     */
    private long doProcessRecords(KafkaConsumer<String, String> kafkaConsumer, ConsumerRecords<String, String> records, String groupId, String clientId) {
        kafkaConsumerMessageHelper.setRecords(records);
        kafkaConsumerMessageHelper.setGroupId(groupId);
        kafkaConsumerMessageHelper.setClientId(clientId);
        kafkaConsumerMessageHelper.setConsumer(kafkaConsumer);
        kafkaConsumerMessageHelper.setKafkaRecordOdsService(kafkaRecordOdsService);
        return kafkaConsumerMessageHelper.save();
    }

    /**
     * 根据消费者、主题及该主题的分区计算该时间的offset偏移量
     * 如果输入的时间对应的偏移量不在该分区的偏移量范围内，返回null
     * @param consumer kafka消费者
     * @param maxPoolIntervalMs 最大消费间隔
     * @param topic 主题
     * @param partition 分区
     * @param date 时间
     * @return Long 偏移量
     */
    @SneakyThrows
    private static Long getOffsetNum(KafkaConsumer consumer, Long maxPoolIntervalMs, String topic, int partition, Date date){
        long timestamp = DateUtil.convertObjToTimestamp(date).getTime();
        Map<TopicPartition,Long> map = new HashMap<>();
        TopicPartition topicPartition = new TopicPartition(topic,partition);
        map.put(topicPartition,timestamp);
        Map<TopicPartition, OffsetAndTimestamp> offset;
        try {
            offset = consumer.offsetsForTimes(map, Duration.ofMillis(maxPoolIntervalMs));
        } catch (Exception e) {
            log.error("获取主题["+topic+"]的分区["+partition+"]指定时间["+date+"]的offset偏移量失败："+e.getMessage());
            return null;
        }
        if (UtilValidate.isEmpty(offset.get(topicPartition))) {
            return  null;
        }
        return offset.get(topicPartition).offset();
    }
}
