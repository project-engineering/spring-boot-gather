package com.springboot.kafka;

import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.generator.FileGenerator;
import cn.org.atool.generator.annotation.*;
import org.junit.jupiter.api.Test;

public class EntityGeneratorDemo {

    // =======================》》以下配置oracle
    private static final String ORACLE_URL = "jdbc:oracle:thin:@127.0.0.1:11521:orcl";
    private static final String ORACLE_USER_NAME ="root";
    private static final String ORACLE_PASS_WORD ="123456";
    private static final String ORACLE_BasePack = "com.springboot.kafka.persistence";

    // ========================》》以下配置mysql
    private static final String MYSQL_URL = "jdbc:mysql://localhost:3306/kafka?useUnicode=true&characterEncoding=utf8";
    private static final String MYSQL_USER_NAME ="root";
    private static final String MYSQL_PASS_WORD ="123456";
    private static final String MYSQL_BasePack = "com.springboot.kafka.persistence";


    @Test
    public void generate() throws Exception {
//        FileGenerator.build(OracleEmpty.class);
        // 引用配置类，build方法允许有多个配置类
        FileGenerator.build(MysqlEmpty.class);
    }

    /**
     * oracle生成
     * */
    @Tables(url = ORACLE_URL, username = ORACLE_USER_NAME, password = ORACLE_PASS_WORD, driver = "oracle.jdbc.OracleDriver",
            dbType = DbType.ORACLE,
            schema = "SD",
            srcDir = "src/main/java",
            basePack = ORACLE_BasePack,
            daoDir = "src/main/java",
            entitySuffix = "",
            /** 如果表定义记录创建 **/
            gmtCreated = "created_stamp",
            /** 如果表定义记录修改 **/
            gmtModified = "last_updated_stamp",
            /** 如果表定义逻辑删除字段 **/
            logicDeleted = "IS_DELETED",
            tables = {
                    @Table(value = {"kafka_record_ods","kafka_record_ods_his","kafka_record_ods_tmp"})
            })
    static class OracleEmpty {
    }

    /**
     * mysql生成
     * */
    @Tables(
            // 设置数据库连接信息
            // 设置数据库连接信息
            url = MYSQL_URL, username = MYSQL_USER_NAME, password = MYSQL_PASS_WORD,
            // 设置entity类生成src目录, 相对于 user.dir
            srcDir = "src/main/java",
            // 设置entity类的package值
            basePack = MYSQL_BasePack,
            // 设置dao接口和实现的src目录, 相对于 user.dir
            daoDir = "src/main/java",
            /** 如果表定义记录创建 **/
            gmtCreated = "created_stamp",
            /** 如果表定义记录修改 **/
            gmtModified = "last_updated_stamp",
            /** 如果表定义逻辑删除字段 **/
            logicDeleted = "is_deleted",
            // 无需后缀
            entitySuffix = "",
            // 设置哪些表要生成Entity文件
            tables = {@Table(value = {"kafka_record_ods","kafka_record_ods_his","kafka_record_ods_tmp"})}
    )
    static class MysqlEmpty  {
    }

}