package com.springboot.kafka.controller;

import com.springboot.futool.DateUtil;
import com.springboot.kafka.api.params.ConsumerMessageParams;
import com.springboot.kafka.consumer.KafkaConsumerWorker;
import com.springboot.kafka.consumer.constant.KafkaConstant;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

/**
 * kafka消费者控制器
 */
@Validated
@RestController
@RequestMapping("/kafka")
public class KafkaController {
    @Resource(name="consumerConf")
    public Properties properties;
    @Resource(name ="executorService")
    public ExecutorService executorService;
    @Resource
    private KafkaRecordOdsService kafkaRecordOdsService;

    /**
     * 消费kafka消息
     * @param params
     */
    @ResponseBody
    @PostMapping("/consumerMessage")
    public void consumerMessage(@Validated @RequestBody ConsumerMessageParams params) {
        KafkaConsumerWorker kafkaConsumerWorker = new KafkaConsumerWorker();
        kafkaConsumerWorker.setProperties(properties);
        kafkaConsumerWorker.setExecutorService(executorService);
        kafkaConsumerWorker.setKafkaRecordOdsService(kafkaRecordOdsService);
        kafkaConsumerWorker.work(params.getTopics(), KafkaConstant.CONSUMER_TYEP_1
                , DateUtil.convertObjToUtilDate(params.getStartDate())
                , DateUtil.convertObjToUtilDate(params.getEndDate()));
    }

}
