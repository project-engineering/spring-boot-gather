package com.springboot.kafka.persistence.service.impl;

import com.springboot.kafka.persistence.entity.KafkaRecordOdsHis;
import com.springboot.kafka.persistence.mapper.KafkaRecordOdsHisMapper;
import com.springboot.kafka.persistence.service.KafkaRecordOdsHisService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class KafkaRecordOdsHisServiceImpl implements KafkaRecordOdsHisService {

    @Resource
    private KafkaRecordOdsHisMapper mapper;

    @Override
    public void save(List<KafkaRecordOdsHis> list) {
        mapper.save(list);
    }

}
