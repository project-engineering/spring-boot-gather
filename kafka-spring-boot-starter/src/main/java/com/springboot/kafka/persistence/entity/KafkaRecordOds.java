package com.springboot.kafka.persistence.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.GmtCreate;
import cn.org.atool.fluent.mybatis.annotation.GmtModified;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * KafkaRecordOds: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@AllArgsConstructor
@NoArgsConstructor
@FluentMybatis(
    table = "kafka_record_ods",
    schema = "kafka",
    suffix = "",
    desc = "kafka消费原始记录表"
)
public class KafkaRecordOds extends RichEntity {
  private static final long serialVersionUID = 1L;

  @TableId(
      value = "id",
      auto = false,
      desc = "主键"
  )
  private String id;

  @TableField(
      value = "client_id",
      desc = "消费者id"
  )
  private String clientId;

  @TableField(
      value = "group_id",
      desc = "消费者组"
  )
  private String groupId;

  @TableField(
      value = "msg",
      desc = "kafka消息记录"
  )
  private String msg;

  @TableField(
      value = "offset_num",
      desc = "偏移量"
  )
  private Integer offsetNum;

  @TableField(
      value = "partitions",
      desc = "分区"
  )
  private Integer partitions;

  @TableField(
      value = "receive_time",
      desc = "接收时间"
  )
  private Date receiveTime;

  @TableField(
      value = "status",
      desc = "记录状态"
  )
  private String status;

  @TableField(
      value = "topic",
      desc = "订阅主题"
  )
  private String topic;

  @TableField(
      value = "created_stamp",
      insert = "now()",
      desc = "创建时间"
  )
  @GmtCreate
  private Date createdStamp;

  @TableField(
      value = "last_updated_stamp",
      insert = "now()",
      update = "now()",
      desc = "最后更新时间"
  )
  @GmtModified
  private Date lastUpdatedStamp;

  @Override
  public final Class entityClass() {
    return KafkaRecordOds.class;
  }
}
