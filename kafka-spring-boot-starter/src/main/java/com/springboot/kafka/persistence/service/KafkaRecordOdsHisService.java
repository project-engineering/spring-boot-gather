package com.springboot.kafka.persistence.service;


import com.springboot.kafka.persistence.entity.KafkaRecordOdsHis;
import java.util.List;

public interface KafkaRecordOdsHisService {
    void save(List<KafkaRecordOdsHis> list);
}
