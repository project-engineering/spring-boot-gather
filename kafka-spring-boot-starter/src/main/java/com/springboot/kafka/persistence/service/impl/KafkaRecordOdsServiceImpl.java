package com.springboot.kafka.persistence.service.impl;

import cn.org.atool.fluent.mybatis.If;
import com.springboot.futool.UtilValidate;
import com.springboot.kafka.persistence.entity.KafkaRecordOds;
import com.springboot.kafka.persistence.mapper.KafkaRecordOdsMapper;
import com.springboot.kafka.persistence.service.KafkaRecordOdsService;
import com.springboot.kafka.persistence.wrapper.KafkaRecordOdsHisQuery;
import com.springboot.kafka.persistence.wrapper.KafkaRecordOdsQuery;
import com.springboot.kafka.persistence.wrapper.KafkaRecordOdsUpdate;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class KafkaRecordOdsServiceImpl implements KafkaRecordOdsService {

    @Resource
    private KafkaRecordOdsMapper mapper;

    @Override
    public int getMaxOffsetByTopic(String topic, Integer partition) {
        int offset = 0;
        List<Map<String,Object>> list = mapper.listMaps(new KafkaRecordOdsQuery()
                .select
                .max
                .offsetNum("offsetNum")
                .end()
                .where.topic().eq(topic)
                .and.partitions().eq(partition).end());
        if (UtilValidate.isNotEmpty(list)) {
            for (Map<String,Object> map:list) {
                if (UtilValidate.isNotEmpty(map)) {
                    offset = (Integer) map.get("offsetNum");
                }
            }
        }
        //查询历史表中的信息
        List<Map<String,Object>> hisList = mapper.listMaps(new KafkaRecordOdsHisQuery()
                .select
                .max
                .offsetNum("offsetNum")
                .end()
                .where.topic().eq(topic)
                .and.partitions().eq(partition).end());
        if (UtilValidate.isNotEmpty(hisList)) {
            int hisOffset = 0;
            for (Map<String,Object> map:hisList) {
                if (UtilValidate.isNotEmpty(map)) {
                    hisOffset = (Integer) map.get("offsetNum");
                }
            }
            if ( hisOffset > offset) {
                offset = hisOffset;
            }
        }
        return offset;
    }

    /**
     * 根据状态查询KafkaRecordOds列表信息
     * @param status
     * @return
     */
    @Override
    public List<KafkaRecordOds> findKafkaRecordOdsByStatusList(String status) {
        Map<String,Object> queryMap = new HashMap<>();
        queryMap.put("status",status);
        return mapper.listByMap(true,queryMap);
    }

    @Override
    public void updateRecordStatus(List<String> list, String status) {
        mapper.updateBy(new KafkaRecordOdsUpdate()
                .set.status().is(status, If::notNull).end()
                .where.id().in(list).end()
        );
    }

    @Override
    public void save(List<KafkaRecordOds> list) {
        mapper.save(list);
    }

    @Override
    public void deleteByIds(List<String> idList) {
        mapper.deleteByIds(idList);
    }

}
