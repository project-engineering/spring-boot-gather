package com.springboot.kafka.persistence.service;

import com.springboot.kafka.persistence.entity.KafkaRecordOds;

import java.util.List;

/**
 * <p>
 * kafka消费原始记录表 服务类
 * </p>
 *
 * @author liuc
 * @since 2023-08-05 06:28:02
 */
public interface KafkaRecordOdsService {

    int getMaxOffsetByTopic(String topic,Integer partition);

    /**
     * 根据状态查询KafkaRecordOds列表信息
     * @return
     */
    List<KafkaRecordOds> findKafkaRecordOdsByStatusList(String status);

    /**
     * 批量更新状态
     * @param list
     * @param status
     */
    void updateRecordStatus(List<String> list,String status);

    void save(List<KafkaRecordOds> list);

    void deleteByIds(List<String> idList);
}
