package com.springboot.kafka.persistence.dao.impl;

import com.springboot.kafka.persistence.dao.base.KafkaRecordOdsBaseDao;
import com.springboot.kafka.persistence.dao.intf.KafkaRecordOdsDao;
import org.springframework.stereotype.Repository;

/**
 * KafkaRecordOdsDaoImpl: 数据操作接口实现
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
@Repository
public class KafkaRecordOdsDaoImpl extends KafkaRecordOdsBaseDao implements KafkaRecordOdsDao {

}
