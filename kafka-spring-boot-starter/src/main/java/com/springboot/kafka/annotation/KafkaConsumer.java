package com.springboot.kafka.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * kafka主题消费者注解
 * @author liuc
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface KafkaConsumer {
    /**
     * 消费的主题
     * @return
     */
    String value();

    /**
     * 描述
     * @return
     */
    String desc() default "";
}
