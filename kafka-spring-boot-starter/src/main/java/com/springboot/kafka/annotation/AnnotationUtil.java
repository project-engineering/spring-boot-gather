package com.springboot.kafka.annotation;

import cn.hutool.json.JSONUtil;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.annotation.Annotation;
import java.util.*;

/**
 * 注解工具类
 * @author liuc
 */
@SuppressWarnings("unchecked")
public class AnnotationUtil {
	private static final Logger logger = LoggerFactory.getLogger(AnnotationUtil.class);
	
	/**
	 * 本地测试
	 */
	public static void main(String[] args) {
		//获取指定包下带有指定注解的java类
		Reflections reflections = new Reflections("com.springboot.kafka");
		//指定自定义注解
		List<Object> list = new ArrayList<>();
		list.add(KafkaConsumer.class);
		//含有指定自定义注解的类
		Set<Class<?>> classesList = new HashSet<>();
		for (Object obj: list) {
			classesList.addAll(reflections.getTypesAnnotatedWith((Class<? extends Annotation>) obj));
		}
		//输出所有使用了特定注解的类的注解值
		try {
			logger.info(JSONUtil.toJsonStr(AnnotationUtil.mapAnnotationAndClass(classesList,list)));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 输出所有使用特定注解的类的注解值
	 */
	public static Map<String,Object> mapAnnotationAndClass(Set<Class<?>> clsList,List<Object> list) {
		Map<String,Object> result = new HashMap<>(8);
		try {
			//注解:类名
			Map<String,Class<?>> serviceMap = new HashMap<>(8);
			//注解:节点名称
			Map<String,String> serviceNameMap = new HashMap<>(8);
			List<String> topicList = new ArrayList<>();
			if (clsList != null && !clsList.isEmpty()) {
				for (Class<?> cls : clsList) {
					if (list != null && !list.isEmpty()) {
						for (Object bean : list) {
							Annotation anno = cls.getAnnotation((Class<? extends Annotation>) bean);
							// @KafkaConsumer注解
							if (anno instanceof KafkaConsumer) {
								KafkaConsumer kafkaConsumerAnnotation = (KafkaConsumer) anno;
								if (serviceMap.get(kafkaConsumerAnnotation.value()) != null) {
									logger.error("注解值[{}]重复!", kafkaConsumerAnnotation.value());
									throw new RuntimeException("注解值[" + kafkaConsumerAnnotation.value() + "]重复!");
								}
								//类名
								serviceMap.put(kafkaConsumerAnnotation.value(), cls);
								//节点名称
								serviceNameMap.put(kafkaConsumerAnnotation.value(), kafkaConsumerAnnotation.desc());
								topicList.add(kafkaConsumerAnnotation.value());
							}
						}
					}
				}
			}
			result.put("serviceMap", serviceMap);
			result.put("serviceNameMap", serviceNameMap);
			result.put("topicList",topicList);
		} catch (RuntimeException e){
			logger.error("获取含有自定义注解的类失败,原因为:{}",e.getMessage());
			throw new RuntimeException("获取含有自定义注解的类失败,原因为:"+e.getMessage());
		}
		logger.info(JSONUtil.toJsonStr(result));
		return result;
	}

}
