package com.springboot.kafka.config;

import cn.org.atool.fluent.mybatis.spring.MapperFactory;
import com.alibaba.druid.pool.DruidDataSource;
import com.springboot.kafka.interceptor.SqlPrintInterceptor;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import java.io.IOException;

@Log4j2
@Configuration
public class MybatisConfig {
    @Bean
    public MapperFactory mapperFactory() {
        return new MapperFactory();
    }

    @Resource
    DruidDataSource dataSource;
    private static final Long MAX_LIMIT = 1000L;

    /**
     * 初始化SqlSessionFactory
     * @return org.apache.ibatis.session.SqlSessionFactory
     */
    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() {
        log.info("初始化SqlSessionFactory");
        try {
            SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean ();
            sqlSessionFactory.setDataSource(dataSource);
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            // 路径正则表达式方式加载
            org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
            configuration.setLazyLoadingEnabled(true);
            configuration.setMapUnderscoreToCamelCase(true);
            configuration.setJdbcTypeForNull(JdbcType.NULL);
            configuration.setMapUnderscoreToCamelCase(true);
            configuration.setCacheEnabled(false);
            //设置主键自增
            configuration.setUseGeneratedKeys(true);
            // 配置打印sql语句
//            configuration.setLogImpl(StdOutImpl.class);
            sqlSessionFactory.setConfiguration(configuration);
            //添加自定义日志打印功能
            sqlSessionFactory.setPlugins(new Interceptor[]{new SqlPrintInterceptor()});
            sqlSessionFactory.setConfiguration(configuration);
            log.info("初始化SqlSessionFactory完成");
            return sqlSessionFactory.getObject();
        } catch (IOException e) {
            log.error("mybatis resolver mapper*xml is error", e);
            return null;
        } catch (Exception e) {
            log.error("mybatis sqlSessionFactoryBean create error", e);
            return null;
        }
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }


    /**
     *  配置事务管理
     * @return org.springframework.jdbc.datasource.DataSourceTransactionManager
     */
    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager() {
        log.info("初始化DataSourceTransactionManager");
        return new DataSourceTransactionManager(dataSource);
    }
}
