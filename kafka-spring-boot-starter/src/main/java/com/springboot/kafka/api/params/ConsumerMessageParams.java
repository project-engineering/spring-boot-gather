package com.springboot.kafka.api.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import java.util.List;

@Data
public class ConsumerMessageParams {
    /**
     * 主题列表
     */
    @NotEmpty(message = "主题列表不能为空")
    private List<String> topics;
    /**
     * 消费起始时间
     */
    @NotEmpty(message = "消费起始时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String startDate;
    /**
     * 消费截止时间
     */
    @NotEmpty(message = "消费截止时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String endDate;
}
