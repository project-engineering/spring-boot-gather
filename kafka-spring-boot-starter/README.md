SpringBoot整合Kafka

# springboot 整合fluent mybatis的过程



[Fluent-Mybatis](https://gitee.com/fluent-mybatis/fluent-mybatis#https://gitee.com/fluent-mybatis/fluent-mybatis/wikis/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%28%E7%AE%80%E5%8D%95%29?sort_id=4098246)














## 一、导入pom依赖
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.1.1</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
		
	<properties>
		<mybatis.version>3.0.0</mybatis.version>
		<fluent-mybatis.version>1.9.9</fluent-mybatis.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-autoconfigure</artifactId>
		</dependency>
		<dependency>
			<groupId>com.mysql</groupId>
			<artifactId>mysql-connector-j</artifactId>
		</dependency>
		<!--mybatis和springboot的整合包-->
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>${mybatis.version}</version>
		</dependency>
		<!-- 引入fluent-mybatis 运行依赖包, scope为compile -->
		<dependency>
			<groupId>com.github.atool</groupId>
			<artifactId>fluent-mybatis</artifactId>
			<version>${fluent-mybatis.version}</version>
		</dependency>
		<!-- 引入fluent-mybatis-processor, scope设置为provider 编译需要，运行时不需要 -->
		<dependency>
			<groupId>com.github.atool</groupId>
			<artifactId>fluent-mybatis-processor</artifactId>
			<scope>provided</scope>
			<version>${fluent-mybatis.version}</version>
		</dependency>
		<!-- 数据源 -->
		<dependency>
			<groupId>com.zaxxer</groupId>
			<artifactId>HikariCP</artifactId>
		</dependency>
	</dependencies>
	<build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.4.2</version>
                <configuration>
                    <skipTests>true</skipTests>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>16</source>
                    <target>16</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

## 二、配置数据库连接
application.yml
```
##端口
server:
  port: 8002
#spring的配置
spring:
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/kafka?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2b8&allowMultiQueries=true
    username: root
    password: 123456
```

## 三、创建数据库表
```sql
CREATE TABLE `kafka_record_ods` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `topic` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订阅主题',
  `partitions` int DEFAULT NULL COMMENT '分区',
  `offset_num` int DEFAULT NULL COMMENT '偏移量',
  `group_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者组',
  `client_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消费者id',
  `msg` longtext COLLATE utf8mb4_general_ci COMMENT 'kafka消息记录',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '记录状态',
  `receive_time` datetime DEFAULT NULL COMMENT '接收时间',
  `created_stamp` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updated_stamp` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=785 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='kafka消费原始记录表';


#测试数据
INSERT INTO kafka.kafka_record_ods
(id, topic, partitions, offset_num, group_id, client_id, msg, status, receive_time, created_stamp, created_tx_stamp, last_updated_stamp, last_updated_tx_stamp)
VALUES(782, 'testTopic2', 1, 7, 'GROUP_testTopic2', 'CLIENT_testTopic2', 'hello tiger', '1', '2023-08-11 16:28:19', '2023-08-11 16:28:19', '2023-08-11 16:28:19');

```

## 四、添加配置类
```java
package com.springboot.kafka.config;

import cn.org.atool.fluent.mybatis.spring.MapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisConfig {
    @Bean
    public MapperFactory mapperFactory() {
        return new MapperFactory();
    }
}
```

## 五、FluentMybatis的Entity代码生成
```java
package com.springboot.kafka;
import cn.org.atool.generator.FileGenerator;
import cn.org.atool.generator.annotation.*;
import org.junit.jupiter.api.Test;

public class EntityGeneratorDemo {
    // 数据源 url
    static final String url = "jdbc:mysql://localhost:3306/kafka?useUnicode=true&characterEncoding=utf8";
    // 数据库用户名
    static final String username = "root";
    // 数据库密码
    static final String password = "123456";


    @Test
    public void generate() throws Exception {
        // 引用配置类，build方法允许有多个配置类
        FileGenerator.build(Empty.class);
    }

    @Tables(
            // 设置数据库连接信息
            url = url, username = username, password = password,
            // 设置entity类生成src目录, 相对于 user.dir
            srcDir = "src/main/java",
            // 设置entity类的package值
            basePack = "com.springboot.kafka.persistence",
            // 设置dao接口和实现的src目录, 相对于 user.dir
            daoDir = "src/main/java",
            /** 如果表定义记录创建，记录修改，逻辑删除字段 **/
            gmtCreated = "created_stamp",
            gmtModified = "last_updated_stamp",
            // 设置哪些表要生成Entity文件
            tables = {@Table(value = {"kafka_record_ods"})}
    )
    static class Empty { //类名随便取, 只是配置定义的一个载体
    }
}
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/56497d352c854b4bbe3f28564693660f.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/cca2bde82a7d493aa884c9cd61913ed1.png)

其中, XyzDao 和 XyzDaoImpl是空类,再次运行代码不会覆盖生成
Entity是Fluent Mybatis运行的正主, 不要手工改动, 重新运行代码生成会进行覆盖, 保持数据库变更的一致。

## 六、生成代码后编译
在Entity代码生成后，执行IDE编译，或者maven(gradle)编译，会在target目录下生成下面代码列表

![在这里插入图片描述](https://img-blog.csdnimg.cn/ca36b8f168dd4164ba6ef3ed8f9c30a8.webp)

大家重点关注 XyzMapper, XyzQuery, XyzUpdate 这3个类即可
Mapper类是Mybatis的接口类，需要加入到mybatis的扫描路径中

@MapperScan({"com.springboot.kafka.persistence.mapper"})

XyzQuery和XyzUpdate是应用中用来拼装查询语句和更新语句的工具类。

Idea的AnnotationProcessor开关

![在这里插入图片描述](https://img-blog.csdnimg.cn/9fa93303161b4a618fd48f0d77c9e7e2.webp)

正常情况下， IDE会自动把target/generated-sources/annotations 添加到源码路径下，如果碰到找不到类的情形

点击maven视图的刷新按钮，会自动添加工程源码路径

![在这里插入图片描述](https://img-blog.csdnimg.cn/a781f593c96749b39c05d1ab7ae1af93.webp)

gradle的同学同理，刷新gradle视图

如果再不行，手工大法

![在这里插入图片描述](https://img-blog.csdnimg.cn/05f1db1d29e74bad8bb2f9f16659d73c.webp)

maven和gradle

maven和gradle打包会自动把target/generated-sources/annotations下文件编译到classes下。
源码也会自动打包到 sources.jar里面

## 七、测试

```java
package com.springboot.kafka.persistence;

import com.springboot.kafka.persistence.mapper.KafkaRecordOdsMapper;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Resource
    private KafkaRecordOdsMapper mapper;

    @GetMapping("/data")
    public Object getData() {
        KafkaRecordOdsEntity person = mapper.findById(782);
        return person;
    }
}
```

浏览器访问http://localhost:8002/data

返回信息
```
{
	"id": 782,
	"clientId": "CLIENT_testTopic2",
	"groupId": "GROUP_testTopic2",
	"msg": "hello tiger",
	"offsetNum": 7,
	"partitions": 1,
	"receiveTime": "2023-08-11T08:28:19.000+00:00",
	"status": "1",
	"topic": "testTopic2",
	"createdStamp": "2023-08-11T08:28:19.000+00:00",
	"lastUpdatedStamp": null
}
```

http://localhost:8002/kafka/consumerMessage
```markdown
{
    "topics": [
        "testTopic"
    ],
    "startDate": "2024-04-18",
    "endDate": "2024-04-20"
}
```

