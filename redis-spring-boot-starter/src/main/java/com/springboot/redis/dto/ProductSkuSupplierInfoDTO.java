package com.springboot.redis.dto;

import lombok.Data;

/**
 * @author liuc
 * @date 2024-10-12 17:10
 */
@Data
public class ProductSkuSupplierInfoDTO {
    private String sku;
}
