package com.springboot.redis.annotation;

import com.springboot.redis.aspect.DistributedLockAspect;
import org.springframework.context.annotation.Import;
import java.lang.annotation.*;

/**
 * 定义分布式锁注解版启动元注解
 * @author liuc
 * @date 2024-10-12 17:06
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({DistributedLockAspect.class})
public @interface EnableDistributedLock {

}
