package com.springboot.redis.lock;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Objects;

/**
 *  RedissonLock 包装的锁对象 实现AutoCloseable接口，在java7的try(with resource)语法，不用显示调用close方法
 * @author liuc
 * @date 2024-10-12 16:58
 */
@AllArgsConstructor
public class ILock implements AutoCloseable {

    /**
     * 持有的锁对象
     */
    @Getter
    private Object lock;

    /**
     * 分布式锁接口
     */
    @Getter
    private IDistributedLock distributedLock;

    @Override
    public void close() throws Exception {
        if(Objects.nonNull(lock)){
            distributedLock.unLock(lock);
        }
    }
}