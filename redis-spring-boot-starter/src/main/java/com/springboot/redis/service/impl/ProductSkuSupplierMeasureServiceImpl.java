package com.springboot.redis.service.impl;

import com.springboot.redis.annotation.DistributedLock;
import com.springboot.redis.lock.IDistributedLock;
import com.springboot.redis.dto.ProductSkuSupplierInfoDTO;
import com.springboot.redis.lock.ILock;
import com.springboot.redis.service.IProductSkuSupplierMeasureService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


/**
 * @author liuc
 * @date 2024-10-12 17:12
 */
@Log4j2
@Service
public class ProductSkuSupplierMeasureServiceImpl implements IProductSkuSupplierMeasureService {

    @Resource
    private IDistributedLock distributedLock;

    @Transactional(rollbackFor = Exception.class)
    @DistributedLock(key = "#dto.sku", lockTime = 10L, keyPrefix = "sku-")
    @Override
    public Boolean editSupplierInfo(ProductSkuSupplierInfoDTO dto) {

        String sku = dto.getSku();
        // try-with-resources 语法糖自动释放锁
        try(ILock lock = distributedLock.lock(dto.getSku(),10L, TimeUnit.SECONDS, false)) {
            if(Objects.isNull(lock)){
                throw new Exception ("Duplicate request for method still in process");
            }

            // 业务代码
        }catch (Exception e) {
            log.error("修改异常", e);
            throw new RuntimeException ("修改异常");
        }
        return Boolean.TRUE;
    }
}
