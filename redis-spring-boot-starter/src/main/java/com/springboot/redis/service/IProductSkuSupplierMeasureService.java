package com.springboot.redis.service;

import com.springboot.redis.dto.ProductSkuSupplierInfoDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author liuc
 * @date 2024-10-12 17:10
 */
public interface IProductSkuSupplierMeasureService {

    @Transactional(rollbackFor = Exception.class)
    Boolean editSupplierInfo(ProductSkuSupplierInfoDTO dto);
}
