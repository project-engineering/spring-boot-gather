package com.springboot.redis.config;

import jakarta.annotation.Resource;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuc
 * @date 2024-10-12 16:57
 */
@Configuration
public class RedissonConfig {

    @Resource
    private RedisConfigProperties redisConfigProperties;
    @Value("${spring.data.redis.host}")
    private String host;

    @Value("${spring.data.redis.port}")
    private int port;

    @Value("${spring.data.redis.password}")
    private String password;

    @Value("${spring.data.redis.timeout}")
    private int timeout;

    //添加redisson的bean
//    @Bean
//    public Redisson redisson() {
//        //redisson版本是3.5，集群的ip前面要加上“redis://”，不然会报错，3.2版本可不加
//        String[] nodes = redisConfigProperties.getCluster().getNodes().split(",");
//        List<String> clusterNodes = new ArrayList<>(nodes.length);
//        Arrays.stream(nodes).forEach((node) -> clusterNodes.add(
//                node.startsWith("redis://") ? node : "redis://" + node)
//        );
//        Config config = new Config();
//        ClusterServersConfig serverConfig = config.useClusterServers()
//                .addNodeAddress(clusterNodes.toArray(new String[0]))
//                .setTimeout(redisConfigProperties.getTimeout());
//        if (StringUtils.hasText(redisConfigProperties.getPassword())) {
//            serverConfig.setPassword(redisConfigProperties.getPassword());
//        }
//        return (Redisson) Redisson.create(config);
//    }

    @Bean
    public RedissonClient redisson() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://" + host + ":" + port)
                .setConnectTimeout(timeout)
                .setDatabase(0); // 设置数据库索引，默认为0

        if (password != null && !password.trim().isEmpty()) {
            config.useSingleServer().setPassword(password);
        }

        return Redisson.create(config);
    }
}