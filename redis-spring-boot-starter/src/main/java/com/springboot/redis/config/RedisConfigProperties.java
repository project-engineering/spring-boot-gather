package com.springboot.redis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author liuc
 * @date 2024-10-12 16:56
 */
@Component
@ConfigurationProperties(prefix = "spring.data.redis")
@Data
public class RedisConfigProperties {

    private Integer timeout;

    private String password;

    private cluster cluster;

    @Data
    public static class cluster {
        private String nodes;
    }
}
