package com.springboot.redis;

import com.springboot.redis.annotation.EnableDistributedLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDistributedLock
@SpringBootApplication
public class RedisSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisSpringBootStarterApplication.class, args);
    }

}
