import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vite.dev/config/

export default defineConfig({
  plugins: [
      vue(),
  AutoImport({
    resolvers: [ElementPlusResolver()],
  }),
  Components({
    resolvers: [ElementPlusResolver()],
  })],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src') // 设置别名
    }
  },
  server: {
    open: true, // vue项目启动时自动打开浏览器
    host: '0.0.0.0', // 允许其他ip访问
    port: 5173, // 端口号
    https: false, // 是否开启https
    cors: true, // 允许跨域
    proxy: {
      '/bpm': {
        target: 'http://localhost:8080/',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/bpm': '' //不改变
        }
      }
    }
  }
})
