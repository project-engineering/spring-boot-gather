import http from "@/http";
import {AddRoleMenuModel} from "./RoleMenuModel";

//给角色分配菜单
export function addRoleMenu(data: AddRoleMenuModel) {
  return http.post("/api/roleMenu/saveRoleMenu", data);
}