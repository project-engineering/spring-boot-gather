import http from '@/http';
import { RegisterReq, LoginReq } from "@/types/user";

/**
 * 注册接口
 * @param param
 */
export const useRegisterApi = (param:RegisterReq) => {
    return http.post('/api/user/register', param)
}

/**
 * 登录接口
 * @param param
 */
export const useLoginApi = (param:LoginReq) => {
    return http.post('/api/login/login', param)
}
