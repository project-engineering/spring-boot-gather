import http from '@/http';
import { MeneType,AssignTreeParam,GetMenuListParam } from './MenuModel';

//获取上级菜单
export function getParentMenuApi() {
  return http.get("/api/menu/getParentMenuList");
}

// 新增菜单
export function addMenuApi(param: MeneType) {
  return http.post("/api/menu/addMenu", param);
}

// 列表
export function listApi() {
  return http.get("/api/menu/list");
}

// 修改菜单
export function updateMenuApi(param: MeneType) {
  return http.put("/api/menu/updateMenu", param);
}

// 删除菜单
export function deleteMenuApi(id: number) {
  return http.delete("/api/menu/deleteMenu/" + id);
}

// 查询菜单树
export function getAssignTreeApi(param:AssignTreeParam) {
  return http.post("/api/menu/getAssignTree",param);
}

// 获取菜单数据
export const getMenuListApi = (param:GetMenuListParam) => {
  return http.post('/api/menu/getMenuList',param);
}

// // 启用菜单
// export function enableMenuApi(id: number) {
//   return http.put("/api/menu/enableMenu/" + id);
// }
// // 禁用菜单
// export function disableMenuApi(id: number) {
//   return http.put("/api/menu/disableMenu/" + id);
// }