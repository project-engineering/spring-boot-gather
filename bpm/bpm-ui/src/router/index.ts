import {Router, createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
//导入组件
import Layout from "@/views/layout/Layout.vue";

//动态路由
export const constantRoutes:Array<RouteRecordRaw> = [
    {
        path: '/login',
        component: () => import('@/views/login/Login.vue'),
        name: "login"
    },{
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('@/views/error/404.vue'),
    },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'Dashboard',
                component: () => import('@/views/layout/dashboard/Dashboard.vue'),
                meta: {
                    title: '首页',
                    icon: 'HomeFilled'
                }
            }
        ]
    }
]

const router:Router = createRouter({
  history: createWebHistory(),
  routes: constantRoutes
});

export default router;