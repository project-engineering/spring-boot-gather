import router from './router'
import {userStore} from "./store/user";
import {menuStore} from "./store/menu";

// 定义白名单，不需要权限校验的路由
const whiteList = ['/login', '/404', '/401']


router.beforeEach(async (to, from, next) => {
    const ustore = userStore();
    const mstore = menuStore();
    // 获取token
    const token = ustore.getToken;
    // 如果token存在
    if (token) {
        // 如果要跳转的页面是登录页面，则直接跳转到主页
        if (to.path === '/login' || to.path === '/') {
            next({path: '/'})
        } else {
            // 如果有token，则判断是否有权限
            // 检查 getPermission 是否返回了一个数组
            const permissions = ustore.permission;
            const hasPermission = permissions.length > 0;
            if (hasPermission) {
                // 如果有权限，则直接进入
                next()
            } else {
                try {
                    // 如果没有权限，则获取用户信息
                    await ustore.getUserInfo();
                    // 获取用户菜单
                    await mstore.getMenuList(router,ustore.getUserId);
                    // 等待路由完全挂载
                    next({...to, replace: true})
                } catch (error) {
                    // 如果获取用户信息失败，则清除token并跳转到登录页面
                    sessionStorage.clear();
                    // 跳转到登录页面
                    next({path: '/login'})
                }
            }
        }
    } else {
        // 白名单直接进入
        if (whiteList.includes(to.path)) {
            next()
        } else {
            // 不在白名单中，跳转到登录
            next({path:'/login'})
        }
    }
});