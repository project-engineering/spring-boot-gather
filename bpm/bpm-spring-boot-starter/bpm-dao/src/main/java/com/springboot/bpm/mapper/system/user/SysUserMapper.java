package com.springboot.bpm.mapper.system.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.bpm.entity.system.user.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
