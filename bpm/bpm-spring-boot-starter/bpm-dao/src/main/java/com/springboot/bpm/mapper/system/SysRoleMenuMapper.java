package com.springboot.bpm.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.bpm.entity.system.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
