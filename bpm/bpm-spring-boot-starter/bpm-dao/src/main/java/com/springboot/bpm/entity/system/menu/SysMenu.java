package com.springboot.bpm.entity.system.menu;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_menu")
@Tag(name = "SysMenu对象", description = "菜单表")
public class SysMenu {

    @Schema(description = "菜单主键ID")
    @TableId(value = "menu_id")
    private String menuId;

    @Schema(description = "父菜单ID")
    @TableField("parent_id")
    private String parentId;

    @Schema(description = "菜单名称")
    @TableField("title")
    private String title;

    @Schema(description = "权限字段")
    @TableField("`code`")
    private String code;

    @Schema(description = "路由name")
    @TableField("`name`")
    private String name;

    @Schema(description = "路由地址")
    @TableField("`path`")
    private String path;

    @Schema(description = "组件路径")
    @TableField("url")
    private String url;

    @Schema(description = "菜单类型(0-目录,1-菜单,2-按钮)")
    @TableField("`type`")
    private String type;

    @Schema(description = "菜单图标")
    @TableField("icon")
    private String icon;

    @Schema(description = "上级菜单名称")
    @TableField("parent_name")
    private String parentName;

    @Schema(description = "显示顺序")
    @TableField("order_num")
    private Integer orderNum;

    @Schema(description = "创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    // 以下字段不存入数据库，仅用于业务逻辑
    @Schema(description = "子菜单")
    @TableField(exist = false)
    private List<SysMenu> children;

    @TableField(exist = false)
    private String value;

    @TableField(exist = false)
    private String label;
}
