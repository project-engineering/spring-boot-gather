# BPM Spring Boot Starter
SprinngBoot3.X版本的BPM流程引擎启动器。

## 环境准备
### 后端环境
| 依赖           | 版本      |
|--------------|---------|
| JDK          | 22      |
| Spring Boot  | 3.3.5   |
| Spring Security | 6.3.4   |
| Activiti     | 8.6.0   |
| MyBatis Plus | 3.5.9   |
| MySQL        | 8.3.0   |
| Lombok       | 1.18.34 |
| Mybitis      | 3.0.3   |
| Knife4j      | 4.5.0   |

### 前端环境
| 依赖           | 版本      | 官方文档地址                           |
|--------------|---------|----------------------------------|
| Vue          | 3.5.12  | https://vuejs.org/guide/introduction.html |
| Vite         | 2.7.10  | https://vitejs.cn/vite3-cn/guide/ |
| Element-PLUS | 2.8.6   | http://element-plus.org/zh-CN/ |
| Axios        | 1.7.7   | https://www.axios-http.cn/ |
| Pinia        | 2.0.12  | https://pinia.vuejs.org/ |
| pinia-plugin-persist | 0.1.1 | https://www.npmjs.com/package/pinia-plugin-persist |
| Sass         | 1.49.9  | https://sass-lang.com/ |
| Vue-router   | 4.0.12  | https://router.vuejs.org/ |
| Typescript   | 4.5.5   | https://www.typescriptlang.org/ |
| Vuex         | 4.0.2   | https://vuex.vuejs.org/ |


## 如何安装
1. 在pom.xml文件中添加以下依赖：
```xml
<dependency>
    <groupId>com.springboot.bpm</groupId>
    <artifactId>bpm-spring-boot-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- 这段代码是为了解决Activiti依赖包下载失败的问题 -->
<repositories>
    <repository>
        <id>maven2-public</id>
        <url>https://repo.maven.apache.org/maven2</url>
    </repository>
    <repository>
        <id>activiti-releases</id>
        <url>https://artifacts.alfresco.com/nexus/content/repositories/activiti-releases</url>
    </repository>
</repositories>
```

2. 在application.yml文件中添加以下配置：
```yaml
spring:
  # activiti7配置
  activiti:
    # 自动部署验证设置：true-开启（默认）、false-关闭
    check-process-definitions: true
    # 保存历史数据
    history-level: full
    # 检测历史表是否存在
    db-history-used: true
    # 关闭SpringAutoDeployment
    deployment-mode: never-fail
    # 对数据库中所有表进行更新操作，如果表不存在，则自动创建
    database-schema-update: true
    # 解决频繁查询SQL问题
    async-executor-activate: false
  jackson:
    ## 默认序列化时间格式
    date-format: yyyy-MM-dd HH:mm:ss
    ## 默认序列化时区
    time-zone: GMT+8
    serialization:
      write-dates-as-timestamps: false
  # 数据源配置
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/bpm?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&autoReconnect=true&allowMultiQueries=true&&nullCatalogMeansCurrent=true #此处的nullCatalogMeansCurrent=true
    username: root
    password: 123456
    type: com.zaxxer.hikari.HikariDataSource
    #hikari连接池配置 对应HikariConfig配置属性类
    hikari:
      #连接池别名
      pool-name: HikariCP-Primary
      #最小空闲连接数
      minimum-idle: 5
      #空闲连接存活最大时间，默认10分钟
      idle-timeout: 600000
      #连接池最大连接数，默认是10
      maximum-pool-size: 30
      #此属性控制从池返回的连接的默认自动提交行为，默认值：true
      auto-commit: true
      #此属性控制池中连接的最长生命周期，值0-表示无限生命周期，默认30分钟
      max-lifetime: 1800000
      #数据库连接超时时间，默认30秒
      connection-timeout: 60000
      #连接测试query
      connection-test-query: SELECT 1
```

## 整合knife4j
1. 在pom.xml文件中添加knife4j依赖：
```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>4.5.0</version>
</dependency>
```
2. 在application.yml文件中添加knife4j配置：
```yaml
# 配置springdoc-openapi，用于文档化和访问API
springdoc:
  # 配置Swagger UI的访问路径和排序方式
  swagger-ui:
    path: /swagger-ui.html  # Swagger UI的访问路径
    tags-sorter: alpha      # 按字母顺序排序标签
    operations-sorter: alpha  # 按字母顺序排序操作
  # 配置API文档的访问路径
  api-docs:
    path: /v3/api-docs  # API文档的访问路径
  # 配置API分组，用于组织和管理API
  group-configs:
    - group: 'default'   # API分组名称
      paths-to-match: '/**'  # 匹配所有路径
      packages-to-scan: com.springboot.bpm.controller  # 扫描的包，用于自动发现API

# knife4j的增强配置，不需要增强可以不配（详细版见下小节）
knife4j:
  enable: true
  setting:
    language: zh_cn  # 语言设置
```
http://localhost:8080/doc.html


## 创建 bpm-ui前端项目
### 安装node.js环境
1. 下载安装包：https://nodejs.org/zh-cn/download/
2. 安装：双击安装包进行安装
3. 检查是否安装成功：在命令行中输入`node -v`和`npm -v`，如果出现版本号则安装成功

### 使用VITE创建VUE前端项目
#### 1. 安装vite并创建vue项目
```sh
npm create vite@latest
```
![1730451629744.png](..%2Fvue%E5%AE%89%E8%A3%85%2Fimages%2Fvite%2F1730451629744.png)

#### 2.启动项目
```sh
cd bpm-ui
npm install
npm run dev
```
![1730451904399.png](..%2Fvue%E5%AE%89%E8%A3%85%2Fimages%2Fvite%2F1730451904399.png)

### 安装依赖包

#### 1、安装@types/node
```sh
npm i @types/node
```

#### 2、安装element-plus
```sh
npm install element-plus --save
```

#### 3、安装unplugin-vue-components和unplugin-auto-import
```sh
npm install -D unplugin-vue-components unplugin-auto-import
```

#### 4、安装icons-vue
```sh
npm install icons-vue --save
```

#### 5、安装vue-router
```sh
npm install vue-router --save
```

#### 6、安装axios
```sh
npm install axios --save
```

#### 7、安装pinia
```sh
npm install pinia --save
```

#### 8、安装sass
```sh
npm install sass -D
```

#### 9、安装typescript
```sh
npm install typescript --save-dev
```

#### 10、安装Vuex
```sh
npm install vuex --save
```

#### 11、安装qs
```sh
npm install qs --save
```

#### 12、安装pinia-plugin-persist
```sh
npm install pinia-plugin-persist --save
```

npm i bpmn-js --save  // bpmn-js核心依赖，包括设计器的画布、样式和左侧工具栏
npm i bpmn-js-properties-panel --save  // bpmn-js右侧属性栏插件
npm i @bpmn-io/properties-panel
npm i inherits