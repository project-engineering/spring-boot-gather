package com.springboot.bpm.common.constants;

public class Constants {
    /**
     * 查询操作
     */
    public static final String SELECT = "SELECT";
    /**
     * 新增操作
     */
    public static final String ADD = "ADD";
    /**
     * 修改操作
     */
    public static final String UPDATE = "UPDATE";
    /**
     * 删除操作
     */
    public static final String DELETE = "DELETE";
}
