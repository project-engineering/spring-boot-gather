package com.springboot.bpm.common.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.springboot.bpm.common.enums.HttpStatusEnum;
import com.springboot.bpm.common.enums.ResultCode;
import com.springboot.bpm.common.util.DateUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "统一出参类Result")
public class Result<T> {
    /**
     * 响应是否成功
     */
    private Boolean status;
    /**
     * 状态码
     */
    @Schema(description = "状态码", defaultValue = "200")
    private int code;
    /**
     * 描述信息
     */
    @Schema(description = "描述信息", defaultValue = "操作成功")
    private String message;

    @Schema(description = "总数")
    private long total;
    @Schema(description = "时间戳")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timestamp;

    /**
     * 返回对象
     */
    @Schema(description = "返回对象")
    private T data;


    public Result(){}

    Result(ResultCode resultCode)  {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.timestamp = LocalDateTime.now();
    }

    public Result(HttpStatusEnum HttpStatusEnum) {
        setTimestamp(DateUtil.getCurrLocalDateTime());
        setCode(HttpStatusEnum.getCode());
        setMessage(HttpStatusEnum.getMessage());
    }

    public Result(int code, String msg, T data) {
        setTimestamp(DateUtil.getCurrLocalDateTime());
        setCode(code);
        setMessage(msg);
        setData(data);
        if (data instanceof List) {
            this.total = ((List<?>) data).size();
        }
    }

    private Result(int code, String message) {
        this.code = code;
        this.message = message;
        this.timestamp = DateUtil.getCurrLocalDateTime();
    }

    private Result(int code, String message,long total, T data) {
        this.code = code;
        this.message = message;
        this.timestamp = DateUtil.getCurrLocalDateTime();
        this.total = total;
        this.data = data;
    }


    /**
     * 返回成功
     *
     * @return 返回成功
     */
    public static <T> Result<T> success() {
        return new Result<>(HttpStatusEnum.SUCCESS);
    }

    /**
     * 返回成功
     *
     * @return 返回成功
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(HttpStatusEnum.SUCCESS.getCode(), HttpStatusEnum.SUCCESS.getMessage(), data);
    }

    public static <T> Result<T> success(T data,long total) {
        return new Result<>(HttpStatusEnum.SUCCESS.getCode(), HttpStatusEnum.SUCCESS.getMessage(), total, data);
    }

    /**
     * 返回失败
     *
     * @return 返回失败
     */
    public static <T> Result<T> error() {
        return new Result<>(HttpStatusEnum.ERROR);
    }
    public static <T> Result<T> error(HttpStatusEnum HttpStatusEnum){
        return error(HttpStatusEnum.getCode(), HttpStatusEnum.getMessage());
    }

    public static <T> Result<T> error(int code, String msg) {
        return new Result<>(code, msg, null);
    }

    public static <T> Result<T> success(long total, List<T> data) {
        return (Result<T>) new Result(HttpStatusEnum.SUCCESS.getCode(), HttpStatusEnum.SUCCESS.getMessage(),total, data);
    }

    public static <T> Result<T> fail() {
        return new Result(HttpStatusEnum.ERROR.getCode(), HttpStatusEnum.ERROR.getMessage());
    }

    public static <T> Result<T> fail(String message) {
        return new Result(HttpStatusEnum.ERROR.getCode(), message);
    }

    public static <T> Result<T> fail(int code,String message) {
        return new Result(code, message);
    }

    public static <T> Result<T> result(HttpStatusEnum code) {
        return new Result(code.getCode(), code.getMessage());
    }

    public static <T> Result<T> result(int code, String message) {
        return new Result(code, message);
    }


    public boolean isSuccess() {
        return HttpStatusEnum.SUCCESS.getCode().equals(this.code);
    }


    /**
     * 获取 code.
     *
     * @return 返回 code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置 code 的值
     *
     * @param code code
     */
    public Result<T> setCode(int code) {
        this.code = code;
        return this;
    }

    /**
     * 获取 message.
     *
     * @return 返回 message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 设置 message 的值
     *
     * @param message message
     */
    public Result<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * 获取 total.
     *
     * @return 返回 total
     */
    public long getTotal() {
        return total;
    }

    /**
     * 设置 total 的值
     *
     * @param total total
     */
    public Result<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    /**
     * 获取 timestamp.
     *
     * @return 返回 timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * 设置 timestamp 的值
     *
     * @param timestamp timestamp
     */
    public Result<T> setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    /**
     * 获取 data.
     *
     * @return 返回 data
     */
    public T getData() {
        return data;
    }

    /**
     * 设置 data 的值
     *
     * @param data data
     */
    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }
}
