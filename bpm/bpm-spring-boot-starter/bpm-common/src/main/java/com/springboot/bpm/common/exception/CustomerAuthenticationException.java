package com.springboot.bpm.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义认证异常类
 */
public class CustomerAuthenticationException extends AuthenticationException {
    public CustomerAuthenticationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public CustomerAuthenticationException(String msg) {
        super(msg);
    }
}
