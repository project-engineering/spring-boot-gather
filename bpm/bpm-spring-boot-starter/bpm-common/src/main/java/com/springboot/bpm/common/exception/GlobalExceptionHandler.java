package com.springboot.bpm.common.exception;

import cn.hutool.core.util.ObjectUtil;
import com.springboot.bpm.common.enums.ResultCode;
import com.springboot.bpm.common.response.Result;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.yaml.snakeyaml.constructor.DuplicateKeyException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;

/**
 * 异常处理 配置
 */
@Log4j2
@RestControllerAdvice
@Order(1)
public class GlobalExceptionHandler {
    /**
     * 处理自定义的业务异常
     * @param e	异常对象
     * @return	错误结果
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public Result businessExceptionHandler(BusinessException e) {
        log.error("发生业务异常，原因是: ", e);
        int code = e.getCode();
        if (ObjectUtil.isEmpty(code)) {
            code = ResultCode.BUSINESS_ERROR.getCode();
        }
        String message = e.getMessage();
        if (ObjectUtil.isEmpty(message)) {
            message = ResultCode.BUSINESS_ERROR.getMessage();
        }
        return Result.fail(code, message);
    }

    @ExceptionHandler({BindException.class})
    public Result handleBindException(BindException e) {
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return Result.fail(ResultCode.PARAMETER_BIND_ERROR.getCode(),message);
    }

    /**
     * 处理参数校验异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("参数校验异常，原因是: ", e);
        String message = e.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining(","));
        return Result.fail(ResultCode.PARAMETER_VALID_ERROR.getCode(), message);
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public Result handleRuntimeException(RuntimeException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生未知异常.", requestURI, e);
        return Result.fail(e.getMessage());
    }

    /**
     * 拦截参数缺失异常
     * @param e 异常对象
     * @param request 请求对象
     * @return 错误结果
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result handleMissingServletRequestParameterException(MissingServletRequestParameterException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生参数缺失异常,原因是: ", requestURI, e);
        return Result.fail(e.getMessage());
    }

    /**
     * 处理未知异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        log.error("发生未知异常，原因是: ",e);
        return Result.fail(ResultCode.UNKUOW_ERROR.getCode(),ResultCode.UNKUOW_ERROR.getMessage());
    }

    /**
     * 处理请求方法不支持异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result handleMethodArgumentNotValidException(HttpRequestMethodNotSupportedException e) {
        log.error("请求方法不支持,原因是：",e);
        return Result.fail(ResultCode.CLIENT_HTTP_METHOD_ERROR.getCode(),ResultCode.CLIENT_HTTP_METHOD_ERROR.getMessage());
    }

    /**
     * 处理重复键异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public Result handleDuplicateKeyException(DuplicateKeyException e){
        log.error("主键冲突异常，原因是：", e);
        return Result.fail(ResultCode.DUPLICATE_KEY_ERROR.getCode(),ResultCode.DUPLICATE_KEY_ERROR.getMessage());
    }

    /**
     * 请求路径中缺少必需的路径变量
     * @param e 异常对象
     * @param request 请求对象
     * @return 错误结果
     */
    @ExceptionHandler(MissingPathVariableException.class)
    public Result handleMissingPathVariableException(MissingPathVariableException e, HttpServletRequest request) {
        log.error("请求路径中缺少必需的路径变量：path={}，error={}", request.getRequestURI(), e.getMessage());
        return Result.fail(String.format("请求路径中缺少必需的路径变量[%s]", e.getVariableName()));
    }

    /**
     * 客户端@RequestBody请求体JSON格式错误或字段类型错误
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Result handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error("客户端请求体JSON格式错误或字段类型不匹配，原因是：", e);
        return Result.fail(ResultCode.CLIENT_REQUEST_BODY_FORMAT_ERROR.getCode(),ResultCode.CLIENT_REQUEST_BODY_FORMAT_ERROR.getMessage());
    }

    /**
     * 通用的业务方法入参检查错误
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    public Result handleIllegalArgumentException(IllegalArgumentException e) {
        log.error("客户端请求参数格式错误，原因是：", e);
        return Result.fail(ResultCode.SERVER_ILLEGAL_ARGUMENT_ERROR.getCode(),ResultCode.SERVER_ILLEGAL_ARGUMENT_ERROR.getMessage());
    }

    /**
     * 数据类型转换异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = ClassCastException.class)
    public Result handleClassCastException(ClassCastException e) {
        log.error("数据类型转换异常，原因是：", e);
        return Result.fail(ResultCode.DATA_TYPE_CONVERSION_ERROR.getCode(), ResultCode.DATA_TYPE_CONVERSION_ERROR.getMessage());
    }

    /**
     * 非法访问异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = IllegalAccessException.class)
    public Result handleIllegalAccessException(IllegalAccessException e) {
        log.error("非法访问异常，原因是：", e);
        return Result.fail(ResultCode.ILLEGAL_ACCESS_ERROR.getCode(), ResultCode.ILLEGAL_ACCESS_ERROR.getMessage());
    }

    /**
     * 数组下标越界异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = IndexOutOfBoundsException.class)
    public Result handleIndexOutOfBoundsException(IndexOutOfBoundsException e) {
        log.error("数组下标越界异常，原因是：", e);
        return Result.fail(ResultCode.ARRAY_INDEX_OUT_OF_BOUNDS_ERROR.getCode(), ResultCode.ARRAY_INDEX_OUT_OF_BOUNDS_ERROR.getMessage());
    }

    /**
     * 文件未找到异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = FileNotFoundException.class)
    public Result handleFileNotFoundException(FileNotFoundException e) {
        log.error("文件未找到异常，原因是：", e);
        return Result.fail(ResultCode.FILE_NOT_FOUND_ERROR.getCode(), ResultCode.FILE_NOT_FOUND_ERROR.getMessage());
    }

    /**
     * 数字格式异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = NumberFormatException.class)
    public Result handleNumberFormatException(NumberFormatException e) {
        log.error("数字格式异常，原因是：", e);
        return Result.fail(ResultCode.NUMBER_FORMAT_ERROR.getCode(), ResultCode.NUMBER_FORMAT_ERROR.getMessage());
    }

    /**
     * 实例化异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = InstantiationException.class)
    public Result handleInstantiationException(InstantiationException e) {
        log.error("实例化异常，原因是：", e);
        return Result.fail(ResultCode.INSTANTIATION_ERROR.getCode(), ResultCode.INSTANTIATION_ERROR.getMessage());
    }

    /**
     * 安全异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = SecurityException.class)
    public Result handleSecurityException(SecurityException e) {
        log.error("安全异常，原因是：", e);
        return Result.fail(ResultCode.SECURITY_ERROR.getCode(), ResultCode.SECURITY_ERROR.getMessage());
    }

    /**
     * 文件已结束异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = EOFException.class)
    public Result handleEOFException(EOFException e) {
        log.error("文件已结束异常，原因是：", e);
        return Result.fail(ResultCode.FILE_END_OF_STREAM_ERROR.getCode(), ResultCode.FILE_END_OF_STREAM_ERROR.getMessage());
    }

    /**
     * 空指针异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = NullPointerException.class)
    public Result handleNullPointerException(NullPointerException e) {
        log.error("空指针异常，原因是：", e);
        return Result.fail(ResultCode.NULL_POINTER_ERROR.getCode(), ResultCode.NULL_POINTER_ERROR.getMessage());
    }

    /**
     * 方法不存在异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = NoSuchMethodError  .class)
    public Result handleNoSuchMethodError(NoSuchMethodError e) {
        log.error("方法不存在异常，原因是：", e);
        return Result.fail(ResultCode.NO_SUCH_METHOD_ERROR.getCode(), ResultCode.NO_SUCH_METHOD_ERROR.getMessage());
    }

    /**
     * SQL异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = SQLException.class)
    public Result handleSQLException(SQLException e) {
        log.error("SQL异常，原因是：", e);
        return Result.fail(ResultCode.SQL_ERROR.getCode(), ResultCode.SQL_ERROR.getMessage());
    }

    /**
     * IO异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = IOException.class)
    public Result handleIOException(IOException e) {
        log.error("IO异常，原因是：", e);
        return Result.fail(ResultCode.IO_ERROR.getCode(), ResultCode.IO_ERROR.getMessage());
    }

    /**
     * 数据格式异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = DataFormatException.class)
    public Result handleDataFormatException(DataFormatException e) {
        log.error("数据格式异常，原因是：", e);
        return Result.fail(ResultCode.DATA_FORMAT_ERROR.getCode(), ResultCode.DATA_FORMAT_ERROR.getMessage());
    }

    /**
     * 字段不存在异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = NoSuchFieldException.class)
    public Result handleNoSuchFieldException(NoSuchFieldException e) {
        log.error("字段不存在异常，原因是：", e);
        return Result.fail(ResultCode.NO_SUCH_FIELD_ERROR.getCode(), ResultCode.NO_SUCH_FIELD_ERROR.getMessage());
    }

    /**
     * 超时异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = TimeoutException.class)
    public Result handleTimeoutException(TimeoutException e) {
        log.error("超时异常，原因是：", e);
        return Result.fail(ResultCode.TIMEOUT_ERROR.getCode(), ResultCode.TIMEOUT_ERROR.getMessage());
    }

    /**
     * 输入不匹配异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = InputMismatchException.class)
    public Result handleInputMismatchException(InputMismatchException e) {
        log.error("输入不匹配异常，原因是：", e);
        return Result.fail(ResultCode.INPUT_MISMATCH_ERROR.getCode(), ResultCode.INPUT_MISMATCH_ERROR.getMessage());
    }

}
