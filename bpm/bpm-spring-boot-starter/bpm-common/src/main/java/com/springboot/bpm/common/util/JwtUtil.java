package com.springboot.bpm.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.*;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * JWT配置类
 */
@Component
@Log4j2
@ConfigurationProperties(prefix = "jwt")
public class JwtUtil {
    @Value("${jwt.issuer}")
    private String issuer;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;


    /**
     * 签发Token
     * @param claims 自定义参数
     * @return Token
     */
    public String sign(Map<String, String> claims) {
        try {
            // 设置过期时间
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, expiration);
            // 创建JWT builder
            JWTCreator.Builder builder = JWT.create();
            // payload
            claims.forEach(builder::withClaim);
            return builder
                    .withIssuer(issuer)
                    .withIssuedAt(new Date())
                    .withExpiresAt(calendar.getTime())
                    .sign(Algorithm.HMAC256(secret));
        } catch (Exception e) {
            log.error("token 签发失败", e);
            throw new RuntimeException("token签发失败");
        }
    }

    /**
     * 根据token获取userId
     * @param token token
     * @return userId
     */

    public static String getUserId(String token) {
        try {
            return JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException e) {
            log.error("token 解析失败", e);
            return null;
        }
    }

    /**
     * 校验token
     * @param token token
     * @return true 校验通过，false 校验失败
     */

    public boolean verify(String token) {
        try {
            JWT.require(Algorithm.HMAC256(secret)).withIssuer(issuer).build().verify(token);
        } catch (JWTVerificationException exception) {
            return false;
        }
        return true;
    }

    /**
     * 解析token
     */
    public DecodedJWT decode(String token) {
        try {
            return JWT.require(Algorithm.HMAC256(secret)).withIssuer(issuer).build().verify(token);
        } catch (SignatureVerificationException e) {
            log.error("token 签名验证失败", e);
            throw new RuntimeException("token签名验证失败");
        } catch (AlgorithmMismatchException e) {
            log.error("token 算法不匹配", e);
            throw new RuntimeException("token算法不匹配");
        } catch (TokenExpiredException e) {
            log.error("token 已过期", e);
            throw new RuntimeException("token已过期");
        } catch (Exception e) {
            log.error("token 解析失败", e);
            throw new RuntimeException("token解析失败");
        }
    }
}