package com.springboot.bpm.api.param.system.menu;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Tag(name = "获取用户信息入参")
public class GetMenuListParam {
    @Schema(description = "用户ID")
    @NotEmpty(message = "用户ID不能为空")
    private String userId;
}
