package com.springboot.bpm.api.param.system.role;

import com.springboot.bpm.common.response.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Tag(name = "角色查询参数")
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleParam extends Page {
    @Schema(description = "角色名称")
    private String roleName;
}
