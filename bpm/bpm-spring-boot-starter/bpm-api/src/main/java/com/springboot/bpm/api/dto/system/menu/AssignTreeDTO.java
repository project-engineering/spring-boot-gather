package com.springboot.bpm.api.dto.system.menu;

import com.springboot.bpm.entity.system.menu.SysMenu;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Tag(name = "查询菜单树出差")
@Data
public class AssignTreeDTO {
    private List<SysMenu> menuList = new ArrayList<>();
    private Object[] checkList;
}
