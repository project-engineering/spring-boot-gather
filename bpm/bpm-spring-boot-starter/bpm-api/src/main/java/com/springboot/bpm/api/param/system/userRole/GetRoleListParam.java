package com.springboot.bpm.api.param.system.userRole;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name = "用户角色查询参数")
public class GetRoleListParam {
    @Schema(description = "用户id")
    private String userId;
}
