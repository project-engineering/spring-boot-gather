package com.springboot.bpm.api.param.system.user;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Tag(name = "密码修改入参")
public class UpdatePwdParam {
    @Schema(description = "用户ID")
    @NotEmpty(message = "用户ID不能为空")
    private String userId;

    @Schema(description = "旧密码")
    @NotEmpty(message = "旧密码不能为空")
    private String oldPassword;

    @Schema(description = "新密码")
    @NotEmpty(message = "新密码不能为空")
    private String password;
}
