package com.springboot.bpm.api.dto.system;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Tag(name = "登录出参")
@Data
public class LoginDTO {
    @Schema(description = "登录token")
    private String token;

    @Schema(description = "用户ID")
    private String userId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "别名")
    private String nickName;
}
