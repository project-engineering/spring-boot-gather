package com.springboot.bpm.controller.system;

import com.springboot.bpm.api.param.system.userRole.GetRoleListParam;
import com.springboot.bpm.common.annotation.WebLog;
import com.springboot.bpm.common.constants.Constants;
import com.springboot.bpm.common.response.Result;
import com.springboot.bpm.service.system.SysUserRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "用户角色管理", description = "用户角色管理")
@RestController
@RequestMapping("/api/userRole")
public class SysUserRoleController {
    @Resource
    SysUserRoleService sysUserRoleService;

    @Operation(summary = "用户角色查询", description = "根据用户ID查询用户角色")
    @WebLog(modul = "用户角色管理", type = Constants.SELECT, desc = "用户角色查询")
    @PostMapping("/getRoleListByUserId")
    public Result getRoleListByUserId(@RequestBody GetRoleListParam param){
        return Result.success(sysUserRoleService.getRoleListByUserId(param.getUserId()));
    }

}
