package com.springboot.bpm.service.system.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.bpm.api.dto.system.role.RoleSelectType;
import com.springboot.bpm.api.param.system.role.RoleParam;
import com.springboot.bpm.entity.system.role.SysRole;
import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysRoleService extends IService<SysRole> {
    IPage<SysRole> getList(RoleParam param);

    List<RoleSelectType> getRoleSelectList();

    boolean addRole(SysRole sysRole);

    List<SysRole> getRoleListByUserId(String userId);
}
