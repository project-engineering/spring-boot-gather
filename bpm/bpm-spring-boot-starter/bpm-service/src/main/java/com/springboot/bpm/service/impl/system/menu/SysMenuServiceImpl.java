package com.springboot.bpm.service.impl.system.menu;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.api.dto.system.menu.AssignTreeDTO;
import com.springboot.bpm.api.dto.system.menu.RouterDTO;
import com.springboot.bpm.api.param.system.menu.AssignTreeParam;
import com.springboot.bpm.common.exception.BusinessException;
import com.springboot.bpm.entity.system.SysRoleMenu;
import com.springboot.bpm.entity.system.SysUserRole;
import com.springboot.bpm.entity.system.menu.SysMenu;
import com.springboot.bpm.entity.system.user.SysUser;
import com.springboot.bpm.mapper.system.menu.SysMenuMapper;
import com.springboot.bpm.service.system.SysRoleMenuService;
import com.springboot.bpm.service.system.SysUserRoleService;
import com.springboot.bpm.service.system.menu.SysMenuService;
import com.springboot.bpm.service.system.user.SysUserService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Log4j2
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Resource
    private SysMenuMapper mapper;
    @Resource
    private SysUserRoleService sysUserRoleService;
    @Resource
    private SysRoleMenuService sysRoleMenuService;
    @Lazy
    @Resource
    private SysUserService sysUserService;

    @Override
    public void saveMenu(SysMenu sysMenu) {
        sysMenu.setMenuId(IdUtil.simpleUUID());
        mapper.insert(sysMenu);
    }

    @Override
    public void updateMenu(SysMenu sysMenu) {
        mapper.updateById(sysMenu);
    }

    @Override
    public void deleteMenu(String menuId) {
        //如果存在下级，不能删除
        List<SysMenu> sysMenuList = mapper.selectList(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getParentId, menuId));
        if (ObjectUtil.isNotEmpty(sysMenuList)) {
            throw new BusinessException("存在下级菜单，不能删除");
        }
        mapper.deleteById(menuId);
    }

    @Override
    public List<SysMenu> listMenu() {
        LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(SysMenu::getOrderNum);
        return mapper.selectList(queryWrapper);
    }

    @Override
    public List<SysMenu> buildTree(List<SysMenu> sysMenuList, String parentId) {
        List<SysMenu> list = new ArrayList<>();
        Optional.ofNullable(sysMenuList).orElse(new ArrayList<>())
                .stream()
                .filter(item -> ObjectUtil.isNotEmpty(item) && ObjectUtil.equals(parentId, item.getParentId()))
                .forEach(item -> {
                    SysMenu sysMenu = new SysMenu();
                    BeanUtils.copyProperties(item, sysMenu);
                    sysMenu.setLabel(item.getTitle());
                    sysMenu.setValue(item.getMenuId());
                    // 递归获取子菜单
                    List<SysMenu> children = buildTree(sysMenuList, item.getMenuId());
                    sysMenu.setChildren(children);
                    list.add(sysMenu);
                });
        return list;
    }

    @Override
    public List<SysMenu> getParentMenuList() {
        LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SysMenu::getType, Arrays.asList("0", "1"));
        queryWrapper.orderByAsc(SysMenu::getOrderNum);
        List<SysMenu> menuList = mapper.selectList(queryWrapper);
        //组装顶级树
        SysMenu sysMenu = new SysMenu();
        sysMenu.setTitle("顶级菜单");
        sysMenu.setLabel("顶级菜单");
        sysMenu.setParentId("-1");
        sysMenu.setMenuId("0");
        sysMenu.setValue("0");
        menuList.add(sysMenu);
        //组装树结构
        return buildTree(menuList, "-1");
    }

    @Override
    public List<SysMenu> getMenuListByUserId(String userId) {
        List<SysMenu> menuList = null;
        //获取角色列表
        List<SysUserRole> roleIdList = sysUserRoleService.getRoleListByUserId(userId);
        if (ObjectUtil.isNotEmpty(roleIdList)) {
            List<SysRoleMenu> roleMenuList = sysRoleMenuService.getRoleMenuListByRoleIdList(roleIdList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList()));
            if (ObjectUtil.isNotEmpty(roleMenuList)) {
                List<String> menuIdList = roleMenuList.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
                if (ObjectUtil.isNotEmpty(menuIdList)) {
                    LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
                    queryWrapper.in(SysMenu::getMenuId, menuIdList);
                    queryWrapper.orderByAsc(SysMenu::getOrderNum);
                    menuList = mapper.selectList(queryWrapper);
                }
            }
        }
        return menuList;
    }

    @Override
    public List<SysMenu> getMenuListByRoleId(String roleId) {
        List<SysMenu> menuList = null;
        List<SysRoleMenu> roleMenuList = sysRoleMenuService.getRoleMenuListByRoleIdList(Collections.singletonList(roleId));
        if (ObjectUtil.isNotEmpty(roleMenuList)) {
            List<String> menuIdList = roleMenuList.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
            if (ObjectUtil.isNotEmpty(menuIdList)) {
                LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.in(SysMenu::getMenuId, menuIdList);
                queryWrapper.orderByAsc(SysMenu::getOrderNum);
                menuList = mapper.selectList(queryWrapper);
            }
        }
        return menuList;
    }

    @Override
    public AssignTreeDTO getAssignTree(AssignTreeParam assignTreeParam) {
        List<SysMenu> menuList;
        // 查询用户信息
        SysUser sysUser = sysUserService.getUserByUserId(assignTreeParam.getUserId());
        // 判断是否是超级管理员
        if (ObjectUtil.isNotEmpty(sysUser) && ObjectUtil.equals(sysUser.getIsAdmin(),"1")) {
            // 是超级管理员，查询所有菜单
            menuList = mapper.selectList(null);
        } else {
            menuList = getMenuListByUserId(assignTreeParam.getUserId());
        }
        // 组装树
        List<SysMenu> treeList = buildTree(menuList, "0");
        // 查询角色原来的菜单
        List<SysMenu> roleList = getMenuListByRoleId(assignTreeParam.getRoleId());
        List<String> ids = new ArrayList<>();
        Optional.ofNullable(roleList).orElse(new ArrayList<>())
                .stream()
                .filter(item -> ObjectUtil.isNotEmpty(item.getMenuId()))
                .forEach(item -> {
                    ids.add(item.getMenuId());
                });
        // 组装返回数据
        AssignTreeDTO assignTreeDTO = new AssignTreeDTO();
        assignTreeDTO.setCheckList(ids.toArray());
        assignTreeDTO.setMenuList(treeList);
        return assignTreeDTO;
    }

    @Override
    public List<RouterDTO> buildRouter(List<SysMenu> menuList, String parentId) {
        //构建存放路由数据的容器
        List<RouterDTO> routerList = new ArrayList<>();
        //过滤出当前父菜单下的所有子菜单
        Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .filter(item -> ObjectUtil.isNotEmpty(item.getMenuId()) && ObjectUtil.equals(item.getParentId(), parentId))
                .forEach(item -> {
                    RouterDTO routerDTO = RouterDTO.builder()
                            .name(item.getName())
                            .path(item.getPath())
                            .build();
                     //设置children递归调用
                     List<RouterDTO> children = buildRouter(menuList, item.getMenuId());
                     if (ObjectUtil.isNotEmpty(children)) {
                         routerDTO.setChildren(children);
                     }
                     // 如果是上级是0，那么它的component就是Layout，否则就是Menu
                     if (ObjectUtil.equals(item.getParentId(), "0")) {
                         routerDTO.setComponent("Layout");
                         //判断该数据是目录还是菜单
                         if (ObjectUtil.equals(item.getType(), "1")) {//如果一级菜单是菜单类型，单独处理
                             routerDTO.setRedirect(item.getPath());
                             //菜单需要设置children
                             List<RouterDTO> listChildren = new ArrayList<>();
                             RouterDTO child = RouterDTO.builder()
                                     .name(item.getName())
                                     .path(item.getPath())
                                     .component(item.getUrl())
                                     .meta(RouterDTO.Meta.builder()
                                     .title(item.getTitle())
                                     .icon(item.getIcon())
                                     .roles(item.getCode().split(","))
                                     .build())
                                     .build();
                             listChildren.add(child);
                             routerDTO.setChildren(listChildren);
                             routerDTO.setPath(item.getPath()+"parent");
                             routerDTO.setName(item.getName()+"parent");
                         }
                     } else {
                         routerDTO.setComponent(item.getUrl());
                     }
                     routerDTO.setMeta(RouterDTO.Meta.builder()
                            .title(item.getTitle())
                            .icon(item.getIcon())
                            .roles(item.getCode().split(","))
                            .build());
                     routerList.add(routerDTO);
                 });
        return routerList;
    }

    @Override
    public List<RouterDTO> getMenuList(String userId) {
        SysUser sysUser = sysUserService.getById(userId);
        if (ObjectUtil.isEmpty(sysUser)) {
            throw new IllegalArgumentException("用户不存在");
        }
        List<SysMenu> menuList;
        // 判断是否为超级管理员
        if (ObjectUtil.equals(sysUser.getIsAdmin(),"1")) {
            menuList = listMenu();
        } else {
            menuList = getMenuListByUserId(userId);
        }
        // 过滤菜单数据，只保留菜单类型为目录和菜单
        Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .filter(item -> ObjectUtil.isNotEmpty(item.getType()) && !ObjectUtil.equals(item.getType(),"2"))
                .toList();
        // 构建路由数据
        return buildRouter(menuList,"0");
    }
}
