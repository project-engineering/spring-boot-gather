package com.springboot.bpm.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.bpm.entity.system.SysUserRole;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    List<SysUserRole> getRoleListByUserId(String userId);
}
