package com.springboot.bpm.service.impl.system.log;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.entity.system.log.SysLogInfo;
import com.springboot.bpm.mapper.system.log.SysLogInfoMapper;
import com.springboot.bpm.service.system.log.SysLogInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysLogInfoServiceImpl extends ServiceImpl<SysLogInfoMapper, SysLogInfo> implements SysLogInfoService {

}
