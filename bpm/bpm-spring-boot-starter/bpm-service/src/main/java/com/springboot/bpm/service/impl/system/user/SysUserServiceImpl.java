package com.springboot.bpm.service.impl.system.user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.api.dto.system.LoginDTO;
import com.springboot.bpm.api.dto.system.user.UserInfoDTO;
import com.springboot.bpm.api.param.system.LoginParam;
import com.springboot.bpm.api.param.system.RegisterParam;
import com.springboot.bpm.api.param.system.user.UpdatePwdParam;
import com.springboot.bpm.api.param.system.user.UserParam;
import com.springboot.bpm.common.enums.ResultCode;
import com.springboot.bpm.common.exception.BusinessException;
import com.springboot.bpm.common.util.JwtUtil;
import com.springboot.bpm.entity.system.menu.SysMenu;
import com.springboot.bpm.entity.system.user.SysUser;
import com.springboot.bpm.entity.system.SysUserRole;
import com.springboot.bpm.mapper.system.user.SysUserMapper;
import com.springboot.bpm.service.impl.system.security.CustomerUserDetailsService;
import com.springboot.bpm.service.system.SysUserRoleService;
import com.springboot.bpm.service.system.menu.SysMenuService;
import com.springboot.bpm.service.system.user.SysUserService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 */
@Log4j2
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    SysUserMapper mapper;
    @Lazy
    @Resource
    private SysMenuService sysMenuService;
    @Resource
    SysUserRoleService sysUserRoleService;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private JwtUtil jwtUtil;
    @Resource
    private AuthenticationManager authenticationManager;
    @Resource
    private CustomerUserDetailsService customerUserDetailsService;

    @Transactional
    @Override
    public boolean addUser(SysUser sysUser) {
        sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
        sysUser.setUserId(IdUtil.simpleUUID());
        //新增用户
        int insert = mapper.insert(sysUser);
        //设置用户角色
        if (insert > 0) {
            SysUserRole sysUserRole = SysUserRole.builder()
                    .userRoleId(IdUtil.simpleUUID())
                    .userId(sysUser.getUserId())
                    .roleId(sysUser.getRoleId())
                    .build();
            sysUserRoleService.save(sysUserRole);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @Override
    public boolean updateUser(SysUser sysUser) {
        int i = mapper.updateById(sysUser);
        //更新用户角色，判断角色是否发生变化
        if (i > 0) {
           //先删除，在插入
           sysUserRoleService.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,sysUser.getUserId()));
           SysUserRole sysUserRole = SysUserRole.builder()
                   .userRoleId(IdUtil.simpleUUID())
                   .userId(sysUser.getUserId())
                   .roleId(sysUser.getRoleId())
                   .build();
           sysUserRoleService.save(sysUserRole);
           return true;
        } else {
           return false;
        }
    }

    @Transactional
    @Override
    public boolean deleteUser(String userId) {
        int i = mapper.deleteById(userId);
        if (i > 0) {
            sysUserRoleService.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,userId));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public IPage<SysUser> getList(UserParam param) {
        int pageNo = param.getPageNo();
        if (ObjectUtil.isEmpty(pageNo)) {
            pageNo = 1;
        }
        int pageSize = param.getPageSize();
        if (ObjectUtil.isEmpty(pageSize)) {
            pageSize = 10;
        }
        //构造分页对象
        IPage<SysUser> page = new Page<>(pageNo,pageSize);
        //构造查询条件
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(param.getUserId())){
            queryWrapper.lambda().eq(SysUser::getUserId,param.getUserId());
        }
        if (ObjectUtil.isNotEmpty(param.getUsername())){
            queryWrapper.lambda().like(SysUser::getUsername,param.getUsername());
        }
        if (ObjectUtil.isNotEmpty(param.getPhone())){
            queryWrapper.lambda().like(SysUser::getPhone,param.getPhone());
        }
        if (ObjectUtil.isNotEmpty(param.getNickName())){
            queryWrapper.lambda().like(SysUser::getNickName,param.getNickName());
        }
        queryWrapper.lambda().orderByDesc(SysUser::getCreateTime);
        //执行查询
        return mapper.selectPage(page,queryWrapper);
    }

    @Override
    public int resetPwd(String userId) {
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getUserId, userId);
        updateWrapper.set(SysUser::getPassword, passwordEncoder.encode("123456"));
        return mapper.update(updateWrapper);
    }

    @Override
    public SysUser getUserByUsername(String username) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername,username);
        return mapper.selectOne(queryWrapper);
    }

    @Transactional
    @Override
    public void register(RegisterParam registerParam) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(SysUser::getUserId).last("limit 1");
        queryWrapper.eq(SysUser::getUsername,registerParam.getUsername());
        SysUser sysUser = mapper.selectOne(queryWrapper);
        if (ObjectUtil.isNotEmpty(sysUser)){
            throw new BusinessException(ResultCode.USER_EXISTS.getCode(),ResultCode.USER_EXISTS.getMessage());
        }
        SysUser user = BeanUtil.copyProperties(registerParam, SysUser.class);
        addUser(user);
    }

    @Override
    public LoginDTO login(LoginParam param) {
        // 查询用户信息
        SysUser sysUser = (SysUser) customerUserDetailsService.loadUserByUsername(param.getUsername());
        if (sysUser == null) {
            throw new BusinessException(ResultCode.USER_NOT_EXISTS.getCode(), ResultCode.USER_NOT_EXISTS.getMessage());
        }

        // 创建UsernamePasswordAuthenticationToken
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                param.getUsername(), param.getPassword(), sysUser.getAuthorities());

        // 交给Spring Security进行验证
        Authentication authentication = authenticationManager.authenticate(token);

        // 如果验证通过，设置认证信息
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // 生成token
        Map<String, String> claims = new HashMap<>();
        claims.put("userId", sysUser.getUserId());
        claims.put("username", sysUser.getUsername());
        String tokenStr = jwtUtil.sign(claims);

        // 创建登录返回对象
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUserId(sysUser.getUserId());
        loginDTO.setUsername(sysUser.getUsername());
        loginDTO.setToken(tokenStr);
        loginDTO.setNickName(sysUser.getNickName());

        return loginDTO;
    }

    @Override
    public SysUser getUserByUserId(String userId) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUserId,userId);
        return mapper.selectOne(queryWrapper);
    }

    @Override
    public void updatePwd(UpdatePwdParam param) {
        if (ObjectUtil.equals(param.getOldPassword(),param.getPassword())) {
            throw new BusinessException(ResultCode.USER_PASSWORD_NOT_MATCH.getCode(),ResultCode.USER_PASSWORD_NOT_MATCH.getMessage());
        }
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUserId,param.getUserId());
        SysUser sysUser = mapper.selectOne(queryWrapper);
        if (ObjectUtil.isEmpty(sysUser)){
            throw new BusinessException(ResultCode.USER_NOT_EXISTS.getCode(),ResultCode.USER_NOT_EXISTS.getMessage());
        }
        String password = sysUser.getPassword();
        // 创建BCryptPasswordEncoder实例
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (encoder.matches(passwordEncoder.encode(param.getOldPassword()), password)){
            throw new BusinessException(ResultCode.ORIGINAL_PASSWORD_INCORRECT.getCode(),ResultCode.ORIGINAL_PASSWORD_INCORRECT.getMessage());
        }
        //加密新密码
        String newPassword = passwordEncoder.encode(param.getPassword());
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getUserId,param.getUserId());
        updateWrapper.set(SysUser::getPassword,newPassword);
        mapper.update(updateWrapper);
    }

    @Override
    public UserInfoDTO getUserInfo(String userId) {
        List<SysMenu> menuList;
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUserId,userId);
        SysUser sysUser = mapper.selectOne(queryWrapper);
        if (ObjectUtil.isEmpty(sysUser)) {
            throw new BusinessException(ResultCode.USER_NOT_EXISTS.getCode(), ResultCode.USER_NOT_EXISTS.getMessage());
        }
        menuList = sysMenuService.getMenuListByUserId(userId);
        // 过滤权限字段
        List<String> permissionList = Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .map(SysMenu::getCode)
                .filter(ObjectUtil::isNotEmpty)
                .toList();
        // 设置返回值
        return UserInfoDTO.builder()
                .userId(sysUser.getUserId())
                .userName(sysUser.getUsername())
                .permissions(permissionList.toArray()).build();
    }
}
