package com.springboot.bpm.service.impl.system.log;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.entity.system.log.SysLogErrorInfo;
import com.springboot.bpm.mapper.system.log.SysLogErrorInfoMapper;
import com.springboot.bpm.service.system.log.SysLogErrorInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 异常信息表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysLogErrorInfoServiceImpl extends ServiceImpl<SysLogErrorInfoMapper, SysLogErrorInfo> implements SysLogErrorInfoService {

}
