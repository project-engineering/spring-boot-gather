package com.springboot.bpm.service.impl.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.entity.system.SysUserRole;
import com.springboot.bpm.mapper.system.SysUserRoleMapper;
import com.springboot.bpm.service.system.SysUserRoleService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {
    @Resource
    SysUserRoleMapper mapper;
    /**
     * 根据用户id查询角色信息
     * @param userId 用户id
     * @return 角色信息
     */
    @Override
    public List<SysUserRole> getRoleListByUserId(String userId) {
        //构造查询条件
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUserRole::getUserId, userId);
        //执行查询
        return mapper.selectList(queryWrapper);
    }
}
