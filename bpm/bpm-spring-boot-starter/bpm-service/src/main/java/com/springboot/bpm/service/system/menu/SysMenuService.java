package com.springboot.bpm.service.system.menu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.bpm.api.dto.system.menu.AssignTreeDTO;
import com.springboot.bpm.api.dto.system.menu.RouterDTO;
import com.springboot.bpm.api.param.system.menu.AssignTreeParam;
import com.springboot.bpm.entity.system.menu.SysMenu;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysMenuService extends IService<SysMenu> {

    void saveMenu(SysMenu sysMenu);

    void updateMenu(SysMenu sysMenu);

    void deleteMenu(String  menuId);

    List<SysMenu> listMenu();

    List<SysMenu> buildTree(List<SysMenu> list,String parentId);

    List<SysMenu> getParentMenuList();

    List<SysMenu> getMenuListByUserId(String userId);

    List<SysMenu> getMenuListByRoleId(String roleId);
    // 查询菜单树
    AssignTreeDTO getAssignTree(AssignTreeParam assignTreeParam);

    List<RouterDTO> buildRouter(List<SysMenu> menuList, String parentId);

    List<RouterDTO> getMenuList(String userId);
}
