package com.springboot.bpm.service.code;

import com.springboot.bpm.entity.code.ColumnDetail;

import java.util.*;

/**
 * 代码生成器业务层接口
 */
public interface ICodeGeneratorService {

    /**
     * 获取数据库中所有表信息
     */
    List<ColumnDetail> getAllTableInfo();

    /**
     * 获取表中所有字段信息
     *
     * @param tableName 表名
     */
    List<ColumnDetail> getColumnDetails(String tableName);

    /**
     * 生成代码
     *
     * @param columnDetailList 表字段信息
     * @param delPrefix        需要去除的前缀（tb_）
     * @param packageName      文件所在包（cn.molu.generator）
     * @param type             生成类型（java/entity.java、vue3/vue2Page.vue3...）
     */
    String generateCode(List<ColumnDetail> columnDetailList, String delPrefix, String packageName, String type);

    /**
     * 生成代码
     */
    void generateCode ();
}
