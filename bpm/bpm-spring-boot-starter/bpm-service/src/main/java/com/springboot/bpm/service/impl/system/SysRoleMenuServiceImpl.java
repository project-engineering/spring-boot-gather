package com.springboot.bpm.service.impl.system;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.api.param.system.menu.SaveMenuParam;
import com.springboot.bpm.entity.system.SysRoleMenu;
import com.springboot.bpm.mapper.system.SysRoleMenuMapper;
import com.springboot.bpm.service.system.SysRoleMenuService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {
    @Resource
    private SysRoleMenuMapper mapper;

    @Override
    public List<SysRoleMenu> getRoleMenuListByRoleIdList(List<String> roleIdList) {
        LambdaQueryWrapper<SysRoleMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SysRoleMenu::getRoleId, roleIdList);
        List<SysRoleMenu> roleMenuList = mapper.selectList(queryWrapper);
        if (ObjectUtil.isNotEmpty(roleMenuList)) {
            return roleMenuList;
        }
        return null;
    }

    @Override
    public void saveRoleMenu(SaveMenuParam saveMenuParam) {
        // 先删除原有角色菜单关系
        LambdaQueryWrapper<SysRoleMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysRoleMenu::getRoleId, saveMenuParam.getRoleId());
        mapper.delete(queryWrapper);
        List<String> menuIdList = saveMenuParam.getMenuIdList();
        // 保存新的角色菜单关系
        for (String menuId : menuIdList) {
            SysRoleMenu roleMenu = new SysRoleMenu();
            roleMenu.setRoleMenuId(IdUtil.simpleUUID());
            roleMenu.setRoleId(saveMenuParam.getRoleId());
            roleMenu.setMenuId(menuId);
            mapper.insert(roleMenu);
        }
    }
}
