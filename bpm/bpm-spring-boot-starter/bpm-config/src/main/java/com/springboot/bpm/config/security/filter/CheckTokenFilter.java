package com.springboot.bpm.config.security.filter;

import cn.hutool.core.util.ObjectUtil;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.springboot.bpm.common.exception.CustomerAuthenticationException;
import com.springboot.bpm.common.util.JwtUtil;
import com.springboot.bpm.config.security.handler.LoginFailureHandler;
import com.springboot.bpm.entity.system.user.SysUser;
import com.springboot.bpm.service.impl.system.security.CustomerUserDetailsService;
import jakarta.annotation.Resource;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Log4j2
@Component
public class CheckTokenFilter extends OncePerRequestFilter {
    @Resource
    private JwtUtil jwtUtil;
    @Resource
    private CustomerUserDetailsService customerUserDetailsService;
    @Resource
    private LoginFailureHandler loginFailureHandler;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            // 获取请求的url
            String url = request.getRequestURI();
            // 白名单
            List<String> ignoreUrls = new ArrayList<>();
            ignoreUrls.add("/bpm/api/login/login");
            ignoreUrls.add("/bpm/api/login/getCaptcha");
            ignoreUrls.add("/bpm/api/user/getUserInfo");
            ignoreUrls.add("/bpm/api/menu/getMenuList");
            ignoreUrls.add("/bpm/doc.html");
            ignoreUrls.add("/bpm/v3/api-docs/swagger-config");
            // 添加正则表达式匹配webjars路径
            String webjarsPattern = "/bpm/webjars/.*";
            String apiDocsPattern = "/bpm/v3/api-docs/.*";
            // 判断是否在白名单中
            if (!ignoreUrls.contains(url) && !url.matches(webjarsPattern) && !url.matches(apiDocsPattern)) {
                // 验证token
                validateToken(request);
            }
        } catch (AuthenticationException e) {
            loginFailureHandler.commence(request,response,e);
            return;
        }
        filterChain.doFilter(request, response);
    }

    protected void validateToken(HttpServletRequest request) {
        log.info("开始验证token：{}",request.getRequestURI());
        // 从请求头中获取token
        String token = request.getHeader("token");
        // 判断token是否为空
        if (ObjectUtil.isEmpty(token)) {
            token = request.getParameter("token");
        }
        // 判断token是否为空
        if (ObjectUtil.isEmpty(token)) {
            throw new CustomerAuthenticationException("请传递token");
        }
        // token验证逻辑
        if (!jwtUtil.verify(token)) {
            throw new CustomerAuthenticationException("非法的token");
        }
        // 解析token
        DecodedJWT decodedJWT = jwtUtil.decode(token);
        Map<String, Claim> claims = decodedJWT.getClaims();
        String username = claims.get("username").asString();
        // 用户认证
        SysUser sysUser = (SysUser) customerUserDetailsService.loadUserByUsername(username);
        Collection<? extends GrantedAuthority> authorities = sysUser.getAuthorities();
        log.info("用户权限: {}", authorities);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(sysUser, null, authorities);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        // 将用户认证信息存入SecurityContextHolder
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("token验证成功");
    }
}
