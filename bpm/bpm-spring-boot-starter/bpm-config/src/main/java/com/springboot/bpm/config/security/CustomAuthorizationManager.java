package com.springboot.bpm.config.security;

import cn.hutool.core.util.ObjectUtil;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.springboot.bpm.common.exception.CustomerAuthenticationException;
import com.springboot.bpm.common.util.JwtUtil;
import com.springboot.bpm.entity.system.user.SysUser;
import com.springboot.bpm.service.impl.system.security.CustomerUserDetailsService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Component
@Log4j2
public class CustomAuthorizationManager implements AuthorizationManager<RequestAuthorizationContext> {
    @Resource
    private JwtUtil jwtUtil;
    @Resource
    private CustomerUserDetailsService customerUserDetailsService;

    /**
     * 自定义权限校验
     *
     * @param authentication
     * @param object
     * @return
     */
    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        // 获取访问url
        String requestURI = object.getRequest().getRequestURI();
        // 白名单
        List<String> ignoreUrls = new ArrayList<>();
        ignoreUrls.add("/bpm/api/login/login");
        ignoreUrls.add("/bpm/api/login/getCaptcha");
        ignoreUrls.add("/bpm/api/user/getUserInfo");
        ignoreUrls.add("/bpm/api/menu/getMenuList");
        ignoreUrls.add("/bpm/doc.html");
        ignoreUrls.add("/bpm/v3/api-docs/swagger-config");
        // 添加正则表达式匹配webjars路径
        String webjarsPattern = "/bpm/webjars/.*";
        String apiDocsPattern = "/bpm/v3/api-docs/.*";
        // 判断是否是需要权限的url
        if (ignoreUrls.contains(requestURI) || requestURI.matches(webjarsPattern) || requestURI.matches(apiDocsPattern)) {
            // 白名单，不需要权限
            log.info("白名单，不需要权限：" + requestURI);
            return new AuthorizationDecision(true); // 返回有权限
        }
        // 从数据库或者缓存里面查询拥有当前URI的权限的角色
        String[] allRole = query(object);
        // 获取当前用户权限
        Collection<? extends GrantedAuthority> authorities = authentication.get().getAuthorities();
        // 判断是否拥有权限
        for (String role : allRole) {
            for (GrantedAuthority r : authorities) {
                if (role.equals(r.getAuthority())) {
                    return new AuthorizationDecision(true); // 返回有权限
                }
            }
        }
        return new AuthorizationDecision(false); //返回没有权限
    }
    /**
     * 查询当前拥有对应url的权限的角色
     *
     * @param object
     * @return
     */
    private String[] query(RequestAuthorizationContext object) {
        // 从请求头中获取token
        String token = object.getRequest().getHeader("token");
        // 判断token是否为空
        if (ObjectUtil.isEmpty(token)) {
            token = object.getRequest().getParameter("token");
        }
        // 判断token是否为空
        if (ObjectUtil.isEmpty(token)) {
            throw new CustomerAuthenticationException("请传递token");
        }
        // token验证逻辑
        if (!jwtUtil.verify(token)) {
            throw new CustomerAuthenticationException("非法的token");
        }
        // 解析token
        DecodedJWT decodedJWT = jwtUtil.decode(token);
        Map<String, Claim> claims = decodedJWT.getClaims();
        String username = claims.get("username").asString();
        // 用户认证
        SysUser sysUser = (SysUser) customerUserDetailsService.loadUserByUsername(username);
        Collection<? extends GrantedAuthority> authorities = sysUser.getAuthorities();
        List<String> roles = authorities.stream().map(GrantedAuthority::getAuthority).toList();
        return roles.toArray(new String[0]);
    }
}