package com.springboot.bpm.config.security;
import com.springboot.bpm.config.security.filter.CheckTokenFilter;
import com.springboot.bpm.config.security.handler.CustomAccessDeniedHandler;
import com.springboot.bpm.config.security.handler.LoginFailureHandler;
import com.springboot.bpm.service.impl.system.security.CustomerUserDetailsService;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import java.util.List;

/**
 * @Configuration 注解表示将该类以配置类的方式注册到spring容器中
 */
@Configuration
/**
 * @EnableWebSecurity 注解表示启动spring security
 */
@EnableWebSecurity
/**
 * @EnableMethodSecurity 注解表示启动全局函数权限
 */
@EnableMethodSecurity
public class WebSecurityConfig {

    /**
     * 权限不足处理逻辑
     */
    @Resource
    private CustomAccessDeniedHandler accessDeniedHandler;

    /**
     * 未授权处理逻辑
     */
    @Resource
    private CustomAuthenticationEntryPoint authenticationEntryPoint;

    /**
     * 访问统一处理器
     */
    @Resource
    private CheckTokenFilter checkTokenFilter;

    /**
     * 自定义权限校验逻辑
     */
    @Resource
    private CustomAuthorizationManager customAuthorizationManager;
    @Resource
    private CustomerUserDetailsService customerUserDetailsService;
    @Resource
    private LoginFailureHandler loginFailureHandler;

    /**
     * spring security的核心过滤器链
     *
     * @param httpSecurity
     * @return
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        // 定义安全请求拦截规则
        httpSecurity.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry -> {
                    authorizationManagerRequestMatcherRegistry
                            .requestMatchers("/bpm/api/login/getCaptcha"
                                    ,"/bpm/api/login/login"
                                    ,"/bpm/api/user/getUserInfo"
                                    ,"/bpm/api/menu/getMenuList"
                                    ,"/bpm/doc.html"
                                    ,"/bpm/v3/api-docs/**"
                                    ,"/bpm/webjars/**")
                            .permitAll() // 接口放行，不进行权限校验
                            .anyRequest()
                            // .hasRole() 其他接口不进行role具体校验，进行动态权限校验
                            .access(customAuthorizationManager); // 动态权限校验逻辑
                })
                // 禁用默认的登录页面
                .formLogin(AbstractHttpConfigurer::disable)
                // 禁用默认的logout页面
                .logout(AbstractHttpConfigurer::disable)
                // 前后端分离，关闭csrf
                .csrf(AbstractHttpConfigurer::disable)
                .cors(httpSecurityCorsConfigurer -> {
                    httpSecurityCorsConfigurer.configurationSource(corsConfigurationSource());
                })
                // 前后端分离架构禁用session
                .sessionManagement(httpSecuritySessionManagementConfigurer -> {
                    httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                })
                // 访问异常处理
                .exceptionHandling(httpSecurityExceptionHandlingConfigurer -> {
                    httpSecurityExceptionHandlingConfigurer.authenticationEntryPoint(loginFailureHandler) //匿名处理
                       .accessDeniedHandler(accessDeniedHandler);
                })
                // 未授权异常处理
                .exceptionHandling(httpSecurityExceptionHandlingConfigurer -> {
                    httpSecurityExceptionHandlingConfigurer.authenticationEntryPoint(authenticationEntryPoint);
                })
                .headers(httpSecurityHeadersConfigurer -> {
                    // 禁用缓存
                    httpSecurityHeadersConfigurer.cacheControl(HeadersConfigurer.CacheControlConfig::disable);
                    httpSecurityHeadersConfigurer.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable);
                });
        // 添加入口filter， 前后端分离的时候，可以进行token解析操作
        httpSecurity.addFilterBefore(checkTokenFilter, UsernamePasswordAuthenticationFilter.class);

        return httpSecurity.build();
    }

    /**
     * 明文密码加密
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 忽略权限校验
     *
     * @return
     */
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web -> web.ignoring().requestMatchers("/bpm/api/login/getCaptcha"
                ,"/bpm/api/login/login"
                ,"/bpm/api/user/getUserInfo"
                ,"/bpm/api/menu/getMenuList"
                ,"/bpm/doc.html"
                ,"/bpm/webjars/**"
                ,"/bpm/v3/api-docs/**"
                ,"/images/**"));
    }

    @Bean
    public AuthenticationManager authenticationManager(PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        //将编写的UserDetailsService注入进来
        provider.setUserDetailsService(customerUserDetailsService);
        //将使用的密码编译器加入进来
        provider.setPasswordEncoder(passwordEncoder);
        //将provider放置到AuthenticationManager 中
        return new ProviderManager(provider);
    }

    /**
     * 跨域配置
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(List.of("http://localhost:5173","http://127.0.0.1:5173")); // 替换为您的实际域名
        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(List.of("*"));
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}