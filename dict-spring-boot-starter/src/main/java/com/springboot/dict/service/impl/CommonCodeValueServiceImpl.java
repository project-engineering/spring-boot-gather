package com.springboot.dict.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.dict.entity.CommonCodeValue;
import com.springboot.dict.mapper.CommonCodeValueMapper;
import com.springboot.dict.common.param.AddCommonCodeValueParam;
import com.springboot.dict.common.param.DeleteCommonCodeValueParam;
import com.springboot.dict.common.param.QueryCodeValueListParam;
import com.springboot.dict.common.param.UpdateCommonCodeValueParam;
import com.springboot.dict.service.CommonCodeValueService;
import com.springboot.dict.common.util.PageResult;
import com.springboot.futool.UtilValidate;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 码值表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-07-17
 */
@Service
public class CommonCodeValueServiceImpl extends ServiceImpl<CommonCodeValueMapper, CommonCodeValue> implements CommonCodeValueService {
    @Resource
    private CommonCodeValueMapper commonCodeValueMapper;

    @Override
    public PageResult<CommonCodeValue> queryCodeValueList(QueryCodeValueListParam queryCodeValueListParam) {
        LambdaQueryWrapper<CommonCodeValue> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getCode())) {
            queryWrapper.eq(CommonCodeValue::getCode, queryCodeValueListParam.getCode());
        }
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getValue())) {
            queryWrapper.eq(CommonCodeValue::getValue, queryCodeValueListParam.getValue());
        }
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getName())) {
            queryWrapper.like(CommonCodeValue::getName, queryCodeValueListParam.getName());
        }
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getGroupCode())) {
            queryWrapper.eq(CommonCodeValue::getGroupCode, queryCodeValueListParam.getGroupCode());
        }
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getGroupName())) {
            queryWrapper.like(CommonCodeValue::getGroupName, queryCodeValueListParam.getGroupName());
        }
        if (UtilValidate.isNotEmpty(queryCodeValueListParam.getParentCode())) {
            queryWrapper.eq(CommonCodeValue::getParentCode, queryCodeValueListParam.getParentCode());
        }
        Page<CommonCodeValue> page = commonCodeValueMapper.selectPage(new Page<>(queryCodeValueListParam.getPageNo(), queryCodeValueListParam.getPageSize()), queryWrapper);
        return new PageResult<>(page);
    }

    @Override
    public void addCommonCodeValue(AddCommonCodeValueParam addCommonCodeValueParam) {
        CommonCodeValue commonCodeValue = BeanUtil.copyProperties(addCommonCodeValueParam, CommonCodeValue.class);
        commonCodeValueMapper.insert(commonCodeValue);
    }

    @Override
    public void updateCommonCodeValue(UpdateCommonCodeValueParam updateCommonCodeValueParam) {
        LambdaQueryWrapper<CommonCodeValue> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommonCodeValue::getCode, updateCommonCodeValueParam.getCode());
        CommonCodeValue commonCodeValue = commonCodeValueMapper.selectOne(queryWrapper);
        if (UtilValidate.isEmpty(commonCodeValue)) {
            throw new RuntimeException("该码值不存在");
        }
        commonCodeValueMapper.updateById(BeanUtil.copyProperties(updateCommonCodeValueParam, CommonCodeValue.class));
    }

    @Override
    public void deleteCommonCodeValue(DeleteCommonCodeValueParam deleteCommonCodeValueParam) {
        commonCodeValueMapper.deleteById(deleteCommonCodeValueParam.getCode());
    }

    @Override
    public List<CommonCodeValue> selectAllByAsc(String[] columns) {
        QueryWrapper<CommonCodeValue> ew = new QueryWrapper<>();
        ew.orderBy(true,true, Arrays.asList(columns));
        List<CommonCodeValue> list = commonCodeValueMapper.selectList(ew);
        return list;
    }

    @Override
    public List<CommonCodeValue> selectGroupByAndAsc(String[] groupBy, String[] columns) {
        QueryWrapper<CommonCodeValue> ew = new QueryWrapper<>();
        ew.select("group_code", "group_name");
        ew.groupBy(Arrays.asList(groupBy));
        ew.orderBy(true,true, Arrays.asList(columns));
        List<CommonCodeValue> list = commonCodeValueMapper.selectList(ew);
        return list;
    }
}
