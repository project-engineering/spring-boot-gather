package com.springboot.dict.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.dict.entity.CommonCodeValue;
import com.springboot.dict.common.param.AddCommonCodeValueParam;
import com.springboot.dict.common.param.DeleteCommonCodeValueParam;
import com.springboot.dict.common.param.QueryCodeValueListParam;
import com.springboot.dict.common.param.UpdateCommonCodeValueParam;
import com.springboot.dict.common.util.PageResult;

import java.util.List;

/**
 * <p>
 * 码值表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-07-17
 */
public interface CommonCodeValueService extends IService<CommonCodeValue> {

    PageResult<?> queryCodeValueList(QueryCodeValueListParam queryCodeValueListParam);

    void addCommonCodeValue(AddCommonCodeValueParam addCommonCodeValueParam);

    void updateCommonCodeValue(UpdateCommonCodeValueParam updateCommonCodeValueParam);

    void deleteCommonCodeValue(DeleteCommonCodeValueParam deleteCommonCodeValueParam);

    List<CommonCodeValue> selectAllByAsc(String[] strings);

    List<CommonCodeValue> selectGroupByAndAsc(String[] groupBy, String[] strings);
}
