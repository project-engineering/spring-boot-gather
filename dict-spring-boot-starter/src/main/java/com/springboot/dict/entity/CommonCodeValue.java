package com.springboot.dict.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 码值表
 * </p>
 *
 * @author liuc
 * @since 2024-07-17
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("common_code_value")
@Schema(title = "码值表")
public class CommonCodeValue {

    @Schema(description = "码值小类key")
    @TableId("`code`")
    private String code;

    @Schema(description = "码值小类value")
    @TableField("`value`")
    private String value;

    @Schema(description = "码值小类中文说明")
    @TableField("`name`")
    private String name;

    @Schema(description = "码值大类key")
    @TableField("group_code")
    private String groupCode;

    @Schema(description = "码值大类中文说明")
    @TableField("group_name")
    private String groupName;

    @Schema(description = "码值小类key父id")
    @TableField("parent_code")
    private String parentCode;

    @Schema(description = "状态:0-无效,1-有效")
    @TableField("status_id")
    private String statusId;

    @Schema(description = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
