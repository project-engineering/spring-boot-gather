package com.springboot.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.dict.entity.CommonCodeValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 码值表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-07-17
 */
@Mapper
public interface CommonCodeValueMapper extends BaseMapper<CommonCodeValue> {

}
