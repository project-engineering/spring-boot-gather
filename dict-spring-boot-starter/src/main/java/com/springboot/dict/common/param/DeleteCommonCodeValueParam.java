package com.springboot.dict.common.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author liuc
 * @date 2024-07-18 14:43
 */
@Data
@Schema(description = "删除通用代码值参数")
public class DeleteCommonCodeValueParam {
    @Schema(description = "码值小类key")
    private String code;
}
