package com.springboot.dict.common.annotation;

import com.springboot.dict.common.validator.EnumValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 校验枚举类值注解
 */
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = {EnumValidator.class})
public @interface VerifyEnum {

    /**
     * 枚举校验使用的类
     */
    Class<? extends Enum<?>> enumClass();
    /**
     * 枚举校验使用的方法名
     */
    String enumMethod();
    /**
     * 错误消息
     **/
    String message() default "";
    /**
     * 验证组
     */
    Class<?>[] groups() default { };
    /**
     * 负载
     */
    Class<? extends Payload>[] payload() default {};
}
