package com.springboot.dict.common.enums;

import org.apache.commons.lang3.ObjectUtils;

/**
 * 状态枚举类
 *
 * @author liuc
 */
public enum StatusIdEnum {
    INACTIVE("0", "无效"),
    ACTIVE("1", "有效");

    private String code;
    private String desc;

    StatusIdEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 获取状态描述
     * @param code
     * @return
     */
    public static String getDescByCode(String code) {
        for (StatusIdEnum statusIdEnum : StatusIdEnum.values()) {
            if (statusIdEnum.code == code) {
                return statusIdEnum.desc;
            }
        }
        return null;
    }

    public static boolean isValueValid(String code) {
        if(!ObjectUtils.isEmpty(code)){
            for (StatusIdEnum enumObj : StatusIdEnum.values()) {
                if (enumObj.getCode().equals(code)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
}
