package com.springboot.dict.common.param;

import com.springboot.dict.common.annotation.VerifyEnum;
import com.springboot.dict.common.enums.StatusIdEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author liuc
 * @date 2024-07-18 14:43
 */
@Data
@Schema(description = "添加通用代码值参数")
public class AddCommonCodeValueParam {
    @Schema(description = "码值小类key")
    @NotBlank(message = "码值小类key不能为空")
    private String code;

    @Schema(description = "码值小类value")
    @NotBlank(message = "码值小类value不能为空")
    private String value;

    @Schema(description = "码值小类中文说明")
    @NotBlank(message = "码值小类中文说明不能为空")
    private String name;

    @Schema(description = "码值大类key")
    @NotBlank(message = "码值大类key不能为空")
    private String groupCode;

    @Schema(description = "码值大类中文说明")
    @NotBlank(message = "码值大类中文说明不能为空")
    private String groupName;

    @Schema(description = "父级码值key")
    private String parentCode;

    @Schema(description = "状态:0-无效,1-有效")
    @VerifyEnum(enumClass = StatusIdEnum.class, enumMethod = "isValueValid", message = "statusId可填值只能为[0,1]")
    private String statusId;
}
