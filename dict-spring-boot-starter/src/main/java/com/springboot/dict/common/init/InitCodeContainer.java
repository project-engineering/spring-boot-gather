package com.springboot.dict.common.init;

import com.springboot.dict.entity.CommonCodeValue;
import com.springboot.dict.common.exception.BusinessException;
import com.springboot.dict.service.CommonCodeValueService;
import com.springboot.futool.UtilValidate;
import com.springboot.futool.exception.BusinessErrorCode;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 项目启动后初始化码值
 */
@Configuration
@Log4j2
public class InitCodeContainer {
    private static final String MODULE = InitCodeContainer.class.getName();

    @Resource
    public CommonCodeValueService commonCodeValueService;

    @Bean
    @DependsOn({"dataSourceInitializer"})
    public Object init(){
        log.info(">>>>>>>>>>>>>>>系统启动中。。。初始化码值开始<<<<<<<<<<<<<");
        //获取开始时间
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            List<CommonCodeValue> queryList = commonCodeValueService.selectAllByAsc(new String[] {"group_code","value"});
            String[] groupBy = new String[]{"group_code","group_name"};
            List<CommonCodeValue> queryListDistinct = commonCodeValueService.selectGroupByAndAsc(groupBy,new String[] {"group_code"});
            for (CommonCodeValue tempOut : queryListDistinct) {
                String groupCode = tempOut.getGroupCode();
                String groupName = tempOut.getGroupName();
                Map<String,Object> codeTypeLinkCodeKeyMap = new ConcurrentHashMap<String,Object>(8);
                Map<String,Object> codeTypeLinkCodeKeyNameMap = new ConcurrentHashMap<String,Object>(8);
                Map<String,Object> codeTypeLinkValueKeyNameMap = new ConcurrentHashMap<String,Object>(8);

                Predicate<CommonCodeValue> predicate = new Predicate<CommonCodeValue>() {
                    @Override
                    public boolean evaluate(CommonCodeValue gv) {
                        String oneGroupCode = gv.getGroupCode();
                        return oneGroupCode != null && oneGroupCode.equals(groupCode);
                    }
                };

                List<CommonCodeValue> queryListByCodeTypeKey = (List<CommonCodeValue>) CollectionUtils.select(queryList, predicate);
                for (CommonCodeValue tempIn : queryListByCodeTypeKey) {
                    String code = tempIn.getCode();
                    String value = tempIn.getValue();
                    String name = tempIn.getName();
                    String groupCode1 = tempIn.getGroupCode();
                    //避免码值大类key值和码值小类key值，以及小类的value值为空造成在取该码值时造成的空指针问题，在此处加载码值时抛出异常由catch中的UnifyException捕获
                    if (UtilValidate.isEmpty(groupCode)
                            || UtilValidate.isEmpty(groupCode1)
                            || UtilValidate.isEmpty(value)
                            || UtilValidate.isEmpty(name)) {
                        throw new BusinessException(BusinessErrorCode.CSRCB30006.getCode(), BusinessErrorCode.CSRCB30006.getMsg());
                    }
                    addValueCodeTypeLinkCodeKey(codeTypeLinkCodeKeyMap,groupCode,groupCode1,code,value);
                    addCodeTypeLinkCodeKeyName(codeTypeLinkCodeKeyNameMap,groupCode,groupCode1,code,name);
                    addCodeTypeLinkValueKeyName(codeTypeLinkValueKeyNameMap,groupCode,groupCode1,value,name);
                }
                if (!codeTypeLinkCodeKeyMap.isEmpty()) {
                    CodeContainer.CODE_TYPE_LINK_CODE_KEY.put(groupCode, codeTypeLinkCodeKeyMap);
                }
                if (!codeTypeLinkCodeKeyNameMap.isEmpty()) {
                    CodeContainer.CODE_TYPE_LINK_CODE_KEY_NAME.put(groupCode, codeTypeLinkCodeKeyNameMap);
                }
                if (!codeTypeLinkValueKeyNameMap.isEmpty()) {
                    CodeContainer.CODE_TYPE_LINK_VALUE_KEY_NAME.put(groupCode, codeTypeLinkValueKeyNameMap);
                }
                if (!CodeContainer.CODE_TYPE_CONTAINER.containsKey(groupCode)) {
                   CodeContainer.CODE_TYPE_CONTAINER.put(groupCode, groupName);
                }
            }
        } catch (Exception e){
            log.error(e.getMessage(),MODULE);
        }
        //获取结束时间
        stopWatch.stop();
        log.info("码值容器加载时间：{}毫秒！",stopWatch.getTotalTimeMillis());
        log.info(">>>>>>>>>>>>>>>系统启动中。。。初始化码值完成<<<<<<<<<<<<<");
        return null;
    }

    @PreDestroy
    public void destroy(){
        System.out.println("系统运行结束");
    }

    /**
     * 向 码值的大类 【码值小类关联key-value】 关联中添加 【码值小类关联key-value】的对应关系
     * @param loopMap	循环中定义的 Map【码值小类关联key-value】
     * @param codeTypeKey	循环第一层大类Type值
     * @param codeTypeKeyIn	循环第二层大类Type值
     * @param codeTypeIdKeyIn	循环第二层小类key值
     * @param codeTypeIdValueIn 循环第二层小类Value值
     */
    private static void addValueCodeTypeLinkCodeKey(Map<String, Object> loopMap, String codeTypeKey, String codeTypeKeyIn,
                                                    String codeTypeIdKeyIn, String codeTypeIdValueIn) {
        if (!CodeContainer.CODE_TYPE_LINK_CODE_KEY.containsKey(codeTypeKey)) {
            if (UtilValidate.areEqual(codeTypeKey, codeTypeKeyIn)) {
                loopMap.put(codeTypeIdKeyIn, codeTypeIdValueIn);
            }
        }
    }

    /**
     * 向 码值的大类 【码值小类关联key-name】 关联中添加 【码值小类关联key-name】的对应关系
     * @param loopMap 循环中定义的 Map【码值小类关联key-name】
     * @param codeTypeKey 循环第一层大类Type值
     * @param codeTypeKeyIn 循环第二层大类Type值
     * @param codeTypeIdKeyIn 循环第二层小类key值
     * @param codeTypeNameIn 循环第二层小类key值的中文描述
     */
    private static void addCodeTypeLinkCodeKeyName(Map<String, Object> loopMap, String codeTypeKey, String codeTypeKeyIn,
                                                   String codeTypeIdKeyIn, String codeTypeNameIn) {
        if (!CodeContainer.CODE_TYPE_LINK_CODE_KEY_NAME.containsKey(codeTypeKey)) {
            if (UtilValidate.areEqual(codeTypeKey, codeTypeKeyIn)) {
                loopMap.put(codeTypeIdKeyIn, codeTypeNameIn);
            }
        }
    }


    /**
     * 码值的大类 【码值小类关联value-name】联中添加 【码值小类关联value-name】的对应关系
     * @param loopMap	循环中定义的 Map【码值小类关联value-name】
     * @param codeTypeKey	循环第一层大类Type值
     * @param codeTypeKeyIn	循环第二层大类Type值
     * @param codeTypeIdValueIn	循环第二层小类value值
     * @param codeTypeNameIn	循环第二层小类key值的中文描述
     */
    private static void addCodeTypeLinkValueKeyName(Map<String, Object> loopMap, String codeTypeKey, String codeTypeKeyIn, String codeTypeIdValueIn, String codeTypeNameIn) {
        if (!CodeContainer.CODE_TYPE_LINK_VALUE_KEY_NAME.containsKey(codeTypeKey)) {
            if (UtilValidate.areEqual(codeTypeKey, codeTypeKeyIn)) {
                loopMap.put(codeTypeIdValueIn, codeTypeNameIn);
            }
        }
    }
}
