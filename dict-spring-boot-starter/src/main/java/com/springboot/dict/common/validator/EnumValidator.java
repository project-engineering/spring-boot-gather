package com.springboot.dict.common.validator;

import com.springboot.dict.common.annotation.VerifyEnum;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * 注解@VerifyEnum 处理逻辑方法
 */
@Log4j2
public class EnumValidator implements ConstraintValidator<VerifyEnum, Object> {

    private Class<? extends Enum<?>> enumClass;
    private String enumMethod;

    /**
     * 初始化方法
     * @param constraintAnnotation 注解
     */
    @Override
    public void initialize(VerifyEnum constraintAnnotation) {
        enumMethod = constraintAnnotation.enumMethod();
        enumClass = constraintAnnotation.enumClass();
    }

    /**
     * 校验方法
     * @param o 待校验对象
     * @param constraintValidatorContext 校验上下文
     * @return 校验结果
     */
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        // 值没传的情况，直接返回true
        if (ObjectUtils.isEmpty(o)) return Boolean.TRUE;
        if (enumClass == null || ObjectUtils.isEmpty(enumMethod)) return Boolean.TRUE;

        Class<?> vclass = o.getClass();
        try {
            // 反射机制获取具体的校验方法
            Method method = enumClass.getMethod(enumMethod, vclass);
            if (!Boolean.TYPE.equals(method.getReturnType()) && !Boolean.class.equals(method.getReturnType())) {
                throw new RuntimeException("校验方法不是布尔类型!");
            }
            if (!Modifier.isStatic(method.getModifiers())) {
                throw new RuntimeException("校验方法不是静态方法!");
            }
            method.setAccessible(true);
            // 调用具体的方法
            Boolean res = (Boolean) method.invoke(null, o);
            return res != null && res;
        } catch (NoSuchMethodException e) {
            log.error("NoSuchMethodException: 类 {} 中不存在方法 {}", enumClass.getName(), enumMethod, e);
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            log.error("IllegalAccessException: 无法访问方法 {}", enumMethod, e);
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            log.error("InvocationTargetException: 方法 {} 调用失败", enumMethod, e);
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            log.error("NullPointerException: 参数或对象为 null", e);
            throw new RuntimeException(e);
        }
    }
}