package com.springboot.dict.common.handler;

import com.springboot.futool.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 字段自动填充
 */
@Slf4j
@Component
public class MetaObjectHandler implements com.baomidou.mybatisplus.core.handlers.MetaObjectHandler {
    /**
     *   insert操作时填充方法
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        //填充 创建时间
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, DateUtil.getCurrLocalDateTime());
    }

    /**
     * update操作时填充方法
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        //填充 更新时间
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, DateUtil.getCurrLocalDateTime());
    }
}