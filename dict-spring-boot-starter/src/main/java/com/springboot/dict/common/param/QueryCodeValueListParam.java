package com.springboot.dict.common.param;

import com.springboot.dict.common.util.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author liuc
 * @date 2024-07-17 18:37
 */
@Data
@Schema(description = "查询代码值列表参数")
public class QueryCodeValueListParam extends Page {
    @Schema(description = "码值小类key")
    private String code;

    @Schema(description = "码值小类value")
    private String value;

    @Schema(description = "码值小类中文说明")
    private String name;

    @Schema(description = "码值大类key")
    private String groupCode;

    @Schema(description = "码值大类中文说明")
    private String groupName;

    @Schema(description = "父级码值key")
    private String parentCode;
}
