package com.springboot.dict.common.util;

import com.springboot.dict.common.enums.ResultCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuc
 * @date 2024-07-17 18:41
 */
@Data
@Schema(description = "通用返回结果")
public class Result<T> implements Serializable {
    @Schema(description = "返回码")
    private String code;

    @Schema(description = "返回消息")
    private String message;

    @Schema(description = "是否成功")
    private boolean success;

    @Schema(description = "全局唯一ID")
    private String traceId;

    @Schema(description = "返回数据")
    private T data;

    @Schema(description = "时间戳")
    private long timestamp;

    public Result() {
    }

    Result(ResultCode resultCode)  {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.timestamp = System.currentTimeMillis();
    }

    private Result(String code, String message)  {
        this.code = code;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
    }

    private Result(T data, String code, String message) {
        this.code = code;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
        this.data = data;
    }

    private Result( T data, ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.timestamp = System.currentTimeMillis();
        this.data = data;
    }

    private void setResultCode(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public static <T> Result<T> success() {
        return new Result(ResultCode.SUCCESS);
    }

    public static <T> Result<T> success(T data) {
        Result<T> result = new Result(ResultCode.SUCCESS);
        result.data = data;
        return result;
    }

    public static <T> Result<T> success(T data, String message) {
        Result<T> result = new Result(ResultCode.SUCCESS.getCode(), message);
        result.data = data;
        return result;
    }
    public static <T> Result<T> success(PageResult<T> pageResult) {
        return new PageResult(pageResult);
    }

    public static <T> Result<T> success(int pageNo,int pageSize,long total, T data) {
        PageResult<T> pageResult = new PageResult<>(pageNo, pageSize, total, data);
        return pageResult;
    }

    public static <T> Result<T> fail() {
        return new Result(ResultCode.SYSTEM_ERROR);
    }

    public static Result fail(ResultCode resultCode) {
        Result result = new Result();
        result.setSuccess(false);
        result.setResultCode(resultCode);
        return result;
    }

    public static <T> Result<T> fail(String message) {
        return new Result(ResultCode.SYSTEM_ERROR.getCode(), message);
    }

    public static <T> Result<T> fail(String code, String message) {
        return new Result(code, message);
    }

    public static <T> Result<T> result(ResultCode code) {
        return new Result(code.getCode(), code.getMessage());
    }

    public static <T> Result<T> result(T data, ResultCode code) {
        Result<T> result = new Result(code.getCode(), code.getMessage());
        result.data = data;
        return result;
    }

    public static <T> Result<T> result(T data, String code, String message) {
        Result<T> result = new Result(code, message);
        result.data = data;
        return result;
    }

    public boolean isSuccess() {
        return ResultCode.SUCCESS.getCode().equals(this.code);
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public T getData() {
        return this.data;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getTraceId() {
        return this.traceId;
    }

    public Result<T> setTraceId(String traceId) {
        this.traceId = traceId;
        return this;
    }
}

