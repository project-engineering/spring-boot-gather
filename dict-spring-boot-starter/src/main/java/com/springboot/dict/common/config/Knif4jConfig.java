package com.springboot.dict.common.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author liuc
 * @date 2024-07-18 9:53
 */
@Configuration
public class Knif4jConfig extends WebMvcConfigurationSupport {

    /**
     * 设置静态资源映射
     *
     * @param registry
     */
    protected void addResourceHandlers (ResourceHandlerRegistry registry) {
        registry.addResourceHandler ("/doc.html").addResourceLocations ("classpath:/META-INF/resources/");
        registry.addResourceHandler ("/webjars/**").addResourceLocations ("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public OpenAPI apiInfo () {
        return new OpenAPI ()
                .info (new Info()
                        .title ("码值配置中心服务")
                        .version ("1.0.0")
                        .description ("码值配置中心服务API文档")
                        .license (new License().name ("Apache 2.0")
                                .url ("http://www.yang.com/"))
                );
    }
}
