package com.springboot.dict.common.config;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.util.StopWatch;


/**
 * 初始化数据库配置类
 */
@Configuration
@Log4j2
public class InitDataSourceConfig {
    private static final String MODULE = InitDataSourceConfig.class.getName();
    @Resource
    HikariDataSource dataSource;

    /**
     * 初始化数据库表及表数据
     * @throws
     */
    @Bean("dataSourceInitializer")
    public DataSourceInitializer dataSourceInitializer() {
        log.info("初始化数据库");
        //获取开始时间
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final DataSourceInitializer initializer = new DataSourceInitializer();
        // 设置数据源
        initializer.setDataSource(dataSource);
        log.info("初始化数据库完成");
        //获取结束时间
        stopWatch.stop();
        log.info("初始化数据库完成："+stopWatch.getTotalTimeMillis()+"毫秒！",MODULE);
        return initializer;
    }
}
