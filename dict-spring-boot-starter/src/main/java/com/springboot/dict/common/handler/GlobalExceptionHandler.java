package com.springboot.dict.common.handler;

import com.springboot.dict.common.enums.ResultCode;
import com.springboot.dict.common.exception.BusinessException;
import com.springboot.dict.common.util.Result;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.UnexpectedTypeException;
import jakarta.validation.ValidationException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.yaml.snakeyaml.constructor.DuplicateKeyException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.zip.DataFormatException;

/**
 * 全局异常处理类
 * @RestControllerAdvice(@ControllerAdvice)，拦截异常并统一处理
 */
@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义的业务异常
     * @param e	异常对象
     * @return	错误结果
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public Result businessExceptionHandler(BusinessException e) {
        log.error("发生业务异常，原因是: ", e);
        String code = e.getCode();
        if (StringUtils.isEmpty(code)) {
            code = ResultCode.BUSINESS_ERROR.getCode();
        }
        String message = e.getMessage();
        if (StringUtils.isEmpty(message)) {
            message = ResultCode.BUSINESS_ERROR.getMessage();
        }
        return Result.fail(code, message);
    }

    /**
     * 绑定异常
     */
    @ExceptionHandler({BindException.class})
    public Result handleBindException(BindException e) {
        log.error("参数绑定异常，原因是: ", e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return Result.fail(ResultCode.PARAMETER_BIND_ERROR.getCode(), message);
    }

    /**
     * 处理参数绑定异常
     * @param e	异常对象
     * @return	错误结果
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        String message = Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage();
        return Result.fail(ResultCode.PARAMETER_VALID_ERROR.getCode(), message);
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public Result handleRuntimeException(RuntimeException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生未知异常.", requestURI, e);
        return Result.fail(ResultCode.UNKUOW_ERROR.getCode(),e.getMessage());
    }

    /**
     * 拦截参数校验异常
     */
    @ExceptionHandler(ValidationException.class)
    public Result handleValidationException(ValidationException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生参数校验异常,原因：", requestURI,e);
        return Result.fail(ResultCode.PARAMETER_VALID_ERROR.getCode(),e.getCause().getMessage());
    }

    /**
     * 拦截参数缺失异常
     * @param e 异常对象
     * @param request 请求对象
     * @return 错误结果
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result handleMissingServletRequestParameterException(MissingServletRequestParameterException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生参数缺失异常,原因：", requestURI, e);
        return Result.fail(ResultCode.PARAMETER_MISSING_ERROR.getCode(),e.getMessage());
    }

    /**
     * 处理未知异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        log.error("发生未知异常，原因是：",e);
        return Result.fail(ResultCode.UNKUOW_ERROR);
    }

    /**
     * 处理请求方法不支持异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result handleMethodArgumentNotValidException(HttpRequestMethodNotSupportedException e) {
        log.error("请求方法不支持,原因是：",e);
        return Result.fail(ResultCode.CLIENT_HTTP_METHOD_ERROR);
    }

    /**
     * 处理重复键异常
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public Result handleDuplicateKeyException(DuplicateKeyException e){
        log.error("主键冲突异常，原因是：", e);
        return Result.fail(ResultCode.DUPLICATE_KEY_ERROR);
    }

    /**
     * 请求路径中缺少必需的路径变量
     */
    @ExceptionHandler(MissingPathVariableException.class)
    public Result handleMissingPathVariableException(MissingPathVariableException e, HttpServletRequest request) {
        log.error("请求路径中缺少必需的路径变量：path={}，error={}", request.getRequestURI(), e.getMessage());
        return Result.fail(ResultCode.PATH_VARIABLE_MISSING_ERROR.getCode(), String.format("请求路径中缺少必需的路径变量[%s]", e.getVariableName()));
    }

    /**
     * 客户端@RequestBody请求体JSON格式错误或字段类型错误
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Result handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error("客户端请求体JSON格式错误或字段类型不匹配，原因是：", e);
        return Result.fail(ResultCode.CLIENT_REQUEST_BODY_FORMAT_ERROR);
    }

    /**
     * 客户端@RequestParam参数校验不通过
     * 主要是未能通过Hibernate Validator校验的异常处理
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result handleConstraintViolationException(ConstraintViolationException e) {
        log.error("客户端请求参数校验不通过，原因是：", e);
        return Result.fail(ResultCode.CLIENT_REQUEST_PARAM_PARSE_ERROR);
    }

    /**
     * 通用的业务方法入参检查错误
     * @param e 异常对象
     * @return 错误结果
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    public Result handleIllegalArgumentException(IllegalArgumentException e) {
        log.error("客户端请求参数格式错误，原因是：", e);
        return Result.fail(ResultCode.SERVER_ILLEGAL_ARGUMENT_ERROR);
    }

    /**
     * 数据类型转换异常
     */
    @ExceptionHandler(value = ClassCastException.class)
    public Result handleClassCastException(ClassCastException e) {
        log.error("数据类型转换异常，原因是：", e);
        return Result.fail(ResultCode.DATA_TYPE_CONVERSION_ERROR);
    }

    /**
     * 非法访问异常
     */
    @ExceptionHandler(value = IllegalAccessException.class)
    public Result handleIllegalAccessException(IllegalAccessException e) {
        log.error("非法访问异常，原因是：", e);
        return Result.fail(ResultCode.ILLEGAL_ACCESS_ERROR);
    }

    /**
     * 数组下标越界异常
     */
    @ExceptionHandler(value = IndexOutOfBoundsException.class)
    public Result handleIndexOutOfBoundsException(IndexOutOfBoundsException e) {
        log.error("数组下标越界异常，原因是：", e);
        return Result.fail(ResultCode.ARRAY_INDEX_OUT_OF_BOUNDS_ERROR);
    }

    /**
     * 文件未找到异常
     */
    @ExceptionHandler(value = FileNotFoundException.class)
    public Result handleFileNotFoundException(FileNotFoundException e) {
        log.error("文件未找到异常，原因是：", e);
        return Result.fail(ResultCode.FILE_NOT_FOUND_ERROR);
    }

    /**
     * 数字格式异常
     */
    @ExceptionHandler(value = NumberFormatException.class)
    public Result handleNumberFormatException(NumberFormatException e) {
        log.error("数字格式异常，原因是：", e);
        return Result.fail(ResultCode.NUMBER_FORMAT_ERROR);
    }

    /**
     * 实例化异常
     */
    @ExceptionHandler(value = InstantiationException.class)
    public Result handleInstantiationException(InstantiationException e) {
        log.error("实例化异常，原因是：", e);
        return Result.fail(ResultCode.INSTANTIATION_ERROR);
    }

    /**
     * 安全异常
     */
    @ExceptionHandler(value = SecurityException.class)
    public Result handleSecurityException(SecurityException e) {
        log.error("安全异常，原因是：", e);
        return Result.fail(ResultCode.SECURITY_ERROR);
    }

    /**
     * 文件已结束异常
     */
    @ExceptionHandler(value = EOFException.class)
    public Result handleEOFException(EOFException e) {
        log.error("文件已结束异常，原因是：", e);
        return Result.fail(ResultCode.FILE_END_OF_STREAM_ERROR);
    }

    /**
     * 空指针异常
     */
    @ExceptionHandler(value = NullPointerException.class)
    public Result handleNullPointerException(NullPointerException e) {
        log.error("空指针异常，原因是：", e);
        return Result.fail(ResultCode.NULL_POINTER_ERROR);
    }

    /**
     * 方法不存在异常
     */
    @ExceptionHandler(value = NoSuchMethodError  .class)
    public Result handleNoSuchMethodError(NoSuchMethodError e) {
        log.error("方法不存在异常，原因是：", e);
        return Result.fail(ResultCode.NO_SUCH_METHOD_ERROR);
    }

    /**
     * SQL异常
     */
    @ExceptionHandler(value = SQLException.class)
    public Result handleSQLException(SQLException e) {
        log.error("SQL异常，原因是：", e);
        return Result.fail(ResultCode.SQL_ERROR);
    }

    /**
     * IO异常
     */
    @ExceptionHandler(value = IOException.class)
    public Result handleIOException(IOException e) {
        log.error("IO异常，原因是：", e);
        return Result.fail(ResultCode.IO_ERROR);
    }

    /**
     * 数据格式异常
     */
    @ExceptionHandler(value = DataFormatException.class)
    public Result handleDataFormatException(DataFormatException e) {
        log.error("数据格式异常，原因是：", e);
        return Result.fail(ResultCode.DATA_FORMAT_ERROR);
    }

    /**
     * 字段不存在异常
     */
    @ExceptionHandler(value = NoSuchFieldException.class)
    public Result handleNoSuchFieldException(NoSuchFieldException e) {
        log.error("字段不存在异常，原因是：", e);
        return Result.fail(ResultCode.NO_SUCH_FIELD_ERROR);
    }

    /**
     * 超时异常
     */
    @ExceptionHandler(value = TimeoutException.class)
    public Result handleTimeoutException(TimeoutException e) {
        log.error("超时异常，原因是：", e);
        return Result.fail(ResultCode.TIMEOUT_ERROR);
    }

    /**
     * 输入不匹配异常
     */
    @ExceptionHandler(value = InputMismatchException.class)
    public Result handleInputMismatchException(InputMismatchException e) {
        log.error("输入不匹配异常，原因是：", e);
        return Result.fail(ResultCode.INPUT_MISMATCH_ERROR);
    }

    @ExceptionHandler(value = UnexpectedTypeException.class)
    public Result handleUnexpectedTypeException(UnexpectedTypeException e) {
        log.error("类型不匹配异常，原因是：", e.getMessage());
        return Result.fail(ResultCode.UNEXPECTED_TYPE_ERROR);
    }
}
