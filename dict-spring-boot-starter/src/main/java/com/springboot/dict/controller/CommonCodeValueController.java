package com.springboot.dict.controller;

import com.springboot.dict.common.init.UtilCommonCode;
import com.springboot.dict.common.param.AddCommonCodeValueParam;
import com.springboot.dict.common.param.DeleteCommonCodeValueParam;
import com.springboot.dict.common.param.QueryCodeValueListParam;
import com.springboot.dict.common.param.UpdateCommonCodeValueParam;
import com.springboot.dict.service.CommonCodeValueService;
import com.springboot.dict.common.util.PageResult;
import com.springboot.dict.common.util.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 码值表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-07-17
 */
@Tag(name = "码值表前端控制器")
@RestController
@RequestMapping("/code")
@Log4j2
public class CommonCodeValueController {
    @Resource
    private CommonCodeValueService commonCodeValueService;

    @Operation(summary = "查询码值列表")
    @PostMapping("/queryCodeValueList")
    public Result<?> queryCodeValueList(@Valid @RequestBody QueryCodeValueListParam queryCodeValueListParam) {
        PageResult<?> pageResult = commonCodeValueService.queryCodeValueList(queryCodeValueListParam);
        return Result.success(pageResult);
    }

    @Operation(summary = "新增码值")
    @PostMapping("addCommonCodeValue")
    public Result<Void> addCommonCodeValue(@Valid @RequestBody AddCommonCodeValueParam addCommonCodeValueParam) {
        commonCodeValueService.addCommonCodeValue(addCommonCodeValueParam);
        return Result.success();
    }

    @Operation(summary = "修改码值")
    @PostMapping("/updateCommonCodeValue")
    public Result<Void> updateCommonCodeValue(@Valid @RequestBody UpdateCommonCodeValueParam updateCommonCodeValueParam) {
        commonCodeValueService.updateCommonCodeValue(updateCommonCodeValueParam);
        return Result.success();
    }

    @Operation(summary = "删除码值")
    @PostMapping("/deleteCommonCodeValue")
    public Result<Void> deleteCommonCodeValue(@Valid @RequestBody DeleteCommonCodeValueParam deleteCommonCodeValueParam) {
        commonCodeValueService.deleteCommonCodeValue(deleteCommonCodeValueParam);
        return Result.success();
    }

    @Operation(summary = "测试码值")
    @PostMapping("/test")
    public Result<String> test() {
        log.info("码值结果：{}",UtilCommonCode.getCodeValueByType("CD003200", "CD0032037"));
        return Result.success(UtilCommonCode.getCodeValueByType("CD003200", "CD0032037"));
    }
}
