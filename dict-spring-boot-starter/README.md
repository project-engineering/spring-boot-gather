## Spring Boot3 集成knif4j
### 引入依赖
```markdown
    <dependency>
      <groupId>com.github.xiaoymin</groupId>
      <artifactId>knife4j-openapi3-jakarta-spring-boot-starter</artifactId>
      <version>${knife4j-openapi3.version}</version>
    </dependency>
```
### 配置文件
```ymal
springdoc:
  swagger-ui:
    path: /swagger-ui.html
    tags-sorter: alpha
    operations-sorter: alpha
  api-docs:
    path: /v3/api-docs
  group-configs:
    - group: 'default'
      paths-to-match: '/**'
      packages-to-scan: com.springboot.config.controller
# knife4j的增强配置，不需要增强可以不配
knife4j:
  enable: true
  setting:
    language: zh_cn
```

### 配置类
```java
@Configuration
public class Knif4jConfig extends WebMvcConfigurationSupport {

    /**
     * 设置静态资源映射
     *
     * @param registry
     */
    protected void addResourceHandlers (ResourceHandlerRegistry registry) {
        registry.addResourceHandler ("/doc.html").addResourceLocations ("classpath:/META-INF/resources/");
        registry.addResourceHandler ("/webjars/**").addResourceLocations ("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public OpenAPI apiInfo () {
        return new OpenAPI ()
                .info (new Info()
                        .title ("码值配置中心服务")
                        .version ("1.0.0")
                        .description ("码值配置中心服务API文档")
                        .license (new License().name ("Apache 2.0")
                                .url ("http://www.yang.com/"))
                );
    }
}
```

### 启动类
```java
@SpringBootApplication
@EnableKnife4j
public class SpringBootDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoApplication.class, args);
    }
}
```

### 访问地址
- http://ip:port/doc.html