package com.springboot.liuc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.springboot"})
public class SpringBootLiucApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootLiucApplication.class, args);
    }

}
