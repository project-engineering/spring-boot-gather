package com.springboot.liuc.controller;

import com.springboot.baseline.enums.ResultCode;
import com.springboot.baseline.entity.Result;
import com.springboot.limit.annotation.FixedWindowRateLimiter;
import com.springboot.limit.annotation.RedisLimiter;
import com.springboot.limit.annotation.TokenBucketLimiter;
import com.springboot.lock.config.DistributedLockConfig;
import com.springboot.lock.factory.DistributedLockFactory;
import com.springboot.lock.impl.IDistributedLock;
import com.springboot.baseline.util.ResultUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
public class SdkController {

    @Resource
    DistributedLockFactory factory;
    @Resource
    DistributedLockConfig config;

    /**
     * 基于Redis AOP限流
     */
    @GetMapping("/test")
    @RedisLimiter(key = "redis-limit:test", permitsPerSecond = 1, expire = 1, msg = "当前排队人数较多，请稍后再试！")
    public Result test() {
        log.info("限流成功。。。");
        return ResultUtils.success("限流成功");
    }

    @GetMapping("/test2")
    @TokenBucketLimiter(key="test2",permitsPerSecond = 1, timeout = 500, timeunit = TimeUnit.MILLISECONDS,msg = "当前排队人数较多，请稍后再试！")
    public Result limit2() {
        log.info("令牌桶limit2获取令牌成功");
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        map.put("name","张三");
        map.put("age",20);
        Map<String,Object> map1 = new HashMap<>();
        map1.put("name","李四");
        map1.put("age",25);
        list.add(map);
        list.add(map1);
        return ResultUtils.success(list);
    }

    @GetMapping("/test3")
    @FixedWindowRateLimiter(timeout = 4000, timeunit = TimeUnit.MILLISECONDS,msg = "当前排队人数较多，请稍后再试！")
    public Result limit3() throws InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(20, 50, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>(10));
        //模拟不同窗口内的调用
        for (int i = 0; i < 3; i++) {
            CountDownLatch countDownLatch = new CountDownLatch(20);
            //20个线程并发调用
            for (int j = 0; j < 20; j++) {
                threadPoolExecutor.execute(() -> {
                    countDownLatch.countDown();
                });
            }
            countDownLatch.await();
            //休眠1min
//            TimeUnit.MINUTES.sleep(1);

        }
        return ResultUtils.success();
    }

    @GetMapping("/hello")
    public Result post() {
        //使用分布式锁
        IDistributedLock lock = factory.getDistributedLock(config.getType());
        String lockName = "234234_7777777" ;
        boolean isLock = lock.getExclusiveLock(lockName);
        if (!isLock) {
            log.info(111111 + "，请勿重复提交!");
            return  ResultUtils.error(ResultCode.PREVENT_SUBMIT_ERROR);
        }
        String returnStr = null;
        try{
            returnStr = "Hello World!";
            try {
                TimeUnit.SECONDS.sleep(6);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }finally {
            if (lock.isLock(lockName)) {
                //释放锁
                lock.unLock(lockName);
            }
        }
        return ResultUtils.success(returnStr);
    }

}