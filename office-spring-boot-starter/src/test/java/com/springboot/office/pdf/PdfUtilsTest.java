package com.springboot.office.pdf;

import com.itextpdf.text.PageSize;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 测试pdf生成
 */
class PdfUtilsTest {

    @Test
    void createPdf() {
        try {
            // 生成后的路径
            String outputFile = "E://data/temp/OtherApplyReport.pdf";
            Map<String, Object> dataMap = new HashMap<String, Object>(16);

            List<String> titleList = Arrays.asList("属性1", "属性2", "属性3", "属性4", "属性5", "属性6", "属性7");
            dataMap.put("titleList", titleList);

            List<List<String>> dataList = new ArrayList<List<String>>();
            for (int i = 0; i < 100; i++) {
                dataList
                        .add(Arrays.asList("数据1_" + i, "数据2_" + i, "数据3_数据3_数据3_数据3_数据3_数据3_数据3_数据3_数据3_数据3_" + i,
                                "数据4_" + i, "数据5_" + i, "数据6_" + i, "数据7_" + i));
            }
            dataMap.put("dataList", dataList);
            dataMap.put("contractName", "测试合同名称");
            dataMap.put("applyType", "3");
            dataMap.put("applyCategory", "2");
            dataMap.put("applyReasonAnaly", "鬼画符鬼画符\n给哈哈哈哈");
            dataMap.put("surveyTraceItemOnCheck", "鬼画符鬼画符\n就将计就计");
//            File water = ResourceUtils.getFileFromRelativePath("templates/watermark/water.png");
//            PdfUtil.createPdf("templates/creidt_review_report_word7.html", dataMap, outputFile, PageSize.A4, "", true, water);
            String leftHeader = "武汉畅立行网络科技有限公司";
            String rightHeader = "Copyright © 2021 畅立行网络科技有限公司 版权所有";
            PdfUtil pdfUtil = new PdfUtil.Builder()
                    .htmlTemplate("templates/OtherApplyReport.html")
                    .dataMap(dataMap)
                    .targetPdf(outputFile)
                    .pageSize(PageSize.A4)
                    .header("")
                    .isFooter(true)
                    .leftHeader(leftHeader)
                    .rightHeader(rightHeader)
                    .build();
            pdfUtil.createPdf();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}