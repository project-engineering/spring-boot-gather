package com.springboot.office.excel.poiexcel.converter;

import com.springboot.office.excel.poi.converter.Converter;
import com.springboot.office.excel.poiexcel.enums.SexEnum;

/**
 * @author liuc
 * @date 2024-09-18 11:37
 */
public class SexConverter implements Converter<String> {
    @Override
    public Object convertToExcelData(Object value) throws UnsupportedOperationException {
        return SexEnum.getDesc((String) value);
    }

    @Override
    public String convertToJavaData(Object value) throws UnsupportedOperationException {
        return null;
    }
}
