package com.springboot.office.excel.poiexcel;

import com.springboot.office.excel.poi.annotation.ExcelColumn;
import com.springboot.office.excel.poiexcel.converter.SexConverter;
import lombok.Builder;
import lombok.Data;

/**
 * @author liuc
 * @date 2024-09-15 17:36
 */
@Data
@Builder
public class Person {
    @ExcelColumn(value= {"学生信息","序号","序号"},index = 0)
    private int id;

    @ExcelColumn(value= {"学生信息","基本信息","姓名"},index = 1)
    private String name;

    @ExcelColumn(value= {"学生信息","基本信息","年龄"},index = 2)
    private int age;

    @ExcelColumn(value= {"学生信息","基本信息","性别"},index = 3,converter = SexConverter.class,merge = true)
    private String sex;

    @ExcelColumn(value= {"学生信息","基本信息","地址"},index = 4)
    private String address;
}
