package com.springboot.office.excel.poiexcel;

import com.springboot.office.excel.poi.PoiExcelUtil;
import com.springboot.office.excel.poi.strategy.AutoSizeColumnStrategy;
import com.springboot.office.excel.poi.strategy.ColumnStyleStrategy;
import com.springboot.office.excel.poi.strategy.CustomHeaderStyleStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuc
 * @date 2024-09-15 17:38
 */
public class ExcelExportServiceTest {
    public static void main(String[] args) {
        // 创建一个Person对象的列表
        List<Person> personList = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            personList.add(new Person(i,"姓名" + i, 20 + (i % 50), "1", "地址" + i));
        }

        // 指定导出的Excel文件名
        String fileName = "people_data.xlsx";
        // 创建一个ExcelExportService实例
        new PoiExcelUtil.Builder()
                .registerHeaderStyleStrategy(new CustomHeaderStyleStrategy())
                .registerCellWriteStrategy(new AutoSizeColumnStrategy())
                .registerCellWriteStrategy(new ColumnStyleStrategy())
                .sheetName("学生信息")
                .maxPerSheet(100000)
                .build()
                .write(personList, fileName);
    }
}
