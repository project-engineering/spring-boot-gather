package com.springboot.office.excel.easyexcel;

import com.alibaba.excel.support.ExcelTypeEnum;
import com.springboot.office.excel.easyexcel.listener.CustomExcelListener;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author liuc
 * @date 2024-08-14 17:07
 */
@Log4j2
public class EasyExcelUtilTest {

    public static final String FILE_NAME = "E:\\test_04.xlsx";


    @Test
    public void writeExcelByMulSheetAndMulWrite() {
        String fileName = FILE_NAME;
        //获取数据
        List<ExcelBean> date = getDate();
        EasyExcelUtil.export(fileName, date,ExcelBean.class);
    }

    /**
     * 获取excel 导出的数据
     *
     * @return list 集合
     */
    public static List<ExcelBean> getDate(){
        log.info("开始生成数据");
        //创建返回数据集合
        List<ExcelBean> list = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 2000000; i++) {
            //创建数据对象
            ExcelBean excelBean = new ExcelBean();
            excelBean.setId(UUID.randomUUID().toString());
            excelBean.setName("小明" + (10000 + i));
            excelBean.setAddress("浙江省杭州市西湖");
            excelBean.setAge(i);
            excelBean.setNumber(i + 10000);
            excelBean.setHigh(1.234 * i);
            excelBean.setDistance(3.14 * i);
            excelBean.setStartTime(LocalDateTime.now());
            excelBean.setEndTime(LocalDateTime.now().plusDays(1));
            list.add(excelBean);
        }
        log.info("数据生成结束，数据量={}，耗时={}ms", list.size(), System.currentTimeMillis() - startTime);
        return list;
    }

    @Test
    public void importExcel() {
        EasyExcelUtil.importExcel(FILE_NAME, ExcelBean.class, new CustomExcelListener(), ExcelTypeEnum.XLSX);
    }
}
