package com.springboot.office.excel.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.springboot.office.excel.easyexcel.converter.LocalDateTimeConverter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * @author liuc
 * @date 2024-08-15 14:49
 */
@Accessors(chain = true)
public class ExcelBean {

    @ExcelProperty(value = {"测试","主键id"})
    private String id;

    @ExcelProperty(value = {"测试","姓名"})
    private String name;

    @ExcelProperty(value = {"测试","地址"})
    private String address;

    @ExcelProperty(value = {"测试","年龄"})
    private Integer age;

    @ExcelProperty(value = {"测试","数量"})
    private Integer number;

    @NumberFormat("#.##")
    @ExcelProperty(value = {"测试","身高"})
    private Double high;

    @ExcelProperty(value = {"测试","距离"})
    @NumberFormat("#.##")
    private Double distance;

    @ExcelProperty(value = {"测试","开始时间"}, converter = LocalDateTimeConverter.class)
    private LocalDateTime startTime;

    @ExcelProperty(value = {"测试","结束时间"}, converter = LocalDateTimeConverter.class)
    @DateTimeFormat("yyyy/MM/dd HH:mm:ss")
    private LocalDateTime endTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ExcelBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                ", number=" + number +
                ", high=" + high +
                ", distance=" + distance +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}