package com.springboot.office.excel.easyexcel.listener;

/**
 * @author liuc
 * @date 2024-08-16 17:26
 */
public class CustomExcelListener extends BatchDataExcelListener{
    @Override
    protected void processData(Object data) {
        System.out.println(data);
    }
}
