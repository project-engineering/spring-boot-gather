package com.springboot.office.excel.poiexcel.enums;

import lombok.Getter;

/**
 * @author liuc
 * @date 2024-09-18 11:42
 */
@Getter
public enum SexEnum {
    MALE("0", "男"),
    FEMALE("1", "女");

    private String type;
    private String desc;

    SexEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static String getDesc(String type) {
        for (SexEnum sexEnum : SexEnum.values()) {
            if (sexEnum.getType().equals(type)) {
                return sexEnum.getDesc();
            }
        }
        return null;
    }
}
