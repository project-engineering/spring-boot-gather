package com.springboot.office.word.poi;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuc
 * @date 2024-08-22 20:32
 */
public class PoiWordUtilTest {
    @Test
    public void exportWordFromTemplate() {
        // 替换的内容
        Map<String, Object> replacements = new HashMap<>();
        replacements.put("orgName", "武汉畅立行网络科技有限公司");
        replacements.put("handleBankName", "武汉银行股份有限公司");
        replacements.put("handleBankLegal", "李四");
        replacements.put("handleBankAccount", "6217000100000000000");
        replacements.put("handleBankAddress", "武汉市汉阳区汉阳街道汉阳路1号");
        replacements.put("handleBankPhone", "13512345679");
        replacements.put("serialNo", "TBBHJY0001YH000120240822171359048");
        replacements.put("orgSocialCreditCode", "91350105MA32PW3N59");
        replacements.put("orgLegalName", "刘云畅");
        replacements.put("orgAddress", "湖北省武汉市武昌区汉街街道汉街路1号");
        replacements.put("orgTelPhone", "13512345678");
        replacements.put("orgFax", "027-87888888");
        replacements.put("orgEmail", "13512345678@qq.com");
        replacements.put("projectName", "国泰测试H-招商工程保证金0827A-1");
        replacements.put("sectionCode", "T2300000001000039001001");
        replacements.put("bidName", "中控数科（陕西）信息科技有限公司");
        replacements.put("bidLegal", "庄孝思");
        replacements.put("bidPhone", "13512345688");
        replacements.put("bidAddress", "陕西省西安市雁塔区西安大道100号");
        replacements.put("tendereeName", "测试元气森林食品有限公司");
        replacements.put("guaranteeAmountGt", "3000000");
        replacements.put("year", "2024");
        replacements.put("month", "08");
        replacements.put("day", "22");
        List<Map<Integer, List<Map<String, Object>>>> tableDataList = new ArrayList<>();
        Map<Integer, List<Map<String, Object>>> tableData1 = new HashMap<>();
        List<Map<String, Object>> rows1 = new ArrayList<>();
        for(int i=1;i<=10;i++){
            Map<String, Object> map=new HashMap<>();
            map.put("seq", i);
            map.put("name", "产品名称"+i);
            map.put("age", 20+i);
            map.put("sex", "男");
            map.put("image","E://data//templates//word//logo.png");
            rows1.add(map);
        }
        tableData1.put(0,rows1);
        tableDataList.add(tableData1);
        Map<Integer, List<Map<String, Object>>> tableData2 = new HashMap<>();
        List<Map<String, Object>> rows2 = new ArrayList<>();
        for(int i=1;i<=10;i++){
            Map<String, Object> map=new HashMap<>();
            map.put("data", "2024-08-23");
            map.put("amount", "1000000");
            map.put("rate", "10%");
            map.put("remark", "备注");
            map.put("contractDate","2024-08-24");
            map.put("principal","500000");
            rows2.add(map);
        }
        tableData2.put(1,rows2);
        tableDataList.add(tableData2);
        Map<String, Object> picData = new HashMap<>();
        String imageUrl = PoiWordUtil.getResourcesPath("/templates/word/logo.png");
        picData.put("imageUrl", imageUrl);
        String path = PoiWordUtil.getResourcesPath("/templates/word/履约保函协议书.docx");
        List<Integer> tableIndexList = new ArrayList<>();
        tableIndexList.add(1);
        tableIndexList.add(2);
        // 导出Word文档
        PoiWordUtil.exportWordFromTemplate(path, "E://data//履约保函协议书.docx", replacements,picData,tableIndexList,tableDataList);
        // 导出Word文档
//        picData.put("imageUrl", "E://data//templates//word//logo.png");
//        PoiWordUtil.exportWordFromTemplate("E://data//templates//word//履约保函协议书.docx", "E://data//履约保函协议书.docx", replacements,picData);
    }


    @Test
    public void testInsertImage() throws IOException {
        String imagePath = "E://data//templates//word//logo.png";
        XWPFDocument doc = new XWPFDocument();
        XWPFParagraph paragraph = doc.createParagraph();
        XWPFRun run = paragraph.createRun();

        try (FileInputStream is = new FileInputStream(imagePath)) {
            int width = Units.toEMU(150);
            int height = Units.toEMU(100);
            run.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, imagePath, width, height);
        } catch (InvalidFormatException e) {
            throw new RuntimeException(e);
        }

        String outputPath = "E://data//testImage.docx";
        try (FileOutputStream out = new FileOutputStream(outputPath)) {
            doc.write(out);
        }
    }
}
