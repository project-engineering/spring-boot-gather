package com.springboot.office.word.documents4j;

import org.junit.jupiter.api.Test;

/**
 * @author liuc
 * @date 2024-08-20 19:30
 */
public class WordUtilTest {
    @Test
    void convertWordToPdf() {
        WordUtil.convertWordToPdf("E:\\gitlab\\spring-boot-gather\\office-spring-boot-starter\\src\\test\\resources\\保函协议书.docx",
                "E:\\data\\保函协议书.pdf");
    }
}
