package com.springboot.office.excel.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.log4j.Log4j2;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 批量读取excel文件监听器
 * @author liuc
 * @date 2024-08-16 17:23
 */
@Log4j2
public abstract class BatchDataExcelListener<T> extends AnalysisEventListener<T> {
    // 默认线程池大小
    private static final int DEFAULT_THREAD_POOL_SIZE = 10;
    private final ExecutorService executorService;

    public BatchDataExcelListener() {
        this(DEFAULT_THREAD_POOL_SIZE);
    }

    public BatchDataExcelListener(int threadPoolSize) {
        this.executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    @Override
    public void invoke(T data, AnalysisContext context) {
        executorService.submit(() -> {
            try {
                processData(data);
            } catch (Exception e) {
                handleException(e);
            }
        });
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        shutdownExecutorService();
    }

    private void shutdownExecutorService() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt(); // 恢复中断状态
        }
    }

    /**
     * 处理数据
     *
     * @param data 数据
     */
    protected abstract void processData(T data);

    /**
     * 异常处理
     *
     * @param e 异常
     */
    protected void handleException(Exception e) {
        // 默认实现为空，子类可以重写此方法进行自定义异常处理
    }
}
