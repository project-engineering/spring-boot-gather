package com.springboot.office.excel.poi.strategy;

import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * @author liuc
 * @date 2024-09-16 15:44
 */
@Log4j2
public class ColumnStyleStrategy implements CellWriteStrategy {

    private static volatile CellStyle defaultStyle;

    private CellStyle createCellStyle(SXSSFWorkbook workbook) {
        if (defaultStyle == null) {
            synchronized (ColumnStyleStrategy.class) {
                if (defaultStyle == null) {
                    defaultStyle = workbook.createCellStyle();
                    // 设置样式属性
                    defaultStyle.setBorderBottom(BorderStyle.THIN);
                    defaultStyle.setBorderLeft(BorderStyle.THIN);
                    defaultStyle.setBorderRight(BorderStyle.THIN);
                    defaultStyle.setBorderTop(BorderStyle.THIN);
                    defaultStyle.setAlignment(HorizontalAlignment.CENTER);
                    defaultStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                    defaultStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                    Font font = workbook.createFont();
                    font.setFontName("微软雅黑");
                    font.setFontHeightInPoints((short) 12);
                    defaultStyle.setFont(font);
                }
            }
        }
        return defaultStyle;
    }

    @Override
    public void apply(Cell cell, Object value, Sheet sheet, SXSSFWorkbook workbook) {
        try {
            if (cell != null && workbook != null) {
                cell.setCellStyle(createCellStyle(workbook));
            } else {
                throw new IllegalArgumentException("Cell or Workbook cannot be null");
            }
        } catch (Exception e) {
            log.error("设置单元格样式失败", e);
            throw new RuntimeException("设置单元格样式失败", e);
        }
    }
}
