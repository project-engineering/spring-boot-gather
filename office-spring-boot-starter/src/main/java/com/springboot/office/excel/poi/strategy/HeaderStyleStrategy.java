package com.springboot.office.excel.poi.strategy;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * @author liuc
 * @date 2024-09-16 20:23
 */
public interface HeaderStyleStrategy{
    void apply(Row row, Cell cell, Sheet sheet, SXSSFWorkbook workbook);
}
