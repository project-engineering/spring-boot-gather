package com.springboot.office.excel.poi;

import com.springboot.office.excel.poi.annotation.ExcelColumn;
import com.springboot.office.excel.poi.converter.Converter;
import com.springboot.office.excel.poi.strategy.CellWriteStrategy;
import com.springboot.office.excel.poi.strategy.DefaultCellWriteStrategy;
import com.springboot.office.excel.poi.strategy.DefaultHeaderStyleStrategy;
import com.springboot.office.excel.poi.strategy.HeaderStyleStrategy;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StopWatch;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author liuc
 * @date 2024-09-15 17:11
 */
@Log4j2
public class PoiExcelUtil {
    /**
     * 默认的表头样式策略列表
     */
    private final List<HeaderStyleStrategy> headerStyleStrategyList;
    /**
     * 存储注册的单元格写入策略
     */
    private final List<CellWriteStrategy> cellWriteStrategies;
    /**
     * sheet名称
     */
    private String sheetName;
    /**
     * 单个sheet中最大数据量
     */
    private int maxPerSheet;


    private PoiExcelUtil(Builder builder) {
        // 注册默认的单元格写入策略
        this.headerStyleStrategyList = builder.headerStyleStrategyList;
        this.cellWriteStrategies = builder.cellWriteStrategies;
        this.sheetName = builder.sheetName;
        this.maxPerSheet = builder.maxPerSheet;
    }

    /**
     * 构建器
     */
    public static class Builder {
        /**
         * 默认的表头样式策略列表
         */
        private final List<HeaderStyleStrategy> headerStyleStrategyList = new ArrayList<>();
        /**
         * 默认的单元格写入策略列表
         */
        private final List<CellWriteStrategy> cellWriteStrategies = new ArrayList<>();
        /**
         * sheet名称
         * 默认为Sheet1
         */
        private String sheetName;
        /**
         * 单个sheet中最大数据量
         */
        private int maxPerSheet;

        /**
         * 注册表头样式策略
         * @param strategy 表头样式策略
         * @return 构建器
         */
        public Builder registerHeaderStyleStrategy(HeaderStyleStrategy strategy) {
            this.headerStyleStrategyList.add(strategy);
            return this;
        }

        /**
         * 注册单元格写入策略
         * @param strategy 单元格写入策略
         * @return 构建器
         */
        public Builder registerCellWriteStrategy(CellWriteStrategy strategy) {
            this.cellWriteStrategies.add(strategy);
            return this;
        }

        /**
         * sheet名称
         * 默认为Sheet1
         */
        public Builder sheetName(String sheetName) {
            this.sheetName = sheetName;
            return this;
        }
        /**
         * 单个sheet中最大数据量
         * 默认为300000
         */
        public Builder maxPerSheet(int maxPerSheet) {
            this.maxPerSheet = maxPerSheet;
            return this;
        }

        public PoiExcelUtil build() {
            return new PoiExcelUtil(Builder.this);
        }
    }

    /**
     * 注册单元格写入策略
     * @param strategy 单元格写入策略
     */
    public void registerHeaderStyleStrategy(HeaderStyleStrategy strategy) {
        headerStyleStrategyList.add(strategy);
    }

    /**
     * 注册单元格写入策略
     * @param strategy 单元格写入策略
     */
    public void registerCellWriteStrategy(CellWriteStrategy strategy) {
        cellWriteStrategies.add(strategy);
    }

    /**
     * 导出数据到Excel文件
     * @param list 数据列表
     * @param fileName 文件名
     */
    public void write(List<?> list, String fileName) {
        log.info("开始导出数据到Excel文件，数据量：{}, 文件名：{}", list.size(), fileName);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        if (list.isEmpty()) {
            throw new IllegalArgumentException("数据列表不能为空");
        }

        Class<?> clazz = list.get(0).getClass();
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        executor.initialize();
        if (ObjectUtils.isEmpty(maxPerSheet)) {
            maxPerSheet = 300000;
        }
        try (SXSSFWorkbook workbook = new SXSSFWorkbook(-1)) {
            workbook.setCompressTempFiles(true);
            int numSheets = (int) Math.ceil((double) list.size() / maxPerSheet);
            int start = 0;
            CompletableFuture<?>[] futures = new CompletableFuture[numSheets];
            if (ObjectUtils.isEmpty(sheetName)) {
                sheetName = "Sheet";
            }
            for (int i = 0; i < numSheets; i++) {
                Sheet sheet = workbook.createSheet(sheetName + (i + 1));
                createHeader(clazz, sheet,workbook);
                int end = Math.min(start + maxPerSheet, list.size());
                List<?> sublist = list.subList(start, end);
                futures[i] = CompletableFuture.runAsync(() -> writeDataToSheet(sublist, sheet,workbook, clazz), executor);
                start = end;
            }
            // 等待所有写入操作完成
            CompletableFuture.allOf(futures).join();

            try (FileOutputStream out = new FileOutputStream(fileName)) {
                workbook.write(out);
            }
        } catch (IOException e) {
            log.error("导出Excel文件过程中发生IO错误", e);
        } finally {
            executor.shutdown();
        }
        stopWatch.stop();
        log.info("导出数据到Excel文件完成，耗时：{} ms", stopWatch.getTotalTimeMillis());
    }

    /**
     * 创建表头行
     * @param clazz 类
     * @param sheet 表格
     */
    public void createHeader(Class<?> clazz, Sheet sheet,SXSSFWorkbook workbook) {
        // 获取带有ExcelColumn注解的字段列表，并按索引排序
        List<Field> fields = Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(ExcelColumn.class))
                .sorted(Comparator.comparingInt(f -> f.getAnnotation(ExcelColumn.class).index()))
                .collect(Collectors.toList());

        // 创建表头行
        int maxHeaderDepth = calculateMaxHeaderDepth(fields);
        createHeaderRows(sheet, maxHeaderDepth);

        // 记录每个值的列索引和行索引
        Map<String, List<int[]>> valueToCellIndexMap = new HashMap<>();

        // 填充表头单元格并记录位置
        int currentCellIndex = 0;
        fillHeaderCellsAndRecordPositions(workbook,sheet, fields, valueToCellIndexMap, currentCellIndex);

        // 合并具有相同值的单元格
        mergeCellsWithSameValue(sheet, valueToCellIndexMap);
    }

    /**
     * 计算表头最大深度
     * @param fields 字段列表
     * @return 最大深度
     */
    private int calculateMaxHeaderDepth(List<Field> fields) {
        return fields.stream()
                .mapToInt(f -> f.getAnnotation(ExcelColumn.class).value().length)
                .max()
                .orElse(0);
    }

    /**
     * 创建表头行
     * @param sheet 表格
     * @param maxHeaderDepth 最大深度
     */
    private void createHeaderRows(Sheet sheet, int maxHeaderDepth) {
        for (int i = 0; i < maxHeaderDepth; i++) {
            sheet.createRow(i);
        }
    }

    /**
     * 填充表头单元格并记录位置
     * @param sheet 表格
     * @param fields 字段列表
     * @param valueToCellIndexMap 值-单元格位置映射
     * @param currentCellIndex 当前单元格索引
     */
    private void fillHeaderCellsAndRecordPositions(SXSSFWorkbook workbook,Sheet sheet, List<Field> fields,
                                                   Map<String, List<int[]>> valueToCellIndexMap, int currentCellIndex) {
        for (Field field : fields) {
            ExcelColumn column = field.getAnnotation(ExcelColumn.class);
            String[] values = column.value();
            for (int i = 0; i < values.length; i++) {
                Row headerRow = sheet.getRow(i);
                Cell cell = headerRow.createCell(currentCellIndex);
                cell.setCellValue(values[i]);
                if(ObjectUtils.isEmpty(headerStyleStrategyList)) {
                    registerHeaderStyleStrategy(new DefaultHeaderStyleStrategy());
                }
                for (HeaderStyleStrategy headerStyleStrategy : headerStyleStrategyList) {
                    headerStyleStrategy.apply(headerRow, cell, sheet, workbook);
                }
                // 将值映射到列索引和行索引
                valueToCellIndexMap.computeIfAbsent(values[i], k -> new ArrayList<>()).add(new int[]{currentCellIndex, i});
            }
            currentCellIndex++;
        }
    }

    /**
     * 合并具有相同值的单元格
     * @param sheet 表格
     * @param valueToCellIndexMap 值-单元格位置映射
     */
    public void mergeCellsWithSameValue(Sheet sheet, Map<String, List<int[]>> valueToCellIndexMap) {
        // 合并相同列不同行的相同值
        mergeCellsRow(sheet, valueToCellIndexMap);

        // 合并相同行不同列的相同值
        mergeCellsColumn(sheet, valueToCellIndexMap);
    }

    /**
     * 合并单元格行
     * @param sheet 表格
     * @param valueToCellIndexMap 值-单元格位置映射
     */
    private void mergeCellsRow(Sheet sheet, Map<String, List<int[]>> valueToCellIndexMap){
        for (Map.Entry<String, List<int[]>> entry : valueToCellIndexMap.entrySet()) {
            List<int[]> cellPositions = entry.getValue();
            if (cellPositions.size() > 1) {
                cellPositions.sort(Comparator.comparingInt(o -> o[0])); // 首先按列排序

                int firstCol = cellPositions.get(0)[0];
                int lastCol = firstCol;
                int firstRow = cellPositions.get(0)[1];
                int lastRow = firstRow;

                for (int[] pos : cellPositions) {
                    if (pos[0] == lastCol) {
                        lastRow = pos[1];
                    } else {
                        if (lastRow > firstRow) {
                            sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
                        }
                        firstCol = pos[0];
                        firstRow = pos[1];
                        lastCol = firstCol;
                        lastRow = firstRow;
                    }
                }
                if (lastRow > firstRow) {
                    sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
                }
            }
        }
    }

    /**
     * 合并单元格列
     * @param sheet 表格
     * @param valueToCellIndexMap 值-单元格位置映射
     */
    private void mergeCellsColumn(Sheet sheet, Map<String, List<int[]>> valueToCellIndexMap) {
        for (Map.Entry<String, List<int[]>> entry : valueToCellIndexMap.entrySet()) {
            List<int[]> cellPositions = entry.getValue();
            if (cellPositions.size() > 1) {
                cellPositions.sort(Comparator.comparingInt(o -> o[1])); // 首先按行排序

                int firstRow = cellPositions.get(0)[1];
                int lastRow = firstRow;
                int firstCol = cellPositions.get(0)[0];
                int lastCol = firstCol;

                for (int[] pos : cellPositions) {
                    if (pos[1] == lastRow) {
                        lastCol = pos[0];
                    } else {
                        if (lastCol > firstCol) {
                            sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
                        }
                        firstRow = pos[1];
                        firstCol = pos[0];
                        lastRow = firstRow;
                        lastCol = firstCol;
                    }
                }
                if (lastCol > firstCol) {
                    sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
                }
            }
        }
    }

    /**
     * 将数据写入表格
     * @param sublist 数据列表
     * @param sheet 表格
     * @param clazz 类
     */
    private void writeDataToSheet(List<?> sublist, Sheet sheet, SXSSFWorkbook workbook, Class<?> clazz) {
        // 获取当前工作表最后一行的索引，然后加1
        int startRow = sheet.getLastRowNum() + 1;
        for (int i = 0; i < sublist.size(); i++) {
            Object item = sublist.get(i);
            Row row = sheet.createRow(startRow + i);
            // 获取当前行的所有字段，并按索引排序
            List<Field> fields = Arrays.stream(clazz.getDeclaredFields())
                    .filter(f -> f.isAnnotationPresent(ExcelColumn.class))
                    .sorted(Comparator.comparingInt(f -> f.getAnnotation(ExcelColumn.class).index()))
                    .collect(Collectors.toList());
            int cellIndex = 0;
            for (Field field : fields) {
                // 使得私有字段也可以访问
                field.setAccessible(true);
                try {
                    Object value = field.get(item);
                    Cell cell = row.createCell(cellIndex++);
                    setCellValue(cell, value, sheet, workbook, field);
                } catch (IllegalAccessException e) {
                    // 日志记录错误信息
                    log.error("无法访问字段 " + field.getName(), e);
                    throw new RuntimeException("无法访问字段 " + field.getName());
                }
            }
        }

        // 合并去表头外的相同列相同值的单元格
        mergeCellsWithoutHeader(sheet, clazz);
    }

    /**
     * 合并去表头外的相同列相同值的单元格
     * @param sheet 表格
     */
    private void mergeCellsWithoutHeader(Sheet sheet, Class<?> clazz) {
        int headerRowNum = sheet.getFirstRowNum();
        // 获取需要合并的列索引列表
        List<Integer> mergeColumnIndices = getMergeColumnIndices(clazz);

        for (Integer colNum : mergeColumnIndices) {
            int lastRowNum = sheet.getLastRowNum();
            String lastValue = null;
            int firstRow = headerRowNum + 1;
            int lastRow = -1;

            for (int rowNum = headerRowNum + 1; rowNum <= lastRowNum; rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row == null) continue;

                Cell cell = row.getCell(colNum);
                if (cell == null) continue;

                String value = getCellValue(cell);

                if (lastValue == null || !lastValue.equals(value)) {
                    if (lastRow > firstRow) {
                        mergeCells(sheet, firstRow, lastRow, colNum);
                    }
                    firstRow = rowNum;
                    lastValue = value;
                }
                lastRow = rowNum;
            }

            if (lastRow > firstRow) {
                mergeCells(sheet, firstRow, lastRow, colNum);
            }
        }
    }

    /**
     * 获取需要合并的列索引列表
     * @param clazz 类
     * @return 列索引列表
     */
    private List<Integer> getMergeColumnIndices(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(ExcelColumn.class))
                .filter(f -> f.getAnnotation(ExcelColumn.class).merge())
                .map(f -> f.getAnnotation(ExcelColumn.class).index())
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 合并单元格
     * @param sheet 表格
     * @param firstRow 起始行
     * @param lastRow 结束行
     * @param colNum 列索引
     */
    private void mergeCells(Sheet sheet, int firstRow, int lastRow, int colNum) {
        if (firstRow < lastRow) {
            sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, colNum, colNum));
            // 设置合并单元格的值
            Row row = sheet.getRow(firstRow);
            Cell cell = row.getCell(colNum);
            if (cell == null) {
                cell = row.createCell(colNum);
            }
            cell.setCellValue(new XSSFRichTextString(getCellValue(cell)));
        }
    }

    /**
     * 获取单元格值
     * @param cell 单元格
     * @return 值
     */
    private String getCellValue(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    /**
     * 设置单元格值
     * @param cell 单元格
     * @param value 值
     */
    private void setCellValue(Cell cell, Object value, Sheet sheet, SXSSFWorkbook workbook,Field field) {
        // 默认策略：直接设置值
        if (value == null) {
            return;
        }
        ExcelColumn column = field.getAnnotation(ExcelColumn.class);
        if (column != null) {
            Class<? extends Converter> converterClass = column.converter();
            // 避免直接比较类型
            if (!isSameConverterClass(converterClass, Converter.class)) {
                // 如果 converterClass 不是 Converter 的实例，则尝试实例化
                try {
                    Converter converter = converterClass.newInstance();
                    // ... 使用 converter 进行转换
                    value = converter.convertToExcelData(value);
                } catch (InstantiationException | IllegalAccessException e) {
                    log.error("无法实例化转换器 {}", converterClass.getName(), e);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                if (value instanceof String) {
                    cell.setCellValue((String) value);
                } else if (value instanceof Number) {
                    cell.setCellValue(((Number) value).doubleValue());
                } else if (value instanceof Boolean) {
                    cell.setCellValue((Boolean) value);
                } else {
                    cell.setCellValue(value.toString());
                }
            }
        }

        if(ObjectUtils.isEmpty(cellWriteStrategies)) {
            registerCellWriteStrategy(new DefaultCellWriteStrategy());
        }
        for (CellWriteStrategy strategy : cellWriteStrategies) {
            strategy.apply(cell, value, sheet, workbook);
        }
    }

    /**
     * 判断两个类是否是同一个类
     * @param class1 类1
     * @param class2 类2
     * @return 是否是同一个类
     */
    public boolean isSameConverterClass(Class<?> class1, Class<?> class2) {
        return class1.isAssignableFrom(class2) && class2.isAssignableFrom(class1);
    }
}
