package com.springboot.office.excel.poi.strategy;

import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 单元格自适应策略
 * @author liuc
 * @date 2024-09-16 15:43
 */
public class AutoSizeColumnStrategy implements CellWriteStrategy {
    @Override
    public void apply(Cell cell, Object value, Sheet sheet, SXSSFWorkbook workbook) {
        // 自适应策略：设置值并调整列宽
        if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Number) {
            cell.setCellValue(((Number) value).doubleValue());
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else if (value instanceof LocalDateTime) {
            cell.setCellValue((LocalDateTime) value);
        } else if (value instanceof LocalDate) {
            cell.setCellValue((LocalDate) value);
        }

        // 调整列宽
        adjustColumnWidth(cell, sheet, workbook);
    }

    private void adjustColumnWidth(Cell cell, Sheet sheet, Workbook workbook) {
        // 获取单元格的样式
        CellStyle cellStyle = cell.getCellStyle();
        // 获取单元格样式的字体索引
        int fontIndex = cellStyle.getFontIndex();
        // 根据字体索引获取字体
        Font font = workbook.getFontAt(fontIndex);

        // 获取单元格的值
        String cellValue = getCellValueAsString(cell);

        // 假设每个字符的平均宽度是字体大小的1/2，这是一个简化的估计
        double charWidth = font.getFontHeightInPoints() / 3.5;
        int columnIndex = cell.getColumnIndex();
        int newColumnWidth = (int) (charWidth * cellValue.length() * 256) + 10; // 加上一些额外的空间

        // 确保新的列宽不小于当前列宽
        int currentWidth = sheet.getColumnWidth(columnIndex);
        if (newColumnWidth > currentWidth) {
            sheet.setColumnWidth(columnIndex, newColumnWidth);
        }
    }

    private String getCellValueAsString(Cell cell) {
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                } else {
                    return Double.toString(cell.getNumericCellValue());
                }
            case BOOLEAN:
                return Boolean.toString(cell.getBooleanCellValue());
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            case ERROR:
                return ErrorEval.getText(cell.getErrorCellValue());
            default:
                return "Unknown type";
        }
    }
}
