package com.springboot.office.excel.poi.strategy;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 默认单元格写入策略
 * @author liuc
 * @date 2024-09-16 15:42
 */
public class DefaultCellWriteStrategy implements CellWriteStrategy {
    @Override
    public void apply(Cell cell, Object value, Sheet sheet, SXSSFWorkbook workbook) {
        // 默认策略：直接设置值
        if (value == null) {
            return;
        }
        if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Number) {
            cell.setCellValue(((Number) value).doubleValue());
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue(value.toString());
        }
    }
}