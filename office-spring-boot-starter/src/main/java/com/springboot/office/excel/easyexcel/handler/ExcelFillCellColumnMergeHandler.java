package com.springboot.office.excel.easyexcel.handler;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义合并单元格策略Handler 列合并 向上合并相同值的单元格
 */
public class ExcelFillCellColumnMergeHandler implements CellWriteHandler{

    /**
     * 自定义合并单元格的列 例： int[] mergeColumeIndex = {0, 1, 11};
     */
    private int[] mergeColumnIndex;
    /**
     * 自定义合并单元格的行  一般来说填表头行高 例如行高位为3则 int mergeRowIndex = 3;
     */
    private int mergeRowIndex;

    public ExcelFillCellColumnMergeHandler() {
    }

    /**
     * 构造方法
     * @param mergeRowIndex 自定义合并单元格的行
     * @param mergeColumnIndex 自定义合并单元格的列
     */
    public ExcelFillCellColumnMergeHandler(int mergeRowIndex, int[] mergeColumnIndex) {
        this.mergeRowIndex = mergeRowIndex;
        this.mergeColumnIndex = mergeColumnIndex;
    }

    /**
     * 单元格创造之前的操作
     * @param writeSheetHolder 写入sheet的holder
     * @param writeTableHolder 写入表的holder
     * @param row 当前行
     * @param head 当前列头
     * @param integer 当前行索引
     * @param integer1 当前列索引
     * @param aBoolean 是否是表头
     */
    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                 Head head, Integer integer, Integer integer1, Boolean aBoolean) {

    }

    /**
     * 单元格创造之后的操作
     * @param writeSheetHolder 写入sheet的holder
     * @param writeTableHolder 写入表的holder
     * @param cell 当前单元格
     * @param head 当前列头
     * @param integer 当前行索引
     * @param aBoolean 是否是表头
     */
    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer integer, Boolean aBoolean) {

    }

    @Override
    public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, WriteCellData<?> cellData, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

    }

    /**
     * afterCellDispose方法是在单元格创建后销毁前的一个时机。这时候我们可以改变单元格内容。
     * mergeColumnIndex 自定义合并单元格的列 例： int[] mergeColumeIndex = {0, 1, 11};
     * mergeRowIndex 自定义合并单元格的行
     * 一般来说填表头行高 如表头行高位为3则 int mergeRowIndex = 3;
     * @param writeSheetHolder 写入sheet的holder
     * @param writeTableHolder 写入表的holder
     * @param cellDataList 写入的cell数据
     * @param cell 当前单元格
     * @param head 当前列头
     * @param relativeRowIndex 当前行索引
     * @param isHead 是否是表头
     */
    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        // 当前行
        int curRowIndex = cell.getRowIndex();
        // 当前列
        int curColIndex = cell.getColumnIndex();
        // 判断当前行是否大于等于指定的开始行
        if (curRowIndex >= mergeRowIndex) {
            for (int columnIndex : mergeColumnIndex) {
                // 判断当前列是否等于指定的列
                if (curColIndex == columnIndex) {
                    mergeWithPrevRow(writeSheetHolder, cell, curRowIndex, curColIndex);
                    break;
                }
            }
        }
    }


    /**
     * 合并当前单元格与上一行相同值的单元格
     * @param writeSheetHolder 写入sheet的holder
     * @param cell 当前单元格
     * @param curRowIndex 当前行
     * @param curColIndex 当前列
     */
    public void mergeWithPrevRow(WriteSheetHolder writeSheetHolder, Cell cell, int curRowIndex, int curColIndex) {
        Sheet sheet = writeSheetHolder.getSheet();
        if (curRowIndex <= mergeRowIndex) {
            // 当前行为指定的开始行或更早的行，无法与上一行合并
            return;
        }

        Row curRow = sheet.getRow(curRowIndex);
        Row prevRow = sheet.getRow(curRowIndex - 1);

        if (curRow == null || prevRow == null) {
            // 当前行或上一行为空，无法进行合并
            return;
        }

        Cell curCell = curRow.getCell(curColIndex);
        Cell prevCell = prevRow.getCell(curColIndex);

        if (curCell == null || prevCell == null) {
            // 当前单元格或上一行对应的单元格为空，无法进行合并
            return;
        }

        Object curValue = getCellValue(curCell);
        Object prevValue = getCellValue(prevCell);

        if (curValue.equals(prevValue)) {
            // 创建新的单元格范围并加入待合并的范围列表
            CellRangeAddress newMergeRegion = new CellRangeAddress(prevRow.getRowNum(), curRow.getRowNum(), curColIndex, curColIndex);
            mergeOrExtendRegion(sheet, newMergeRegion);
        }
    }

    /**
     * 合并或扩展已有的合并单元格范围
     * @param sheet 要合并的sheet
     * @param newMergeRegion 新合并单元格范围
     */
    private void mergeOrExtendRegion(Sheet sheet, CellRangeAddress newMergeRegion) {
        // 获取已合并的单元格范围列表
        List<CellRangeAddress> mergedRegions = getMergedRegions(sheet);

        // 检查新合并单元格范围是否与已有范围重叠
        List<CellRangeAddress> overlappingRegions = new ArrayList<>();
        for (CellRangeAddress existingRegion : mergedRegions) {
            if (existingRegion.intersects(newMergeRegion)) {
                overlappingRegions.add(existingRegion);
            }
        }

        // 合并重叠的范围
        for (CellRangeAddress overlappingRegion : overlappingRegions) {
            // 查找重叠范围的索引
            int regionIndex = -1;
            for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
                CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
                if (mergedRegion.getFirstRow() == overlappingRegion.getFirstRow()
                        && mergedRegion.getFirstColumn() == overlappingRegion.getFirstColumn()) {
                    regionIndex = i;
                    break;
                }
            }
            // 移除重叠范围
            if (regionIndex >= 0) {
                sheet.removeMergedRegion(regionIndex);
            }

            // 更新新合并单元格范围的位置和大小
            newMergeRegion.setFirstRow(Math.min(newMergeRegion.getFirstRow(), overlappingRegion.getFirstRow()));
            newMergeRegion.setFirstColumn(Math.min(newMergeRegion.getFirstColumn(), overlappingRegion.getFirstColumn()));
            newMergeRegion.setLastRow(Math.max(newMergeRegion.getLastRow(), overlappingRegion.getLastRow()));
            newMergeRegion.setLastColumn(Math.max(newMergeRegion.getLastColumn(), overlappingRegion.getLastColumn()));
        }
        // 添加新的合并单元格范围
        sheet.addMergedRegion(newMergeRegion);
    }

    /**
     * 获取已合并的单元格范围列表
     * @param sheet 要获取的sheet
     * @return 已合并的单元格范围列表
     */
    private List<CellRangeAddress> getMergedRegions(Sheet sheet) {
        List<CellRangeAddress> mergedRegions = new ArrayList<>();
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            mergedRegions.add(sheet.getMergedRegion(i));
        }
        return mergedRegions;
    }

    /**
     * 获取单元格的值
     * @param cell 单元格
     * @return 单元格的值
     */
    private Object getCellValue(Cell cell) {
        Object value = null;
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                    value = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        value =cell.getDateCellValue();
                        break;
                    } else {
                        value =cell.getNumericCellValue();
                        break;
                    }
                case BOOLEAN:
                    value = cell.getBooleanCellValue();
                    break;
                case FORMULA:
                    value = cell.getCellFormula();
                    break;
                default:
                    break;
            }
        }
        return value;
    }
}
