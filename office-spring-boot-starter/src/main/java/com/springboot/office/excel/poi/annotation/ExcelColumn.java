package com.springboot.office.excel.poi.annotation;

import com.springboot.office.excel.poi.converter.Converter;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ExcelColumn {
    /**
     * 列名，默认为""，表示使用注解的field名作为列名
     * @return
     */
    String[] value() default {""};
    /**
     * 列索引，从0开始，默认为-1，表示不指定列索引，使用注解的value作为列名
     * @return
     */
    int index() default -1;

    int order() default Integer.MAX_VALUE;
    /**
     * 是否合并单元格(去表头外)
     * @return
     */
    boolean merge() default false;

    /**
     * 转换器，默认为Converter.class，表示使用默认转换器
     * @return
     */
    Class<? extends Converter> converter() default Converter.class;

}
