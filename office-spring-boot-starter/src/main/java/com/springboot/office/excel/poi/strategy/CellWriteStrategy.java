package com.springboot.office.excel.poi.strategy;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * 单元格写入策略接口
 */
public interface CellWriteStrategy {
    void apply(Cell cell, Object value, Sheet sheet, SXSSFWorkbook workbook);
}
