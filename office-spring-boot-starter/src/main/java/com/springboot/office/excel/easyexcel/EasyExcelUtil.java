package com.springboot.office.excel.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.springboot.office.excel.easyexcel.strategy.CustomCellStyleStrategy;
import com.springboot.office.excel.easyexcel.strategy.CustomWidthStyleStrategy;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StopWatch;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * EasyExcel工具类
 * @author liuc
 * @date 2024-08-13 8:50
 */
@Log4j2
public class EasyExcelUtil {
    // 每个 sheet 写入的数据
    public static final int NUM_PER_SHEET = 300000;
    // 每次向 sheet 中写入的数据（分页写入）
    public static final int NUM_BY_TIMES = 50000;
    // 默认线程池大小
    private static final int DEFAULT_THREAD_POOL_SIZE = 10;

    /**
     * 导出数据到Excel文件的方法，支持自定义单元格样式和列宽样式
     *
     * @param fileName 文件名
     *                 例如： "D:/test.xlsx"
     * @param data     数据
     * @param clazz    实体类
     */
    public static void export(String fileName, List<?> data, Class<?> clazz) {
        log.info("导出excel名称={}", fileName);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        int dataSize = data.size();
        // 计算sheet数量
        int sheetNum = (dataSize + NUM_PER_SHEET - 1) / NUM_PER_SHEET;
        // 计算每个sheet写入次数
        int writeNumPerSheet = (NUM_PER_SHEET + NUM_BY_TIMES - 1) / NUM_BY_TIMES;
        // 最后一个sheet写入数量
        int writeNumLastSheet = dataSize - (sheetNum - 1) * NUM_PER_SHEET;
        // 最后一个sheet写入次数
        int writeNumPerLastSheet = (writeNumLastSheet + NUM_BY_TIMES - 1) / NUM_BY_TIMES;

        try (ExcelWriter excelWriter = EasyExcel.write(fileName, clazz).build()) {
            for (int i = 0; i < sheetNum; i++) {
                String sheetName = "sheet" + i;
                WriteSheet writeSheet = EasyExcel.writerSheet(i, sheetName)
                        .registerWriteHandler(new CustomCellStyleStrategy()) // 自定义单元格样式
                        .registerWriteHandler(new CustomWidthStyleStrategy()) // 自定义列宽样式
                        .build();
                // 每个sheet写入的次数
                int writeNum = i == sheetNum - 1 ? writeNumPerLastSheet : writeNumPerSheet;
                // 每个sheet最后一次写入的最后行数
                int endEndNum = i == sheetNum - 1 ? dataSize : (i + 1) * NUM_PER_SHEET;

                for (int j = 0; j < writeNum; j++) {
                    int startNum = i * NUM_PER_SHEET + j * NUM_BY_TIMES;
                    // 避免越界
                    int endNum = Math.min(startNum + NUM_BY_TIMES, endEndNum);
                    excelWriter.write(data.subList(startNum, endNum), writeSheet);
                    log.info("写入{}，数据量={}-{}={}", sheetName, endNum, startNum, endNum - startNum);
                }
            }
        } catch (Exception e) {
            log.error("导出数据时发生错误", e);
        } finally {
            stopWatch.stop();
            log.info("导出excel结束,总数据量={}，耗时={}ms", dataSize, stopWatch.getTotalTimeMillis());
        }
    }


    /**
     * 读取excel
     * @param fileName   Excel文件名 例如："D:/test.xlsx"
     * @param dataClass     数据模型类
     * @param readListener  读取监听器
     * @param excelTypeEnum Excel文件类型
     */

    public static void importExcel(String fileName, Class<?> dataClass, ReadListener<?> readListener, ExcelTypeEnum excelTypeEnum) {
        ExecutorService executorService = Executors.newFixedThreadPool(DEFAULT_THREAD_POOL_SIZE);
        try {
            // 获取 Excel 文件的 Sheet 列表
            List<ReadSheet> sheets = getSheets(fileName, dataClass, excelTypeEnum,readListener);
            log.info("开始读取 Excel 文件, sheet 数量={}", sheets.size());
            List<Future<?>> futures = new ArrayList<>(sheets.size());
            for (ReadSheet readSheet : sheets) {
                Future<?> future = executorService.submit(() -> {
                    try (ExcelReader excelReader = EasyExcel.read(fileName, dataClass, readListener)
                            .excelType(excelTypeEnum)
                            .build()) {
                        log.info("开始读取 sheet {}", readSheet.getSheetNo());
                        excelReader.read(readSheet);
                    } catch (Exception e) {
                        log.error("读取 sheet {} 时发生错误", readSheet.getSheetNo(), e);
                        // 可以在这里添加额外的错误处理逻辑，例如重试等
                    }
                });
                futures.add(future);
            }

            // 等待所有任务完成
            for (Future<?> future : futures) {
                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    log.error("任务执行过程中发生错误", e);
                }
            }
        } finally {
            // 确保线程池关闭
            executorService.shutdown();
        }
    }

    /**
     * 获取 Excel 文件的 Sheet 列表
     * @param fileName   Excel 文件名 例如："D:/test.xlsx"
     * @param dataClass  数据模型类
     * @param excelTypeEnum Excel 文件类型
     * @param readListener  读取监听器
     * @return List<ReadSheet>  Excel 文件的 Sheet 列表
     */
    private static List<ReadSheet> getSheets(String fileName, Class<?> dataClass, ExcelTypeEnum excelTypeEnum,ReadListener<?> readListener) {
        try (ExcelReader excelReader = EasyExcel.read(fileName, dataClass,readListener)
                .excelType(excelTypeEnum)
                .build()) {
            return excelReader.excelExecutor().sheetList();
        } catch (Exception e) {
            log.error("获取 sheet 列表时发生错误", e);
            return new ArrayList<>();
        }
    }

}
