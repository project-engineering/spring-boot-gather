package com.springboot.office.excel.poi.converter;


public interface Converter <T>{
    /**
     * 用于导出Excel时，将Java对象转换为Excel单元格值
     * @param value Java对象
     * @return Excel单元格值
     * @throws Exception 转换异常
     */
    Object convertToExcelData(Object value) throws UnsupportedOperationException;
    /**
     * 用于导入Excel时，将Excel单元格值转换为Java对象
     * @param value Excel单元格值
     * @return Java对象
     * @throws Exception 转换异常
     */
    Object convertToJavaData(Object value) throws UnsupportedOperationException;
}
