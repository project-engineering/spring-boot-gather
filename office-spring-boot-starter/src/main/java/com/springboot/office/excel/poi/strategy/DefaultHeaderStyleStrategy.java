package com.springboot.office.excel.poi.strategy;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * 默认表头样式策略
 * @author liuc
 * @date 2024-09-16 20:28
 */
public class DefaultHeaderStyleStrategy implements HeaderStyleStrategy {
    @Override
    public void apply(Row row, Cell cell, Sheet sheet, SXSSFWorkbook workbook) {
        // 设置特定样式
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setWrapText(true);

        Font headerFont = workbook.createFont();
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        headerFont.setFontHeightInPoints((short) 12);
        headerStyle.setFont(headerFont);

        cell.setCellStyle(headerStyle);
    }
}