package com.springboot.office.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.log4j.Log4j2;
import java.text.SimpleDateFormat;

/**
 * JSON转换工具类
 * @author liuc
 * @version V1.0
 * @date 2021/11/1 14:56
 * @since JDK1.8
 */
@Log4j2
public class JsonUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    static {
        objectMapper.registerModule(new JavaTimeModule());
        // 统一日期格式yyyy-MM-dd HH:mm:ss
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 将对象转换为json字符串
     * @param object 需要转换的对象
     * @return json字符串
     */
    public static String toJson(Object object)  {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("json序列化异常,{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 将对象转换为json字符串，忽略值为null的属性
     * @param object 需要转换的对象
     * @param ignoreNullValue 是否忽略值为null的属性
     * @return json字符串
     */
    public static String toJson(Object object,boolean ignoreNullValue)  {
        try {
            if (ignoreNullValue) {
                // 设置序列化时忽略值为 null 的属性
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                return objectMapper.writeValueAsString(object);
            } else {
                return objectMapper.writeValueAsString(object);
            }
        } catch (JsonProcessingException e) {
            log.error("json序列化异常,{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 将json字符串转换为对象
     * @param json 字符串
     * @param clazz 对象类型
     * @return 对象
     */
    public static <T> T fromJson(String json, Class<T> clazz){
        try {
            return objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            log.error("json反序列化异常,{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 将json字符串转换为对象
     * @param json 字符串
     * @param typeReference 类型引用
     * @return 对象
     */
    public static <T> T fromJson(String json, TypeReference<T> typeReference){
        try {
            return objectMapper.readValue(json, typeReference);
        } catch (JsonProcessingException e) {
            log.error("json反序列化异常, {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
