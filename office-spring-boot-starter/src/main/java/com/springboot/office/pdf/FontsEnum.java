package com.springboot.office.pdf;

import com.itextpdf.io.font.PdfEncodings;

import java.util.HashMap;
import java.util.Map;

/**
 * 字体枚举类
 * @author liuc
 */
public enum FontsEnum {
    /**
     * 微软雅黑
     */
    MICROSOFT_ELEGANT_BLAC("MicrosoftYaHei", "fonts/msyh.ttc,0", PdfEncodings.IDENTITY_H),
    /**
     * 宋体
     */
    SONG_TYPEFACE("SongTi", "fonts/simsun.ttc,0",PdfEncodings.IDENTITY_H),
    /**
     * 新宋体
     */
    XIN_SONG_TYPEFACE("XinSongTi", "fonts/simsun.ttc,1",PdfEncodings.IDENTITY_H),
    /**
     * 仿宋
     */
    FANG_SONG("FangSong", "fonts/simfang.ttf",PdfEncodings.IDENTITY_H),
    /**
     * 华文仿宋
     */
    HUA_WEN_FANG_SONG("HuaWenFangSong", "fonts/STFANGSO.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文楷体
     */
    HUA_WEN_KAI_TI("HuaWenKaiTi", "fonts/STKAITI.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文隶书
     */
    HUA_WEN_LI_SHU("HuaWenLiShu", "fonts/STLITI.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文宋体
     */
    HUA_WEN_SONG_TI("HuaWenSongTi", "fonts/STSONG.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文细黑
     */
    HUA_WEN_XI_HEI("HuaWenXiHei", "fonts/STXIHEI.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文新魏
     */
    HUA_WEN_XIN_WEI("HuaWenXinWei", "fonts/STXINWEI.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文行楷
     */
    HUA_WEN_XING_KAI("HuaWenXingKai", "fonts/STXINGKA.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 楷体
     */
    KAI_TYPEFACE("KaiTi", "fonts/simkai.ttf",PdfEncodings.IDENTITY_H),
    /**
     * 隶书
     */
    LI_SHU("LiShu", "fonts/SIMLI.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文中宋
     */
    HUA_WEN_ZHONG_SONG("HuaWenZhongSong", "fonts/STZHONGS.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文彩云
     */
    HUA_WEN_CAI_YUN("HuaWenCaiYun", "fonts/STCAIYUN.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 华文琥珀
     */
    HUA_WEN_HU_PO("HuaWenHuPo", "fonts/STHUPO.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 黑体
     */
    BLACK_TYPEFACE("HeiTi", "fonts/simhei.ttf",PdfEncodings.IDENTITY_H),
    /**
     * 幼圆
     */
    YOU_YUAN("YouYuan", "fonts/SIMYOU.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 方正姚体
     */
    FANG_ZHENG_YAO_TI("FangZhengYaoTi", "fonts/FZYTK.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 方正舒体
     */
    FANG_ZHENG_SHU_TI("FangZhengShuTi", "fonts/FZSTK.TTF",PdfEncodings.IDENTITY_H),
    /**
     * 疯狂行草
     */
    FENG_KUANG_XING_CAO("FengKuangXingCao", "fonts/crazycursivefont.ttf",PdfEncodings.IDENTITY_H),
    /**
     * 汉仪黄科行书简体
     */
    HAN_YI_HUANG_KE_XING_SHU_J("HanYiHuangKeXingShuJ", "fonts/hanyihuangkexingshujian.ttf",PdfEncodings.IDENTITY_H),
    /**
     * 极字经典行草简繁
     */
    JI_ZI_JING_DIAN_XING_CAO_JIAN_FAN("JiZiJingDianXingCaoJianFan", "fonts/jizijingdianxingcaojianfan.ttf",PdfEncodings.IDENTITY_H);

    /**
     * 别名，对应HTML文件中font-family属性，如果需要用黑体，那么就要在HTML中将font-family属性设置如下
     * <h1 style="font-family:HeiTi">内容</h1>
     */
    private String fontFamily;
    /**
     * 字体存储的路径
     */
    private String fontName;
    /**
     * 编码
     */
    private String encoding;

    FontsEnum(String fontFamily,String fontName, String encoding) {
        this.fontFamily = fontFamily;
        this.fontName = fontName;
        this.encoding = encoding;
    }

    public static Map<String,Object> of(String fontFamily) {
        Map<String,Object> map = new HashMap<>();
        for(FontsEnum fontsEnum : FontsEnum.values()) {
            if(fontsEnum.fontFamily.equalsIgnoreCase(fontFamily)) {
                map.put("fontName",fontsEnum.fontName);
                map.put("encoding",fontsEnum.encoding);
                return map;
            }
        }
        return map;
    }
}
