package com.springboot.office.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.net.FileRetrieve;
import com.itextpdf.tool.xml.net.ReadingProcessor;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.html.ImageProvider;
import com.springboot.office.util.FileUtil;
import com.springboot.office.util.JsonUtil;
import com.springboot.office.util.ResourceUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * pdf 导出工具类
 * @author liuc
 */
@Log4j2
public class PdfUtil {
    /**
     * 模板路径
     */
    private final String htmlTemplate;
    /**
     * 输出到模板的具体内容
     */
    private Map<String, Object> dataMap;
    /**
     * 创建好pdf所存储的目标路径
     */
    private final String targetPdf;
    /**
     * 纸张类型
     * 参考：PageSize.A4,PageSize.A3,PageSize.A2,PageSize.A1,PageSize.A0,PageSize.B5,PageSize.B4,PageSize.LETTER,PageSize.LEGAL,PageSize.EXECUTIVE,PageSize.FOLIO,PageSize.LEDGER,PageSize.TABLOID
     * 自定义：new Rectangle(width, height)
     * 默认：PageSize.A4
     * 注意：自定义纸张类型时，width和height单位为磅，1磅=1/72英寸
     */
    private final Rectangle pageSize;
    /**
     * 页眉
     */
    private final String header;
    /**
     * 是否添加页脚
     */
    private final boolean isFooter;
    /**
     * 水印
     */
    private final File watermark;
    /**
     * 左侧页眉
     */
    private final String leftHeader;
    /**
     * 右侧页眉
     */
    private final String rightHeader;

    public PdfUtil(Builder builder) {
        this.htmlTemplate = builder.htmlTemplate;
        this.dataMap = builder.dataMap;
        this.targetPdf = builder.targetPdf;
        this.pageSize = builder.pageSize;
        this.header = builder.header;
        this.isFooter = builder.isFooter;
        this.watermark = builder.watermark;
        this.leftHeader = builder.leftHeader;
        this.rightHeader = builder.rightHeader;
    }

    public static class Builder {
        private String htmlTemplate;
        private Map<String, Object> dataMap;
        private String targetPdf;
        private Rectangle pageSize;
        private String header;
        private boolean isFooter;
        private File watermark;
        private String leftHeader;
        private String rightHeader;

        /**
         * 模板路径
         * @param htmlTemplate 模板路径
         * @return Builder 对象
         */
        public Builder htmlTemplate(String htmlTemplate) {
            this.htmlTemplate = htmlTemplate;
            return this;
        }

        /**
         * 输出到模板的具体内容
         * @param dataMap 输出到模板的具体内容
         * @return Builder 对象
         */
        public Builder dataMap(Map<String, Object> dataMap) {
            this.dataMap = dataMap;
            return this;
        }

        /**
         * 创建好pdf所存储的目标路径
         * @param targetPdf 目标路径
         * @return Builder 对象
         */
        public Builder targetPdf(String targetPdf) {
            this.targetPdf = targetPdf;
            return this;
        }

        /**
         * 纸张类型
         * @param pageSize 纸张类型
         * @return Builder 对象
         */
        public Builder pageSize(Rectangle pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        /**
         * 页眉
         * @param header 页眉
         * @return Builder 对象
         */
        public Builder header(String header) {
            this.header = header;
            return this;
        }

        /**
         * 是否添加页脚
         * @param isFooter 是否添加页脚
         * @return Builder 对象
         */
        public Builder isFooter(boolean isFooter) {
            this.isFooter = isFooter;
            return this;
        }

        /**
         * 水印
         * @param watermark 水印
         * @return Builder 对象
         */
        public Builder watermark(File watermark) {
            this.watermark = watermark;
            return this;
        }

        /**
         * 左侧页眉
         * @param leftHeader 左侧页眉
         * @return Builder 对象
         */
        public Builder leftHeader(String leftHeader) {
            this.leftHeader = leftHeader;
            return this;
        }

        /**
         * 右侧页眉
         * @param rightHeader 右侧页眉
         * @return Builder 对象
         */
        public Builder rightHeader(String rightHeader) {
            this.rightHeader = rightHeader;
            return this;
        }

        /**
         * 构建PdfUtil对象
         * @return PdfUtil 对象
         */
        public PdfUtil build() {
            return new PdfUtil(this);
        }
    }

    @Override
    public String toString() {
        return "PdfUtil{" +
                "htmlTemplate='" + htmlTemplate + '\'' +
                ", dataMap=" + dataMap +
                ", targetPdf='" + targetPdf + '\'' +
                ", pageSize=" + pageSize +
                ", header='" + header + '\'' +
                ", isFooter=" + isFooter +
                ", watermark=" + watermark +
                ", leftHeader='" + leftHeader + '\'' +
                ", rightHeader='" + rightHeader + '\'' +
                '}';
    }

    /**
     * 根据模板生成html字符串
     * @param template 模板路径
     * @param variables 输出到模板的具体内容
     * @return html字符串
     */
    public static String htmlGenerate(String template, Map<String, Object> variables) {
        String htmlStr = null;
        try {
            Configuration config = new Configuration(Configuration.VERSION_2_3_23);
            config.setDirectoryForTemplateLoading(new File(ResourceUtils.getParent(template)));
            config.setLogTemplateExceptions(false);
            config.setWrapUncheckedExceptions(true);
            Template tp = config.getTemplate(ResourceUtils.getFileName(template));
            StringWriter stringWriter = new StringWriter();
            BufferedWriter writer = new BufferedWriter(stringWriter);
            tp.process(variables, writer);
            htmlStr = formatHtml(stringWriter.toString());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            log.error("", e);
        }
        return htmlStr;
    }

    /**
     * 创建pdf
     *
     */
    public void createPdf()
            throws Exception {
        log.info("开始生成pdf文件...");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Assert.notNull(htmlTemplate, "htmlTemplate can not be null");
        Assert.notNull(dataMap, "Content can not be null");
        /*
         * 根据freemarker模板生成html
         */
        //将参数中的换行符\n替换成<br/>标签
        String str = JsonUtil.toJson(dataMap).replace("\\n","<br/>");
        dataMap = JsonUtil.fromJson(str,Map.class);
        String htmlStr = htmlGenerate(htmlTemplate, dataMap);
        //编码格式
        final String charsetName = "UTF-8";
        File file = FileUtil.getFile(targetPdf);
        OutputStream out = new FileOutputStream(file);
        /*
         * 设置文档格式,数字边距
         */
        Document document = new Document(pageSize);
        document.setMargins(30, 30, 30, 30);
        PdfWriter writer = PdfWriter.getInstance(document, out);
        /*
         * 添加页码
         */
        PdfBuilder builder = new PdfBuilder(header, 10, pageSize, watermark, isFooter,leftHeader,rightHeader);
        writer.setPageEvent(builder);
        // 打开文档
        document.open();
//        // 生成第一页
//        document.newPage();
//        //设置作者
//        document.addAuthor("liuc");
//        //设置创建日期
//        document.addCreationDate();
//        // 设置创建者
//        document.addCreator("www.baidu.com");
//        // 设置生产者
//        document.addProducer();
//        // 设置关键字
//        document.addKeywords("my");
//        //设置标题
//        document.addTitle("Set Attribute Example");
//        //设置主题
//        document.addSubject("An example to show how attributes can be added to pdf files.");

        /*
         * html内容解析
         */
        HtmlPipelineContext htmlContext =
                new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider() {
                    @Override
                    public Font getFont(String fontName, String encoding, float size, final int style) {
                        Font font = null;
                        try {
                            /*
                             * 根据CSS中不同的字体名返回不同的字体实例
                             * 若出现*`Font '/yourFontDir/yourFont.ttf' with 'Identity-H' is not recognized`*这样的异常，请尝试在你的字体文件路径后面加上 [,0 |,1|,2]，即：
                             * BaseFont.createFont("/yourFontDir/yourFont.ttf,1",BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                             * 如有特殊需求，只需要在FontsEnum加入新的定义，然后再HTML模板中加入样式就行了，比如
                             * 1、在FontsEnum枚举类中加入配置
                             *    //微软雅黑
                             *    MICROSOFT_ELEGANT_BLAC("MicrosoftYaHei", "fonts/msyh.ttc,0", PdfEncodings.IDENTITY_H),
                             * 2、在 HTML中需要特殊样式的位置加上 font-family:MicrosoftYaHei
                             *    <h1 style="font-family:MicrosoftYaHei">内容</h1>
                             * 3、注意yourFontDir中有你需要的样式库如/yourFontDir/msyh.ttc
                             */
                            if (fontName == null) {
                                //默认宋体
                                fontName = "SongTi";
                            }
                            Map<String,Object> fontsEnumMap = FontsEnum.of(fontName);
                            fontName = (String) fontsEnumMap.get("fontName");
                            String encode = (String) fontsEnumMap.get("encoding");
                            if (fontName == null) {
                                log.error("字体不存在,请检查!");
                                throw new RuntimeException("字体不存在,请检查!");
                            }
                            return new Font(BaseFont.createFont(fontName, encode, BaseFont.NOT_EMBEDDED), size,
                                    style);
                        } catch (Exception e) {
                            log.error("", e);
                        }
                        return font;
                    }
                })) {
                    @Override
                    public HtmlPipelineContext clone() throws CloneNotSupportedException {
                        HtmlPipelineContext context = super.clone();
                        ImageProvider imageProvider = this.getImageProvider();
                        context.setImageProvider(imageProvider);
                        return context;
                    }
                };

        /*
         * 图片解析
         */
        htmlContext.setImageProvider(new AbstractImageProvider() {

            String rootPath = "C:\\Users\\Administrator\\Desktop\\刘亦菲\\";

            @Override
            public String getImageRootPath() {
                return rootPath;
            }

            @Override
            public Image retrieve(String src) {
                if (StringUtils.isEmpty(src)) {
                    return null;
                }
                try {
                    Image image = Image.getInstance(new File(rootPath, src).toURI().toString());
                    /*
                     * 图片显示位置
                     */
                    image.setAbsolutePosition(400, 400);
                    store(src, image);
                    return image;
                } catch (Exception e) {
                    log.error("", e);
                }
                return super.retrieve(src);
            }
        });
        htmlContext.setAcceptUnknown(true).autoBookmark(true)
                .setTagFactory(Tags.getHtmlTagProcessorFactory());

        /*
         * css解析
         */
        CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
        cssResolver.setFileRetrieve(new FileRetrieve() {
            @Override
            public void processFromStream(InputStream in, ReadingProcessor processor) throws IOException {
                try (InputStreamReader reader = new InputStreamReader(in, charsetName)) {
                    int i = -1;
                    while (-1 != (i = reader.read())) {
                        processor.process(i);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
            }

            /**
             * 解析href
             */
            @Override
            public void processFromHref(String href, ReadingProcessor processor) throws IOException {
                InputStream is = new ByteArrayInputStream(href.getBytes());
                try {
                    InputStreamReader reader = new InputStreamReader(is, charsetName);
                    int i = -1;
                    while (-1 != (i = reader.read())) {
                        processor.process(i);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }

            }
        });

        HtmlPipeline htmlPipeline =
                new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer));
        Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
        XMLWorker worker = null;
        worker = new XMLWorker(pipeline, true);
        XMLParser parser = new XMLParser(true, worker, Charset.forName(charsetName));
        try (InputStream inputStream = new ByteArrayInputStream(htmlStr.getBytes())) {
            parser.parse(inputStream, Charset.forName(charsetName));
        }
        // 关闭文档
        document.close();
        stopWatch.stop();
        log.info("生成pdf文件成功，耗时：{} 毫秒!", stopWatch.getTotalTimeMillis());
    }


    /**
     * jsoup 格式化html
     * @param html html字符串
     * @return 格式化后的html字符串
     */
    private static String formatHtml(String html) {
        org.jsoup.nodes.Document doc = Jsoup.parse(html);
        // 去除过大的宽度
        String style = doc.attr("style");
        if ((!style.isEmpty()) && style.contains("width")) {
            doc.attr("style", "");
        }
        Elements divs = doc.select("div");
        for (Element div : divs) {
            String divStyle = div.attr("style");
            if ((!divStyle.isEmpty()) && divStyle.contains("width")) {
                div.attr("style", "");
            }
        }
        // jsoup生成闭合标签
        doc.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        return doc.html();
    }

}
