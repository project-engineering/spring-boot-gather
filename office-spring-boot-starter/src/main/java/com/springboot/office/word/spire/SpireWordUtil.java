package com.springboot.office.word.spire;

import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;

public class SpireWordUtil {
    public static void main(String[] args) {

        //创建一个 PdfDocument 对象
        PdfDocument doc = new PdfDocument();

        //加载 PDF 文件
        doc.loadFromFile("E:\\A1-20231020-TL.pdf");

        //将 PDF 转换为流动形态的Word
        doc.getConvertOptions().setConvertToWordUsingFlow(true);

        //将PDF转换为Doc格式文件并保存
        doc.saveToFile("E:\\A1-20231020-TL.doc", FileFormat.DOC);

        //将PDF转换为Docx格式文件并保存
        doc.saveToFile("E:\\A1-20231020-TL.docx", FileFormat.DOCX);
        doc.close();
    }
}
