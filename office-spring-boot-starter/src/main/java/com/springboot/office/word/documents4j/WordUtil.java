package com.springboot.office.word.documents4j;

import cn.hutool.core.util.ObjectUtil;
import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StopWatch;
import java.io.*;

/**
 * @author liuc
 * @date 2024-08-20 19:25
 */
@Log4j2
public class WordUtil {

    /**
     * 2003- 版本的word
     */
    private static final String word2003L = ".doc";
    /**
     * 2007+ 版本的word
     */
    private final static String word2007U = ".docx";
    /**
     * pdf 文件后缀
     */
    private final static String PDF = ".pdf";

    /**
     * 将Word文档转换为PDF
     *
     * <p>
     * 支持 Word2003、Word2007+ 版本的Word文档转换为PDF
     * <p>
     * 注意：
     *    通过documents4j 实现word转pdf -- linux 环境 需要有 libreoffice 服务
     *
     * @param inputWordFile word文件地址 如 /root/example.doc
     * @param outputPdfFile pdf文件存放地址 如 /root/example.pdf
     */
    public static void convertWordToPdf(String inputWordFile, String outputPdfFile) {
        log.info("开始将word文件转换为pdf文件,输入文件:{}", inputWordFile);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        validateInputParameters(inputWordFile, outputPdfFile);

        try (
                InputStream docxInputStream = new FileInputStream(inputWordFile);
                OutputStream outputStream = new FileOutputStream(outputPdfFile)
        ) {
            IConverter converter = LocalConverter.builder().build();

            if (inputWordFile.endsWith(word2003L)) {
                convertAndExecute(converter, docxInputStream, outputStream, DocumentType.DOC);
            } else if (inputWordFile.endsWith(word2007U)) {
                convertAndExecute(converter, docxInputStream, outputStream, DocumentType.DOCX);
            }

            closeConverter(converter);
        } catch (Exception e) {
            log.error("word转pdf失败: {}", e.getMessage(), e);
            throw new RuntimeException("word转pdf失败:", e);
        }
        stopWatch.stop();
        log.info("word转pdf成功: {}，耗时:{} ms", outputPdfFile, stopWatch.getTotalTimeMillis());
    }

    /**
     * 校验输入参数
     *
     * @param inputWordFile 输入word文件地址
     * @param outputPdfFile 输出pdf文件地址
     */
    private static void validateInputParameters(String inputWordFile, String outputPdfFile) {
        if (ObjectUtil.isEmpty(inputWordFile) || ObjectUtil.isEmpty(outputPdfFile)) {
            throw new IllegalArgumentException("输入参数不能为空");
        }
        if (!inputWordFile.endsWith(word2003L) && !inputWordFile.endsWith(word2007U)) {
            throw new IllegalArgumentException("输入文件格式不正确，仅支持doc和docx格式");
        }
        if (!outputPdfFile.endsWith(PDF)) {
            throw new IllegalArgumentException("输出文件格式不正确，仅支持pdf格式");
        }
    }

    /**
     * 执行转换操作
     *
     * @param converter              转换器
     * @param inputStream            输入流
     * @param outputStream           输出流
     * @param inputDocumentType      输入文档类型
     */
    private static void convertAndExecute(IConverter converter, InputStream inputStream, OutputStream outputStream, DocumentType inputDocumentType) {
        converter.convert(inputStream)
                .as(inputDocumentType)
                .to(outputStream)
                .as(DocumentType.PDF)
                .execute();
    }

    /**
     * 关闭转换器
     *
     * @param converter 转换器
     */
    private static void closeConverter(IConverter converter) {
        converter.shutDown();
    }
}
