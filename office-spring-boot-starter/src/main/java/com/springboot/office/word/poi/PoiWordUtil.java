package com.springboot.office.word.poi;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StopWatch;
import java.io.*;
import java.nio.file.Path;
import java.util.*;

/**
 * word操作工具类
 *
 * @author liuc
 * @date 2024-08-22 19:36
 */
@Log4j2
public class PoiWordUtil {
    /**
     * 根据模板导出Word文档
     *
     * @param templatePath 模板文件路径
     * @param outputPath   导出的文件路径
     * @param replacements 替换的内容，键为模板中的占位符，值为替换后的内容
     */
    public static void exportWordFromTemplate(String templatePath, String outputPath,
                                              Map<String, Object> replacements) {
        exportWordFromTemplate(templatePath, outputPath, replacements, null, null, null);
    }
    /**
     * 根据模板导出Word文档
     *
     * @param templatePath 模板文件路径
     * @param outputPath   导出的文件路径
     * @param replacements 替换的内容，键为模板中的占位符，值为替换后的内容
     * @param picData      图片数据，键为模板中的占位符，值为替换后的图片路径
     * @param tableHeads   表格头部行索引列表
     *                         例如：模板中有3个表格，tableHeads为{1, 2, 4}，表示第一个表格的头部在第1行，第二个表格的头部在第2行，第三个表格的头部在第4行
     * @param tableDataList 表格数据列表，每个元素为一个表格的数据列表，其中每个数据项为一个Map，键为表格列索引，值为单元格内容
     */
    public static void exportWordFromTemplate(String templatePath, String outputPath,
                                              Map<String, Object> replacements,
                                              Map<String, Object> picData,
                                              List<Integer> tableHeads,
                                              List<Map<Integer, List<Map<String, Object>>>> tableDataList) {
        try {
            log.info("开始导出Word文档...");
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            // 加载模板文档
            XWPFDocument doc = new XWPFDocument(new FileInputStream(templatePath));
            if (ObjectUtils.isEmpty(replacements)) {
                throw new RuntimeException("替换内容不能为空！");
            }
            // 遍历所有段落
            processParagraphs(doc, replacements);
            if (ObjectUtils.isEmpty(tableHeads) && ObjectUtils.isNotEmpty(tableDataList)) {
                throw new RuntimeException("表格头部行索引列表不能为空！");
            }
            if (ObjectUtils.isNotEmpty(tableHeads) && ObjectUtils.isEmpty(tableDataList)) {
                throw new RuntimeException("表格数据列表不能为空！");
            }
            if (ObjectUtils.isNotEmpty(tableHeads) && ObjectUtils.isNotEmpty(tableDataList)) {
                // 遍历所有表格并替换数据
                for (Map<Integer, List<Map<String, Object>>> tableDataMap : tableDataList) {
                    processTables(doc, tableHeads, tableDataMap);
                }
            }

            if (ObjectUtils.isNotEmpty(picData)) {
                // 遍历所有图片
                processPictures(doc, picData);
            }

            // 保存文档
            try (FileOutputStream out = new FileOutputStream(outputPath)) {
                doc.write(out);
            }
            stopWatch.stop();
            log.info("导出Word文档成功,路径：{}，耗时：{}毫秒!", outputPath, stopWatch.getTotalTimeMillis());
        } catch (IOException e) {
            log.error("导出Word文档失败！", e);
            throw new RuntimeException("导出Word文档失败,原因：" + e.getMessage(), e);
        }
    }

    /**
     * 遍历所有表格并替换数据
     *
     * @param doc          文档对象
     * @param tableHeads    表格头部行索引列表
     * @param tableDataMap 表格数据映射，键为表格索引，值为表格数据列表
     */
    private static void processTables(XWPFDocument doc, List<Integer> tableHeads, Map<Integer, List<Map<String, Object>>> tableDataMap) {
        List<XWPFTable> tables = doc.getTables();
        if (tables.size() != tableHeads.size()) {
            throw new RuntimeException("表格数量与表格头部行索引列表数量不匹配！");
        }
        if (tables.size() != tableDataMap.size()) {
            throw new RuntimeException("表格数量与表格数据列表数量不匹配！");
        }
        for (int i = 0; i < tables.size(); i++) {
            XWPFTable table = tables.get(i);
            // 获取当前表格的数据
            List<Map<String, Object>> dataList = tableDataMap.get(i);
            Integer tableHeadIndex = tableHeads.get(i);
            if (dataList != null) {
                processTable(table,tableHeadIndex, dataList);
                // 替换表格中的图片占位符
                replacePicInTable(table, dataList);
            }
        }
    }

    /**
     * 处理单个表格
     *
     * @param table        表格对象
     * @param tableHeadIndex 表格头部行索引
     * @param dataList     表格数据列表
     */
    private static void processTable(XWPFTable table,Integer tableHeadIndex, List<Map<String, Object>> dataList) {
        // 获取模板行（假设第二行为模板行）
        XWPFTableRow templateRow = table.getRow(tableHeadIndex);
        // 获取模板行的列数
        int columnCount = templateRow.getTableCells().size();
        // 从数据列表中为每个数据项创建行
        for (Map<String, Object> data : dataList) {
            // 添加新行并复制模板行的样式
            XWPFTableRow newRow = table.createRow();
            copyTableRow(templateRow, newRow, columnCount);
            // 然后替换新行中的文本
            replaceRowText(newRow, data);
        }
        // 删除模板行，但不删除表头行
        if (table.getRows().size() > tableHeadIndex) {
            table.removeRow(tableHeadIndex);
        }
    }

    /**
     * 替换表格中的图片占位符
     *
     * @param table        表格对象
     * @param dataList     表格数据映射，其中包含图片数据
     */
    private static void replacePicInTable(XWPFTable table, List<Map<String, Object>> dataList) {
        for(int rowIndex = 0; rowIndex < table.getRows().size(); rowIndex++) {
            XWPFTableRow row = table.getRow(rowIndex);
            for(int cellIndex = 0; cellIndex < row.getTableCells().size(); cellIndex++) {
                XWPFTableCell cell = row.getCell(cellIndex);
                // 获取当前单元格的数据
                for (Map<String, Object> cellData : dataList){
                    if (cellData != null) {
                        // 遍历单元格中的所有段落
                        for (XWPFParagraph paragraph : cell.getParagraphs()) {
                            if (checkPic(paragraph.getText())) {
                                // 替换段落中的图片占位符
                                replacePicValue(paragraph, cellData);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 遍历所有段落
     *
     * @param doc       文档对象
     * @param textMap   替换内容映射，键为模板中的占位符，值为替换后的内容
     */
    private static void processParagraphs(XWPFDocument doc, Map<String, Object> textMap) {
        Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
        while (iterator.hasNext()) {
            XWPFParagraph paragraph = iterator.next();
            // 判断此段落是否需要替换
            if (checkText(paragraph.getText())) {
                // 进行替换
                changeText(paragraph, textMap);
            }
        }
    }

    /**
     * 复制表格行
     *
     * @param sourceRow  源行
     * @param targetRow  目标行
     * @param columnCount 列数
     */
    private static void copyTableRow(XWPFTableRow sourceRow, XWPFTableRow targetRow, int columnCount) {
        for (int i = 0; i < columnCount; i++) {
            XWPFTableCell sourceCell = sourceRow.getCell(i);
            XWPFTableCell targetCell = targetRow.getCell(i);
            if (targetCell == null) {
                targetCell = targetRow.createCell();
            }
            // 复制单元格的格式
            targetCell.getCTTc().setTcPr(sourceCell.getCTTc().getTcPr());
            // 复制段落
            for (XWPFParagraph sourceParagraph : sourceCell.getParagraphs()) {
                XWPFParagraph targetParagraph = targetCell.addParagraph();
                targetParagraph.getCTP().setPPr(sourceParagraph.getCTP().getPPr());
                for (XWPFRun sourceRun : sourceParagraph.getRuns()) {
                    XWPFRun targetRun = targetParagraph.createRun();
                    targetRun.setText(sourceRun.getText(0));
                    // 复制运行属性
                    targetRun.getCTR().setRPr(sourceRun.getCTR().getRPr());
                }
            }
        }
    }

    /**
     * 替换表格中的文本
     *
     * @param row  表格行
     * @param data 数据
     */
    private static void replaceRowText(XWPFTableRow row, Map<String, Object> data) {
        for (XWPFTableCell cell : row.getTableCells()) {
            for (XWPFParagraph paragraph : cell.getParagraphs()) {
                for (XWPFRun run : paragraph.getRuns()) {
                    String text = run.getText(0);
                    // 仅当文本不包含图片占位符时，才替换文本
                    if (text != null) {
                        replaceValue(run, data);
                    }
                }
            }
        }
    }

    /**
     * 替换段落中的文本
     *
     * @param run  运行对象
     * @param data 数据
     */
    private static void replaceValue(XWPFRun run, Map<String, Object> data) {
        String text = run.getText(0);
        if (text != null) {
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value != null && text.contains("${" + key + "}")) {
                    text = text.replace("${" + key + "}", value.toString());
                }
            }
            run.setText(text, 0);
        }
    }

    /**
     * 处理文档中的图片
     *
     * @param doc 文档对象
     */
    private static void processPictures(XWPFDocument doc, Map<String, Object> picData) {
        changePic(doc, picData);
    }

    /**
     * 获取资源文件路径
     *
     * @param filePath 资源文件路径
     * @return java.lang.String
     */
    public static String getResourcesPath(String filePath) {
        String resourcePath=null;
        try {
            Resource resource = new ClassPathResource(filePath);
            Path path = resource.getFile().toPath();
            resourcePath = path.toString();
        } catch (IOException e) {
            log.error("获取资源文件失败！", e);
            throw new RuntimeException(e);
        }
        return resourcePath;
    }


    /***
     * 替换段落文本
     * @param paragraph 段落对象
     * @param textMap  需要替换的信息集合
     */
    private static void changeText(XWPFParagraph paragraph, Map<String, Object> textMap) {
        // 获取段落集合
        Iterator<XWPFParagraph> iterator = paragraph.getDocument().getParagraphsIterator();
        while (iterator.hasNext()) {
            paragraph = iterator.next();
            // 判断此段落是否需要替换
            if (checkText(paragraph.getText())) {
                replaceValue(paragraph, textMap);
            }
        }
    }

    /**
     * 替换图片
     *
     * @param document 文档对象
     * @param picData 图片数据
     */

    private static void changePic(XWPFDocument document, Map<String, Object> picData) {
        // 获取段落集合
        Iterator<XWPFParagraph> iterator = document.getParagraphsIterator();
        XWPFParagraph paragraph;
        while (iterator.hasNext()) {
            paragraph = iterator.next();
            // 判断此段落是否需要替换
            String text = paragraph.getText();
            if (checkText(text)) {
                replacePicValue(paragraph, picData);
            }
        }
    }

    /***
     * 检查文本中是否包含指定的字符(此处为“$”)
     *
     * @param text 文本
     * @return boolean
     */
    private static boolean checkText(String text) {
        return text.contains("$");
    }

    /**
     * 检查文本是否为图片
     *
     * @param text 文本
     * @return boolean 是否为图片true/false
     */
    private static boolean checkPic(String text) {
        String[] imageFormats = new String[] {".jpg", ".jpeg", ".png", ".gif", ".bmp", ".dib", ".emf", ".wmf", ".pict", ".tif", ".tiff", ".eps", ".wpg"};
        boolean check = false;
        for (String format : imageFormats) {
            if (text.endsWith(format)) {
                check = true;
                break;
            }
        }
        return check;
    }

    /***
     * 替换内容
     *
     * @param paragraph 段落对象
     * @param textMap  需要替换的信息集合
     */
    private static void replaceValue(XWPFParagraph paragraph, Map<String, Object> textMap) {
        XWPFRun run, nextRun;
        String runsText;
        List<XWPFRun> runs = paragraph.getRuns();
        for (int i = 0; i < runs.size(); i++) {
            run = runs.get(i);
            runsText = run.getText(0);
            if (runsText.contains("${") || (runsText.contains("$") && runs.get(i + 1).getText(0).substring(0, 1).equals("{"))) {
                while (!runsText.contains("}")) {
                    nextRun = runs.get(i + 1);
                    runsText = runsText + nextRun.getText(0);
                    //删除该节点下的数据
                    paragraph.removeRun(i + 1);
                }
                Object value = changeValue(runsText, textMap);
                //判断key在Map中是否存在
                String replaceText = runsText.replace("${", "").replace("}", "");
                if (textMap.containsKey(replaceText)) {
                    run.setText(value.toString(), 0);
                } else {
                    //如果匹配不到，则不修改
                    run.setText(runsText, 0);
                }
            }
        }
    }

    /**
     * 替换图片
     *
     * @param paragraph 段落对象
     * @param picData 图片数据
     */
    private static void replacePicValue(XWPFParagraph paragraph, Map<String, Object> picData) {
        try {
            List<XWPFRun> runs = paragraph.getRuns();
            for (Map.Entry<String, Object> entry : picData.entrySet()) {
                for (XWPFRun run : runs) {
                    // 清空所有的文本
                    run.setText("", 0);

                    // 插入图片
                    String imagePath = (String) picData.get(entry.getKey());
                    String picType = imagePath.substring(imagePath.lastIndexOf(".") + 1);
                    int pictureType = getPictureType(picType);
                    File imageFile = new File(imagePath);
                    if (!imageFile.exists()) {
                        log.error("图片文件不存在！");
                        throw new FileNotFoundException("图片文件不存在： " + imagePath);
                    }
                    try (FileInputStream is = new FileInputStream(imageFile)) {
                        int width = Units.toEMU(150);
                        int height = Units.toEMU(150);
                        run.addPicture(is, pictureType, imagePath, width, height);
                    }
                    // 完成替换后退出循环
                    break;
                }
                // 删除替换后的文本占位符
                for (XWPFRun run : runs) {
                    run.setText("", 0);
                }
                break;
            }

        } catch (FileNotFoundException e) {
            log.error("图片文件未找到！", e);
            throw new RuntimeException("图片文件未找到，原因：" + e.getMessage(), e);
        } catch (IOException e) {
            log.error("IO异常！", e);
            throw new RuntimeException("IO异常，原因：" + e.getMessage(), e);
        } catch (Exception e) {
            log.error("替换图片失败！", e);
            throw new RuntimeException("替换图片失败，原因：" + e.getMessage(), e);
        }
    }

    /**
     * 根据图片类型，取得对应的图片类型代码
     * @param picType 图片类型
     * @return int
     */
    private static int getPictureType(String picType){
        int res = XWPFDocument.PICTURE_TYPE_PICT;
        if(picType != null){
            if(picType.equalsIgnoreCase("png")){
                res = XWPFDocument.PICTURE_TYPE_PNG;
            } else if(picType.equalsIgnoreCase("dib")){
                res = XWPFDocument.PICTURE_TYPE_DIB;
            } else if(picType.equalsIgnoreCase("emf")){
                res = XWPFDocument.PICTURE_TYPE_EMF;
            } else if(picType.equalsIgnoreCase("jpg") || picType.equalsIgnoreCase("jpeg")){
                res = XWPFDocument.PICTURE_TYPE_JPEG;
            } else if(picType.equalsIgnoreCase("wmf")){
                res = XWPFDocument.PICTURE_TYPE_WMF;
            } else if(picType.equalsIgnoreCase("gif")){
                res = XWPFDocument.PICTURE_TYPE_GIF;
            } else if(picType.equalsIgnoreCase("tiff")){
                res = XWPFDocument.PICTURE_TYPE_TIFF;
            } else if(picType.equalsIgnoreCase("eps")){
                res = XWPFDocument.PICTURE_TYPE_EPS;
            } else if(picType.equalsIgnoreCase("bmp")){
                res = XWPFDocument.PICTURE_TYPE_BMP;
            } else if(picType.equalsIgnoreCase("wpg")){
                res = XWPFDocument.PICTURE_TYPE_WPG;
            } else{
                log.error("图片格式不支持！");
                throw new RuntimeException("图片格式不支持：" + picType);
            }
        }
        return res;
    }

    /***
     * 匹配参数
     *
     * @param value 文本
     * @param textMap 需要替换的信息集合
     * @return java.lang.Object
     */
    private static Object changeValue(String value, Map<String, Object> textMap) {
        Object valu = "";
        for (Map.Entry<String, Object> textSet : textMap.entrySet()) {
            // 匹配模板与替换值 格式${key}
            String key = textSet.getKey();
            if (value.contains(key)) {
                valu = textSet.getValue();
            }
        }
        return valu;
    }
}
