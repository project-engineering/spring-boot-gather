package com.springboot.baseline.module.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.baseline.module.common.dao.CustCodeValueMapper;
import com.springboot.baseline.module.common.entity.CustCodeValue;
import com.springboot.baseline.module.common.service.CustCodeValueService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

/**
 * 码值表服务实现类
 * @className: CustCodeValue
 * @author: liuc
 * @date: 2019-11-12
 */
@Service
public class CustCodeValueServiceImpl extends ServiceImpl<CustCodeValueMapper, CustCodeValue> implements CustCodeValueService {
    @Resource
    private CustCodeValueMapper mapper;

    @Override
    public List<CustCodeValue> selectAllByAsc(String[] columns) {
        QueryWrapper<CustCodeValue> ew = new QueryWrapper<>();
        ew.orderBy(true,true, Arrays.asList(columns));
        List<CustCodeValue> list = mapper.selectList(ew);
        return list;
    }

    @Override
    public List<CustCodeValue> selectGroupByAndAsc(String[] groupBy, String[] columns) {
        QueryWrapper<CustCodeValue> ew = new QueryWrapper<>();
        ew.select("code_type_key", "code_type_key_name");
        ew.groupBy(Arrays.asList(groupBy));
        ew.orderBy(true,true, Arrays.asList(columns));
        List<CustCodeValue> list = mapper.selectList(ew);
        return list;
    }
}
