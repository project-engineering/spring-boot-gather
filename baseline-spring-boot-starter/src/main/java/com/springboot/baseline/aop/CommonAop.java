package com.springboot.baseline.aop;//package com.springboot.param.aop;
//
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//
//@Slf4j
//@Aspect
//@Component
//public class CommonAop {
//
//
//    @Pointcut("@within(com.springboot.param.annotation.ResponseResult)")
//    private void check() {
//
//    }
//
//
//    @Around("check()")
//    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
//        Object object = joinPoint.proceed();
//        return object;
//    }
//}