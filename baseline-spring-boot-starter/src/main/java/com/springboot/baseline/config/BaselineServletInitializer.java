package com.springboot.baseline.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;


@Configuration
@Log4j2
public class BaselineServletInitializer implements CommandLineRunner {

    @Override
    public void run(String... args){
        log.info("=====Baseline应用已经成功启动=====");
    }
}