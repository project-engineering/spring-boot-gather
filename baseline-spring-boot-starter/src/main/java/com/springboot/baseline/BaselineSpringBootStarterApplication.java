package com.springboot.baseline;

import com.springboot.baseline.entity.Result;
import com.springboot.baseline.init.UtilCommonCode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class BaselineSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaselineSpringBootStarterApplication.class, args);
    }

    @GetMapping("/hello")
    public Result hello() {
        return Result.success(UtilCommonCode.getCodeValueByType("CD003600", "CD003602"));
    }
}
