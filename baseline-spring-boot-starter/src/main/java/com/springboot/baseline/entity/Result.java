package com.springboot.baseline.entity;

import com.springboot.baseline.enums.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

@Data
@ApiModel(value = "返回结果实体类", description = "结果实体类")
public class Result implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回码")
    private String code;

    @ApiModelProperty(value = "返回状态")
    private boolean success;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Object data;

    private Result() {

    }

    public Result(ResultCode resultCode, Object data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    private void setResultCode(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    // 返回成功
    public static Result success() {
        Result result = new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setSuccess(true);
        result.setMessage(ResultCode.SUCCESS.getMessage());
        return result;
    }

    // 返回成功
    public static Result success(Object data) {
        Result result = new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setSuccess(true);
        result.setMessage(ResultCode.SUCCESS.getMessage());
        result.setData(data);
        return result;
    }

    // 返回失败
    public static Result fail(ResultCode resultCode) {
        Result result = new Result();
        result.setSuccess(false);
        result.setResultCode(resultCode);
        return result;
    }

    public static Result fail(String message) {
        Result result = new Result();
        result.setCode(ResultCode.UNKUOW_ERROR.getCode());
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

    public static Result fail(String code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

    public static Result fail(ResultCode resultCode, Throwable e) {
        Result result = new Result();
        result.setCode(resultCode.getCode());
        result.setSuccess(false);
        result.setMessage(resultCode.getMessage());
        result.setData(e.getClass().getName());
        return result;
    }

    public static Result fail(ResultCode resultCode, Throwable e, String message) {
        Result result = Result.fail(resultCode, e);
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

}