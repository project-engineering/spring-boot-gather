package com.springboot.baseline.util;

import com.springboot.baseline.entity.Result;
import com.springboot.baseline.enums.ResultCode;

public class ResultUtils {
    /**
     * 成功时返回的方法（data为空）
     */
    public static Result success() {
        return Result.success();
    }

    /**
     * 成功时返回的方法（data不为空）
     */
    public static Result success(Object o) {
        return Result.success(o);
    }
    /**
     * 失败时返回的方法
     */
    public static Result error(ResultCode resultCode) {
        return Result.fail(resultCode);
    }
    /**
     * 失败时返回的方法
     */
    public static Result error(String message) {
        return Result.fail(message);
    }
    /**
     * 失败时返回的方法
     */
    public static Result error(String code, String message) {
        return Result.fail(code, message);
    }

}