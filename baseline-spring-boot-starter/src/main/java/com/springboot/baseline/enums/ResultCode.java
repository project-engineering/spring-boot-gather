package com.springboot.baseline.enums;

public enum ResultCode {

    /* 成功状态码 */
    SUCCESS("000000", "操作成功"),
    /* 系统错误 100000-199999*/
    SYSTEM_ERROR("100000", "系统异常，请稍后重试!"),
    LIMIT_ERROR("100001","当前排队人数较多，请稍后再试!"),
    PREVENT_SUBMIT_ERROR("100002","请勿重复提交!"),
    /* 用户错误 200000-299999*/
    USER_NOTLOGGED_IN("200000", "用户未登录"),
    USER_LOGIN_ERROR("200001", "账号不存在或密码错误"),
    /**
     * 客户端request body参数错误
     * 主要是未能通过Hibernate Validator校验的异常处理
     * <p>
     * org.springframework.web.bind.MethodArgumentNotValidException
     */
    CLIENT_REQUEST_BODY_CHECK_ERROR("300000", "客户端请求体参数校验不通过"),
    PARAM_IS_BLANK("300001", "参数为空"),
    PARAM_TYPE_BIND_ERROR("300002", "参数类型错误"),
    PARAM_NOT_COMPLETE("300003", "参数缺失"),
    UNABLE_FIND_RESOURCES("300004","无法找到资源"),
    /**
     * 客户端HTTP请求方法错误
     * org.springframework.web.HttpRequestMethodNotSupportedException
     */
    CLIENT_HTTP_METHOD_ERROR("300005","客户端HTTP请求方法错误"),
    /**
     * 客户端@PathVariable参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.validation.BindException
     */
    CLIENT_PATH_VARIABLE_ERROR("300006","客户端URL中的参数类型错误"),
    /**
     * 客户端@RequestBody参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.bind.MethodArgumentConversionNotSupportedException
     */
    CLIENT_REQUEST_BODY_ERROR("300007","客户端请求体参数类型错误"),
    /**
     * 客户端@RequestParam参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
     */
    CLIENT_REQUEST_PARAM_ERROR("300008","客户端请求参数类型错误"),
    /**
     * 客户端@CookieValue参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.CookieValueMethodArgumentTypeMismatchException
     */
    CLIENT_COOKIE_VALUE_ERROR("300009","客户端Cookie参数类型错误"),
    /**
     * 客户端@RequestHeader参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
     */
    CLIENT_REQUEST_HEADER_ERROR("300010","客户端请求头参数类型错误"),
    /**
     * 客户端@ModelAttribute参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.ModelAttributeMethodProcessor.resolveArgument
     */
    CLIENT_MODEL_ATTRIBUTE_ERROR("300011","客户端@ModelAttribute参数类型错误"),
    /**
     * 客户端@SessionAttribute参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.bind.MissingSessionAttributeException
     */
    CLIENT_SESSION_ATTRIBUTE_ERROR("300012","客户端@SessionAttribute参数类型错误"),
    /**
     * 客户端@RequestBody参数解析错误
     * 一般是JSON格式错误，无法正确解析
     * org.springframework.http.converter.HttpMessageNotReadableException
     */
    CLIENT_REQUEST_BODY_PARSE_ERROR("300013","客户端请求体参数解析错误"),
    /**
     * 客户端@RequestBody参数序列化错误
     * 一般是JSON格式错误，无法正确序列化
     * org.springframework.http.converter.HttpMessageNotWritableException
     */
    CLIENT_REQUEST_BODY_SERIALIZE_ERROR("300014","客户端请求体参数序列化错误"),
    /**
     * 客户端@PathVariable参数解析错误
     * 一般是路径参数错误，无法正确解析
     * org.springframework.web.servlet.mvc.method.annotation.PathVariableMethodArgumentResolver.resolveArgument
     */
    CLIENT_PATH_VARIABLE_PARSE_ERROR("300015","客户端URL中的参数解析错误"),
    /**
     * 客户端@RequestParam参数解析错误
     * 一般是请求参数错误，无法正确解析
     * org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.resolveArgument
     */
    CLIENT_REQUEST_PARAM_PARSE_ERROR("300016","客户端请求参数解析错误"),
    /**
     * 客户端@CookieValue参数解析错误
     * 一般是Cookie参数错误，无法正确解析
     * org.springframework.web.method.annotation.CookieValueMethodArgumentResolver.resolveArgument
     */
    CLIENT_COOKIE_VALUE_PARSE_ERROR("300017","客户端Cookie参数解析错误"),
    /**
     * 客户端@RequestHeader参数解析错误
     * 一般是请求头参数错误，无法正确解析
     * org.springframework.web.method.annotation.RequestHeaderMethodArgumentResolver.resolveArgument
     */
    CLIENT_REQUEST_HEADER_PARSE_ERROR("300018","客户端请求头参数解析错误"),
    /**
     * 客户端@ModelAttribute参数解析错误
     * 一般是@ModelAttribute参数错误，无法正确解析
     * org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor.resolveArgument
     */
    CLIENT_MODEL_ATTRIBUTE_PARSE_ERROR("300019","客户端@ModelAttribute参数解析错误"),
    /**
     * 客户端@SessionAttribute参数解析错误
     * 一般是@SessionAttribute参数错误，无法正确解析
     * org.springframework.web.bind.support.SessionAttributeMethodArgumentResolver.resolveArgument
     */
    CLIENT_SESSION_ATTRIBUTE_PARSE_ERROR("300020","客户端@SessionAttribute参数解析错误"),
    /**
     * 客户端@RequestBody请求体JSON格式错误或字段类型错误
     * org.springframework.http.converter.HttpMessageNotReadableException
     * <p>
     * eg:
     * 1、参数类型不对:{"test":"abc"}，本身类型是Long
     * 2、{"test":}  test属性没有给值
     */
    CLIENT_REQUEST_BODY_FORMAT_ERROR("300021","客户端请求体JSON格式错误或字段类型不匹配"),
    /**
     * 客户端@RequestParam请求参数缺失
     * org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.handleMissingValue
     */
    CLIENT_REQUEST_PARAM_REQUIRED_ERROR("300022","客户端请求参数缺失"),
    /**
     * 通用的业务方法入参检查错误
     * java.lang.IllegalArgumentException
     */
    SERVER_ILLEGAL_ARGUMENT_ERROR("300023", "业务方法参数检查不通过"),
    /**
     * 数据类型转换异常
     */
    DATA_TYPE_CONVERSION_ERROR("300024", "数据类型转换异常"),
    /**
     * 非法访问异常
     */
    ILLEGAL_ACCESS_ERROR("300025", "非法访问"),
    /**
     * 数组下标越界异常
     */
    ARRAY_INDEX_OUT_OF_BOUNDS_ERROR("300026", "数组下标越界异常"),
    /**
     * 文件不存在异常
     */
    FILE_NOT_FOUND_ERROR("300027", "文件不存在"),
    /**
     * 数字格式异常
     */
    NUMBER_FORMAT_ERROR("300028", "数字格式异常"),
    /**
     * 实例化异常
     */
    INSTANTIATION_ERROR("300029", "实例化异常"),
    /**
     * 安全异常
     * 如：无权限访问，身份验证失败等
     * 一般是由于权限不足导致的，需要进行权限校验
     */
    SECURITY_ERROR("300030", "安全异常"),
    /**
     * 文件结束异常
     */
    FILE_END_OF_STREAM_ERROR("300031", "文件结束异常"),
    /**
     * 空指针异常
     */
    NULL_POINTER_ERROR("300032", "空指针异常"),
    /**
     * 方法不存在异常
     */
    NO_SUCH_METHOD_ERROR("300033", "方法不存在"),
    /**
     * SQL异常
     */
    SQL_ERROR("300034", "SQL异常"),
    /**
     * IO异常
     */
    IO_ERROR("300035", "IO异常"),
    /**
     * 数据格式异常
     */
    DATA_FORMAT_ERROR("300036", "数据格式异常"),
    /**
     * 字段不存在异常
     */
    NO_SUCH_FIELD_ERROR("300037", "字段不存在"),
    /**
     * 超时异常
     */
    TIMEOUT_ERROR("300038", "超时异常"),
    /**
     * 输入不匹配异常
     */
    INPUT_MISMATCH_ERROR("300039", "输入不匹配"),
    /**
     * 主键冲突，请检查数据是否重复
     */
    DUPLICATE_KEY_ERROR("300040", "主键冲突，请检查数据是否重复"),
    /**
     * 处理参数绑定异常
     */
    PARAMETER_BIND_ERROR("300041", "处理参数绑定异常"),
    /**
     * 通用业务方法异常
     */
    UNKUOW_ERROR("999999","系统未知错误");

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ResultCode(String code,String message) {
        this.code = code;
        this.message = message;
    }
}