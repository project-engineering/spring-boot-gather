各种基础功能项目

```
1、全局异常处理功能
2、统一出参报文信息
```


# mysql8+ springboot+mybatis plus报错
```
### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: Expression #2 of ORDER BY clause is not in GROUP BY clause and contains nonaggregated column 'customer_center.cust_code_value.code_type_id_value' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
```
## 通过配置文件永久修改sql_mode
### 1、Linux下修改配置文件

1）登录进入MySQL
使用命令 mysql -u username -p 进行登陆，然后输入密码，输入SQL：
```sql
show variables like '%sql_mode';
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210223163705644.png)

2）编辑my.cnf文件
文件地址一般在：/etc/my.cnf，/etc/mysql/my.cnf
使用vim命令编辑文件，不知道vim命令怎么使用的，可以参考我的另外篇文章：Linux中使用vi工具进行文本编辑
找到sql-mode的位置，去掉ONLY_FULL_GROUP_BY
然后重启MySQL；
有的my.cnf中可能没有sql-mode，需要追加：
```sql
sql-mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

注意要加入到[mysqld]下面，如加入到其他地方，重启后也不生效，具体的如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021030321462738.png) 

3）修改成功后重启MySQL服务
```bash
service mysql restart
```

重启好后，再登录mysql，输入SQL：show variables like ‘%sql_mode’; 如果没有ONLY_FULL_GROUP_BY，就说明已经成功了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210223164125875.png)

如果还不行，那么只保留STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION 即可
追加内容为：
```sql
sql-mode=STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION
```


### 2、window下修改配置文件
1）找到mysql安装目录，用记事本直接打开my.ini文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210223165210136.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI2NjA0NjQ=,size_16,color_FFFFFF,t_70)

2）编辑my.ini文件，在[mysql]标签下追加内容
```
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210223165740708.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI2NjA0NjQ=,size_16,color_FFFFFF,t_70)

3）重启mysql 服务
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210223170004271.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI2NjA0NjQ=,size_16,color_FFFFFF,t_70)

备注：
网上有些提供了sql_mode的值，却导致重启mysql服务启动不了
```
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

这时，只需要将sql_mode 值中 “NO_AUTO_CREATE_USER” 这个属性去掉即可。
