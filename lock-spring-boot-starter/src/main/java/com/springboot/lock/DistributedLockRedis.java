package com.springboot.lock;

import com.springboot.lock.impl.IDistributedLock;
import com.springboot.lock.util.RedisUtils;
import com.springboot.lock.util.SpringContextUtil;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * Redis分布式锁
 * @author liuc
 */
public class DistributedLockRedis implements IDistributedLock {
	private static final Logger logger = LoggerFactory.getLogger(DistributedLockRedis.class);
	public static final String MODULE = DistributedLockRedis.class.getName();
	@Resource
	private RedisUtils redisUtils = (RedisUtils) SpringContextUtil.getBean("redisUtils");
	@Value("${distributed.lock.timeout:30000}")
	private long unlocktime;

	@Override
	public void getLock(String lockName) {
		boolean is = redisUtils.tryLock(lockName,unlocktime);
		if(is) {
			logger.info("****使用Redis分布式锁，上锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，上锁失败，lock="+lockName+"****", MODULE);
		}
	}

    @Override
    public void getLock(String lockName, long unlocktime) {

    }

    @Override
	public void unLock(String lockName) {
		boolean is = redisUtils.unlock(lockName);
		if(is) {
			logger.info("****使用Redis分布式锁，解锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，解锁失败，lock="+lockName+"****", MODULE);
		}
	}

	/**
	 * 判断lockName是否被锁着的
	 * @param lockName
	 * @return
	 */
    @Override
    public boolean isLock(String lockName) {
        return redisUtils.hasKey(lockName);
    }

    @Override
	public boolean getExclusiveLock(String lockName) {
		return getExclusiveLock(lockName,unlocktime);
	}

    @Override
    public boolean getExclusiveLock(String lockName, long unlocktime) {
		boolean is = redisUtils.tryLock(lockName,unlocktime);
		if(is) {
			logger.info("****使用Redis分布式锁，上锁成功，lock="+lockName+"****", MODULE);
		}else {
			logger.warn("****使用Redis分布式锁，上锁失败，lock="+lockName+"****", MODULE);
		}
		return is;
    }

}
