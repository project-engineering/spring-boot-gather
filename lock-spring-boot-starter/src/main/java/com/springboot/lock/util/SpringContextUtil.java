package com.springboot.lock.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 获取spring中的bean对象工具类
 */
@Component
public class SpringContextUtil implements ApplicationContextAware, InitializingBean {

    /**
     *  Spring应用上下文环境
     */
    private static ApplicationContext applicationContext = null;

    /**
     * 实现ApplicationContextAware接口的回调方法，设置上下文环境
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringContextUtil.applicationContext == null) {
            SpringContextUtil.applicationContext = applicationContext;
        }

    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 获取对象 这里重写了bean方法，起主要作用
     */
    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    /**
     * 获取对象 这里重写了bean方法，起主要作用
     */
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 打印IOC容器中所有的Bean名称
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
//        String[] names = applicationContext.getBeanDefinitionNames();
//        for (String name : names) {
//            System.out.println("spring bean名称>>>>>>" + name);
//        }
//        System.out.println("------Bean 总计:" + applicationContext.getBeanDefinitionCount());
    }

}
