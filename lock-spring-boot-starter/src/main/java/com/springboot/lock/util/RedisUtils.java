package com.springboot.lock.util;

import com.springboot.futool.UtilValidate;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

@Component
@Log4j2
public class RedisUtils {
    @Resource
    private RedissonClient redissonClient;

    // 加锁
    public Boolean lock(String lockName) {
        if (redissonClient == null) {
            log.info("DistributedRedisLock redissonClient is null");
            return false;
        }
        try {
            RLock lock = redissonClient.getLock(lockName);
            // 锁10秒后自动释放，防止死锁
            lock.lock(10, TimeUnit.SECONDS);
            log.info("Thread [{}] DistributedRedisLock lock [{}] success", Thread.currentThread().getName(), lockName);
            // 加锁成功
            return true;
        } catch (Exception e) {
            log.error("DistributedRedisLock lock [{}] Exception:", lockName, e);
            return false;
        }
    }

    // 释放锁
    public Boolean unlock(String lockName) {
        if (redissonClient == null) {
            log.info("DistributedRedisLock redissonClient is null");
            return false;
        }
        try {
            RLock lock = redissonClient.getLock(lockName);
            if(lock.isLocked()&&lock.isHeldByCurrentThread()){
                lock.unlock();
            }
            log.info("Thread [{}] DistributedRedisLock unlock [{}] success", Thread.currentThread().getName(), lockName);
            // 释放锁成功
            return true;
        } catch (Exception e) {
            log.error("DistributedRedisLock unlock [{}] Exception:", lockName, e);
            return false;
        }
    }

    /**
     * 悲观锁机制-加锁
     */
    public static boolean tryLock(String lockName){
        return  tryLock(lockName);
    }

    /**
     * 悲观锁机制-加锁
     */
    public boolean tryLock(String lockName,long unlocktime){
        //获取锁对象
        RLock mylock = redissonClient.getLock(lockName);
        //加锁，并且设置锁过期时间3秒，防止死锁的产生  uuid+threadId
        try {
            mylock.tryLock(unlocktime, TimeUnit.MICROSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //加锁失败
            return false;
        }
        //加锁成功
        return  true;
    }

    /**
     * 判断当前key值 是否存在
     * @param key
     */
    public boolean hasKey(String key) {
        boolean flag = false;
        RLock lock = redissonClient.getLock(key);
        Object value = null;
        if(lock.isLocked()&&lock.isHeldByCurrentThread()){
            value = lock.getName();
        }
        if (UtilValidate.isNotEmpty(value)) {
            flag = true;
        }
        log.debug("RedisUtil:get cache key={},value={}",key, value);
        return flag;
    }

    /**
     * 设置缓存，并且自己指定过期时间
     * @param key
     * @param value
     * @param expireTime 过期时间
     */
    public void setWithExpireTime( String key, String value, int expireTime) {
        redissonClient.getBucket(key).set(value);
        redissonClient.getBucket(key).expire(expireTime,TimeUnit.MICROSECONDS);
        log.debug("RedisUtil:setWithExpireTime cache key={},value={},expireTime={}", key, value, expireTime);
    }
    /**
     * 获取指定key的缓存
     * @param key
     */
    public Object get(String key) {
        RLock lock = redissonClient.getLock(key);
        Object value = null;
        if(lock.isLocked()&&lock.isHeldByCurrentThread()){
            value = lock.getName();
        }
        log.debug("RedisUtil:get cache key={},value={}",key, value);
        return value;
    }
}
