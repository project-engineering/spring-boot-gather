package com.springboot.lock.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.lock.dao.DistributedLockMapper;
import com.springboot.lock.entity.DistributedLock;
import com.springboot.lock.service.DistributedLockService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * 分布式锁表服务实现类
 * @className: DistributedLockServiceImpl
 * @author: liuc
 * @date: 2019-11-12
 */
@Service
public class DistributedLockServiceImpl extends ServiceImpl<DistributedLockMapper, DistributedLock> implements DistributedLockService {
    @Resource
    private DistributedLockMapper mapper;
    @Override
    public List<DistributedLock> listByMap(Map<String, Object> columnMap) {
        return super.listByMap(columnMap);
    }

    @Override
    public boolean removeByMap(Map<String, Object> columnMap) {
        return super.removeByMap(columnMap);
    }
}