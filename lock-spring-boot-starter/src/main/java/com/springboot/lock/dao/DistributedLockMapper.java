package com.springboot.lock.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.lock.entity.DistributedLock;
import org.apache.ibatis.annotations.Mapper;

/**
 * 分布式锁表 Mapper 接口
 * @className: DistributedLockMapper
 * @author: liuc
 * @date: 2019-11-12
 */
@Mapper
public interface DistributedLockMapper extends BaseMapper<DistributedLock> {

}
