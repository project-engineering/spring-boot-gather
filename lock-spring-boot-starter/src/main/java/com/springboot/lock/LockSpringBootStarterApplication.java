package com.springboot.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = {"com.springboot"},exclude={DataSourceAutoConfiguration.class})
public class LockSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(LockSpringBootStarterApplication.class, args);
    }

}
