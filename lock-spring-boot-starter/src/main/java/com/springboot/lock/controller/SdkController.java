//package com.springboot.lock.controller;
//
//import com.springboot.baseline.entity.Result;
//import com.springboot.baseline.util.ResultUtils;
//import com.springboot.lock.config.DistributedLockConfig;
//import com.springboot.lock.factory.DistributedLockFactory;
//import com.springboot.lock.impl.IDistributedLock;
////import com.springboot.lock.util.DistributedRedisLock;
//import com.springboot.lock.util.RedisUtils;
//import jakarta.annotation.Resource;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import java.util.concurrent.TimeUnit;
//
//@Slf4j
//@RestController
//public class SdkController {
//
//    @Resource
//    DistributedLockFactory factory;
//    @Resource
//    DistributedLockConfig config;
//    private final String LOCK = "LOCK";
//    @Resource
//    private RedisUtils distributedRedisLock;
//    // 实际业务开发使用分布式锁的方式
//    @GetMapping("/hello")
//    public Result post() {
//        //使用分布式锁
//        IDistributedLock lock = factory.getDistributedLock(config.getType());
//        String lockName = "234234_7777777" ;
//        boolean isLock = lock.getExclusiveLock(lockName);
//        if (!isLock) {
//            log.info(111111 + "，请勿重复提交!");
//            return  ResultUtils.error("1","请勿重复提交!");
//        }
//        String returnStr = null;
//        try{
//            returnStr = "Hello World!";
//            try {
//                TimeUnit.SECONDS.sleep(6);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }finally {
//            if (lock.isLock(lockName)) {
//                //释放锁
//                lock.unLock(lockName);
//            }
//        }
//        return ResultUtils.success(returnStr);
//    }
//
////    @GetMapping("/hello")
////    public Result hello(@RequestParam(value = "name", defaultValue = "World") String name) {
////        //使用分布式锁
////        IDistributedLock lock = factory.getDistributedLock(config.getType());
////        String lockName = "234234_7777777" ;
////        boolean isLock = lock.getExclusiveLock(lockName);
////        if (!isLock) {
////            log.info(111111 + "，请勿重复提交!");
////            return  ResultUtils.success("请勿重复提交!");
////        }
////        String returnStr = null;
////        try{
////            returnStr = String.format("Hello %s!", name);
////            try {
////                TimeUnit.SECONDS.sleep(6);
////            } catch (InterruptedException e) {
////                e.printStackTrace();
////            }
////        }finally {
////            if (lock.isLock(lockName)) {
////                //释放锁
////                lock.unLock(lockName);
////            }
////        }
////        return ResultUtils.success(returnStr);
////    }
//
//}