package com.pinyin4jplus;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuc
 * @date 2024-06-26 20:16
 */


public class DoubleArrayTrie {
    private static final int BUF_SIZE = 16384;
    private static final int UNIT_SIZE = 8;
    private int[] check = null;
    private int[] base = null;
    private boolean[] used = null;
    private int size = 0;
    private int allocSize = 0;
    private List<String> key;
    private int keySize;
    private int[] length;
    private int[] value;
    private int progress;
    private int nextCheckPos;
    int error_ = 0;

    private int resize(int newSize) {
        int[] base2 = new int[newSize];
        int[] check2 = new int[newSize];
        boolean[] used2 = new boolean[newSize];
        if (this.allocSize > 0) {
            System.arraycopy(this.base, 0, base2, 0, this.allocSize);
            System.arraycopy(this.check, 0, check2, 0, this.allocSize);
            System.arraycopy(used2, 0, used2, 0, this.allocSize);
        }

        this.base = base2;
        this.check = check2;
        this.used = used2;
        return this.allocSize = newSize;
    }

    private int fetch(DoubleArrayTrie.Node parent, List<DoubleArrayTrie.Node> siblings) {
        if (this.error_ < 0) {
            return 0;
        } else {
            int prev = 0;

            for(int i = parent.left; i < parent.right; ++i) {
                if ((this.length != null ? this.length[i] : ((String)this.key.get(i)).length()) >= parent.depth) {
                    String tmp = (String)this.key.get(i);
                    int cur = 0;
                    if ((this.length != null ? this.length[i] : tmp.length()) != parent.depth) {
                        cur = tmp.charAt(parent.depth) + 1;
                    }

                    if (prev > cur) {
                        this.error_ = -3;
                        return 0;
                    }

                    if (cur != prev || siblings.size() == 0) {
                        DoubleArrayTrie.Node tmp_node = new DoubleArrayTrie.Node();
                        tmp_node.depth = parent.depth + 1;
                        tmp_node.code = cur;
                        tmp_node.left = i;
                        if (siblings.size() != 0) {
                            ((DoubleArrayTrie.Node)siblings.get(siblings.size() - 1)).right = i;
                        }

                        siblings.add(tmp_node);
                    }

                    prev = cur;
                }
            }

            if (siblings.size() != 0) {
                ((DoubleArrayTrie.Node)siblings.get(siblings.size() - 1)).right = parent.right;
            }

            return siblings.size();
        }
    }

    private int insert(List<DoubleArrayTrie.Node> siblings) {
        if (this.error_ < 0) {
            return 0;
        } else {
            int begin = 0;
            int pos = (((DoubleArrayTrie.Node)siblings.get(0)).code + 1 > this.nextCheckPos ? ((DoubleArrayTrie.Node)siblings.get(0)).code + 1 : this.nextCheckPos) - 1;
            int nonzero_num = 0;
            boolean first = false;
            if (this.allocSize <= pos) {
                this.resize(pos + 1);
            }

            while(true) {
                label96:
                while(true) {
                    ++pos;
                    if (this.allocSize <= pos) {
                        this.resize(pos + 1);
                    }

                    if (this.check[pos] != 0) {
                        ++nonzero_num;
                    } else {
                        if (!first) {
                            this.nextCheckPos = pos;
                            first = true;
                        }
                        begin = pos - ((DoubleArrayTrie.Node)siblings.get(0)).code;
                        if (this.allocSize <= begin + ((DoubleArrayTrie.Node)siblings.get(siblings.size() - 1)).code) {
                            double l = 1.05 > 1.0 * (double)this.keySize / (double)(this.progress + 1) ? 1.05 : 1.0 * (double)this.keySize / (double)(this.progress + 1);
                            this.resize((int)((double)this.allocSize * l));
                        }

                        if (!this.used[begin]) {
                            int i;
                            for(i = 1; i < siblings.size(); ++i) {
                                if (this.check[begin + ((DoubleArrayTrie.Node)siblings.get(i)).code] != 0) {
                                    continue label96;
                                }
                            }

                            if (1.0 * (double)nonzero_num / (double)(pos - this.nextCheckPos + 1) >= 0.95) {
                                this.nextCheckPos = pos;
                            }

                            this.used[begin] = true;
                            this.size = this.size > begin + ((DoubleArrayTrie.Node)siblings.get(siblings.size() - 1)).code + 1 ? this.size : begin + ((DoubleArrayTrie.Node)siblings.get(siblings.size() - 1)).code + 1;

                            for(i = 0; i < siblings.size(); ++i) {
                                this.check[begin + ((DoubleArrayTrie.Node)siblings.get(i)).code] = begin;
                            }

                            for(i = 0; i < siblings.size(); ++i) {
                                List<DoubleArrayTrie.Node> new_siblings = new ArrayList();
                                if (this.fetch((DoubleArrayTrie.Node)siblings.get(i), new_siblings) == 0) {
                                    this.base[begin + ((DoubleArrayTrie.Node)siblings.get(i)).code] = this.value != null ? -this.value[((DoubleArrayTrie.Node)siblings.get(i)).left] - 1 : -((DoubleArrayTrie.Node)siblings.get(i)).left - 1;
                                    if (this.value != null && -this.value[((DoubleArrayTrie.Node)siblings.get(i)).left] - 1 >= 0) {
                                        this.error_ = -2;
                                        return 0;
                                    }

                                    ++this.progress;
                                } else {
                                    int h = this.insert(new_siblings);
                                    this.base[begin + ((DoubleArrayTrie.Node)siblings.get(i)).code] = h;
                                }
                            }

                            return begin;
                        }
                    }
                }
            }
        }
    }

    public DoubleArrayTrie() {
    }

    void clear() {
        this.check = null;
        this.base = null;
        this.used = null;
        this.allocSize = 0;
        this.size = 0;
    }

    public int getUnitSize() {
        return 8;
    }

    public int getSize() {
        return this.size;
    }

    public int getTotalSize() {
        return this.size * 8;
    }

    public int getNonzeroSize() {
        int result = 0;

        for(int i = 0; i < this.size; ++i) {
            if (this.check[i] != 0) {
                ++result;
            }
        }

        return result;
    }

    public int build(List<String> key) {
        return this.build(key, (int[])null, (int[])null, key.size());
    }

    public int build(List<String> _key, int[] _length, int[] _value, int _keySize) {
        if (_keySize <= _key.size() && _key != null) {
            this.key = _key;
            this.length = _length;
            this.keySize = _keySize;
            this.value = _value;
            this.progress = 0;
            this.resize(2097152);
            this.base[0] = 1;
            this.nextCheckPos = 0;
            DoubleArrayTrie.Node root_node = new DoubleArrayTrie.Node();
            root_node.left = 0;
            root_node.right = this.keySize;
            root_node.depth = 0;
            List<DoubleArrayTrie.Node> siblings = new ArrayList();
            this.fetch(root_node, siblings);
            this.insert(siblings);
            this.used = null;
            this.key = null;
            return this.error_;
        } else {
            return 0;
        }
    }

    public void open(String fileName) throws IOException {
        File file = new File(fileName);
        this.size = (int)file.length() / 8;
        this.check = new int[this.size];
        this.base = new int[this.size];
        DataInputStream is = null;

        try {
            is = new DataInputStream(new BufferedInputStream(new FileInputStream(file), 16384));

            for(int i = 0; i < this.size; ++i) {
                this.base[i] = is.readInt();
                this.check[i] = is.readInt();
            }
        } finally {
            if (is != null) {
                is.close();
            }

        }

    }

    public void save(String fileName) throws IOException {
        DataOutputStream out = null;

        try {
            out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)));

            for(int i = 0; i < this.size; ++i) {
                out.writeInt(this.base[i]);
                out.writeInt(this.check[i]);
            }

            out.close();
        } finally {
            if (out != null) {
                out.close();
            }

        }
    }

    public int exactMatchSearch(String key) {
        return this.exactMatchSearch(key, 0, 0, 0);
    }

    public int exactMatchSearch(String key, int pos, int len, int nodePos) {
        if (len <= 0) {
            len = key.length();
        }

        if (nodePos <= 0) {
            nodePos = 0;
        }

        int result = -1;
        char[] keyChars = key.toCharArray();
        int b = this.base[nodePos];

        int n;
        for(n = pos; n < len; ++n) {
            int p = b + keyChars[n] + 1;
            if (b != this.check[p]) {
                return result;
            }

            b = this.base[p];
        }

        n = this.base[b];
        if (b == this.check[b] && n < 0) {
            result = -n - 1;
        }

        return result;
    }

    public List<Integer> commonPrefixSearch(String key) {
        return this.commonPrefixSearch(key, 0, 0, 0);
    }

    public List<Integer> commonPrefixSearch(String key, int pos, int len, int nodePos) {
        if (len <= 0) {
            len = key.length();
        }

        if (nodePos <= 0) {
            nodePos = 0;
        }

        List<Integer> result = new ArrayList();
        char[] keyChars = key.toCharArray();
        int b = this.base[nodePos];

        int n;
        for(int i = pos; i < len; ++i) {
            n = this.base[b];
            if (b == this.check[b] && n < 0) {
                result.add(-n - 1);
            }

            int p = b + keyChars[i] + 1;
            if (b != this.check[p]) {
                return result;
            }

            b = this.base[p];
        }

        n = this.base[b];
        if (b == this.check[b] && n < 0) {
            result.add(-n - 1);
        }

        return result;
    }

    public void dump() {
        for(int i = 0; i < this.size; ++i) {
            System.err.println("i: " + i + " [" + this.base[i] + ", " + this.check[i] + "]");
        }

    }

    private static class Node {
        int code;
        int depth;
        int left;
        int right;

        private Node() {
        }
    }
}
