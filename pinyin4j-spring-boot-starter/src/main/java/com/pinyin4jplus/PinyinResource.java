package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-26 20:18
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class PinyinResource {
    private PinyinResource() {
    }

    protected static Reader newClassPathReader(String classpath) {
        InputStream is = PinyinResource.class.getResourceAsStream(classpath);

        try {
            return new InputStreamReader(is, "UTF-8");
        } catch (UnsupportedEncodingException var3) {
            return null;
        }
    }

    protected static Reader newFileReader(String path) throws FileNotFoundException {
        try {
            return new InputStreamReader(new FileInputStream(path), "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            return null;
        }
    }

    protected static Map<String, String> getResource(Reader reader) {
        Map<String, String> map = new ConcurrentHashMap();

        try {
            BufferedReader br = new BufferedReader(reader);
            String line = null;

            while((line = br.readLine()) != null) {
                String[] tokens = line.trim().split("=");
                map.put(tokens[0], tokens[1]);
            }

            br.close();
            return map;
        } catch (IOException var5) {
            throw new RuntimeException(var5);
        }
    }

    protected static Map<String, String> getPinyinResource() {
        return getResource(newClassPathReader("/data/pinyin.dict"));
    }

    protected static Map<String, String> getMutilPinyinResource() {
        return getResource(newClassPathReader("/data/mutil_pinyin.dict"));
    }

    protected static Map<String, String> getChineseResource() {
        return getResource(newClassPathReader("/data/chinese.dict"));
    }
}