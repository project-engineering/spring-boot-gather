package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-26 20:15
 */
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Map;

public final class ChineseHelper {
    private static final String CHINESE_REGEX = "[\\u4e00-\\u9fa5]";
    private static final Map<String, String> CHINESE_MAP = PinyinResource.getChineseResource();

    private ChineseHelper() {
    }

    public static char convertToSimplifiedChinese(char c) {
        String simplifiedChinese = (String)CHINESE_MAP.get(String.valueOf(c));
        return simplifiedChinese != null ? simplifiedChinese.charAt(0) : c;
    }

    public static char convertToTraditionalChinese(char c) {
        String simplifiedChinese = String.valueOf(c);
        Iterator var2 = CHINESE_MAP.entrySet().iterator();

        Map.Entry entry;
        do {
            if (!var2.hasNext()) {
                return c;
            }

            entry = (Map.Entry)var2.next();
        } while(!((String)entry.getValue()).equals(simplifiedChinese));

        return ((String)entry.getKey()).charAt(0);
    }

    public static String convertToSimplifiedChinese(String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;

        for(int len = str.length(); i < len; ++i) {
            char c = str.charAt(i);
            sb.append(convertToSimplifiedChinese(c));
        }

        return sb.toString();
    }

    public static String convertToTraditionalChinese(String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;

        for(int len = str.length(); i < len; ++i) {
            char c = str.charAt(i);
            sb.append(convertToTraditionalChinese(c));
        }

        return sb.toString();
    }

    public static boolean isTraditionalChinese(char c) {
        return CHINESE_MAP.containsKey(String.valueOf(c));
    }

    public static boolean isChinese(char c) {
        return 12295 == c || String.valueOf(c).matches("[\\u4e00-\\u9fa5]");
    }

    public static boolean containsChinese(String str) {
        int i = 0;

        for(int len = str.length(); i < len; ++i) {
            if (isChinese(str.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    public static void addChineseDict(String path) throws FileNotFoundException {
        CHINESE_MAP.putAll(PinyinResource.getResource(PinyinResource.newFileReader(path)));
    }
}