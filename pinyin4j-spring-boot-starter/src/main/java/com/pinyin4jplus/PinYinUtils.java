package com.pinyin4jplus;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuc
 * @date 2024-06-26 19:54
 */
public class PinYinUtils {
    private static final Logger log = LoggerFactory.getLogger(PinYinUtils.class);
    /**
     * 转换为每个汉字对应拼音首字母字符串
     * @param pinYinStr 需转换的汉字
     * @return 拼音字符串
     */
    public static String getPinYinPrefix(String pinYinStr){
        return getPinYinPrefix(pinYinStr, false);
    }

    /**
     * 转换为每个汉字对应拼音首字母字符串
     * @param pinYinStr 需转换的汉字
     * @param uppercase true 大写，false 默认
     * @return 拼音字符串
     */
    public static String getPinYinPrefix(String pinYinStr, boolean uppercase){
        if(StringUtils.isEmpty(pinYinStr)){
            return pinYinStr;
        }
        try{
            String tempStr = PinyinHelper.getShortPinyin(pinYinStr);
            if(uppercase){
                return StringUtils.upperCase(tempStr);
            } else {
                return tempStr;
            }
        } catch (Exception e){
            log.error("汉字转拼音首字母异常："+e.getMessage());
            return pinYinStr;
        }
    }

    /**
     * 获取汉字全拼音，不带声调，不分隔符，全部小写
     * @param pinYinStr 要转换的字符串
     * @return 汉语拼音
     */
    public static String getPinYinWithToneLowercase(String pinYinStr){
        return getPinYinAllLetter(pinYinStr, StringUtils.EMPTY, PinyinFormat.WITHOUT_TONE, false);
    }

    /**
     * 获取汉字全拼音，不带声调，不分隔符，全部大写
     * @param pinYinStr 要转换的字符串
     * @return 汉语拼音
     */
    public static String getPinYinWithOutToneUppercase(String pinYinStr){
        return getPinYinAllLetter(pinYinStr, StringUtils.EMPTY, PinyinFormat.WITHOUT_TONE, true);
    }

    /**
     * 获取汉字全拼音，不带声调，带空格分隔符，全部小写
     * @param pinYinStr 要转换的字符串
     * @return 汉语拼音
     */
    public static String getPinYinWithSpaceWithOutToneLowercase(String pinYinStr){
        return getPinYinAllLetter(pinYinStr, StringUtils.SPACE, PinyinFormat.WITHOUT_TONE, false);
    }

    /**
     * 获取汉字全拼音，不带声调，带空格分隔符，全部大写
     * @param pinYinStr 要转换的字符串
     * @return 汉语拼音
     */
    public static String getPinYinWithSpaceWithOutToneUppercase(String pinYinStr){
        return getPinYinAllLetter(pinYinStr, StringUtils.SPACE, PinyinFormat.WITHOUT_TONE, true);
    }



    /**
     * 获取汉字全拼音，不带声调，不分隔符
     * @param pinYinStr 要转换的字符串
     * @param uppercase true 大写，false 默认
     * @return 汉语拼音
     */
    public static String getPinYinWithOutToneNoneSeparator(String pinYinStr, boolean uppercase){
        return getPinYinAllLetter(pinYinStr, StringUtils.EMPTY, PinyinFormat.WITHOUT_TONE, uppercase);
    }

    /**
     * 获取汉字全拼音
     * @param pinYinStr 要转换的字符串
     * @param separator 分隔符
     * @param uppercase true 大写，false 默认
     * @return 汉语拼音
     */
    public static String getPinYin(String pinYinStr, String separator, boolean uppercase){
        return getPinYinAllLetter(pinYinStr, separator, PinyinFormat.WITHOUT_TONE, uppercase);
    }

    /**
     * 获取汉字全拼音，自定义分隔符，自定义拼音格式，自定义所有字母是否大小写
     * @param pinYinStr 要转换的字符串
     * @param separator 分隔符
     * @param pinyinFormat 拼音格式 WITH_TONE_MARK（带声调）,WITHOUT_TONE（不带声调） WITH_TONE_NUMBER（带声调数字）
     * @param uppercase true 大写，false 默认
     * @return 汉语拼音
     */
    public static String getPinYinAllLetter(String pinYinStr, String separator, PinyinFormat pinyinFormat, boolean uppercase){
        if(StringUtils.isEmpty(pinYinStr)){
            return pinYinStr;
        }
        try{
            String tempStr = PinyinHelper.convertToPinyinString(pinYinStr, separator, pinyinFormat);
            if(uppercase){
                return StringUtils.upperCase(tempStr);
            } else {
                return tempStr;
            }
        } catch (Exception e){
            log.error("获取汉字全拼音异常："+e.getMessage());
            return pinYinStr;
        }
    }

    /**
     * 获取汉字全拼音，自定义分隔符，自定义拼音格式，自定义首字母字母是否大小写
     * @param pinYinStr 要转换的字符串
     * @param separator 分隔符
     * @param pinyinFormat 拼音格式 WITH_TONE_MARK（带声调）,WITHOUT_TONE（不带声调） WITH_TONE_NUMBER（带声调数字）
     * @param uppercase true-首字母大写，false-首字母小写，默认false
     * @return 汉语拼音
     */
    public static String getPinYin(String pinYinStr, String separator, PinyinFormat pinyinFormat, boolean uppercase){
        if(StringUtils.isEmpty(pinYinStr)){
            return pinYinStr;
        }
        try{
            return PinyinHelper.convertToPinyinString(pinYinStr, separator, pinyinFormat,uppercase);
        } catch (Exception e){
            log.error("汉字转拼音首字母异常："+e.getMessage());
            return pinYinStr;
        }
    }

    /**
     * 判断一个汉字是否为多音字
     * @param c 汉字
     * @return 判断结果，是汉字返回true，否则返回false
     */
    public static boolean hasMultiPinyin(char c){
        return PinyinHelper.hasMultiPinyin(c);
    }

    /**
     * 将单个汉字转换成带声调格式的拼音
     * @param c 需要转换成拼音的汉字
     * @return 字符串的拼音
     */
    public static String[] convertToPinyinArray(char c){
        return PinyinHelper.convertToPinyinArray(c);
    }
    /**
     * 将单个汉字转换为相应格式的拼音
     * @param c 需要转换成拼音的汉字
     * @param pinyinFormat 拼音格式：WITH_TONE_NUMBER--数字代表声调，WITHOUT_TONE--不带声调，WITH_TONE_MARK--带声调
     * @return 汉字的拼音
     */
    public static String[] convertToPinyinArray(char c, PinyinFormat pinyinFormat){
        return PinyinHelper.convertToPinyinArray(c, pinyinFormat);
    }
    /**
     * 繁体字转为简体字
     * @param wordStr 字符串
     * @return 转换后的简体字符串
     */
    public static String convertToSimplifiedChinese(String wordStr){
        if(StringUtils.isEmpty(wordStr)){
            return wordStr;
        }
        return ChineseHelper.convertToSimplifiedChinese(wordStr);
    }
    /**
     * 简体字转换为繁体字
     * @param wordStr 字符串
     * @return 转换后的简体字符串
     */
    public static String convertToTraditionalChinese(String wordStr){
        if(StringUtils.isEmpty(wordStr)){
            return wordStr;
        }
        return ChineseHelper.convertToTraditionalChinese(wordStr);
    }

    /**
     * 判断word是否为简体中文
     * @param word
     * @return true 是，false 否
     */
    public static boolean isChinese(char word){
        return ChineseHelper.isChinese(word);
    }

    /**
     * 判断word是否为繁体字
     * @param word 字符
     * @return true 是，false 否
     */
    public static boolean isTraditionalChinese(char word){
        return ChineseHelper.isTraditionalChinese(word);
    }

    /**
     * 判断字符串是否包含中文
     * @param wordStr 要判断的字符串
     * @return true 包含，false 不包含
     */
    public static boolean containsChinese(String wordStr){
        if(StringUtils.isEmpty(wordStr)){
            return false;
        }
        return ChineseHelper.containsChinese(wordStr);
    }

    /**
     * 转换为有声调的拼音字符串
     * @param pinYinStr 汉字
     * @return 有声调的拼音字符串
     */
    public String getMarkPinYin(String pinYinStr){
        String tempStr = null;
        try {
            tempStr =  PinyinHelper.convertToPinyinString(pinYinStr, StringUtils.EMPTY, PinyinFormat.WITH_TONE_MARK);
        } catch (Exception e) {
            log.error("转换为有声调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为有声调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为有声调的拼音字符串
     * @param pinYinStr 汉字
     * @return 有声调的拼音字符串
     */
    public String getMarkPinYinWithSpace(String pinYinStr){
        String tempStr = null;
        try {
            tempStr =  PinyinHelper.convertToPinyinString(pinYinStr, StringUtils.SPACE, PinyinFormat.WITH_TONE_MARK);
        } catch (Exception e) {
            log.error("转换为有声调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为有声调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为有声调的拼音字符串
     * @param pinYinStr 汉字
     * @param uppercase 是否大写
     * @return 有声调的拼音字符串
     */
    public String getMarkPinYinWithEmpty(String pinYinStr,boolean uppercase){
        String tempStr = null;
        try {
            tempStr =  getMarkPinYinWithSeparator(pinYinStr, StringUtils.EMPTY,uppercase);
        } catch (Exception e) {
            log.error("转换为有声调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为有声调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为有声调的拼音字符串
     * @param pinYinStr 汉字
     * @param uppercase 是否大写
     * @return 有声调的拼音字符串
     */
    public String getMarkPinYinWithSpace(String pinYinStr,boolean uppercase){
        String tempStr = null;
        try {
            tempStr = getMarkPinYinWithSeparator(pinYinStr, StringUtils.SPACE,uppercase);
        } catch (Exception e) {
            log.error("转换为有声调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为有声调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为有声调的拼音字符串
     * @param pinYinStr 汉字
     * @param separator 分隔符
     * @param uppercase 是否大写
     * @return 有声调的拼音字符串
     */
    public String getMarkPinYinWithSeparator(String pinYinStr,String separator,boolean uppercase){
        String tempStr = null;
        try {
            tempStr =  PinyinHelper.convertToPinyinString(pinYinStr, separator, PinyinFormat.WITH_TONE_MARK,uppercase);
        } catch (Exception e) {
            log.error("转换为有声调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为有声调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }


    /**
     * 转换为数字声调字符串
     * @param pinYinStr 需转换的汉字
     * @return 转换完成的拼音字符串
     */
    public String getNumberPinYinWithSpace(String pinYinStr){
        String tempStr = null;
        try {
            tempStr = PinyinHelper.convertToPinyinString(pinYinStr, StringUtils.SPACE, PinyinFormat.WITH_TONE_NUMBER);
        } catch (Exception e) {
            log.error("转换为数字声调字符串异常：{}",e);
            throw new RuntimeException("转换为数字声调字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为数字声调字符串，自定义分隔符，自定义首字母大小写
     * @param pinYinStr 需转换的汉字
     * @param separator 分隔符
     * @param uppercase 是否大写
     * @return 转换完成的拼音字符串
     */
    public String getNumberPinYin(String pinYinStr,String separator,boolean uppercase){
        String tempStr = null;
        try {
            tempStr = PinyinHelper.convertToPinyinString(pinYinStr, separator, PinyinFormat.WITH_TONE_NUMBER,uppercase);
        } catch (Exception e) {
            log.error("转换为数字声调字符串异常：{}",e);
            throw new RuntimeException("转换为数字声调字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为不带音调的拼音字符串
     * @param pinYinStr 需转换的汉字
     * @return 拼音字符串
     */
    public String getTonePinYinWithSpace(String pinYinStr){
        String tempStr = null;
        try {
            tempStr =  PinyinHelper.convertToPinyinString(pinYinStr, StringUtils.SPACE, PinyinFormat.WITHOUT_TONE);
        } catch (Exception e) {
            log.error("转换为不带音调的拼音字符串异常：{}",e);
            throw new RuntimeException("转换为不带音调的拼音字符串异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 转换为每个汉字对应拼音首字母字符串
     * @param pinYinStr 需转换的汉字
     * @return 拼音字符串
     */
    public String getShortPinYin(String pinYinStr){
        String tempStr = null;
        try
        {
            tempStr = PinyinHelper.getShortPinyin(pinYinStr);
        } catch (Exception e) {
            log.error("汉字转拼音首字母异常：{}",e);
            throw new RuntimeException("汉字转拼音首字母异常："+e.getMessage());
        }
        return tempStr;

    }

    /**
     * 检查汉字是否为多音字
     * @param pinYinStr 需检查的汉字
     * @return true 多音字，false 不是多音字
     */
    public boolean checkPinYin(char pinYinStr){
        boolean check  = false;
        try
        {
            check = PinyinHelper.hasMultiPinyin(pinYinStr);
        } catch (Exception e) {
            log.error("检查汉字是否为多音字异常：{}",e);
            throw new RuntimeException("检查汉字是否为多音字异常："+e.getMessage());
        }
        return check;
    }

    /**
     * 简体转换为繁体
     * @param pinYinStr
     * @return
     */
    public String getTraditional(String pinYinStr){
        String tempStr = null;
        try {
            tempStr = ChineseHelper.convertToTraditionalChinese(pinYinStr);
        } catch (Exception e) {
            log.error("简体转换为繁体异常：{}",e);
            throw new RuntimeException("简体转换为繁体异常："+e.getMessage());
        }
        return tempStr;
    }

    /**
     * 繁体转换为简体
     * @param pinYinSt
     * @return
     */
    public String getSimplified(String pinYinSt){
        String tempStr = null;
        try {
            tempStr = ChineseHelper.convertToSimplifiedChinese(pinYinSt);
        } catch (Exception e) {
            log.error("繁体转换为简体异常：{}",e);
            throw new RuntimeException("繁体转换为简体异常："+e.getMessage());
        }
        return tempStr;
    }
    public static void main(String[] args) {
        String str = "重庆市市长张庆鹏出席了重庆大学的庆典活动，龙行龘龘，重要活动，我喜歡閱讀。龑讠丨长大重复";
        PinYinUtils jp = new PinYinUtils();
        System.out.println(jp.getMarkPinYinWithEmpty(str,true));
        System.out.println(jp.getMarkPinYinWithSpace(str,true)); //有音标和空格
        System.out.println(jp.getMarkPinYinWithSeparator(str,"",true)); //有音标和空格
        System.out.println(jp.getTonePinYinWithSpace(str)); //无音标和空格
        System.out.println(jp.getNumberPinYinWithSpace(str)); //数字声调
        System.out.println(jp.getShortPinYin(str)); //每个汉字对应拼音首字母
        System.out.println(jp.containsChinese(str)); //是否包含中文
        System.out.println(jp.getTraditional(str)); //繁体
        System.out.println(jp.getSimplified(str)); //简体
        System.out.println(jp.checkPinYin('重'));
    }
}
