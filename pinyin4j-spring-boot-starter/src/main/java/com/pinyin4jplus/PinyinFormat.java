package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-26 20:17
 */
public enum PinyinFormat {
    /** 带声调标记的拼音格式 **/
    WITH_TONE_MARK,
    /** 不带声调的拼音格式 **/
    WITHOUT_TONE,
    /** 带声调数字的拼音格式 **/
    WITH_TONE_NUMBER;
}