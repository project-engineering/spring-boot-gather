package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-26 20:17
 */
public class PinyinException extends Exception {
    private static final long serialVersionUID = 1L;

    public PinyinException(String message) {
        super(message);
    }
}