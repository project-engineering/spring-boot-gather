package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-26 20:17
 */
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pinyin4jplus.PinYinUtils.isChinese;

public final class PinyinHelper {
    private static List<String> dict = new ArrayList();
    private static final Map<String, String> PINYIN_TABLE = PinyinResource.getPinyinResource();
    private static final Map<String, String> MUTIL_PINYIN_TABLE = PinyinResource.getMutilPinyinResource();
    private static final DoubleArrayTrie DOUBLE_ARRAY_TRIE = new DoubleArrayTrie();
    private static final String PINYIN_SEPARATOR = ",";
    private static final char CHINESE_LING = '〇';
    private static final String ALL_UNMARKED_VOWEL = "aeiouv";
    private static final String ALL_MARKED_VOWEL = "āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ";

    private PinyinHelper() {
    }

    private static String[] convertWithToneNumber(String pinyinArrayString) {
        String[] pinyinArray = pinyinArrayString.split(",");

        for(int i = pinyinArray.length - 1; i >= 0; --i) {
            boolean hasMarkedChar = false;
            String originalPinyin = pinyinArray[i].replace("ü", "v");

            for(int j = originalPinyin.length() - 1; j >= 0; --j) {
                char originalChar = originalPinyin.charAt(j);
                if (originalChar < 'a' || originalChar > 'z') {
                    int indexInAllMarked = "āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ".indexOf(originalChar);
                    int toneNumber = indexInAllMarked % 4 + 1;
                    char replaceChar = "aeiouv".charAt((indexInAllMarked - indexInAllMarked % 4) / 4);
                    pinyinArray[i] = originalPinyin.replace(String.valueOf(originalChar), String.valueOf(replaceChar)) + toneNumber;
                    hasMarkedChar = true;
                    break;
                }
            }

            if (!hasMarkedChar) {
                pinyinArray[i] = originalPinyin + "5";
            }
        }

        return pinyinArray;
    }

    private static String[] convertWithoutTone(String pinyinArrayString) {
        for(int i = "āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ".length() - 1; i >= 0; --i) {
            char originalChar = "āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ".charAt(i);
            char replaceChar = "aeiouv".charAt((i - i % 4) / 4);
            pinyinArrayString = pinyinArrayString.replace(String.valueOf(originalChar), String.valueOf(replaceChar));
        }

        String[] pinyinArray = pinyinArrayString.replace("ü", "v").split(",");
        return pinyinArray;
    }

    private static String[] formatPinyin(String pinyinString, PinyinFormat pinyinFormat) {
        if (pinyinFormat == PinyinFormat.WITH_TONE_MARK) {
            return pinyinString.split(",");
        } else if (pinyinFormat == PinyinFormat.WITH_TONE_NUMBER) {
            return convertWithToneNumber(pinyinString);
        } else {
            return pinyinFormat == PinyinFormat.WITHOUT_TONE ? convertWithoutTone(pinyinString) : new String[0];
        }
    }

    public static String[] convertToPinyinArray(char c, PinyinFormat pinyinFormat) {
        String pinyin = (String)PINYIN_TABLE.get(String.valueOf(c));
        if (pinyin != null && !"null".equals(pinyin)) {
            Set<String> set = new LinkedHashSet();
            String[] var4 = formatPinyin(pinyin, pinyinFormat);
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String str = var4[var6];
                set.add(str);
            }

            return (String[])set.toArray(new String[set.size()]);
        } else {
            return new String[0];
        }
    }

    public static String[] convertToPinyinArray(char c) {
        return convertToPinyinArray(c, PinyinFormat.WITH_TONE_MARK);
    }

    public static String convertToPinyinString(String str, String separator, PinyinFormat pinyinFormat) throws PinyinException {
        str = ChineseHelper.convertToSimplifiedChinese(str);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        int strLen = str.length();

        while(i < strLen) {
            String substr = str.substring(i);
            List<Integer> commonPrefixList = DOUBLE_ARRAY_TRIE.commonPrefixSearch(substr);
            String[] pinyinArray;
            if (commonPrefixList.size() == 0) {
                char c = str.charAt(i);
                if (!isChinese(c) && c != 12295) {
                    sb.append(c);
                } else {
                    pinyinArray = convertToPinyinArray(c, pinyinFormat);
                    if (pinyinArray != null) {
                        if (pinyinArray.length <= 0) {
                            throw new PinyinException("Can't convert to pinyin: " + c);
                        }

                        sb.append(pinyinArray[0]);
                    } else {
                        sb.append(str.charAt(i));
                    }
                }

                ++i;
            } else {
                String words = (String)dict.get((Integer)commonPrefixList.get(commonPrefixList.size() - 1));
                pinyinArray = formatPinyin((String)MUTIL_PINYIN_TABLE.get(words), pinyinFormat);
                int j = 0;

                for(int l = pinyinArray.length; j < l; ++j) {
                    sb.append(pinyinArray[j]);
                    if (j < l - 1) {
                        sb.append(separator);
                    }
                }

                i += words.length();
            }

            if (i < strLen) {
                sb.append(separator);
            }
        }

        return sb.toString();
    }

    public static String convertToPinyinString(String str, String separator, PinyinFormat pinyinFormat,boolean upperCase) throws PinyinException {
        str = ChineseHelper.convertToSimplifiedChinese(str);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        int strLen = str.length();

        while(i < strLen) {
            String substr = str.substring(i);
            List<Integer> commonPrefixList = DOUBLE_ARRAY_TRIE.commonPrefixSearch(substr);
            String[] pinyinArray;
            if (commonPrefixList.size() == 0) {
                char c = str.charAt(i);
                if (!isChinese(c) && c != 12295) {
                    sb.append(c);
                } else {
                    pinyinArray = convertToPinyinArray(c, pinyinFormat);
                    if (pinyinArray != null) {
                        if (pinyinArray.length <= 0) {
                            throw new PinyinException("Can't convert to pinyin: " + c);
                        }

                        sb.append(Character.toUpperCase(pinyinArray[0].charAt(0)) + pinyinArray[0].substring(1));
                    } else {
                        sb.append(str.charAt(i));
                    }
                }

                ++i;
            } else {
                String words = (String)dict.get((Integer)commonPrefixList.get(commonPrefixList.size() - 1));
                pinyinArray = formatPinyin((String)MUTIL_PINYIN_TABLE.get(words), pinyinFormat);
                int j = 0;

                for(int l = pinyinArray.length; j < l; ++j) {
                    sb.append(Character.toUpperCase(pinyinArray[j].charAt(0)) + pinyinArray[j].substring(1));
                    if (j < l - 1) {
                        sb.append(separator);
                    }
                }

                i += words.length();
            }

            if (i < strLen) {
                sb.append(separator);
            }
        }

        return sb.toString();
    }

    public static String convertToPinyinString(String str, String separator) throws PinyinException {
        return convertToPinyinString(str, separator, PinyinFormat.WITH_TONE_MARK);
    }

    public static boolean hasMultiPinyin(char c) {
        String[] pinyinArray = convertToPinyinArray(c);
        return pinyinArray != null && pinyinArray.length > 1;
    }

    public static String getShortPinyin(String str) throws PinyinException {
        String separator = "#";
        StringBuilder sb = new StringBuilder();
        char[] charArray = new char[str.length()];
        int i = 0;

        for(int len = str.length(); i < len; ++i) {
            char c = str.charAt(i);
            if (!isChinese(c) && c != 12295) {
                charArray[i] = c;
            } else {
                int j = i + 1;
                sb.append(c);

                while(j < len && (ChineseHelper.isChinese(str.charAt(j)) || str.charAt(j) == 12295)) {
                    sb.append(str.charAt(j));
                    ++j;
                }

                String hanziPinyin = convertToPinyinString(sb.toString(), separator, PinyinFormat.WITHOUT_TONE);
                String[] pinyinArray = hanziPinyin.split(separator);
                String[] var10 = pinyinArray;
                int var11 = pinyinArray.length;

                for(int var12 = 0; var12 < var11; ++var12) {
                    String string = var10[var12];
                    charArray[i] = string.charAt(0);
                    ++i;
                }

                --i;
                sb.setLength(0);
            }
        }

        return String.valueOf(charArray);
    }

    public static void addPinyinDict(String path) throws FileNotFoundException {
        PINYIN_TABLE.putAll(PinyinResource.getResource(PinyinResource.newFileReader(path)));
    }

    public static void addMutilPinyinDict(String path) throws FileNotFoundException {
        MUTIL_PINYIN_TABLE.putAll(PinyinResource.getResource(PinyinResource.newFileReader(path)));
        dict.clear();
        DOUBLE_ARRAY_TRIE.clear();
        Iterator var1 = MUTIL_PINYIN_TABLE.keySet().iterator();

        while(var1.hasNext()) {
            String word = (String)var1.next();
            dict.add(word);
        }

        Collections.sort(dict);
        DOUBLE_ARRAY_TRIE.build(dict);
    }

    static {
        Iterator var0 = MUTIL_PINYIN_TABLE.keySet().iterator();

        while(var0.hasNext()) {
            String word = (String)var0.next();
            dict.add(word);
        }

        Collections.sort(dict);
        DOUBLE_ARRAY_TRIE.build(dict);
    }
}