package com.pinyin4jplus;

/**
 * @author liuc
 * @date 2024-06-27 19:48
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicateLines {
    public static void main(String[] args) {
        String inputFile = "E:\\gitlab\\spring-boot-gather\\pinyin4j-spring-boot-starter\\src\\main\\resources\\data\\mutil_pinyin.dict"; // 输入文件名
        String outputFile = "output.txt"; // 输出文件名

        Set<String> lines = new HashSet<>(); // 使用HashSet存储数据，自动去重

        // 读取文件
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line); // 将每一行添加到HashSet中
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 输出去重后的数据到文件
        try (FileWriter writer = new FileWriter(outputFile)) {
            for (String uniqueLine : lines) {
                writer.write(uniqueLine + "\n"); // 写入每一行数据到文件
            }
            System.out.println("去重后的数据已写入文件：" + outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}