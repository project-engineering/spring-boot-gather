package com.springboot.generator;

import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author liuc
 * @date 2024-08-22 14:06
 */
@Log4j2
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Resource
    private CodeGenerator generator;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Start generating code...");
        generator.generate();
    }
}
