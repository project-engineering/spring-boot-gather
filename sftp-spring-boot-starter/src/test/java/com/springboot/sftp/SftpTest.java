package com.springboot.sftp;

import com.springboot.sftp.util.SftpUtil;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author liuc
 * @date 2024-09-11 22:00
 */

@SpringBootTest
public class SftpTest {

    @Resource
    private SftpUtil sftpUtil;

    @Test
    void downloadTest() {
        byte[] dockerfiles = sftpUtil.download("/data/input/20231010/190703", "Dockerfile");
        System.out.println("FileSize =>" + dockerfiles.length);
    }

    @Test
    void downloadFileTest() {
        sftpUtil.downloadFile("/data/lending/guarantee_invitation_letter/2024-09-13", "20240913094211_担保函和邀请函.zip", "E:\\data\\");
    }

    @Test
    void batchDownloadTest() {
        sftpUtil.batchDownloadFile("/data/lending/", "E:\\data\\lending\\");
    }

    @Test
    void uploadFileTest() {
        sftpUtil.uploadFile("E:\\data\\export1.pdf", "/data/lending/", "1.pdf");
    }

    @Test
    void batchUploadFileTest() {
        sftpUtil.batchUploadFile("E:\\data\\", "/data/lending/liuc/");
    }
}