package com.springboot.sftp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SFtpApplication {

    public static void main(String[] args) {
        SpringApplication.run(SFtpApplication.class, args);
    }

}
