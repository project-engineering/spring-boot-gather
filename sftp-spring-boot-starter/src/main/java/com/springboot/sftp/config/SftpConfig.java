package com.springboot.sftp.config;

import com.springboot.sftp.pool.SftpFactory;
import com.springboot.sftp.pool.SftpPool;
import com.springboot.sftp.util.SftpUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuc
 * @date 2024-09-11 21:37
 */
@Configuration
@EnableConfigurationProperties(SftpProperties.class)
public class SftpConfig {
    /**
     * 工厂
     */
    @Bean
    public SftpFactory sftpFactory(SftpProperties properties) {
        return new SftpFactory(properties);
    }

    /**
     * 连接池
     */
    @Bean
    public SftpPool<SftpFactory> sftpPool(SftpFactory sftpFactory) {
        return new SftpPool<>(sftpFactory);
    }

    /**
     * 辅助类
     */
    @Bean
    public SftpUtil sftpUtil(SftpPool<SftpFactory> sftpPool) {
        return new SftpUtil(sftpPool);
    }
}
