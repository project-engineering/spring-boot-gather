package com.springboot.sftp.config;

import com.jcraft.jsch.ChannelSftp;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import java.time.Duration;

/**
 * @author liuc
 * @date 2024-09-11 21:38
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "sftp")
public class SftpProperties {

    private String host;
    private int port = 22;
    private String username = "root";
    private String password = "root";
    private Pool pool = new Pool();


    @Getter
    @Setter
    public static class Pool extends GenericObjectPoolConfig<ChannelSftp> {
        /**
         * 池可以产生的最大对象数，为负值时表示无限
         */
        private int maxTotal = DEFAULT_MAX_TOTAL;
        /**
         * 池中最大的空闲连接数，为负值时表示无限
         */
        private int maxIdle = DEFAULT_MAX_IDLE;
        /**
         * 池中最小的连接数，只有当 timeBetweenEvictionRuns 为正时才有效
         */
        private int minIdle = DEFAULT_MIN_IDLE;
        /**
         * 池中允许的最大活动连接数，为负值时表示无限
         */
        private long maxActive = -1;
        /**
         * 当池耗尽时，阻塞的最长时间，为负值时无限等待
         */
        private long maxWait = -1;

        /**
         * 从池中取出对象是是否检测可用
         */
        private boolean testOnBorrow = true;

        /**
         * 将对象返还给池时检测是否可用
         */
        private boolean testOnReturn = false;

        /**
         * 检查连接池对象是否可用
         */
        private boolean testWhileIdle = true;

        /**
         * 距离上次空闲线程检测完成多久后再次执行
         */
        private Duration timeBetweenEvictionRuns = Duration.ofMillis(30000);
        /**
         * 连接失败重试间隔(毫秒)
         */
        private long timeBetweenConnectRetries = 30000;
        /**
         * 连接失败重试次数
         */
        private int maxConnectRetries = 3;
        /**
         * 连接超时时间(毫秒)
         */
        private int connectTimeout = 10000;
        /**
         * 发送缓冲区大小
         */
        private int sendBufferSize = 1024;
        /**
         * 接收缓冲区大小
         */
        private int receiveBufferSize = 1024;
        /**
         * 发送窗口大小
         */
        private int sendWindowSize = 2048;
        /**
         * 接收窗口大小
         */
        private int receiveWindowSize = 2048;
        /**
         * 读写超时时间(毫秒)
         */
        private int soTimeout = 10000;
        /**
         * 是否移除不活动的连接
         */
        private boolean removeAbandoned = true;
        /**
         * 移除不活动连接的超时时间(秒)
         */
        private int removeAbandonedTimeout = 300;
        /**
         * 移除不活动连接的检查周期(秒)
         */
        private int removeAbandonedCheckInterval = 30000;
        /**
         * 是否在借用连接的时候移除长时间不活动的连接
         */
        private boolean removeAbandonedOnBorrow = true;
        /**
         * 是否在维护连接的时候移除长时间不活动的连接
         */
        private boolean removeAbandonedOnMaintenance = true;
        /**
         * 借用连接时移除长时间不活动的连接的超时时间(秒)
         */
        private int removeAbandonedTimeoutOnBorrow = 300;
        /**
         * 维护连接时移除长时间不活动的连接的超时时间(秒)
         */
        private int removeAbandonedTimeoutOnMaintenance = 300;
        /**
         * 移除长时间不活动的连接的最大重试次数
         */
        private int removeAbandonedMaxRetries = 3;
        /**
         * 是否在池耗尽时移除长时间不活动的连接
         */
        private boolean removeAbandonedWhenPoolExhausted = true;
        /**
         * 是否在连接失败时移除长时间不活动的连接
         */
        private boolean removeAbandonedWhenConnectFail = true;
        /**
         * 移除连接失败时长时间不活动的连接的最大重试次数
         */
        private int removeAbandonedWhenConnectFailMaxRetries = 3;
        /**
         * 移除连接失败时长时间不活动的连接的超时时间(秒)
         */
        private long removeAbandonedWhenConnectFailTimeout = 300;
        /**
         * 是否记录长时间不活动的连接
         */
        private boolean logAbandonedConnectionsEnabled = true;
        /**
         * 是否记录长时间不活动的连接
         */
        private boolean logAbandonedConnections = true;
        /**
         * 是否记录长时间不活动的连接
         */
        private boolean logAbandoned = true;
        /**
         * 记录长时间不活动的连接的超时时间(秒)
         */
        private long logAbandonedTimeout = 300;
        /**
         * 连接池中连接的最大生命周期(秒)
         */
        private long softMaxEvictableIdleTime = 240000;
        /**
         * 是否在归还连接的时候进行连接验证
         */
        private boolean testOnReturnValue = false;
        /**
         * 是否在借用连接的时候进行连接验证
         */
        private boolean blockWhenBorrowed = true;
        /**
         * 是否在归还连接的时候进行连接验证
         */
        private boolean blockWhenReturned = false;
        /**
         * 连接验证的超时时间(秒)
         */
        private long validationTimeout = 5000;
        /**
         * 连接验证的间隔时间(秒)
         */
        private long validationInterval = 30000;
        /**
         * 是否开启JMX监控
         */
        private boolean jmxEnabled = true;
        /**
         * JMX监控的名称
         */
        private String jmxName = "sftp-pool";
        /**
         * 连接空闲超时时间(毫秒)
         */
        private int idleTimeout = 60000;
        /**
         * 输入缓存大小(字节)
         */
        private int inputBufferSize = 1024;
        /**
         * 输出缓存大小(字节)
         */
        private int outputBufferSize = 1024;
        /**
         * 连接池中最小空闲连接数
         */
        private int numIdle = 5;
        /**
         * 连接池中最大活动连接数
         */
        private int numActive = 5;
        /**
         * 是否阻塞等待连接池资源
         */
        private boolean blockIfQueueFull = true;
        /**
         * 连接池最大连接数
         */
        private int maxPoolSize = 10;
        /**
         * 连接池中连接最大空闲时间(毫秒)
         */
        private long maxIdleTimeMillis = 60000;
        /**
         * 连接池最小连接数
         */
        private int minPoolSize = 1;
        /**
         * 连接池中连接最小空闲时间(毫秒)
         */
        private long minIdleTimeMillis = 10000;

        public Pool() {
            super();
        }
    }
}