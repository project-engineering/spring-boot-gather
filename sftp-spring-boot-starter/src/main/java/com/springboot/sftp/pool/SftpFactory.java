package com.springboot.sftp.pool;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.springboot.sftp.config.SftpProperties;
import com.springboot.sftp.exception.SftpPoolException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import java.util.Properties;

/**
 * @author liuc
 * @date 2024-09-11 21:50
 */
@Setter
@Getter
@Data
@EqualsAndHashCode(callSuper = true)
@Log4j2
public class SftpFactory extends BasePooledObjectFactory<ChannelSftp> {

    /**
     * sftp配置
     */
    private SftpProperties properties;

    /**
     * 构造函数 初始化配置
     *
     * @param properties sftp配置
     */
    public SftpFactory(SftpProperties properties) {
        this.properties = properties;
    }

    /**
     * 创建sftp连接
     *
     * @return sftp连接
     */
    @Override
    public ChannelSftp create() {
        try {
            JSch jsch = new JSch();
            Session sshSession = jsch.getSession(properties.getUsername(), properties.getHost(), properties.getPort());
            sshSession.setPassword(properties.getPassword());
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            ChannelSftp channel = (ChannelSftp) sshSession.openChannel("sftp");
            channel.connect();
            return channel;
        } catch (JSchException e) {
            throw new SftpPoolException("连接sfpt失败", e);
        }
    }

    /**
     * 包装sftp连接
     *
     * @param channelSftp sftp连接
     * @return 包装后的sftp连接
     */
    @Override
    public PooledObject<ChannelSftp> wrap(ChannelSftp channelSftp) {
        return new DefaultPooledObject<>(channelSftp);
    }

    /**
     * 销毁sftp连接
     *
     * @param p 包装后的sftp连接
     */
    @Override
    public void destroyObject(PooledObject<ChannelSftp> p) {
        ChannelSftp channelSftp = p.getObject();
        channelSftp.disconnect();
    }
}