package com.springboot.sftp.util;

import lombok.extern.log4j.Log4j2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author liuc
 * @date 2024-09-11 21:53
 */
@Log4j2
public class ByteUtil {
    /**
     * 将InputStream转换为byte数组
     *
     * @param in 输入流
     * @return byte数组
     */
    public static byte[] inputStreamToByteArray(InputStream in) {
        if (in == null) {
            throw new IllegalArgumentException("InputStream不能为空");
        }
        try (ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            byte[] data = new byte[16384]; // 16KB的缓冲区大小
            int bytesRead;

            while ((bytesRead = in.read(data)) != -1) {
                buffer.write(data, 0, bytesRead);
            }

            return buffer.toByteArray();
        } catch (IOException e) {
            // 处理异常，这里简单地打印堆栈跟踪
            log.error("将InputStream转换为byte数组异常，", e);
            throw new RuntimeException("将InputStream转换为byte数组异常，",e);
        }
    }
}
