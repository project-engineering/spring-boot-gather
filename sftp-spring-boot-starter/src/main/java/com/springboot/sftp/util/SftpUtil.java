package com.springboot.sftp.util;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.springboot.sftp.exception.SftpPoolException;
import com.springboot.sftp.pool.SftpFactory;
import com.springboot.sftp.pool.SftpPool;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StopWatch;
import java.io.*;
import java.util.Vector;

/**
 * @author liuc
 * @date 2024-09-11 21:52
 */
@Log4j2
public class SftpUtil {
    /**
     * sftp连接池
     */
    private final SftpPool<SftpFactory> pool;

    /**
     * 构造方法
     *
     * @param pool sftp连接池
     */
    public SftpUtil(SftpPool<SftpFactory> pool) {
        this.pool = pool;
    }

    /**
     * 下载文件
     *
     * @param dir  远程目录
     * @param name 远程文件名
     * @return 文件字节数组
     */
    public byte[] download(String dir, String name) {
        ChannelSftp sftp = null;
        try {
            sftp = pool.borrowObject();
            sftp.cd(dir);
            InputStream in = sftp.get(name);
            return ByteUtil.inputStreamToByteArray(in);
        } catch (Exception e) {
            throw new SftpPoolException("sftp下载文件出错", e);
        } finally {
            pool.returnObject(sftp);
        }
    }

    /**
     * 批量下载文件
     *
     * @param remotePath  远程目录
     * @param localPath   本地目录
     */
    public void batchDownloadFile(String remotePath, String localPath) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("开始批量从sftp目录：{} 下载文件", remotePath);
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            if (!isExists(remotePath)) {
                log.info("目录{}不存在", remotePath);
            }
            channelSftp.cd(remotePath);

            // 检查本地目录是否存在，如果不存在则创建
            File localDir = new File(localPath);
            if (!localDir.exists()) {
                boolean success = localDir.mkdirs();
                if (!success) {
                    log.error("创建本地目录{}失败，请检查目录路径是否正确", localPath);
                    throw new SftpPoolException("创建本地目录失败，请检查目录路径是否正确");
                }
            }

            // 列出远程目录下的所有文件和目录
            Vector<ChannelSftp.LsEntry> list = channelSftp.ls(".");
            if (!ObjectUtils.isEmpty(list)) {
                for (ChannelSftp.LsEntry entry : list) {
                    String entryName = entry.getFilename();
                    // 忽略 "." 和 ".." 目录
                    if (!entryName.equals(".") && !entryName.equals("..")) {
                        if (entry.getAttrs().isDir()) {
                            // 递归下载子目录
                            batchDownloadFile(remotePath + "/" + entryName, localPath + File.separator + entryName);
                        } else {
                            // 下载文件
                            downloadFile(channelSftp, entryName, localPath);
                        }
                    }
                }
            }
        } catch (Exception e) {
            // 可以在这里添加日志记录
             log.error("sftp批量下载文件出错", e);
            throw new SftpPoolException("sftp批量下载文件出错", e);
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
        stopWatch.stop();
        log.info("sftp批量下载文件完成，耗时：{} 毫秒", stopWatch.getTotalTimeMillis());
    }

    /**
     * 下载文件
     *
     * @param channelSftp sftp连接
     * @param filename    远程文件名
     * @param localPath   本地目录
     */
    private void downloadFile(ChannelSftp channelSftp, String filename, String localPath){
        File localFile = new File(localPath + File.separator + filename);
        try (OutputStream outputStream = new FileOutputStream(localFile)) {
            channelSftp.get(filename, outputStream);
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp下载文件出错", e);
            throw new SftpPoolException("sftp下载文件出错", e);
        }
    }

    /**
     * 下载文件
     *
     * @param remotePath  远程目录
     * @param remoteFilename  远程文件名
     * @param localPath   本地目录
     */
    public void downloadFile(String remotePath, String remoteFilename, String localPath) {
        log.info("开始从sftp目录：{} 下载文件：{}", remotePath, remoteFilename);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            channelSftp.cd(remotePath);
            downloadFile(channelSftp, remoteFilename, localPath);
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp下载文件出错", e);
            throw new SftpPoolException("sftp下载文件出错", e);
        } finally {
            if (channelSftp != null) {
               pool.returnObject(channelSftp);
            }
        }
        stopWatch.stop();
        log.info("sftp下载文件完成，存放路径：{}，耗时：{} 毫秒",localPath, stopWatch.getTotalTimeMillis());
    }

    /**
     * 上传文件
     *
     * @param dir  远程目录
     * @param name 远程文件名
     * @param in   输入流
     */
    public void upload(String dir, String name, InputStream in) {
        ChannelSftp sftp = null;
        try {
            sftp = pool.borrowObject();
            mkdirs(sftp, dir);
            sftp.cd(dir);
            sftp.put(in, name);
        } catch (Exception e) {
            throw new SftpPoolException("sftp上传文件出错", e);
        } finally {
            pool.returnObject(sftp);
        }
    }

    /**
     * 批量上传文件
     *
     * @param localPath  本地目录
     * @param remotePath 远程目录
     */
    public void batchUploadFile(String localPath, String remotePath) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("开始批量上传文件到sftp目录：{} ", remotePath);
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            if (!isExists(remotePath)) {
                log.info("开始创建sftp目录：{} ", remotePath);
                mkdirs(channelSftp, remotePath);
            }
            channelSftp.cd(remotePath);
            File localDir = new File(localPath);
            if (localDir.exists()) {
                if (localDir.isDirectory()) {
                    File[] files = localDir.listFiles();
                    if (!ObjectUtils.isEmpty(files)) {
                        for (File file : files) {
                            if (file.isFile()) {
                                // 上传文件
                                uploadFile(channelSftp, file.getName(), file.getAbsolutePath());
                            } else if (file.isDirectory()) {
                                // 递归上传子目录
                                String remoteSubDir = remotePath + "/" + file.getName();
                                batchUploadFile(file.getAbsolutePath(), remoteSubDir);
                            }
                        }
                    }
                    log.info("批量上传文件完成，耗时：{} 毫秒", stopWatch.getTotalTimeMillis());
                }
            }
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp批量上传文件出错", e);
            throw new SftpPoolException("sftp批量上传文件出错", e);
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
    }

    /**
     * 上传文件
     *
     * @param channelSftp sftp连接
     * @param fileName     本地文件名
     * @param absolutePath 绝对路径（包含文件名）
     */
    private void uploadFile(ChannelSftp channelSftp, String fileName, String absolutePath) {
        try (InputStream inputStream = new FileInputStream(absolutePath)) {
            channelSftp.put(inputStream, fileName);
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp上传文件出错", e);
            throw new SftpPoolException("sftp上传文件出错", e);
        }
    }

    /**
     * 上传文件
     *
     * @param absoluteFilePath  本地文件绝对路径（包含文件名）
     * @param remotePath 远程目录
     * @param remoteFilename  远程文件名
     */
    public void uploadFile(String absoluteFilePath, String remotePath, String remoteFilename) {
        log.info("开始上传文件到sftp目录：{} ", remotePath);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            if (!isExists(remotePath)) {
                log.info("开始创建sftp目录：{} ", remotePath);
                mkdirs(channelSftp, remotePath);
            }
            channelSftp.cd(remotePath);
            uploadFile(channelSftp, remoteFilename, absoluteFilePath);
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp上传文件出错", e);
            throw new SftpPoolException("sftp上传文件出错", e);
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
        stopWatch.stop();
        log.info("sftp上传文件完成，耗时：{} 毫秒", stopWatch.getTotalTimeMillis());
    }

    /**
     * 删除文件
     *
     * @param dir  远程目录
     * @param name 远程文件名
     */
    public void delete(String dir, String name) {
        if(isFileExists(dir, name)) {
            ChannelSftp sftp = null;
            try {
                sftp = pool.borrowObject();
                sftp.cd(dir);
                sftp.rm(name);
            } catch (Exception e) {
                throw new SftpPoolException("sftp删除文件出错", e);
            } finally {
                pool.returnObject(sftp);
            }
        } else {
            log.info("文件{}不存在", dir+name);
        }
    }

    /**
     * 批量删除文件
     *
     * @param remotePath 远程目录
     */
    public void batchDeleteFile(String remotePath) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("开始批量删除sftp目录：{} 下的文件", remotePath);
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            channelSftp.cd(remotePath);

            // 列出远程目录下的所有文件和目录
            Vector<ChannelSftp.LsEntry> list = channelSftp.ls(".");
            if (!ObjectUtils.isEmpty(list)) {
                for (ChannelSftp.LsEntry entry : list) {
                    String entryName = entry.getFilename();
                    // 忽略 "." 和 ".." 目录
                    if (!entryName.equals(".") && !entryName.equals("..")) {
                        if (entry.getAttrs().isDir()) {
                            // 递归删除子目录
                            batchDeleteFile(remotePath + "/" + entryName);
                        } else {
                            // 删除文件
                            deleteFile(channelSftp, entryName);
                        }
                    }
                }
            }
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp批量删除文件出错", e);
            throw new SftpPoolException("sftp批量删除文件出错", e);
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
        stopWatch.stop();
        log.info("sftp批量删除文件完成，耗时：{} 毫秒", stopWatch.getTotalTimeMillis());
    }

    /**
     * 删除文件
     *
     * @param channelSftp sftp连接
     * @param entryName   远程文件名
     */
    private void deleteFile(ChannelSftp channelSftp, String entryName) {
        try {
            channelSftp.rm(entryName);
        } catch (Exception e) {
            // 可以在这里添加日志记录
            log.error("sftp删除文件出错", e);
            throw new SftpPoolException("sftp删除文件出错", e);
        }
    }

    /**
     * 递归创建多级目录
     *
     * @param dir 多级目录
     */
    private void mkdirs(ChannelSftp sftp, String dir) {
        String[] folders = dir.split("/");
        try {
            sftp.cd("/");
            for (String folder : folders) {
                if (!folder.isEmpty()) {
                    try {
                        sftp.cd(folder);
                    } catch (Exception e) {
                        sftp.mkdir(folder);
                        sftp.cd(folder);
                    }
                }
            }
        } catch (SftpException e) {
            throw new SftpPoolException("sftp创建目录出错", e);
        }
    }

    /**
     * 判断文件夹是否存在
     * @param remotePath 远程目录
     * @return true：存在；false：不存在
     */
    public boolean isExists(String remotePath) {
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            channelSftp.cd(remotePath);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
    }

    /**
     * 判断文件是否存在
     * @param remotePath 远程目录
     * @param remoteFilename 远程文件名
     * @return true：存在；false：不存在
     */
    private boolean isFileExists(String remotePath, String remoteFilename) {
        ChannelSftp channelSftp = null;
        try {
            channelSftp = pool.borrowObject();
            channelSftp.cd(remotePath);
            Vector<ChannelSftp.LsEntry> list = channelSftp.ls(".");
            if (!ObjectUtils.isEmpty(list)) {
                for (ChannelSftp.LsEntry entry : list) {
                    String entryName = entry.getFilename();
                    // 忽略 "." 和 ".." 目录
                    if (!entryName.equals(".") && !entryName.equals("..")) {
                        if (entry.getAttrs().isDir()) {
                            // 递归判断子目录
                            return isFileExists(remotePath + "/" + entryName, remoteFilename);
                        } else {
                            // 判断文件
                            if (entryName.equals(remoteFilename)) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("sftp判断文件是否存在出错", e);
            return false;
        } finally {
            if (channelSftp != null) {
                pool.returnObject(channelSftp);
            }
        }
        return false;
    }
}